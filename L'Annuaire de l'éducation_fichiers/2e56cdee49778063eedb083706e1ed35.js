/* compact [
	prive/javascript/jquery.js?1649159400
	prive/javascript/jquery.form.js?1649159400
	prive/javascript/jquery.autosave.js?1649159400
	prive/javascript/jquery.placeholder-label.js?1649159400
	prive/javascript/ajaxCallback.js?1649159400
	prive/javascript/js.cookie.js?1649159400
	prive/javascript/jquery.cookie.js?1649159400
	plugins-dist/mediabox/lib/lity/lity.js?1649159460
	plugins-dist/mediabox/lity/js/lity.mediabox.js?1649159460
	plugins-dist/mediabox/javascript/spip.mediabox.js?1649159460
	plugins/auto/tablesorter/v2.1.3/javascript/jquery.tablesorter.min.js?1649159586
	plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.js?1649159526
	plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.flip.js?1649159526
	plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.carousel.js?1649159525
	plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.scrollVert.js?1649159526
	plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.shuffle.js?1649159526
	plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.tile.js?1649159526
	plugins/auto/chartjs/v1.0.5/js/Chart.js
	plugins/auto/chartjs/v1.0.5/js/chart_extra.js
	local/cache-js/jsdyn-_js_cibloc_js-102cf2dd.js?1651594293
	plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-collapse.js
	plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-dropdown.js
	plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-carousel.js
	plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-transition.js
	plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-tab.js
	local/cache-js/jsdyn-js_scolaspip_js-9b116fe0.js?1651594293
] 60.7% */

/* prive/javascript/jquery.js?1649159400 */

(function(global,factory){
"use strict";
if(typeof module==="object"&&typeof module.exports==="object"){
module.exports=global.document?
factory(global,true):
function(w){
if(!w.document){
throw new Error("jQuery requires a window with a document");
}
return factory(w);
};
}else{
factory(global);
}
})(typeof window!=="undefined"?window:this,function(window,noGlobal){
"use strict";
var arr=[];
var getProto=Object.getPrototypeOf;
var slice=arr.slice;
var flat=arr.flat?function(array){
return arr.flat.call(array);
}:function(array){
return arr.concat.apply([],array);
};
var push=arr.push;
var indexOf=arr.indexOf;
var class2type={};
var toString=class2type.toString;
var hasOwn=class2type.hasOwnProperty;
var fnToString=hasOwn.toString;
var ObjectFunctionString=fnToString.call(Object);
var support={};
var isFunction=function isFunction(obj){
return typeof obj==="function"&&typeof obj.nodeType!=="number"&&
typeof obj.item!=="function";
};
var isWindow=function isWindow(obj){
return obj!=null&&obj===obj.window;
};
var document=window.document;
var preservedScriptAttributes={
type:true,
src:true,
nonce:true,
noModule:true
};
function DOMEval(code,node,doc){
doc=doc||document;
var i,val,
script=doc.createElement("script");
script.text=code;
if(node){
for(i in preservedScriptAttributes){
val=node[i]||node.getAttribute&&node.getAttribute(i);
if(val){
script.setAttribute(i,val);
}
}
}
doc.head.appendChild(script).parentNode.removeChild(script);
}
function toType(obj){
if(obj==null){
return obj+"";
}
return typeof obj==="object"||typeof obj==="function"?
class2type[toString.call(obj)]||"object":
typeof obj;
}
var
version="3.6.0",
jQuery=function(selector,context){
return new jQuery.fn.init(selector,context);
};
jQuery.fn=jQuery.prototype={
jquery:version,
constructor:jQuery,
length:0,
toArray:function(){
return slice.call(this);
},
get:function(num){
if(num==null){
return slice.call(this);
}
return num<0?this[num+this.length]:this[num];
},
pushStack:function(elems){
var ret=jQuery.merge(this.constructor(),elems);
ret.prevObject=this;
return ret;
},
each:function(callback){
return jQuery.each(this,callback);
},
map:function(callback){
return this.pushStack(jQuery.map(this,function(elem,i){
return callback.call(elem,i,elem);
}));
},
slice:function(){
return this.pushStack(slice.apply(this,arguments));
},
first:function(){
return this.eq(0);
},
last:function(){
return this.eq(-1);
},
even:function(){
return this.pushStack(jQuery.grep(this,function(_elem,i){
return(i+1)%2;
}));
},
odd:function(){
return this.pushStack(jQuery.grep(this,function(_elem,i){
return i%2;
}));
},
eq:function(i){
var len=this.length,
j=+i+(i<0?len:0);
return this.pushStack(j>=0&&j<len?[this[j]]:[]);
},
end:function(){
return this.prevObject||this.constructor();
},
push:push,
sort:arr.sort,
splice:arr.splice
};
jQuery.extend=jQuery.fn.extend=function(){
var options,name,src,copy,copyIsArray,clone,
target=arguments[0]||{},
i=1,
length=arguments.length,
deep=false;
if(typeof target==="boolean"){
deep=target;
target=arguments[i]||{};
i++;
}
if(typeof target!=="object"&&!isFunction(target)){
target={};
}
if(i===length){
target=this;
i--;
}
for(;i<length;i++){
if((options=arguments[i])!=null){
for(name in options){
copy=options[name];
if(name==="__proto__"||target===copy){
continue;
}
if(deep&&copy&&(jQuery.isPlainObject(copy)||
(copyIsArray=Array.isArray(copy)))){
src=target[name];
if(copyIsArray&&!Array.isArray(src)){
clone=[];
}else if(!copyIsArray&&!jQuery.isPlainObject(src)){
clone={};
}else{
clone=src;
}
copyIsArray=false;
target[name]=jQuery.extend(deep,clone,copy);
}else if(copy!==undefined){
target[name]=copy;
}
}
}
}
return target;
};
jQuery.extend({
expando:"jQuery"+(version+Math.random()).replace(/\D/g,""),
isReady:true,
error:function(msg){
throw new Error(msg);
},
noop:function(){},
isPlainObject:function(obj){
var proto,Ctor;
if(!obj||toString.call(obj)!=="[object Object]"){
return false;
}
proto=getProto(obj);
if(!proto){
return true;
}
Ctor=hasOwn.call(proto,"constructor")&&proto.constructor;
return typeof Ctor==="function"&&fnToString.call(Ctor)===ObjectFunctionString;
},
isEmptyObject:function(obj){
var name;
for(name in obj){
return false;
}
return true;
},
globalEval:function(code,options,doc){
DOMEval(code,{nonce:options&&options.nonce},doc);
},
each:function(obj,callback){
var length,i=0;
if(isArrayLike(obj)){
length=obj.length;
for(;i<length;i++){
if(callback.call(obj[i],i,obj[i])===false){
break;
}
}
}else{
for(i in obj){
if(callback.call(obj[i],i,obj[i])===false){
break;
}
}
}
return obj;
},
makeArray:function(arr,results){
var ret=results||[];
if(arr!=null){
if(isArrayLike(Object(arr))){
jQuery.merge(ret,
typeof arr==="string"?
[arr]:arr
);
}else{
push.call(ret,arr);
}
}
return ret;
},
inArray:function(elem,arr,i){
return arr==null?-1:indexOf.call(arr,elem,i);
},
merge:function(first,second){
var len=+second.length,
j=0,
i=first.length;
for(;j<len;j++){
first[i++]=second[j];
}
first.length=i;
return first;
},
grep:function(elems,callback,invert){
var callbackInverse,
matches=[],
i=0,
length=elems.length,
callbackExpect=!invert;
for(;i<length;i++){
callbackInverse=!callback(elems[i],i);
if(callbackInverse!==callbackExpect){
matches.push(elems[i]);
}
}
return matches;
},
map:function(elems,callback,arg){
var length,value,
i=0,
ret=[];
if(isArrayLike(elems)){
length=elems.length;
for(;i<length;i++){
value=callback(elems[i],i,arg);
if(value!=null){
ret.push(value);
}
}
}else{
for(i in elems){
value=callback(elems[i],i,arg);
if(value!=null){
ret.push(value);
}
}
}
return flat(ret);
},
guid:1,
support:support
});
if(typeof Symbol==="function"){
jQuery.fn[Symbol.iterator]=arr[Symbol.iterator];
}
jQuery.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),
function(_i,name){
class2type["[object "+name+"]"]=name.toLowerCase();
});
function isArrayLike(obj){
var length=!!obj&&"length"in obj&&obj.length,
type=toType(obj);
if(isFunction(obj)||isWindow(obj)){
return false;
}
return type==="array"||length===0||
typeof length==="number"&&length>0&&(length-1)in obj;
}
var Sizzle=
(function(window){
var i,
support,
Expr,
getText,
isXML,
tokenize,
compile,
select,
outermostContext,
sortInput,
hasDuplicate,
setDocument,
document,
docElem,
documentIsHTML,
rbuggyQSA,
rbuggyMatches,
matches,
contains,
expando="sizzle"+1*new Date(),
preferredDoc=window.document,
dirruns=0,
done=0,
classCache=createCache(),
tokenCache=createCache(),
compilerCache=createCache(),
nonnativeSelectorCache=createCache(),
sortOrder=function(a,b){
if(a===b){
hasDuplicate=true;
}
return 0;
},
hasOwn=({}).hasOwnProperty,
arr=[],
pop=arr.pop,
pushNative=arr.push,
push=arr.push,
slice=arr.slice,
indexOf=function(list,elem){
var i=0,
len=list.length;
for(;i<len;i++){
if(list[i]===elem){
return i;
}
}
return-1;
},
booleans="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|"+
"ismap|loop|multiple|open|readonly|required|scoped",
whitespace="[\\x20\\t\\r\\n\\f]",
identifier="(?:\\\\[\\da-fA-F]{1,6}"+whitespace+
"?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
attributes="\\["+whitespace+"*("+identifier+")(?:"+whitespace+
"*([*^$|!~]?=)"+whitespace+
"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+identifier+"))|)"+
whitespace+"*\\]",
pseudos=":("+identifier+")(?:\\(("+
"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|"+
"((?:\\\\.|[^\\\\()[\\]]|"+attributes+")*)|"+
".*"+
")\\)|)",
rwhitespace=new RegExp(whitespace+"+","g"),
rtrim=new RegExp("^"+whitespace+"+|((?:^|[^\\\\])(?:\\\\.)*)"+
whitespace+"+$","g"),
rcomma=new RegExp("^"+whitespace+"*,"+whitespace+"*"),
rcombinators=new RegExp("^"+whitespace+"*([>+~]|"+whitespace+")"+whitespace+
"*"),
rdescend=new RegExp(whitespace+"|>"),
rpseudo=new RegExp(pseudos),
ridentifier=new RegExp("^"+identifier+"$"),
matchExpr={
"ID":new RegExp("^#("+identifier+")"),
"CLASS":new RegExp("^\\.("+identifier+")"),
"TAG":new RegExp("^("+identifier+"|[*])"),
"ATTR":new RegExp("^"+attributes),
"PSEUDO":new RegExp("^"+pseudos),
"CHILD":new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+
whitespace+"*(even|odd|(([+-]|)(\\d*)n|)"+whitespace+"*(?:([+-]|)"+
whitespace+"*(\\d+)|))"+whitespace+"*\\)|)","i"),
"bool":new RegExp("^(?:"+booleans+")$","i"),
"needsContext":new RegExp("^"+whitespace+
"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+whitespace+
"*((?:-\\d)?\\d*)"+whitespace+"*\\)|)(?=[^-]|$)","i")
},
rhtml=/HTML$/i,
rinputs=/^(?:input|select|textarea|button)$/i,
rheader=/^h\d$/i,
rnative=/^[^{]+\{\s*\[native \w/,
rquickExpr=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
rsibling=/[+~]/,
runescape=new RegExp("\\\\[\\da-fA-F]{1,6}"+whitespace+"?|\\\\([^\\r\\n\\f])","g"),
funescape=function(escape,nonHex){
var high="0x"+escape.slice(1)-0x10000;
return nonHex?
nonHex:
high<0?
String.fromCharCode(high+0x10000):
String.fromCharCode(high>>10|0xD800,high&0x3FF|0xDC00);
},
rcssescape=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
fcssescape=function(ch,asCodePoint){
if(asCodePoint){
if(ch==="\0"){
return"\uFFFD";
}
return ch.slice(0,-1)+"\\"+
ch.charCodeAt(ch.length-1).toString(16)+" ";
}
return"\\"+ch;
},
unloadHandler=function(){
setDocument();
},
inDisabledFieldset=addCombinator(
function(elem){
return elem.disabled===true&&elem.nodeName.toLowerCase()==="fieldset";
},
{dir:"parentNode",next:"legend"}
);
try{
push.apply(
(arr=slice.call(preferredDoc.childNodes)),
preferredDoc.childNodes
);
arr[preferredDoc.childNodes.length].nodeType;
}catch(e){
push={apply:arr.length?
function(target,els){
pushNative.apply(target,slice.call(els));
}:
function(target,els){
var j=target.length,
i=0;
while((target[j++]=els[i++])){}
target.length=j-1;
}
};
}
function Sizzle(selector,context,results,seed){
var m,i,elem,nid,match,groups,newSelector,
newContext=context&&context.ownerDocument,
nodeType=context?context.nodeType:9;
results=results||[];
if(typeof selector!=="string"||!selector||
nodeType!==1&&nodeType!==9&&nodeType!==11){
return results;
}
if(!seed){
setDocument(context);
context=context||document;
if(documentIsHTML){
if(nodeType!==11&&(match=rquickExpr.exec(selector))){
if((m=match[1])){
if(nodeType===9){
if((elem=context.getElementById(m))){
if(elem.id===m){
results.push(elem);
return results;
}
}else{
return results;
}
}else{
if(newContext&&(elem=newContext.getElementById(m))&&
contains(context,elem)&&
elem.id===m){
results.push(elem);
return results;
}
}
}else if(match[2]){
push.apply(results,context.getElementsByTagName(selector));
return results;
}else if((m=match[3])&&support.getElementsByClassName&&
context.getElementsByClassName){
push.apply(results,context.getElementsByClassName(m));
return results;
}
}
if(support.qsa&&
!nonnativeSelectorCache[selector+" "]&&
(!rbuggyQSA||!rbuggyQSA.test(selector))&&
(nodeType!==1||context.nodeName.toLowerCase()!=="object")){
newSelector=selector;
newContext=context;
if(nodeType===1&&
(rdescend.test(selector)||rcombinators.test(selector))){
newContext=rsibling.test(selector)&&testContext(context.parentNode)||
context;
if(newContext!==context||!support.scope){
if((nid=context.getAttribute("id"))){
nid=nid.replace(rcssescape,fcssescape);
}else{
context.setAttribute("id",(nid=expando));
}
}
groups=tokenize(selector);
i=groups.length;
while(i--){
groups[i]=(nid?"#"+nid:":scope")+" "+
toSelector(groups[i]);
}
newSelector=groups.join(",");
}
try{
push.apply(results,
newContext.querySelectorAll(newSelector)
);
return results;
}catch(qsaError){
nonnativeSelectorCache(selector,true);
}finally{
if(nid===expando){
context.removeAttribute("id");
}
}
}
}
}
return select(selector.replace(rtrim,"$1"),context,results,seed);
}
function createCache(){
var keys=[];
function cache(key,value){
if(keys.push(key+" ")>Expr.cacheLength){
delete cache[keys.shift()];
}
return(cache[key+" "]=value);
}
return cache;
}
function markFunction(fn){
fn[expando]=true;
return fn;
}
function assert(fn){
var el=document.createElement("fieldset");
try{
return!!fn(el);
}catch(e){
return false;
}finally{
if(el.parentNode){
el.parentNode.removeChild(el);
}
el=null;
}
}
function addHandle(attrs,handler){
var arr=attrs.split("|"),
i=arr.length;
while(i--){
Expr.attrHandle[arr[i]]=handler;
}
}
function siblingCheck(a,b){
var cur=b&&a,
diff=cur&&a.nodeType===1&&b.nodeType===1&&
a.sourceIndex-b.sourceIndex;
if(diff){
return diff;
}
if(cur){
while((cur=cur.nextSibling)){
if(cur===b){
return-1;
}
}
}
return a?1:-1;
}
function createInputPseudo(type){
return function(elem){
var name=elem.nodeName.toLowerCase();
return name==="input"&&elem.type===type;
};
}
function createButtonPseudo(type){
return function(elem){
var name=elem.nodeName.toLowerCase();
return(name==="input"||name==="button")&&elem.type===type;
};
}
function createDisabledPseudo(disabled){
return function(elem){
if("form"in elem){
if(elem.parentNode&&elem.disabled===false){
if("label"in elem){
if("label"in elem.parentNode){
return elem.parentNode.disabled===disabled;
}else{
return elem.disabled===disabled;
}
}
return elem.isDisabled===disabled||
elem.isDisabled!==!disabled&&
inDisabledFieldset(elem)===disabled;
}
return elem.disabled===disabled;
}else if("label"in elem){
return elem.disabled===disabled;
}
return false;
};
}
function createPositionalPseudo(fn){
return markFunction(function(argument){
argument=+argument;
return markFunction(function(seed,matches){
var j,
matchIndexes=fn([],seed.length,argument),
i=matchIndexes.length;
while(i--){
if(seed[(j=matchIndexes[i])]){
seed[j]=!(matches[j]=seed[j]);
}
}
});
});
}
function testContext(context){
return context&&typeof context.getElementsByTagName!=="undefined"&&context;
}
support=Sizzle.support={};
isXML=Sizzle.isXML=function(elem){
var namespace=elem&&elem.namespaceURI,
docElem=elem&&(elem.ownerDocument||elem).documentElement;
return!rhtml.test(namespace||docElem&&docElem.nodeName||"HTML");
};
setDocument=Sizzle.setDocument=function(node){
var hasCompare,subWindow,
doc=node?node.ownerDocument||node:preferredDoc;
if(doc==document||doc.nodeType!==9||!doc.documentElement){
return document;
}
document=doc;
docElem=document.documentElement;
documentIsHTML=!isXML(document);
if(preferredDoc!=document&&
(subWindow=document.defaultView)&&subWindow.top!==subWindow){
if(subWindow.addEventListener){
subWindow.addEventListener("unload",unloadHandler,false);
}else if(subWindow.attachEvent){
subWindow.attachEvent("onunload",unloadHandler);
}
}
support.scope=assert(function(el){
docElem.appendChild(el).appendChild(document.createElement("div"));
return typeof el.querySelectorAll!=="undefined"&&
!el.querySelectorAll(":scope fieldset div").length;
});
support.attributes=assert(function(el){
el.className="i";
return!el.getAttribute("className");
});
support.getElementsByTagName=assert(function(el){
el.appendChild(document.createComment(""));
return!el.getElementsByTagName("*").length;
});
support.getElementsByClassName=rnative.test(document.getElementsByClassName);
support.getById=assert(function(el){
docElem.appendChild(el).id=expando;
return!document.getElementsByName||!document.getElementsByName(expando).length;
});
if(support.getById){
Expr.filter["ID"]=function(id){
var attrId=id.replace(runescape,funescape);
return function(elem){
return elem.getAttribute("id")===attrId;
};
};
Expr.find["ID"]=function(id,context){
if(typeof context.getElementById!=="undefined"&&documentIsHTML){
var elem=context.getElementById(id);
return elem?[elem]:[];
}
};
}else{
Expr.filter["ID"]=function(id){
var attrId=id.replace(runescape,funescape);
return function(elem){
var node=typeof elem.getAttributeNode!=="undefined"&&
elem.getAttributeNode("id");
return node&&node.value===attrId;
};
};
Expr.find["ID"]=function(id,context){
if(typeof context.getElementById!=="undefined"&&documentIsHTML){
var node,i,elems,
elem=context.getElementById(id);
if(elem){
node=elem.getAttributeNode("id");
if(node&&node.value===id){
return[elem];
}
elems=context.getElementsByName(id);
i=0;
while((elem=elems[i++])){
node=elem.getAttributeNode("id");
if(node&&node.value===id){
return[elem];
}
}
}
return[];
}
};
}
Expr.find["TAG"]=support.getElementsByTagName?
function(tag,context){
if(typeof context.getElementsByTagName!=="undefined"){
return context.getElementsByTagName(tag);
}else if(support.qsa){
return context.querySelectorAll(tag);
}
}:
function(tag,context){
var elem,
tmp=[],
i=0,
results=context.getElementsByTagName(tag);
if(tag==="*"){
while((elem=results[i++])){
if(elem.nodeType===1){
tmp.push(elem);
}
}
return tmp;
}
return results;
};
Expr.find["CLASS"]=support.getElementsByClassName&&function(className,context){
if(typeof context.getElementsByClassName!=="undefined"&&documentIsHTML){
return context.getElementsByClassName(className);
}
};
rbuggyMatches=[];
rbuggyQSA=[];
if((support.qsa=rnative.test(document.querySelectorAll))){
assert(function(el){
var input;
docElem.appendChild(el).innerHTML="<a id='"+expando+"'></a>"+
"<select id='"+expando+"-\r\\' msallowcapture=''>"+
"<option selected=''></option></select>";
if(el.querySelectorAll("[msallowcapture^='']").length){
rbuggyQSA.push("[*^$]="+whitespace+"*(?:''|\"\")");
}
if(!el.querySelectorAll("[selected]").length){
rbuggyQSA.push("\\["+whitespace+"*(?:value|"+booleans+")");
}
if(!el.querySelectorAll("[id~="+expando+"-]").length){
rbuggyQSA.push("~=");
}
input=document.createElement("input");
input.setAttribute("name","");
el.appendChild(input);
if(!el.querySelectorAll("[name='']").length){
rbuggyQSA.push("\\["+whitespace+"*name"+whitespace+"*="+
whitespace+"*(?:''|\"\")");
}
if(!el.querySelectorAll(":checked").length){
rbuggyQSA.push(":checked");
}
if(!el.querySelectorAll("a#"+expando+"+*").length){
rbuggyQSA.push(".#.+[+~]");
}
el.querySelectorAll("\\\f");
rbuggyQSA.push("[\\r\\n\\f]");
});
assert(function(el){
el.innerHTML="<a href='' disabled='disabled'></a>"+
"<select disabled='disabled'><option/></select>";
var input=document.createElement("input");
input.setAttribute("type","hidden");
el.appendChild(input).setAttribute("name","D");
if(el.querySelectorAll("[name=d]").length){
rbuggyQSA.push("name"+whitespace+"*[*^$|!~]?=");
}
if(el.querySelectorAll(":enabled").length!==2){
rbuggyQSA.push(":enabled",":disabled");
}
docElem.appendChild(el).disabled=true;
if(el.querySelectorAll(":disabled").length!==2){
rbuggyQSA.push(":enabled",":disabled");
}
el.querySelectorAll("*,:x");
rbuggyQSA.push(",.*:");
});
}
if((support.matchesSelector=rnative.test((matches=docElem.matches||
docElem.webkitMatchesSelector||
docElem.mozMatchesSelector||
docElem.oMatchesSelector||
docElem.msMatchesSelector)))){
assert(function(el){
support.disconnectedMatch=matches.call(el,"*");
matches.call(el,"[s!='']:x");
rbuggyMatches.push("!=",pseudos);
});
}
rbuggyQSA=rbuggyQSA.length&&new RegExp(rbuggyQSA.join("|"));
rbuggyMatches=rbuggyMatches.length&&new RegExp(rbuggyMatches.join("|"));
hasCompare=rnative.test(docElem.compareDocumentPosition);
contains=hasCompare||rnative.test(docElem.contains)?
function(a,b){
var adown=a.nodeType===9?a.documentElement:a,
bup=b&&b.parentNode;
return a===bup||!!(bup&&bup.nodeType===1&&(
adown.contains?
adown.contains(bup):
a.compareDocumentPosition&&a.compareDocumentPosition(bup)&16
));
}:
function(a,b){
if(b){
while((b=b.parentNode)){
if(b===a){
return true;
}
}
}
return false;
};
sortOrder=hasCompare?
function(a,b){
if(a===b){
hasDuplicate=true;
return 0;
}
var compare=!a.compareDocumentPosition-!b.compareDocumentPosition;
if(compare){
return compare;
}
compare=(a.ownerDocument||a)==(b.ownerDocument||b)?
a.compareDocumentPosition(b):
1;
if(compare&1||
(!support.sortDetached&&b.compareDocumentPosition(a)===compare)){
if(a==document||a.ownerDocument==preferredDoc&&
contains(preferredDoc,a)){
return-1;
}
if(b==document||b.ownerDocument==preferredDoc&&
contains(preferredDoc,b)){
return 1;
}
return sortInput?
(indexOf(sortInput,a)-indexOf(sortInput,b)):
0;
}
return compare&4?-1:1;
}:
function(a,b){
if(a===b){
hasDuplicate=true;
return 0;
}
var cur,
i=0,
aup=a.parentNode,
bup=b.parentNode,
ap=[a],
bp=[b];
if(!aup||!bup){
return a==document?-1:
b==document?1:
aup?-1:
bup?1:
sortInput?
(indexOf(sortInput,a)-indexOf(sortInput,b)):
0;
}else if(aup===bup){
return siblingCheck(a,b);
}
cur=a;
while((cur=cur.parentNode)){
ap.unshift(cur);
}
cur=b;
while((cur=cur.parentNode)){
bp.unshift(cur);
}
while(ap[i]===bp[i]){
i++;
}
return i?
siblingCheck(ap[i],bp[i]):
ap[i]==preferredDoc?-1:
bp[i]==preferredDoc?1:
0;
};
return document;
};
Sizzle.matches=function(expr,elements){
return Sizzle(expr,null,null,elements);
};
Sizzle.matchesSelector=function(elem,expr){
setDocument(elem);
if(support.matchesSelector&&documentIsHTML&&
!nonnativeSelectorCache[expr+" "]&&
(!rbuggyMatches||!rbuggyMatches.test(expr))&&
(!rbuggyQSA||!rbuggyQSA.test(expr))){
try{
var ret=matches.call(elem,expr);
if(ret||support.disconnectedMatch||
elem.document&&elem.document.nodeType!==11){
return ret;
}
}catch(e){
nonnativeSelectorCache(expr,true);
}
}
return Sizzle(expr,document,null,[elem]).length>0;
};
Sizzle.contains=function(context,elem){
if((context.ownerDocument||context)!=document){
setDocument(context);
}
return contains(context,elem);
};
Sizzle.attr=function(elem,name){
if((elem.ownerDocument||elem)!=document){
setDocument(elem);
}
var fn=Expr.attrHandle[name.toLowerCase()],
val=fn&&hasOwn.call(Expr.attrHandle,name.toLowerCase())?
fn(elem,name,!documentIsHTML):
undefined;
return val!==undefined?
val:
support.attributes||!documentIsHTML?
elem.getAttribute(name):
(val=elem.getAttributeNode(name))&&val.specified?
val.value:
null;
};
Sizzle.escape=function(sel){
return(sel+"").replace(rcssescape,fcssescape);
};
Sizzle.error=function(msg){
throw new Error("Syntax error, unrecognized expression: "+msg);
};
Sizzle.uniqueSort=function(results){
var elem,
duplicates=[],
j=0,
i=0;
hasDuplicate=!support.detectDuplicates;
sortInput=!support.sortStable&&results.slice(0);
results.sort(sortOrder);
if(hasDuplicate){
while((elem=results[i++])){
if(elem===results[i]){
j=duplicates.push(i);
}
}
while(j--){
results.splice(duplicates[j],1);
}
}
sortInput=null;
return results;
};
getText=Sizzle.getText=function(elem){
var node,
ret="",
i=0,
nodeType=elem.nodeType;
if(!nodeType){
while((node=elem[i++])){
ret+=getText(node);
}
}else if(nodeType===1||nodeType===9||nodeType===11){
if(typeof elem.textContent==="string"){
return elem.textContent;
}else{
for(elem=elem.firstChild;elem;elem=elem.nextSibling){
ret+=getText(elem);
}
}
}else if(nodeType===3||nodeType===4){
return elem.nodeValue;
}
return ret;
};
Expr=Sizzle.selectors={
cacheLength:50,
createPseudo:markFunction,
match:matchExpr,
attrHandle:{},
find:{},
relative:{
">":{dir:"parentNode",first:true},
" ":{dir:"parentNode"},
"+":{dir:"previousSibling",first:true},
"~":{dir:"previousSibling"}
},
preFilter:{
"ATTR":function(match){
match[1]=match[1].replace(runescape,funescape);
match[3]=(match[3]||match[4]||
match[5]||"").replace(runescape,funescape);
if(match[2]==="~="){
match[3]=" "+match[3]+" ";
}
return match.slice(0,4);
},
"CHILD":function(match){
match[1]=match[1].toLowerCase();
if(match[1].slice(0,3)==="nth"){
if(!match[3]){
Sizzle.error(match[0]);
}
match[4]=+(match[4]?
match[5]+(match[6]||1):
2*(match[3]==="even"||match[3]==="odd"));
match[5]=+((match[7]+match[8])||match[3]==="odd");
}else if(match[3]){
Sizzle.error(match[0]);
}
return match;
},
"PSEUDO":function(match){
var excess,
unquoted=!match[6]&&match[2];
if(matchExpr["CHILD"].test(match[0])){
return null;
}
if(match[3]){
match[2]=match[4]||match[5]||"";
}else if(unquoted&&rpseudo.test(unquoted)&&
(excess=tokenize(unquoted,true))&&
(excess=unquoted.indexOf(")",unquoted.length-excess)-unquoted.length)){
match[0]=match[0].slice(0,excess);
match[2]=unquoted.slice(0,excess);
}
return match.slice(0,3);
}
},
filter:{
"TAG":function(nodeNameSelector){
var nodeName=nodeNameSelector.replace(runescape,funescape).toLowerCase();
return nodeNameSelector==="*"?
function(){
return true;
}:
function(elem){
return elem.nodeName&&elem.nodeName.toLowerCase()===nodeName;
};
},
"CLASS":function(className){
var pattern=classCache[className+" "];
return pattern||
(pattern=new RegExp("(^|"+whitespace+
")"+className+"("+whitespace+"|$)"))&&classCache(
className,function(elem){
return pattern.test(
typeof elem.className==="string"&&elem.className||
typeof elem.getAttribute!=="undefined"&&
elem.getAttribute("class")||
""
);
});
},
"ATTR":function(name,operator,check){
return function(elem){
var result=Sizzle.attr(elem,name);
if(result==null){
return operator==="!=";
}
if(!operator){
return true;
}
result+="";
return operator==="="?result===check:
operator==="!="?result!==check:
operator==="^="?check&&result.indexOf(check)===0:
operator==="*="?check&&result.indexOf(check)>-1:
operator==="$="?check&&result.slice(-check.length)===check:
operator==="~="?(" "+result.replace(rwhitespace," ")+" ").indexOf(check)>-1:
operator==="|="?result===check||result.slice(0,check.length+1)===check+"-":
false;
};
},
"CHILD":function(type,what,_argument,first,last){
var simple=type.slice(0,3)!=="nth",
forward=type.slice(-4)!=="last",
ofType=what==="of-type";
return first===1&&last===0?
function(elem){
return!!elem.parentNode;
}:
function(elem,_context,xml){
var cache,uniqueCache,outerCache,node,nodeIndex,start,
dir=simple!==forward?"nextSibling":"previousSibling",
parent=elem.parentNode,
name=ofType&&elem.nodeName.toLowerCase(),
useCache=!xml&&!ofType,
diff=false;
if(parent){
if(simple){
while(dir){
node=elem;
while((node=node[dir])){
if(ofType?
node.nodeName.toLowerCase()===name:
node.nodeType===1){
return false;
}
}
start=dir=type==="only"&&!start&&"nextSibling";
}
return true;
}
start=[forward?parent.firstChild:parent.lastChild];
if(forward&&useCache){
node=parent;
outerCache=node[expando]||(node[expando]={});
uniqueCache=outerCache[node.uniqueID]||
(outerCache[node.uniqueID]={});
cache=uniqueCache[type]||[];
nodeIndex=cache[0]===dirruns&&cache[1];
diff=nodeIndex&&cache[2];
node=nodeIndex&&parent.childNodes[nodeIndex];
while((node=++nodeIndex&&node&&node[dir]||
(diff=nodeIndex=0)||start.pop())){
if(node.nodeType===1&&++diff&&node===elem){
uniqueCache[type]=[dirruns,nodeIndex,diff];
break;
}
}
}else{
if(useCache){
node=elem;
outerCache=node[expando]||(node[expando]={});
uniqueCache=outerCache[node.uniqueID]||
(outerCache[node.uniqueID]={});
cache=uniqueCache[type]||[];
nodeIndex=cache[0]===dirruns&&cache[1];
diff=nodeIndex;
}
if(diff===false){
while((node=++nodeIndex&&node&&node[dir]||
(diff=nodeIndex=0)||start.pop())){
if((ofType?
node.nodeName.toLowerCase()===name:
node.nodeType===1)&&
++diff){
if(useCache){
outerCache=node[expando]||
(node[expando]={});
uniqueCache=outerCache[node.uniqueID]||
(outerCache[node.uniqueID]={});
uniqueCache[type]=[dirruns,diff];
}
if(node===elem){
break;
}
}
}
}
}
diff-=last;
return diff===first||(diff%first===0&&diff/first>=0);
}
};
},
"PSEUDO":function(pseudo,argument){
var args,
fn=Expr.pseudos[pseudo]||Expr.setFilters[pseudo.toLowerCase()]||
Sizzle.error("unsupported pseudo: "+pseudo);
if(fn[expando]){
return fn(argument);
}
if(fn.length>1){
args=[pseudo,pseudo,"",argument];
return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase())?
markFunction(function(seed,matches){
var idx,
matched=fn(seed,argument),
i=matched.length;
while(i--){
idx=indexOf(seed,matched[i]);
seed[idx]=!(matches[idx]=matched[i]);
}
}):
function(elem){
return fn(elem,0,args);
};
}
return fn;
}
},
pseudos:{
"not":markFunction(function(selector){
var input=[],
results=[],
matcher=compile(selector.replace(rtrim,"$1"));
return matcher[expando]?
markFunction(function(seed,matches,_context,xml){
var elem,
unmatched=matcher(seed,null,xml,[]),
i=seed.length;
while(i--){
if((elem=unmatched[i])){
seed[i]=!(matches[i]=elem);
}
}
}):
function(elem,_context,xml){
input[0]=elem;
matcher(input,null,xml,results);
input[0]=null;
return!results.pop();
};
}),
"has":markFunction(function(selector){
return function(elem){
return Sizzle(selector,elem).length>0;
};
}),
"contains":markFunction(function(text){
text=text.replace(runescape,funescape);
return function(elem){
return(elem.textContent||getText(elem)).indexOf(text)>-1;
};
}),
"lang":markFunction(function(lang){
if(!ridentifier.test(lang||"")){
Sizzle.error("unsupported lang: "+lang);
}
lang=lang.replace(runescape,funescape).toLowerCase();
return function(elem){
var elemLang;
do{
if((elemLang=documentIsHTML?
elem.lang:
elem.getAttribute("xml:lang")||elem.getAttribute("lang"))){
elemLang=elemLang.toLowerCase();
return elemLang===lang||elemLang.indexOf(lang+"-")===0;
}
}while((elem=elem.parentNode)&&elem.nodeType===1);
return false;
};
}),
"target":function(elem){
var hash=window.location&&window.location.hash;
return hash&&hash.slice(1)===elem.id;
},
"root":function(elem){
return elem===docElem;
},
"focus":function(elem){
return elem===document.activeElement&&
(!document.hasFocus||document.hasFocus())&&
!!(elem.type||elem.href||~elem.tabIndex);
},
"enabled":createDisabledPseudo(false),
"disabled":createDisabledPseudo(true),
"checked":function(elem){
var nodeName=elem.nodeName.toLowerCase();
return(nodeName==="input"&&!!elem.checked)||
(nodeName==="option"&&!!elem.selected);
},
"selected":function(elem){
if(elem.parentNode){
elem.parentNode.selectedIndex;
}
return elem.selected===true;
},
"empty":function(elem){
for(elem=elem.firstChild;elem;elem=elem.nextSibling){
if(elem.nodeType<6){
return false;
}
}
return true;
},
"parent":function(elem){
return!Expr.pseudos["empty"](elem);
},
"header":function(elem){
return rheader.test(elem.nodeName);
},
"input":function(elem){
return rinputs.test(elem.nodeName);
},
"button":function(elem){
var name=elem.nodeName.toLowerCase();
return name==="input"&&elem.type==="button"||name==="button";
},
"text":function(elem){
var attr;
return elem.nodeName.toLowerCase()==="input"&&
elem.type==="text"&&
((attr=elem.getAttribute("type"))==null||
attr.toLowerCase()==="text");
},
"first":createPositionalPseudo(function(){
return[0];
}),
"last":createPositionalPseudo(function(_matchIndexes,length){
return[length-1];
}),
"eq":createPositionalPseudo(function(_matchIndexes,length,argument){
return[argument<0?argument+length:argument];
}),
"even":createPositionalPseudo(function(matchIndexes,length){
var i=0;
for(;i<length;i+=2){
matchIndexes.push(i);
}
return matchIndexes;
}),
"odd":createPositionalPseudo(function(matchIndexes,length){
var i=1;
for(;i<length;i+=2){
matchIndexes.push(i);
}
return matchIndexes;
}),
"lt":createPositionalPseudo(function(matchIndexes,length,argument){
var i=argument<0?
argument+length:
argument>length?
length:
argument;
for(;--i>=0;){
matchIndexes.push(i);
}
return matchIndexes;
}),
"gt":createPositionalPseudo(function(matchIndexes,length,argument){
var i=argument<0?argument+length:argument;
for(;++i<length;){
matchIndexes.push(i);
}
return matchIndexes;
})
}
};
Expr.pseudos["nth"]=Expr.pseudos["eq"];
for(i in{radio:true,checkbox:true,file:true,password:true,image:true}){
Expr.pseudos[i]=createInputPseudo(i);
}
for(i in{submit:true,reset:true}){
Expr.pseudos[i]=createButtonPseudo(i);
}
function setFilters(){}
setFilters.prototype=Expr.filters=Expr.pseudos;
Expr.setFilters=new setFilters();
tokenize=Sizzle.tokenize=function(selector,parseOnly){
var matched,match,tokens,type,
soFar,groups,preFilters,
cached=tokenCache[selector+" "];
if(cached){
return parseOnly?0:cached.slice(0);
}
soFar=selector;
groups=[];
preFilters=Expr.preFilter;
while(soFar){
if(!matched||(match=rcomma.exec(soFar))){
if(match){
soFar=soFar.slice(match[0].length)||soFar;
}
groups.push((tokens=[]));
}
matched=false;
if((match=rcombinators.exec(soFar))){
matched=match.shift();
tokens.push({
value:matched,
type:match[0].replace(rtrim," ")
});
soFar=soFar.slice(matched.length);
}
for(type in Expr.filter){
if((match=matchExpr[type].exec(soFar))&&(!preFilters[type]||
(match=preFilters[type](match)))){
matched=match.shift();
tokens.push({
value:matched,
type:type,
matches:match
});
soFar=soFar.slice(matched.length);
}
}
if(!matched){
break;
}
}
return parseOnly?
soFar.length:
soFar?
Sizzle.error(selector):
tokenCache(selector,groups).slice(0);
};
function toSelector(tokens){
var i=0,
len=tokens.length,
selector="";
for(;i<len;i++){
selector+=tokens[i].value;
}
return selector;
}
function addCombinator(matcher,combinator,base){
var dir=combinator.dir,
skip=combinator.next,
key=skip||dir,
checkNonElements=base&&key==="parentNode",
doneName=done++;
return combinator.first?
function(elem,context,xml){
while((elem=elem[dir])){
if(elem.nodeType===1||checkNonElements){
return matcher(elem,context,xml);
}
}
return false;
}:
function(elem,context,xml){
var oldCache,uniqueCache,outerCache,
newCache=[dirruns,doneName];
if(xml){
while((elem=elem[dir])){
if(elem.nodeType===1||checkNonElements){
if(matcher(elem,context,xml)){
return true;
}
}
}
}else{
while((elem=elem[dir])){
if(elem.nodeType===1||checkNonElements){
outerCache=elem[expando]||(elem[expando]={});
uniqueCache=outerCache[elem.uniqueID]||
(outerCache[elem.uniqueID]={});
if(skip&&skip===elem.nodeName.toLowerCase()){
elem=elem[dir]||elem;
}else if((oldCache=uniqueCache[key])&&
oldCache[0]===dirruns&&oldCache[1]===doneName){
return(newCache[2]=oldCache[2]);
}else{
uniqueCache[key]=newCache;
if((newCache[2]=matcher(elem,context,xml))){
return true;
}
}
}
}
}
return false;
};
}
function elementMatcher(matchers){
return matchers.length>1?
function(elem,context,xml){
var i=matchers.length;
while(i--){
if(!matchers[i](elem,context,xml)){
return false;
}
}
return true;
}:
matchers[0];
}
function multipleContexts(selector,contexts,results){
var i=0,
len=contexts.length;
for(;i<len;i++){
Sizzle(selector,contexts[i],results);
}
return results;
}
function condense(unmatched,map,filter,context,xml){
var elem,
newUnmatched=[],
i=0,
len=unmatched.length,
mapped=map!=null;
for(;i<len;i++){
if((elem=unmatched[i])){
if(!filter||filter(elem,context,xml)){
newUnmatched.push(elem);
if(mapped){
map.push(i);
}
}
}
}
return newUnmatched;
}
function setMatcher(preFilter,selector,matcher,postFilter,postFinder,postSelector){
if(postFilter&&!postFilter[expando]){
postFilter=setMatcher(postFilter);
}
if(postFinder&&!postFinder[expando]){
postFinder=setMatcher(postFinder,postSelector);
}
return markFunction(function(seed,results,context,xml){
var temp,i,elem,
preMap=[],
postMap=[],
preexisting=results.length,
elems=seed||multipleContexts(
selector||"*",
context.nodeType?[context]:context,
[]
),
matcherIn=preFilter&&(seed||!selector)?
condense(elems,preMap,preFilter,context,xml):
elems,
matcherOut=matcher?
postFinder||(seed?preFilter:preexisting||postFilter)?
[]:
results:
matcherIn;
if(matcher){
matcher(matcherIn,matcherOut,context,xml);
}
if(postFilter){
temp=condense(matcherOut,postMap);
postFilter(temp,[],context,xml);
i=temp.length;
while(i--){
if((elem=temp[i])){
matcherOut[postMap[i]]=!(matcherIn[postMap[i]]=elem);
}
}
}
if(seed){
if(postFinder||preFilter){
if(postFinder){
temp=[];
i=matcherOut.length;
while(i--){
if((elem=matcherOut[i])){
temp.push((matcherIn[i]=elem));
}
}
postFinder(null,(matcherOut=[]),temp,xml);
}
i=matcherOut.length;
while(i--){
if((elem=matcherOut[i])&&
(temp=postFinder?indexOf(seed,elem):preMap[i])>-1){
seed[temp]=!(results[temp]=elem);
}
}
}
}else{
matcherOut=condense(
matcherOut===results?
matcherOut.splice(preexisting,matcherOut.length):
matcherOut
);
if(postFinder){
postFinder(null,results,matcherOut,xml);
}else{
push.apply(results,matcherOut);
}
}
});
}
function matcherFromTokens(tokens){
var checkContext,matcher,j,
len=tokens.length,
leadingRelative=Expr.relative[tokens[0].type],
implicitRelative=leadingRelative||Expr.relative[" "],
i=leadingRelative?1:0,
matchContext=addCombinator(function(elem){
return elem===checkContext;
},implicitRelative,true),
matchAnyContext=addCombinator(function(elem){
return indexOf(checkContext,elem)>-1;
},implicitRelative,true),
matchers=[function(elem,context,xml){
var ret=(!leadingRelative&&(xml||context!==outermostContext))||(
(checkContext=context).nodeType?
matchContext(elem,context,xml):
matchAnyContext(elem,context,xml));
checkContext=null;
return ret;
}];
for(;i<len;i++){
if((matcher=Expr.relative[tokens[i].type])){
matchers=[addCombinator(elementMatcher(matchers),matcher)];
}else{
matcher=Expr.filter[tokens[i].type].apply(null,tokens[i].matches);
if(matcher[expando]){
j=++i;
for(;j<len;j++){
if(Expr.relative[tokens[j].type]){
break;
}
}
return setMatcher(
i>1&&elementMatcher(matchers),
i>1&&toSelector(
tokens
.slice(0,i-1)
.concat({value:tokens[i-2].type===" "?"*":""})
).replace(rtrim,"$1"),
matcher,
i<j&&matcherFromTokens(tokens.slice(i,j)),
j<len&&matcherFromTokens((tokens=tokens.slice(j))),
j<len&&toSelector(tokens)
);
}
matchers.push(matcher);
}
}
return elementMatcher(matchers);
}
function matcherFromGroupMatchers(elementMatchers,setMatchers){
var bySet=setMatchers.length>0,
byElement=elementMatchers.length>0,
superMatcher=function(seed,context,xml,results,outermost){
var elem,j,matcher,
matchedCount=0,
i="0",
unmatched=seed&&[],
setMatched=[],
contextBackup=outermostContext,
elems=seed||byElement&&Expr.find["TAG"]("*",outermost),
dirrunsUnique=(dirruns+=contextBackup==null?1:Math.random()||0.1),
len=elems.length;
if(outermost){
outermostContext=context==document||context||outermost;
}
for(;i!==len&&(elem=elems[i])!=null;i++){
if(byElement&&elem){
j=0;
if(!context&&elem.ownerDocument!=document){
setDocument(elem);
xml=!documentIsHTML;
}
while((matcher=elementMatchers[j++])){
if(matcher(elem,context||document,xml)){
results.push(elem);
break;
}
}
if(outermost){
dirruns=dirrunsUnique;
}
}
if(bySet){
if((elem=!matcher&&elem)){
matchedCount--;
}
if(seed){
unmatched.push(elem);
}
}
}
matchedCount+=i;
if(bySet&&i!==matchedCount){
j=0;
while((matcher=setMatchers[j++])){
matcher(unmatched,setMatched,context,xml);
}
if(seed){
if(matchedCount>0){
while(i--){
if(!(unmatched[i]||setMatched[i])){
setMatched[i]=pop.call(results);
}
}
}
setMatched=condense(setMatched);
}
push.apply(results,setMatched);
if(outermost&&!seed&&setMatched.length>0&&
(matchedCount+setMatchers.length)>1){
Sizzle.uniqueSort(results);
}
}
if(outermost){
dirruns=dirrunsUnique;
outermostContext=contextBackup;
}
return unmatched;
};
return bySet?
markFunction(superMatcher):
superMatcher;
}
compile=Sizzle.compile=function(selector,match){
var i,
setMatchers=[],
elementMatchers=[],
cached=compilerCache[selector+" "];
if(!cached){
if(!match){
match=tokenize(selector);
}
i=match.length;
while(i--){
cached=matcherFromTokens(match[i]);
if(cached[expando]){
setMatchers.push(cached);
}else{
elementMatchers.push(cached);
}
}
cached=compilerCache(
selector,
matcherFromGroupMatchers(elementMatchers,setMatchers)
);
cached.selector=selector;
}
return cached;
};
select=Sizzle.select=function(selector,context,results,seed){
var i,tokens,token,type,find,
compiled=typeof selector==="function"&&selector,
match=!seed&&tokenize((selector=compiled.selector||selector));
results=results||[];
if(match.length===1){
tokens=match[0]=match[0].slice(0);
if(tokens.length>2&&(token=tokens[0]).type==="ID"&&
context.nodeType===9&&documentIsHTML&&Expr.relative[tokens[1].type]){
context=(Expr.find["ID"](token.matches[0]
.replace(runescape,funescape),context)||[])[0];
if(!context){
return results;
}else if(compiled){
context=context.parentNode;
}
selector=selector.slice(tokens.shift().value.length);
}
i=matchExpr["needsContext"].test(selector)?0:tokens.length;
while(i--){
token=tokens[i];
if(Expr.relative[(type=token.type)]){
break;
}
if((find=Expr.find[type])){
if((seed=find(
token.matches[0].replace(runescape,funescape),
rsibling.test(tokens[0].type)&&testContext(context.parentNode)||
context
))){
tokens.splice(i,1);
selector=seed.length&&toSelector(tokens);
if(!selector){
push.apply(results,seed);
return results;
}
break;
}
}
}
}
(compiled||compile(selector,match))(
seed,
context,
!documentIsHTML,
results,
!context||rsibling.test(selector)&&testContext(context.parentNode)||context
);
return results;
};
support.sortStable=expando.split("").sort(sortOrder).join("")===expando;
support.detectDuplicates=!!hasDuplicate;
setDocument();
support.sortDetached=assert(function(el){
return el.compareDocumentPosition(document.createElement("fieldset"))&1;
});
if(!assert(function(el){
el.innerHTML="<a href='#'></a>";
return el.firstChild.getAttribute("href")==="#";
})){
addHandle("type|href|height|width",function(elem,name,isXML){
if(!isXML){
return elem.getAttribute(name,name.toLowerCase()==="type"?1:2);
}
});
}
if(!support.attributes||!assert(function(el){
el.innerHTML="<input/>";
el.firstChild.setAttribute("value","");
return el.firstChild.getAttribute("value")==="";
})){
addHandle("value",function(elem,_name,isXML){
if(!isXML&&elem.nodeName.toLowerCase()==="input"){
return elem.defaultValue;
}
});
}
if(!assert(function(el){
return el.getAttribute("disabled")==null;
})){
addHandle(booleans,function(elem,name,isXML){
var val;
if(!isXML){
return elem[name]===true?name.toLowerCase():
(val=elem.getAttributeNode(name))&&val.specified?
val.value:
null;
}
});
}
return Sizzle;
})(window);
jQuery.find=Sizzle;
jQuery.expr=Sizzle.selectors;
jQuery.expr[":"]=jQuery.expr.pseudos;
jQuery.uniqueSort=jQuery.unique=Sizzle.uniqueSort;
jQuery.text=Sizzle.getText;
jQuery.isXMLDoc=Sizzle.isXML;
jQuery.contains=Sizzle.contains;
jQuery.escapeSelector=Sizzle.escape;
var dir=function(elem,dir,until){
var matched=[],
truncate=until!==undefined;
while((elem=elem[dir])&&elem.nodeType!==9){
if(elem.nodeType===1){
if(truncate&&jQuery(elem).is(until)){
break;
}
matched.push(elem);
}
}
return matched;
};
var siblings=function(n,elem){
var matched=[];
for(;n;n=n.nextSibling){
if(n.nodeType===1&&n!==elem){
matched.push(n);
}
}
return matched;
};
var rneedsContext=jQuery.expr.match.needsContext;
function nodeName(elem,name){
return elem.nodeName&&elem.nodeName.toLowerCase()===name.toLowerCase();
}
var rsingleTag=(/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i);
function winnow(elements,qualifier,not){
if(isFunction(qualifier)){
return jQuery.grep(elements,function(elem,i){
return!!qualifier.call(elem,i,elem)!==not;
});
}
if(qualifier.nodeType){
return jQuery.grep(elements,function(elem){
return(elem===qualifier)!==not;
});
}
if(typeof qualifier!=="string"){
return jQuery.grep(elements,function(elem){
return(indexOf.call(qualifier,elem)>-1)!==not;
});
}
return jQuery.filter(qualifier,elements,not);
}
jQuery.filter=function(expr,elems,not){
var elem=elems[0];
if(not){
expr=":not("+expr+")";
}
if(elems.length===1&&elem.nodeType===1){
return jQuery.find.matchesSelector(elem,expr)?[elem]:[];
}
return jQuery.find.matches(expr,jQuery.grep(elems,function(elem){
return elem.nodeType===1;
}));
};
jQuery.fn.extend({
find:function(selector){
var i,ret,
len=this.length,
self=this;
if(typeof selector!=="string"){
return this.pushStack(jQuery(selector).filter(function(){
for(i=0;i<len;i++){
if(jQuery.contains(self[i],this)){
return true;
}
}
}));
}
ret=this.pushStack([]);
for(i=0;i<len;i++){
jQuery.find(selector,self[i],ret);
}
return len>1?jQuery.uniqueSort(ret):ret;
},
filter:function(selector){
return this.pushStack(winnow(this,selector||[],false));
},
not:function(selector){
return this.pushStack(winnow(this,selector||[],true));
},
is:function(selector){
return!!winnow(
this,
typeof selector==="string"&&rneedsContext.test(selector)?
jQuery(selector):
selector||[],
false
).length;
}
});
var rootjQuery,
rquickExpr=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
init=jQuery.fn.init=function(selector,context,root){
var match,elem;
if(!selector){
return this;
}
root=root||rootjQuery;
if(typeof selector==="string"){
if(selector[0]==="<"&&
selector[selector.length-1]===">"&&
selector.length>=3){
match=[null,selector,null];
}else{
match=rquickExpr.exec(selector);
}
if(match&&(match[1]||!context)){
if(match[1]){
context=context instanceof jQuery?context[0]:context;
jQuery.merge(this,jQuery.parseHTML(
match[1],
context&&context.nodeType?context.ownerDocument||context:document,
true
));
if(rsingleTag.test(match[1])&&jQuery.isPlainObject(context)){
for(match in context){
if(isFunction(this[match])){
this[match](context[match]);
}else{
this.attr(match,context[match]);
}
}
}
return this;
}else{
elem=document.getElementById(match[2]);
if(elem){
this[0]=elem;
this.length=1;
}
return this;
}
}else if(!context||context.jquery){
return(context||root).find(selector);
}else{
return this.constructor(context).find(selector);
}
}else if(selector.nodeType){
this[0]=selector;
this.length=1;
return this;
}else if(isFunction(selector)){
return root.ready!==undefined?
root.ready(selector):
selector(jQuery);
}
return jQuery.makeArray(selector,this);
};
init.prototype=jQuery.fn;
rootjQuery=jQuery(document);
var rparentsprev=/^(?:parents|prev(?:Until|All))/,
guaranteedUnique={
children:true,
contents:true,
next:true,
prev:true
};
jQuery.fn.extend({
has:function(target){
var targets=jQuery(target,this),
l=targets.length;
return this.filter(function(){
var i=0;
for(;i<l;i++){
if(jQuery.contains(this,targets[i])){
return true;
}
}
});
},
closest:function(selectors,context){
var cur,
i=0,
l=this.length,
matched=[],
targets=typeof selectors!=="string"&&jQuery(selectors);
if(!rneedsContext.test(selectors)){
for(;i<l;i++){
for(cur=this[i];cur&&cur!==context;cur=cur.parentNode){
if(cur.nodeType<11&&(targets?
targets.index(cur)>-1:
cur.nodeType===1&&
jQuery.find.matchesSelector(cur,selectors))){
matched.push(cur);
break;
}
}
}
}
return this.pushStack(matched.length>1?jQuery.uniqueSort(matched):matched);
},
index:function(elem){
if(!elem){
return(this[0]&&this[0].parentNode)?this.first().prevAll().length:-1;
}
if(typeof elem==="string"){
return indexOf.call(jQuery(elem),this[0]);
}
return indexOf.call(this,
elem.jquery?elem[0]:elem
);
},
add:function(selector,context){
return this.pushStack(
jQuery.uniqueSort(
jQuery.merge(this.get(),jQuery(selector,context))
)
);
},
addBack:function(selector){
return this.add(selector==null?
this.prevObject:this.prevObject.filter(selector)
);
}
});
function sibling(cur,dir){
while((cur=cur[dir])&&cur.nodeType!==1){}
return cur;
}
jQuery.each({
parent:function(elem){
var parent=elem.parentNode;
return parent&&parent.nodeType!==11?parent:null;
},
parents:function(elem){
return dir(elem,"parentNode");
},
parentsUntil:function(elem,_i,until){
return dir(elem,"parentNode",until);
},
next:function(elem){
return sibling(elem,"nextSibling");
},
prev:function(elem){
return sibling(elem,"previousSibling");
},
nextAll:function(elem){
return dir(elem,"nextSibling");
},
prevAll:function(elem){
return dir(elem,"previousSibling");
},
nextUntil:function(elem,_i,until){
return dir(elem,"nextSibling",until);
},
prevUntil:function(elem,_i,until){
return dir(elem,"previousSibling",until);
},
siblings:function(elem){
return siblings((elem.parentNode||{}).firstChild,elem);
},
children:function(elem){
return siblings(elem.firstChild);
},
contents:function(elem){
if(elem.contentDocument!=null&&
getProto(elem.contentDocument)){
return elem.contentDocument;
}
if(nodeName(elem,"template")){
elem=elem.content||elem;
}
return jQuery.merge([],elem.childNodes);
}
},function(name,fn){
jQuery.fn[name]=function(until,selector){
var matched=jQuery.map(this,fn,until);
if(name.slice(-5)!=="Until"){
selector=until;
}
if(selector&&typeof selector==="string"){
matched=jQuery.filter(selector,matched);
}
if(this.length>1){
if(!guaranteedUnique[name]){
jQuery.uniqueSort(matched);
}
if(rparentsprev.test(name)){
matched.reverse();
}
}
return this.pushStack(matched);
};
});
var rnothtmlwhite=(/[^\x20\t\r\n\f]+/g);
function createOptions(options){
var object={};
jQuery.each(options.match(rnothtmlwhite)||[],function(_,flag){
object[flag]=true;
});
return object;
}
jQuery.Callbacks=function(options){
options=typeof options==="string"?
createOptions(options):
jQuery.extend({},options);
var
firing,
memory,
fired,
locked,
list=[],
queue=[],
firingIndex=-1,
fire=function(){
locked=locked||options.once;
fired=firing=true;
for(;queue.length;firingIndex=-1){
memory=queue.shift();
while(++firingIndex<list.length){
if(list[firingIndex].apply(memory[0],memory[1])===false&&
options.stopOnFalse){
firingIndex=list.length;
memory=false;
}
}
}
if(!options.memory){
memory=false;
}
firing=false;
if(locked){
if(memory){
list=[];
}else{
list="";
}
}
},
self={
add:function(){
if(list){
if(memory&&!firing){
firingIndex=list.length-1;
queue.push(memory);
}
(function add(args){
jQuery.each(args,function(_,arg){
if(isFunction(arg)){
if(!options.unique||!self.has(arg)){
list.push(arg);
}
}else if(arg&&arg.length&&toType(arg)!=="string"){
add(arg);
}
});
})(arguments);
if(memory&&!firing){
fire();
}
}
return this;
},
remove:function(){
jQuery.each(arguments,function(_,arg){
var index;
while((index=jQuery.inArray(arg,list,index))>-1){
list.splice(index,1);
if(index<=firingIndex){
firingIndex--;
}
}
});
return this;
},
has:function(fn){
return fn?
jQuery.inArray(fn,list)>-1:
list.length>0;
},
empty:function(){
if(list){
list=[];
}
return this;
},
disable:function(){
locked=queue=[];
list=memory="";
return this;
},
disabled:function(){
return!list;
},
lock:function(){
locked=queue=[];
if(!memory&&!firing){
list=memory="";
}
return this;
},
locked:function(){
return!!locked;
},
fireWith:function(context,args){
if(!locked){
args=args||[];
args=[context,args.slice?args.slice():args];
queue.push(args);
if(!firing){
fire();
}
}
return this;
},
fire:function(){
self.fireWith(this,arguments);
return this;
},
fired:function(){
return!!fired;
}
};
return self;
};
function Identity(v){
return v;
}
function Thrower(ex){
throw ex;
}
function adoptValue(value,resolve,reject,noValue){
var method;
try{
if(value&&isFunction((method=value.promise))){
method.call(value).done(resolve).fail(reject);
}else if(value&&isFunction((method=value.then))){
method.call(value,resolve,reject);
}else{
resolve.apply(undefined,[value].slice(noValue));
}
}catch(value){
reject.apply(undefined,[value]);
}
}
jQuery.extend({
Deferred:function(func){
var tuples=[
["notify","progress",jQuery.Callbacks("memory"),
jQuery.Callbacks("memory"),2],
["resolve","done",jQuery.Callbacks("once memory"),
jQuery.Callbacks("once memory"),0,"resolved"],
["reject","fail",jQuery.Callbacks("once memory"),
jQuery.Callbacks("once memory"),1,"rejected"]
],
state="pending",
promise={
state:function(){
return state;
},
always:function(){
deferred.done(arguments).fail(arguments);
return this;
},
"catch":function(fn){
return promise.then(null,fn);
},
pipe:function(){
var fns=arguments;
return jQuery.Deferred(function(newDefer){
jQuery.each(tuples,function(_i,tuple){
var fn=isFunction(fns[tuple[4]])&&fns[tuple[4]];
deferred[tuple[1]](function(){
var returned=fn&&fn.apply(this,arguments);
if(returned&&isFunction(returned.promise)){
returned.promise()
.progress(newDefer.notify)
.done(newDefer.resolve)
.fail(newDefer.reject);
}else{
newDefer[tuple[0]+"With"](
this,
fn?[returned]:arguments
);
}
});
});
fns=null;
}).promise();
},
then:function(onFulfilled,onRejected,onProgress){
var maxDepth=0;
function resolve(depth,deferred,handler,special){
return function(){
var that=this,
args=arguments,
mightThrow=function(){
var returned,then;
if(depth<maxDepth){
return;
}
returned=handler.apply(that,args);
if(returned===deferred.promise()){
throw new TypeError("Thenable self-resolution");
}
then=returned&&
(typeof returned==="object"||
typeof returned==="function")&&
returned.then;
if(isFunction(then)){
if(special){
then.call(
returned,
resolve(maxDepth,deferred,Identity,special),
resolve(maxDepth,deferred,Thrower,special)
);
}else{
maxDepth++;
then.call(
returned,
resolve(maxDepth,deferred,Identity,special),
resolve(maxDepth,deferred,Thrower,special),
resolve(maxDepth,deferred,Identity,
deferred.notifyWith)
);
}
}else{
if(handler!==Identity){
that=undefined;
args=[returned];
}
(special||deferred.resolveWith)(that,args);
}
},
process=special?
mightThrow:
function(){
try{
mightThrow();
}catch(e){
if(jQuery.Deferred.exceptionHook){
jQuery.Deferred.exceptionHook(e,
process.stackTrace);
}
if(depth+1>=maxDepth){
if(handler!==Thrower){
that=undefined;
args=[e];
}
deferred.rejectWith(that,args);
}
}
};
if(depth){
process();
}else{
if(jQuery.Deferred.getStackHook){
process.stackTrace=jQuery.Deferred.getStackHook();
}
window.setTimeout(process);
}
};
}
return jQuery.Deferred(function(newDefer){
tuples[0][3].add(
resolve(
0,
newDefer,
isFunction(onProgress)?
onProgress:
Identity,
newDefer.notifyWith
)
);
tuples[1][3].add(
resolve(
0,
newDefer,
isFunction(onFulfilled)?
onFulfilled:
Identity
)
);
tuples[2][3].add(
resolve(
0,
newDefer,
isFunction(onRejected)?
onRejected:
Thrower
)
);
}).promise();
},
promise:function(obj){
return obj!=null?jQuery.extend(obj,promise):promise;
}
},
deferred={};
jQuery.each(tuples,function(i,tuple){
var list=tuple[2],
stateString=tuple[5];
promise[tuple[1]]=list.add;
if(stateString){
list.add(
function(){
state=stateString;
},
tuples[3-i][2].disable,
tuples[3-i][3].disable,
tuples[0][2].lock,
tuples[0][3].lock
);
}
list.add(tuple[3].fire);
deferred[tuple[0]]=function(){
deferred[tuple[0]+"With"](this===deferred?undefined:this,arguments);
return this;
};
deferred[tuple[0]+"With"]=list.fireWith;
});
promise.promise(deferred);
if(func){
func.call(deferred,deferred);
}
return deferred;
},
when:function(singleValue){
var
remaining=arguments.length,
i=remaining,
resolveContexts=Array(i),
resolveValues=slice.call(arguments),
primary=jQuery.Deferred(),
updateFunc=function(i){
return function(value){
resolveContexts[i]=this;
resolveValues[i]=arguments.length>1?slice.call(arguments):value;
if(!(--remaining)){
primary.resolveWith(resolveContexts,resolveValues);
}
};
};
if(remaining<=1){
adoptValue(singleValue,primary.done(updateFunc(i)).resolve,primary.reject,
!remaining);
if(primary.state()==="pending"||
isFunction(resolveValues[i]&&resolveValues[i].then)){
return primary.then();
}
}
while(i--){
adoptValue(resolveValues[i],updateFunc(i),primary.reject);
}
return primary.promise();
}
});
var rerrorNames=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
jQuery.Deferred.exceptionHook=function(error,stack){
if(window.console&&window.console.warn&&error&&rerrorNames.test(error.name)){
window.console.warn("jQuery.Deferred exception: "+error.message,error.stack,stack);
}
};
jQuery.readyException=function(error){
window.setTimeout(function(){
throw error;
});
};
var readyList=jQuery.Deferred();
jQuery.fn.ready=function(fn){
readyList
.then(fn)
.catch(function(error){
jQuery.readyException(error);
});
return this;
};
jQuery.extend({
isReady:false,
readyWait:1,
ready:function(wait){
if(wait===true?--jQuery.readyWait:jQuery.isReady){
return;
}
jQuery.isReady=true;
if(wait!==true&&--jQuery.readyWait>0){
return;
}
readyList.resolveWith(document,[jQuery]);
}
});
jQuery.ready.then=readyList.then;
function completed(){
document.removeEventListener("DOMContentLoaded",completed);
window.removeEventListener("load",completed);
jQuery.ready();
}
if(document.readyState==="complete"||
(document.readyState!=="loading"&&!document.documentElement.doScroll)){
window.setTimeout(jQuery.ready);
}else{
document.addEventListener("DOMContentLoaded",completed);
window.addEventListener("load",completed);
}
var access=function(elems,fn,key,value,chainable,emptyGet,raw){
var i=0,
len=elems.length,
bulk=key==null;
if(toType(key)==="object"){
chainable=true;
for(i in key){
access(elems,fn,i,key[i],true,emptyGet,raw);
}
}else if(value!==undefined){
chainable=true;
if(!isFunction(value)){
raw=true;
}
if(bulk){
if(raw){
fn.call(elems,value);
fn=null;
}else{
bulk=fn;
fn=function(elem,_key,value){
return bulk.call(jQuery(elem),value);
};
}
}
if(fn){
for(;i<len;i++){
fn(
elems[i],key,raw?
value:
value.call(elems[i],i,fn(elems[i],key))
);
}
}
}
if(chainable){
return elems;
}
if(bulk){
return fn.call(elems);
}
return len?fn(elems[0],key):emptyGet;
};
var rmsPrefix=/^-ms-/,
rdashAlpha=/-([a-z])/g;
function fcamelCase(_all,letter){
return letter.toUpperCase();
}
function camelCase(string){
return string.replace(rmsPrefix,"ms-").replace(rdashAlpha,fcamelCase);
}
var acceptData=function(owner){
return owner.nodeType===1||owner.nodeType===9||!(+owner.nodeType);
};
function Data(){
this.expando=jQuery.expando+Data.uid++;
}
Data.uid=1;
Data.prototype={
cache:function(owner){
var value=owner[this.expando];
if(!value){
value={};
if(acceptData(owner)){
if(owner.nodeType){
owner[this.expando]=value;
}else{
Object.defineProperty(owner,this.expando,{
value:value,
configurable:true
});
}
}
}
return value;
},
set:function(owner,data,value){
var prop,
cache=this.cache(owner);
if(typeof data==="string"){
cache[camelCase(data)]=value;
}else{
for(prop in data){
cache[camelCase(prop)]=data[prop];
}
}
return cache;
},
get:function(owner,key){
return key===undefined?
this.cache(owner):
owner[this.expando]&&owner[this.expando][camelCase(key)];
},
access:function(owner,key,value){
if(key===undefined||
((key&&typeof key==="string")&&value===undefined)){
return this.get(owner,key);
}
this.set(owner,key,value);
return value!==undefined?value:key;
},
remove:function(owner,key){
var i,
cache=owner[this.expando];
if(cache===undefined){
return;
}
if(key!==undefined){
if(Array.isArray(key)){
key=key.map(camelCase);
}else{
key=camelCase(key);
key=key in cache?
[key]:
(key.match(rnothtmlwhite)||[]);
}
i=key.length;
while(i--){
delete cache[key[i]];
}
}
if(key===undefined||jQuery.isEmptyObject(cache)){
if(owner.nodeType){
owner[this.expando]=undefined;
}else{
delete owner[this.expando];
}
}
},
hasData:function(owner){
var cache=owner[this.expando];
return cache!==undefined&&!jQuery.isEmptyObject(cache);
}
};
var dataPriv=new Data();
var dataUser=new Data();
var rbrace=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
rmultiDash=/[A-Z]/g;
function getData(data){
if(data==="true"){
return true;
}
if(data==="false"){
return false;
}
if(data==="null"){
return null;
}
if(data===+data+""){
return+data;
}
if(rbrace.test(data)){
return JSON.parse(data);
}
return data;
}
function dataAttr(elem,key,data){
var name;
if(data===undefined&&elem.nodeType===1){
name="data-"+key.replace(rmultiDash,"-$&").toLowerCase();
data=elem.getAttribute(name);
if(typeof data==="string"){
try{
data=getData(data);
}catch(e){}
dataUser.set(elem,key,data);
}else{
data=undefined;
}
}
return data;
}
jQuery.extend({
hasData:function(elem){
return dataUser.hasData(elem)||dataPriv.hasData(elem);
},
data:function(elem,name,data){
return dataUser.access(elem,name,data);
},
removeData:function(elem,name){
dataUser.remove(elem,name);
},
_data:function(elem,name,data){
return dataPriv.access(elem,name,data);
},
_removeData:function(elem,name){
dataPriv.remove(elem,name);
}
});
jQuery.fn.extend({
data:function(key,value){
var i,name,data,
elem=this[0],
attrs=elem&&elem.attributes;
if(key===undefined){
if(this.length){
data=dataUser.get(elem);
if(elem.nodeType===1&&!dataPriv.get(elem,"hasDataAttrs")){
i=attrs.length;
while(i--){
if(attrs[i]){
name=attrs[i].name;
if(name.indexOf("data-")===0){
name=camelCase(name.slice(5));
dataAttr(elem,name,data[name]);
}
}
}
dataPriv.set(elem,"hasDataAttrs",true);
}
}
return data;
}
if(typeof key==="object"){
return this.each(function(){
dataUser.set(this,key);
});
}
return access(this,function(value){
var data;
if(elem&&value===undefined){
data=dataUser.get(elem,key);
if(data!==undefined){
return data;
}
data=dataAttr(elem,key);
if(data!==undefined){
return data;
}
return;
}
this.each(function(){
dataUser.set(this,key,value);
});
},null,value,arguments.length>1,null,true);
},
removeData:function(key){
return this.each(function(){
dataUser.remove(this,key);
});
}
});
jQuery.extend({
queue:function(elem,type,data){
var queue;
if(elem){
type=(type||"fx")+"queue";
queue=dataPriv.get(elem,type);
if(data){
if(!queue||Array.isArray(data)){
queue=dataPriv.access(elem,type,jQuery.makeArray(data));
}else{
queue.push(data);
}
}
return queue||[];
}
},
dequeue:function(elem,type){
type=type||"fx";
var queue=jQuery.queue(elem,type),
startLength=queue.length,
fn=queue.shift(),
hooks=jQuery._queueHooks(elem,type),
next=function(){
jQuery.dequeue(elem,type);
};
if(fn==="inprogress"){
fn=queue.shift();
startLength--;
}
if(fn){
if(type==="fx"){
queue.unshift("inprogress");
}
delete hooks.stop;
fn.call(elem,next,hooks);
}
if(!startLength&&hooks){
hooks.empty.fire();
}
},
_queueHooks:function(elem,type){
var key=type+"queueHooks";
return dataPriv.get(elem,key)||dataPriv.access(elem,key,{
empty:jQuery.Callbacks("once memory").add(function(){
dataPriv.remove(elem,[type+"queue",key]);
})
});
}
});
jQuery.fn.extend({
queue:function(type,data){
var setter=2;
if(typeof type!=="string"){
data=type;
type="fx";
setter--;
}
if(arguments.length<setter){
return jQuery.queue(this[0],type);
}
return data===undefined?
this:
this.each(function(){
var queue=jQuery.queue(this,type,data);
jQuery._queueHooks(this,type);
if(type==="fx"&&queue[0]!=="inprogress"){
jQuery.dequeue(this,type);
}
});
},
dequeue:function(type){
return this.each(function(){
jQuery.dequeue(this,type);
});
},
clearQueue:function(type){
return this.queue(type||"fx",[]);
},
promise:function(type,obj){
var tmp,
count=1,
defer=jQuery.Deferred(),
elements=this,
i=this.length,
resolve=function(){
if(!(--count)){
defer.resolveWith(elements,[elements]);
}
};
if(typeof type!=="string"){
obj=type;
type=undefined;
}
type=type||"fx";
while(i--){
tmp=dataPriv.get(elements[i],type+"queueHooks");
if(tmp&&tmp.empty){
count++;
tmp.empty.add(resolve);
}
}
resolve();
return defer.promise(obj);
}
});
var pnum=(/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;
var rcssNum=new RegExp("^(?:([+-])=|)("+pnum+")([a-z%]*)$","i");
var cssExpand=["Top","Right","Bottom","Left"];
var documentElement=document.documentElement;
var isAttached=function(elem){
return jQuery.contains(elem.ownerDocument,elem);
},
composed={composed:true};
if(documentElement.getRootNode){
isAttached=function(elem){
return jQuery.contains(elem.ownerDocument,elem)||
elem.getRootNode(composed)===elem.ownerDocument;
};
}
var isHiddenWithinTree=function(elem,el){
elem=el||elem;
return elem.style.display==="none"||
elem.style.display===""&&
isAttached(elem)&&
jQuery.css(elem,"display")==="none";
};
function adjustCSS(elem,prop,valueParts,tween){
var adjusted,scale,
maxIterations=20,
currentValue=tween?
function(){
return tween.cur();
}:
function(){
return jQuery.css(elem,prop,"");
},
initial=currentValue(),
unit=valueParts&&valueParts[3]||(jQuery.cssNumber[prop]?"":"px"),
initialInUnit=elem.nodeType&&
(jQuery.cssNumber[prop]||unit!=="px"&&+initial)&&
rcssNum.exec(jQuery.css(elem,prop));
if(initialInUnit&&initialInUnit[3]!==unit){
initial=initial/2;
unit=unit||initialInUnit[3];
initialInUnit=+initial||1;
while(maxIterations--){
jQuery.style(elem,prop,initialInUnit+unit);
if((1-scale)*(1-(scale=currentValue()/initial||0.5))<=0){
maxIterations=0;
}
initialInUnit=initialInUnit/scale;
}
initialInUnit=initialInUnit*2;
jQuery.style(elem,prop,initialInUnit+unit);
valueParts=valueParts||[];
}
if(valueParts){
initialInUnit=+initialInUnit||+initial||0;
adjusted=valueParts[1]?
initialInUnit+(valueParts[1]+1)*valueParts[2]:
+valueParts[2];
if(tween){
tween.unit=unit;
tween.start=initialInUnit;
tween.end=adjusted;
}
}
return adjusted;
}
var defaultDisplayMap={};
function getDefaultDisplay(elem){
var temp,
doc=elem.ownerDocument,
nodeName=elem.nodeName,
display=defaultDisplayMap[nodeName];
if(display){
return display;
}
temp=doc.body.appendChild(doc.createElement(nodeName));
display=jQuery.css(temp,"display");
temp.parentNode.removeChild(temp);
if(display==="none"){
display="block";
}
defaultDisplayMap[nodeName]=display;
return display;
}
function showHide(elements,show){
var display,elem,
values=[],
index=0,
length=elements.length;
for(;index<length;index++){
elem=elements[index];
if(!elem.style){
continue;
}
display=elem.style.display;
if(show){
if(display==="none"){
values[index]=dataPriv.get(elem,"display")||null;
if(!values[index]){
elem.style.display="";
}
}
if(elem.style.display===""&&isHiddenWithinTree(elem)){
values[index]=getDefaultDisplay(elem);
}
}else{
if(display!=="none"){
values[index]="none";
dataPriv.set(elem,"display",display);
}
}
}
for(index=0;index<length;index++){
if(values[index]!=null){
elements[index].style.display=values[index];
}
}
return elements;
}
jQuery.fn.extend({
show:function(){
return showHide(this,true);
},
hide:function(){
return showHide(this);
},
toggle:function(state){
if(typeof state==="boolean"){
return state?this.show():this.hide();
}
return this.each(function(){
if(isHiddenWithinTree(this)){
jQuery(this).show();
}else{
jQuery(this).hide();
}
});
}
});
var rcheckableType=(/^(?:checkbox|radio)$/i);
var rtagName=(/<([a-z][^\/\0>\x20\t\r\n\f]*)/i);
var rscriptType=(/^$|^module$|\/(?:java|ecma)script/i);
(function(){
var fragment=document.createDocumentFragment(),
div=fragment.appendChild(document.createElement("div")),
input=document.createElement("input");
input.setAttribute("type","radio");
input.setAttribute("checked","checked");
input.setAttribute("name","t");
div.appendChild(input);
support.checkClone=div.cloneNode(true).cloneNode(true).lastChild.checked;
div.innerHTML="<textarea>x</textarea>";
support.noCloneChecked=!!div.cloneNode(true).lastChild.defaultValue;
div.innerHTML="<option></option>";
support.option=!!div.lastChild;
})();
var wrapMap={
thead:[1,"<table>","</table>"],
col:[2,"<table><colgroup>","</colgroup></table>"],
tr:[2,"<table><tbody>","</tbody></table>"],
td:[3,"<table><tbody><tr>","</tr></tbody></table>"],
_default:[0,"",""]
};
wrapMap.tbody=wrapMap.tfoot=wrapMap.colgroup=wrapMap.caption=wrapMap.thead;
wrapMap.th=wrapMap.td;
if(!support.option){
wrapMap.optgroup=wrapMap.option=[1,"<select multiple='multiple'>","</select>"];
}
function getAll(context,tag){
var ret;
if(typeof context.getElementsByTagName!=="undefined"){
ret=context.getElementsByTagName(tag||"*");
}else if(typeof context.querySelectorAll!=="undefined"){
ret=context.querySelectorAll(tag||"*");
}else{
ret=[];
}
if(tag===undefined||tag&&nodeName(context,tag)){
return jQuery.merge([context],ret);
}
return ret;
}
function setGlobalEval(elems,refElements){
var i=0,
l=elems.length;
for(;i<l;i++){
dataPriv.set(
elems[i],
"globalEval",
!refElements||dataPriv.get(refElements[i],"globalEval")
);
}
}
var rhtml=/<|&#?\w+;/;
function buildFragment(elems,context,scripts,selection,ignored){
var elem,tmp,tag,wrap,attached,j,
fragment=context.createDocumentFragment(),
nodes=[],
i=0,
l=elems.length;
for(;i<l;i++){
elem=elems[i];
if(elem||elem===0){
if(toType(elem)==="object"){
jQuery.merge(nodes,elem.nodeType?[elem]:elem);
}else if(!rhtml.test(elem)){
nodes.push(context.createTextNode(elem));
}else{
tmp=tmp||fragment.appendChild(context.createElement("div"));
tag=(rtagName.exec(elem)||["",""])[1].toLowerCase();
wrap=wrapMap[tag]||wrapMap._default;
tmp.innerHTML=wrap[1]+jQuery.htmlPrefilter(elem)+wrap[2];
j=wrap[0];
while(j--){
tmp=tmp.lastChild;
}
jQuery.merge(nodes,tmp.childNodes);
tmp=fragment.firstChild;
tmp.textContent="";
}
}
}
fragment.textContent="";
i=0;
while((elem=nodes[i++])){
if(selection&&jQuery.inArray(elem,selection)>-1){
if(ignored){
ignored.push(elem);
}
continue;
}
attached=isAttached(elem);
tmp=getAll(fragment.appendChild(elem),"script");
if(attached){
setGlobalEval(tmp);
}
if(scripts){
j=0;
while((elem=tmp[j++])){
if(rscriptType.test(elem.type||"")){
scripts.push(elem);
}
}
}
}
return fragment;
}
var rtypenamespace=/^([^.]*)(?:\.(.+)|)/;
function returnTrue(){
return true;
}
function returnFalse(){
return false;
}
function expectSync(elem,type){
return(elem===safeActiveElement())===(type==="focus");
}
function safeActiveElement(){
try{
return document.activeElement;
}catch(err){}
}
function on(elem,types,selector,data,fn,one){
var origFn,type;
if(typeof types==="object"){
if(typeof selector!=="string"){
data=data||selector;
selector=undefined;
}
for(type in types){
on(elem,type,selector,data,types[type],one);
}
return elem;
}
if(data==null&&fn==null){
fn=selector;
data=selector=undefined;
}else if(fn==null){
if(typeof selector==="string"){
fn=data;
data=undefined;
}else{
fn=data;
data=selector;
selector=undefined;
}
}
if(fn===false){
fn=returnFalse;
}else if(!fn){
return elem;
}
if(one===1){
origFn=fn;
fn=function(event){
jQuery().off(event);
return origFn.apply(this,arguments);
};
fn.guid=origFn.guid||(origFn.guid=jQuery.guid++);
}
return elem.each(function(){
jQuery.event.add(this,types,fn,data,selector);
});
}
jQuery.event={
global:{},
add:function(elem,types,handler,data,selector){
var handleObjIn,eventHandle,tmp,
events,t,handleObj,
special,handlers,type,namespaces,origType,
elemData=dataPriv.get(elem);
if(!acceptData(elem)){
return;
}
if(handler.handler){
handleObjIn=handler;
handler=handleObjIn.handler;
selector=handleObjIn.selector;
}
if(selector){
jQuery.find.matchesSelector(documentElement,selector);
}
if(!handler.guid){
handler.guid=jQuery.guid++;
}
if(!(events=elemData.events)){
events=elemData.events=Object.create(null);
}
if(!(eventHandle=elemData.handle)){
eventHandle=elemData.handle=function(e){
return typeof jQuery!=="undefined"&&jQuery.event.triggered!==e.type?
jQuery.event.dispatch.apply(elem,arguments):undefined;
};
}
types=(types||"").match(rnothtmlwhite)||[""];
t=types.length;
while(t--){
tmp=rtypenamespace.exec(types[t])||[];
type=origType=tmp[1];
namespaces=(tmp[2]||"").split(".").sort();
if(!type){
continue;
}
special=jQuery.event.special[type]||{};
type=(selector?special.delegateType:special.bindType)||type;
special=jQuery.event.special[type]||{};
handleObj=jQuery.extend({
type:type,
origType:origType,
data:data,
handler:handler,
guid:handler.guid,
selector:selector,
needsContext:selector&&jQuery.expr.match.needsContext.test(selector),
namespace:namespaces.join(".")
},handleObjIn);
if(!(handlers=events[type])){
handlers=events[type]=[];
handlers.delegateCount=0;
if(!special.setup||
special.setup.call(elem,data,namespaces,eventHandle)===false){
if(elem.addEventListener){
elem.addEventListener(type,eventHandle);
}
}
}
if(special.add){
special.add.call(elem,handleObj);
if(!handleObj.handler.guid){
handleObj.handler.guid=handler.guid;
}
}
if(selector){
handlers.splice(handlers.delegateCount++,0,handleObj);
}else{
handlers.push(handleObj);
}
jQuery.event.global[type]=true;
}
},
remove:function(elem,types,handler,selector,mappedTypes){
var j,origCount,tmp,
events,t,handleObj,
special,handlers,type,namespaces,origType,
elemData=dataPriv.hasData(elem)&&dataPriv.get(elem);
if(!elemData||!(events=elemData.events)){
return;
}
types=(types||"").match(rnothtmlwhite)||[""];
t=types.length;
while(t--){
tmp=rtypenamespace.exec(types[t])||[];
type=origType=tmp[1];
namespaces=(tmp[2]||"").split(".").sort();
if(!type){
for(type in events){
jQuery.event.remove(elem,type+types[t],handler,selector,true);
}
continue;
}
special=jQuery.event.special[type]||{};
type=(selector?special.delegateType:special.bindType)||type;
handlers=events[type]||[];
tmp=tmp[2]&&
new RegExp("(^|\\.)"+namespaces.join("\\.(?:.*\\.|)")+"(\\.|$)");
origCount=j=handlers.length;
while(j--){
handleObj=handlers[j];
if((mappedTypes||origType===handleObj.origType)&&
(!handler||handler.guid===handleObj.guid)&&
(!tmp||tmp.test(handleObj.namespace))&&
(!selector||selector===handleObj.selector||
selector==="**"&&handleObj.selector)){
handlers.splice(j,1);
if(handleObj.selector){
handlers.delegateCount--;
}
if(special.remove){
special.remove.call(elem,handleObj);
}
}
}
if(origCount&&!handlers.length){
if(!special.teardown||
special.teardown.call(elem,namespaces,elemData.handle)===false){
jQuery.removeEvent(elem,type,elemData.handle);
}
delete events[type];
}
}
if(jQuery.isEmptyObject(events)){
dataPriv.remove(elem,"handle events");
}
},
dispatch:function(nativeEvent){
var i,j,ret,matched,handleObj,handlerQueue,
args=new Array(arguments.length),
event=jQuery.event.fix(nativeEvent),
handlers=(
dataPriv.get(this,"events")||Object.create(null)
)[event.type]||[],
special=jQuery.event.special[event.type]||{};
args[0]=event;
for(i=1;i<arguments.length;i++){
args[i]=arguments[i];
}
event.delegateTarget=this;
if(special.preDispatch&&special.preDispatch.call(this,event)===false){
return;
}
handlerQueue=jQuery.event.handlers.call(this,event,handlers);
i=0;
while((matched=handlerQueue[i++])&&!event.isPropagationStopped()){
event.currentTarget=matched.elem;
j=0;
while((handleObj=matched.handlers[j++])&&
!event.isImmediatePropagationStopped()){
if(!event.rnamespace||handleObj.namespace===false||
event.rnamespace.test(handleObj.namespace)){
event.handleObj=handleObj;
event.data=handleObj.data;
ret=((jQuery.event.special[handleObj.origType]||{}).handle||
handleObj.handler).apply(matched.elem,args);
if(ret!==undefined){
if((event.result=ret)===false){
event.preventDefault();
event.stopPropagation();
}
}
}
}
}
if(special.postDispatch){
special.postDispatch.call(this,event);
}
return event.result;
},
handlers:function(event,handlers){
var i,handleObj,sel,matchedHandlers,matchedSelectors,
handlerQueue=[],
delegateCount=handlers.delegateCount,
cur=event.target;
if(delegateCount&&
cur.nodeType&&
!(event.type==="click"&&event.button>=1)){
for(;cur!==this;cur=cur.parentNode||this){
if(cur.nodeType===1&&!(event.type==="click"&&cur.disabled===true)){
matchedHandlers=[];
matchedSelectors={};
for(i=0;i<delegateCount;i++){
handleObj=handlers[i];
sel=handleObj.selector+" ";
if(matchedSelectors[sel]===undefined){
matchedSelectors[sel]=handleObj.needsContext?
jQuery(sel,this).index(cur)>-1:
jQuery.find(sel,this,null,[cur]).length;
}
if(matchedSelectors[sel]){
matchedHandlers.push(handleObj);
}
}
if(matchedHandlers.length){
handlerQueue.push({elem:cur,handlers:matchedHandlers});
}
}
}
}
cur=this;
if(delegateCount<handlers.length){
handlerQueue.push({elem:cur,handlers:handlers.slice(delegateCount)});
}
return handlerQueue;
},
addProp:function(name,hook){
Object.defineProperty(jQuery.Event.prototype,name,{
enumerable:true,
configurable:true,
get:isFunction(hook)?
function(){
if(this.originalEvent){
return hook(this.originalEvent);
}
}:
function(){
if(this.originalEvent){
return this.originalEvent[name];
}
},
set:function(value){
Object.defineProperty(this,name,{
enumerable:true,
configurable:true,
writable:true,
value:value
});
}
});
},
fix:function(originalEvent){
return originalEvent[jQuery.expando]?
originalEvent:
new jQuery.Event(originalEvent);
},
special:{
load:{
noBubble:true
},
click:{
setup:function(data){
var el=this||data;
if(rcheckableType.test(el.type)&&
el.click&&nodeName(el,"input")){
leverageNative(el,"click",returnTrue);
}
return false;
},
trigger:function(data){
var el=this||data;
if(rcheckableType.test(el.type)&&
el.click&&nodeName(el,"input")){
leverageNative(el,"click");
}
return true;
},
_default:function(event){
var target=event.target;
return rcheckableType.test(target.type)&&
target.click&&nodeName(target,"input")&&
dataPriv.get(target,"click")||
nodeName(target,"a");
}
},
beforeunload:{
postDispatch:function(event){
if(event.result!==undefined&&event.originalEvent){
event.originalEvent.returnValue=event.result;
}
}
}
}
};
function leverageNative(el,type,expectSync){
if(!expectSync){
if(dataPriv.get(el,type)===undefined){
jQuery.event.add(el,type,returnTrue);
}
return;
}
dataPriv.set(el,type,false);
jQuery.event.add(el,type,{
namespace:false,
handler:function(event){
var notAsync,result,
saved=dataPriv.get(this,type);
if((event.isTrigger&1)&&this[type]){
if(!saved.length){
saved=slice.call(arguments);
dataPriv.set(this,type,saved);
notAsync=expectSync(this,type);
this[type]();
result=dataPriv.get(this,type);
if(saved!==result||notAsync){
dataPriv.set(this,type,false);
}else{
result={};
}
if(saved!==result){
event.stopImmediatePropagation();
event.preventDefault();
return result&&result.value;
}
}else if((jQuery.event.special[type]||{}).delegateType){
event.stopPropagation();
}
}else if(saved.length){
dataPriv.set(this,type,{
value:jQuery.event.trigger(
jQuery.extend(saved[0],jQuery.Event.prototype),
saved.slice(1),
this
)
});
event.stopImmediatePropagation();
}
}
});
}
jQuery.removeEvent=function(elem,type,handle){
if(elem.removeEventListener){
elem.removeEventListener(type,handle);
}
};
jQuery.Event=function(src,props){
if(!(this instanceof jQuery.Event)){
return new jQuery.Event(src,props);
}
if(src&&src.type){
this.originalEvent=src;
this.type=src.type;
this.isDefaultPrevented=src.defaultPrevented||
src.defaultPrevented===undefined&&
src.returnValue===false?
returnTrue:
returnFalse;
this.target=(src.target&&src.target.nodeType===3)?
src.target.parentNode:
src.target;
this.currentTarget=src.currentTarget;
this.relatedTarget=src.relatedTarget;
}else{
this.type=src;
}
if(props){
jQuery.extend(this,props);
}
this.timeStamp=src&&src.timeStamp||Date.now();
this[jQuery.expando]=true;
};
jQuery.Event.prototype={
constructor:jQuery.Event,
isDefaultPrevented:returnFalse,
isPropagationStopped:returnFalse,
isImmediatePropagationStopped:returnFalse,
isSimulated:false,
preventDefault:function(){
var e=this.originalEvent;
this.isDefaultPrevented=returnTrue;
if(e&&!this.isSimulated){
e.preventDefault();
}
},
stopPropagation:function(){
var e=this.originalEvent;
this.isPropagationStopped=returnTrue;
if(e&&!this.isSimulated){
e.stopPropagation();
}
},
stopImmediatePropagation:function(){
var e=this.originalEvent;
this.isImmediatePropagationStopped=returnTrue;
if(e&&!this.isSimulated){
e.stopImmediatePropagation();
}
this.stopPropagation();
}
};
jQuery.each({
altKey:true,
bubbles:true,
cancelable:true,
changedTouches:true,
ctrlKey:true,
detail:true,
eventPhase:true,
metaKey:true,
pageX:true,
pageY:true,
shiftKey:true,
view:true,
"char":true,
code:true,
charCode:true,
key:true,
keyCode:true,
button:true,
buttons:true,
clientX:true,
clientY:true,
offsetX:true,
offsetY:true,
pointerId:true,
pointerType:true,
screenX:true,
screenY:true,
targetTouches:true,
toElement:true,
touches:true,
which:true
},jQuery.event.addProp);
jQuery.each({focus:"focusin",blur:"focusout"},function(type,delegateType){
jQuery.event.special[type]={
setup:function(){
leverageNative(this,type,expectSync);
return false;
},
trigger:function(){
leverageNative(this,type);
return true;
},
_default:function(){
return true;
},
delegateType:delegateType
};
});
jQuery.each({
mouseenter:"mouseover",
mouseleave:"mouseout",
pointerenter:"pointerover",
pointerleave:"pointerout"
},function(orig,fix){
jQuery.event.special[orig]={
delegateType:fix,
bindType:fix,
handle:function(event){
var ret,
target=this,
related=event.relatedTarget,
handleObj=event.handleObj;
if(!related||(related!==target&&!jQuery.contains(target,related))){
event.type=handleObj.origType;
ret=handleObj.handler.apply(this,arguments);
event.type=fix;
}
return ret;
}
};
});
jQuery.fn.extend({
on:function(types,selector,data,fn){
return on(this,types,selector,data,fn);
},
one:function(types,selector,data,fn){
return on(this,types,selector,data,fn,1);
},
off:function(types,selector,fn){
var handleObj,type;
if(types&&types.preventDefault&&types.handleObj){
handleObj=types.handleObj;
jQuery(types.delegateTarget).off(
handleObj.namespace?
handleObj.origType+"."+handleObj.namespace:
handleObj.origType,
handleObj.selector,
handleObj.handler
);
return this;
}
if(typeof types==="object"){
for(type in types){
this.off(type,selector,types[type]);
}
return this;
}
if(selector===false||typeof selector==="function"){
fn=selector;
selector=undefined;
}
if(fn===false){
fn=returnFalse;
}
return this.each(function(){
jQuery.event.remove(this,types,fn,selector);
});
}
});
var
rnoInnerhtml=/<script|<style|<link/i,
rchecked=/checked\s*(?:[^=]|=\s*.checked.)/i,
rcleanScript=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
function manipulationTarget(elem,content){
if(nodeName(elem,"table")&&
nodeName(content.nodeType!==11?content:content.firstChild,"tr")){
return jQuery(elem).children("tbody")[0]||elem;
}
return elem;
}
function disableScript(elem){
elem.type=(elem.getAttribute("type")!==null)+"/"+elem.type;
return elem;
}
function restoreScript(elem){
if((elem.type||"").slice(0,5)==="true/"){
elem.type=elem.type.slice(5);
}else{
elem.removeAttribute("type");
}
return elem;
}
function cloneCopyEvent(src,dest){
var i,l,type,pdataOld,udataOld,udataCur,events;
if(dest.nodeType!==1){
return;
}
if(dataPriv.hasData(src)){
pdataOld=dataPriv.get(src);
events=pdataOld.events;
if(events){
dataPriv.remove(dest,"handle events");
for(type in events){
for(i=0,l=events[type].length;i<l;i++){
jQuery.event.add(dest,type,events[type][i]);
}
}
}
}
if(dataUser.hasData(src)){
udataOld=dataUser.access(src);
udataCur=jQuery.extend({},udataOld);
dataUser.set(dest,udataCur);
}
}
function fixInput(src,dest){
var nodeName=dest.nodeName.toLowerCase();
if(nodeName==="input"&&rcheckableType.test(src.type)){
dest.checked=src.checked;
}else if(nodeName==="input"||nodeName==="textarea"){
dest.defaultValue=src.defaultValue;
}
}
function domManip(collection,args,callback,ignored){
args=flat(args);
var fragment,first,scripts,hasScripts,node,doc,
i=0,
l=collection.length,
iNoClone=l-1,
value=args[0],
valueIsFunction=isFunction(value);
if(valueIsFunction||
(l>1&&typeof value==="string"&&
!support.checkClone&&rchecked.test(value))){
return collection.each(function(index){
var self=collection.eq(index);
if(valueIsFunction){
args[0]=value.call(this,index,self.html());
}
domManip(self,args,callback,ignored);
});
}
if(l){
fragment=buildFragment(args,collection[0].ownerDocument,false,collection,ignored);
first=fragment.firstChild;
if(fragment.childNodes.length===1){
fragment=first;
}
if(first||ignored){
scripts=jQuery.map(getAll(fragment,"script"),disableScript);
hasScripts=scripts.length;
for(;i<l;i++){
node=fragment;
if(i!==iNoClone){
node=jQuery.clone(node,true,true);
if(hasScripts){
jQuery.merge(scripts,getAll(node,"script"));
}
}
callback.call(collection[i],node,i);
}
if(hasScripts){
doc=scripts[scripts.length-1].ownerDocument;
jQuery.map(scripts,restoreScript);
for(i=0;i<hasScripts;i++){
node=scripts[i];
if(rscriptType.test(node.type||"")&&
!dataPriv.access(node,"globalEval")&&
jQuery.contains(doc,node)){
if(node.src&&(node.type||"").toLowerCase()!=="module"){
if(jQuery._evalUrl&&!node.noModule){
jQuery._evalUrl(node.src,{
nonce:node.nonce||node.getAttribute("nonce")
},doc);
}
}else{
DOMEval(node.textContent.replace(rcleanScript,""),node,doc);
}
}
}
}
}
}
return collection;
}
function remove(elem,selector,keepData){
var node,
nodes=selector?jQuery.filter(selector,elem):elem,
i=0;
for(;(node=nodes[i])!=null;i++){
if(!keepData&&node.nodeType===1){
jQuery.cleanData(getAll(node));
}
if(node.parentNode){
if(keepData&&isAttached(node)){
setGlobalEval(getAll(node,"script"));
}
node.parentNode.removeChild(node);
}
}
return elem;
}
jQuery.extend({
htmlPrefilter:function(html){
return html;
},
clone:function(elem,dataAndEvents,deepDataAndEvents){
var i,l,srcElements,destElements,
clone=elem.cloneNode(true),
inPage=isAttached(elem);
if(!support.noCloneChecked&&(elem.nodeType===1||elem.nodeType===11)&&
!jQuery.isXMLDoc(elem)){
destElements=getAll(clone);
srcElements=getAll(elem);
for(i=0,l=srcElements.length;i<l;i++){
fixInput(srcElements[i],destElements[i]);
}
}
if(dataAndEvents){
if(deepDataAndEvents){
srcElements=srcElements||getAll(elem);
destElements=destElements||getAll(clone);
for(i=0,l=srcElements.length;i<l;i++){
cloneCopyEvent(srcElements[i],destElements[i]);
}
}else{
cloneCopyEvent(elem,clone);
}
}
destElements=getAll(clone,"script");
if(destElements.length>0){
setGlobalEval(destElements,!inPage&&getAll(elem,"script"));
}
return clone;
},
cleanData:function(elems){
var data,elem,type,
special=jQuery.event.special,
i=0;
for(;(elem=elems[i])!==undefined;i++){
if(acceptData(elem)){
if((data=elem[dataPriv.expando])){
if(data.events){
for(type in data.events){
if(special[type]){
jQuery.event.remove(elem,type);
}else{
jQuery.removeEvent(elem,type,data.handle);
}
}
}
elem[dataPriv.expando]=undefined;
}
if(elem[dataUser.expando]){
elem[dataUser.expando]=undefined;
}
}
}
}
});
jQuery.fn.extend({
detach:function(selector){
return remove(this,selector,true);
},
remove:function(selector){
return remove(this,selector);
},
text:function(value){
return access(this,function(value){
return value===undefined?
jQuery.text(this):
this.empty().each(function(){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
this.textContent=value;
}
});
},null,value,arguments.length);
},
append:function(){
return domManip(this,arguments,function(elem){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
var target=manipulationTarget(this,elem);
target.appendChild(elem);
}
});
},
prepend:function(){
return domManip(this,arguments,function(elem){
if(this.nodeType===1||this.nodeType===11||this.nodeType===9){
var target=manipulationTarget(this,elem);
target.insertBefore(elem,target.firstChild);
}
});
},
before:function(){
return domManip(this,arguments,function(elem){
if(this.parentNode){
this.parentNode.insertBefore(elem,this);
}
});
},
after:function(){
return domManip(this,arguments,function(elem){
if(this.parentNode){
this.parentNode.insertBefore(elem,this.nextSibling);
}
});
},
empty:function(){
var elem,
i=0;
for(;(elem=this[i])!=null;i++){
if(elem.nodeType===1){
jQuery.cleanData(getAll(elem,false));
elem.textContent="";
}
}
return this;
},
clone:function(dataAndEvents,deepDataAndEvents){
dataAndEvents=dataAndEvents==null?false:dataAndEvents;
deepDataAndEvents=deepDataAndEvents==null?dataAndEvents:deepDataAndEvents;
return this.map(function(){
return jQuery.clone(this,dataAndEvents,deepDataAndEvents);
});
},
html:function(value){
return access(this,function(value){
var elem=this[0]||{},
i=0,
l=this.length;
if(value===undefined&&elem.nodeType===1){
return elem.innerHTML;
}
if(typeof value==="string"&&!rnoInnerhtml.test(value)&&
!wrapMap[(rtagName.exec(value)||["",""])[1].toLowerCase()]){
value=jQuery.htmlPrefilter(value);
try{
for(;i<l;i++){
elem=this[i]||{};
if(elem.nodeType===1){
jQuery.cleanData(getAll(elem,false));
elem.innerHTML=value;
}
}
elem=0;
}catch(e){}
}
if(elem){
this.empty().append(value);
}
},null,value,arguments.length);
},
replaceWith:function(){
var ignored=[];
return domManip(this,arguments,function(elem){
var parent=this.parentNode;
if(jQuery.inArray(this,ignored)<0){
jQuery.cleanData(getAll(this));
if(parent){
parent.replaceChild(elem,this);
}
}
},ignored);
}
});
jQuery.each({
appendTo:"append",
prependTo:"prepend",
insertBefore:"before",
insertAfter:"after",
replaceAll:"replaceWith"
},function(name,original){
jQuery.fn[name]=function(selector){
var elems,
ret=[],
insert=jQuery(selector),
last=insert.length-1,
i=0;
for(;i<=last;i++){
elems=i===last?this:this.clone(true);
jQuery(insert[i])[original](elems);
push.apply(ret,elems.get());
}
return this.pushStack(ret);
};
});
var rnumnonpx=new RegExp("^("+pnum+")(?!px)[a-z%]+$","i");
var getStyles=function(elem){
var view=elem.ownerDocument.defaultView;
if(!view||!view.opener){
view=window;
}
return view.getComputedStyle(elem);
};
var swap=function(elem,options,callback){
var ret,name,
old={};
for(name in options){
old[name]=elem.style[name];
elem.style[name]=options[name];
}
ret=callback.call(elem);
for(name in options){
elem.style[name]=old[name];
}
return ret;
};
var rboxStyle=new RegExp(cssExpand.join("|"),"i");
(function(){
function computeStyleTests(){
if(!div){
return;
}
container.style.cssText="position:absolute;left:-11111px;width:60px;"+
"margin-top:1px;padding:0;border:0";
div.style.cssText=
"position:relative;display:block;box-sizing:border-box;overflow:scroll;"+
"margin:auto;border:1px;padding:1px;"+
"width:60%;top:1%";
documentElement.appendChild(container).appendChild(div);
var divStyle=window.getComputedStyle(div);
pixelPositionVal=divStyle.top!=="1%";
reliableMarginLeftVal=roundPixelMeasures(divStyle.marginLeft)===12;
div.style.right="60%";
pixelBoxStylesVal=roundPixelMeasures(divStyle.right)===36;
boxSizingReliableVal=roundPixelMeasures(divStyle.width)===36;
div.style.position="absolute";
scrollboxSizeVal=roundPixelMeasures(div.offsetWidth/3)===12;
documentElement.removeChild(container);
div=null;
}
function roundPixelMeasures(measure){
return Math.round(parseFloat(measure));
}
var pixelPositionVal,boxSizingReliableVal,scrollboxSizeVal,pixelBoxStylesVal,
reliableTrDimensionsVal,reliableMarginLeftVal,
container=document.createElement("div"),
div=document.createElement("div");
if(!div.style){
return;
}
div.style.backgroundClip="content-box";
div.cloneNode(true).style.backgroundClip="";
support.clearCloneStyle=div.style.backgroundClip==="content-box";
jQuery.extend(support,{
boxSizingReliable:function(){
computeStyleTests();
return boxSizingReliableVal;
},
pixelBoxStyles:function(){
computeStyleTests();
return pixelBoxStylesVal;
},
pixelPosition:function(){
computeStyleTests();
return pixelPositionVal;
},
reliableMarginLeft:function(){
computeStyleTests();
return reliableMarginLeftVal;
},
scrollboxSize:function(){
computeStyleTests();
return scrollboxSizeVal;
},
reliableTrDimensions:function(){
var table,tr,trChild,trStyle;
if(reliableTrDimensionsVal==null){
table=document.createElement("table");
tr=document.createElement("tr");
trChild=document.createElement("div");
table.style.cssText="position:absolute;left:-11111px;border-collapse:separate";
tr.style.cssText="border:1px solid";
tr.style.height="1px";
trChild.style.height="9px";
trChild.style.display="block";
documentElement
.appendChild(table)
.appendChild(tr)
.appendChild(trChild);
trStyle=window.getComputedStyle(tr);
reliableTrDimensionsVal=(parseInt(trStyle.height,10)+
parseInt(trStyle.borderTopWidth,10)+
parseInt(trStyle.borderBottomWidth,10))===tr.offsetHeight;
documentElement.removeChild(table);
}
return reliableTrDimensionsVal;
}
});
})();
function curCSS(elem,name,computed){
var width,minWidth,maxWidth,ret,
style=elem.style;
computed=computed||getStyles(elem);
if(computed){
ret=computed.getPropertyValue(name)||computed[name];
if(ret===""&&!isAttached(elem)){
ret=jQuery.style(elem,name);
}
if(!support.pixelBoxStyles()&&rnumnonpx.test(ret)&&rboxStyle.test(name)){
width=style.width;
minWidth=style.minWidth;
maxWidth=style.maxWidth;
style.minWidth=style.maxWidth=style.width=ret;
ret=computed.width;
style.width=width;
style.minWidth=minWidth;
style.maxWidth=maxWidth;
}
}
return ret!==undefined?
ret+"":
ret;
}
function addGetHookIf(conditionFn,hookFn){
return{
get:function(){
if(conditionFn()){
delete this.get;
return;
}
return(this.get=hookFn).apply(this,arguments);
}
};
}
var cssPrefixes=["Webkit","Moz","ms"],
emptyStyle=document.createElement("div").style,
vendorProps={};
function vendorPropName(name){
var capName=name[0].toUpperCase()+name.slice(1),
i=cssPrefixes.length;
while(i--){
name=cssPrefixes[i]+capName;
if(name in emptyStyle){
return name;
}
}
}
function finalPropName(name){
var final=jQuery.cssProps[name]||vendorProps[name];
if(final){
return final;
}
if(name in emptyStyle){
return name;
}
return vendorProps[name]=vendorPropName(name)||name;
}
var
rdisplayswap=/^(none|table(?!-c[ea]).+)/,
rcustomProp=/^--/,
cssShow={position:"absolute",visibility:"hidden",display:"block"},
cssNormalTransform={
letterSpacing:"0",
fontWeight:"400"
};
function setPositiveNumber(_elem,value,subtract){
var matches=rcssNum.exec(value);
return matches?
Math.max(0,matches[2]-(subtract||0))+(matches[3]||"px"):
value;
}
function boxModelAdjustment(elem,dimension,box,isBorderBox,styles,computedVal){
var i=dimension==="width"?1:0,
extra=0,
delta=0;
if(box===(isBorderBox?"border":"content")){
return 0;
}
for(;i<4;i+=2){
if(box==="margin"){
delta+=jQuery.css(elem,box+cssExpand[i],true,styles);
}
if(!isBorderBox){
delta+=jQuery.css(elem,"padding"+cssExpand[i],true,styles);
if(box!=="padding"){
delta+=jQuery.css(elem,"border"+cssExpand[i]+"Width",true,styles);
}else{
extra+=jQuery.css(elem,"border"+cssExpand[i]+"Width",true,styles);
}
}else{
if(box==="content"){
delta-=jQuery.css(elem,"padding"+cssExpand[i],true,styles);
}
if(box!=="margin"){
delta-=jQuery.css(elem,"border"+cssExpand[i]+"Width",true,styles);
}
}
}
if(!isBorderBox&&computedVal>=0){
delta+=Math.max(0,Math.ceil(
elem["offset"+dimension[0].toUpperCase()+dimension.slice(1)]-
computedVal-
delta-
extra-
0.5
))||0;
}
return delta;
}
function getWidthOrHeight(elem,dimension,extra){
var styles=getStyles(elem),
boxSizingNeeded=!support.boxSizingReliable()||extra,
isBorderBox=boxSizingNeeded&&
jQuery.css(elem,"boxSizing",false,styles)==="border-box",
valueIsBorderBox=isBorderBox,
val=curCSS(elem,dimension,styles),
offsetProp="offset"+dimension[0].toUpperCase()+dimension.slice(1);
if(rnumnonpx.test(val)){
if(!extra){
return val;
}
val="auto";
}
if((!support.boxSizingReliable()&&isBorderBox||
!support.reliableTrDimensions()&&nodeName(elem,"tr")||
val==="auto"||
!parseFloat(val)&&jQuery.css(elem,"display",false,styles)==="inline")&&
elem.getClientRects().length){
isBorderBox=jQuery.css(elem,"boxSizing",false,styles)==="border-box";
valueIsBorderBox=offsetProp in elem;
if(valueIsBorderBox){
val=elem[offsetProp];
}
}
val=parseFloat(val)||0;
return(val+
boxModelAdjustment(
elem,
dimension,
extra||(isBorderBox?"border":"content"),
valueIsBorderBox,
styles,
val
)
)+"px";
}
jQuery.extend({
cssHooks:{
opacity:{
get:function(elem,computed){
if(computed){
var ret=curCSS(elem,"opacity");
return ret===""?"1":ret;
}
}
}
},
cssNumber:{
"animationIterationCount":true,
"columnCount":true,
"fillOpacity":true,
"flexGrow":true,
"flexShrink":true,
"fontWeight":true,
"gridArea":true,
"gridColumn":true,
"gridColumnEnd":true,
"gridColumnStart":true,
"gridRow":true,
"gridRowEnd":true,
"gridRowStart":true,
"lineHeight":true,
"opacity":true,
"order":true,
"orphans":true,
"widows":true,
"zIndex":true,
"zoom":true
},
cssProps:{},
style:function(elem,name,value,extra){
if(!elem||elem.nodeType===3||elem.nodeType===8||!elem.style){
return;
}
var ret,type,hooks,
origName=camelCase(name),
isCustomProp=rcustomProp.test(name),
style=elem.style;
if(!isCustomProp){
name=finalPropName(origName);
}
hooks=jQuery.cssHooks[name]||jQuery.cssHooks[origName];
if(value!==undefined){
type=typeof value;
if(type==="string"&&(ret=rcssNum.exec(value))&&ret[1]){
value=adjustCSS(elem,name,ret);
type="number";
}
if(value==null||value!==value){
return;
}
if(type==="number"&&!isCustomProp){
value+=ret&&ret[3]||(jQuery.cssNumber[origName]?"":"px");
}
if(!support.clearCloneStyle&&value===""&&name.indexOf("background")===0){
style[name]="inherit";
}
if(!hooks||!("set"in hooks)||
(value=hooks.set(elem,value,extra))!==undefined){
if(isCustomProp){
style.setProperty(name,value);
}else{
style[name]=value;
}
}
}else{
if(hooks&&"get"in hooks&&
(ret=hooks.get(elem,false,extra))!==undefined){
return ret;
}
return style[name];
}
},
css:function(elem,name,extra,styles){
var val,num,hooks,
origName=camelCase(name),
isCustomProp=rcustomProp.test(name);
if(!isCustomProp){
name=finalPropName(origName);
}
hooks=jQuery.cssHooks[name]||jQuery.cssHooks[origName];
if(hooks&&"get"in hooks){
val=hooks.get(elem,true,extra);
}
if(val===undefined){
val=curCSS(elem,name,styles);
}
if(val==="normal"&&name in cssNormalTransform){
val=cssNormalTransform[name];
}
if(extra===""||extra){
num=parseFloat(val);
return extra===true||isFinite(num)?num||0:val;
}
return val;
}
});
jQuery.each(["height","width"],function(_i,dimension){
jQuery.cssHooks[dimension]={
get:function(elem,computed,extra){
if(computed){
return rdisplayswap.test(jQuery.css(elem,"display"))&&
(!elem.getClientRects().length||!elem.getBoundingClientRect().width)?
swap(elem,cssShow,function(){
return getWidthOrHeight(elem,dimension,extra);
}):
getWidthOrHeight(elem,dimension,extra);
}
},
set:function(elem,value,extra){
var matches,
styles=getStyles(elem),
scrollboxSizeBuggy=!support.scrollboxSize()&&
styles.position==="absolute",
boxSizingNeeded=scrollboxSizeBuggy||extra,
isBorderBox=boxSizingNeeded&&
jQuery.css(elem,"boxSizing",false,styles)==="border-box",
subtract=extra?
boxModelAdjustment(
elem,
dimension,
extra,
isBorderBox,
styles
):
0;
if(isBorderBox&&scrollboxSizeBuggy){
subtract-=Math.ceil(
elem["offset"+dimension[0].toUpperCase()+dimension.slice(1)]-
parseFloat(styles[dimension])-
boxModelAdjustment(elem,dimension,"border",false,styles)-
0.5
);
}
if(subtract&&(matches=rcssNum.exec(value))&&
(matches[3]||"px")!=="px"){
elem.style[dimension]=value;
value=jQuery.css(elem,dimension);
}
return setPositiveNumber(elem,value,subtract);
}
};
});
jQuery.cssHooks.marginLeft=addGetHookIf(support.reliableMarginLeft,
function(elem,computed){
if(computed){
return(parseFloat(curCSS(elem,"marginLeft"))||
elem.getBoundingClientRect().left-
swap(elem,{marginLeft:0},function(){
return elem.getBoundingClientRect().left;
})
)+"px";
}
}
);
jQuery.each({
margin:"",
padding:"",
border:"Width"
},function(prefix,suffix){
jQuery.cssHooks[prefix+suffix]={
expand:function(value){
var i=0,
expanded={},
parts=typeof value==="string"?value.split(" "):[value];
for(;i<4;i++){
expanded[prefix+cssExpand[i]+suffix]=
parts[i]||parts[i-2]||parts[0];
}
return expanded;
}
};
if(prefix!=="margin"){
jQuery.cssHooks[prefix+suffix].set=setPositiveNumber;
}
});
jQuery.fn.extend({
css:function(name,value){
return access(this,function(elem,name,value){
var styles,len,
map={},
i=0;
if(Array.isArray(name)){
styles=getStyles(elem);
len=name.length;
for(;i<len;i++){
map[name[i]]=jQuery.css(elem,name[i],false,styles);
}
return map;
}
return value!==undefined?
jQuery.style(elem,name,value):
jQuery.css(elem,name);
},name,value,arguments.length>1);
}
});
function Tween(elem,options,prop,end,easing){
return new Tween.prototype.init(elem,options,prop,end,easing);
}
jQuery.Tween=Tween;
Tween.prototype={
constructor:Tween,
init:function(elem,options,prop,end,easing,unit){
this.elem=elem;
this.prop=prop;
this.easing=easing||jQuery.easing._default;
this.options=options;
this.start=this.now=this.cur();
this.end=end;
this.unit=unit||(jQuery.cssNumber[prop]?"":"px");
},
cur:function(){
var hooks=Tween.propHooks[this.prop];
return hooks&&hooks.get?
hooks.get(this):
Tween.propHooks._default.get(this);
},
run:function(percent){
var eased,
hooks=Tween.propHooks[this.prop];
if(this.options.duration){
this.pos=eased=jQuery.easing[this.easing](
percent,this.options.duration*percent,0,1,this.options.duration
);
}else{
this.pos=eased=percent;
}
this.now=(this.end-this.start)*eased+this.start;
if(this.options.step){
this.options.step.call(this.elem,this.now,this);
}
if(hooks&&hooks.set){
hooks.set(this);
}else{
Tween.propHooks._default.set(this);
}
return this;
}
};
Tween.prototype.init.prototype=Tween.prototype;
Tween.propHooks={
_default:{
get:function(tween){
var result;
if(tween.elem.nodeType!==1||
tween.elem[tween.prop]!=null&&tween.elem.style[tween.prop]==null){
return tween.elem[tween.prop];
}
result=jQuery.css(tween.elem,tween.prop,"");
return!result||result==="auto"?0:result;
},
set:function(tween){
if(jQuery.fx.step[tween.prop]){
jQuery.fx.step[tween.prop](tween);
}else if(tween.elem.nodeType===1&&(
jQuery.cssHooks[tween.prop]||
tween.elem.style[finalPropName(tween.prop)]!=null)){
jQuery.style(tween.elem,tween.prop,tween.now+tween.unit);
}else{
tween.elem[tween.prop]=tween.now;
}
}
}
};
Tween.propHooks.scrollTop=Tween.propHooks.scrollLeft={
set:function(tween){
if(tween.elem.nodeType&&tween.elem.parentNode){
tween.elem[tween.prop]=tween.now;
}
}
};
jQuery.easing={
linear:function(p){
return p;
},
swing:function(p){
return 0.5-Math.cos(p*Math.PI)/2;
},
_default:"swing"
};
jQuery.fx=Tween.prototype.init;
jQuery.fx.step={};
var
fxNow,inProgress,
rfxtypes=/^(?:toggle|show|hide)$/,
rrun=/queueHooks$/;
function schedule(){
if(inProgress){
if(document.hidden===false&&window.requestAnimationFrame){
window.requestAnimationFrame(schedule);
}else{
window.setTimeout(schedule,jQuery.fx.interval);
}
jQuery.fx.tick();
}
}
function createFxNow(){
window.setTimeout(function(){
fxNow=undefined;
});
return(fxNow=Date.now());
}
function genFx(type,includeWidth){
var which,
i=0,
attrs={height:type};
includeWidth=includeWidth?1:0;
for(;i<4;i+=2-includeWidth){
which=cssExpand[i];
attrs["margin"+which]=attrs["padding"+which]=type;
}
if(includeWidth){
attrs.opacity=attrs.width=type;
}
return attrs;
}
function createTween(value,prop,animation){
var tween,
collection=(Animation.tweeners[prop]||[]).concat(Animation.tweeners["*"]),
index=0,
length=collection.length;
for(;index<length;index++){
if((tween=collection[index].call(animation,prop,value))){
return tween;
}
}
}
function defaultPrefilter(elem,props,opts){
var prop,value,toggle,hooks,oldfire,propTween,restoreDisplay,display,
isBox="width"in props||"height"in props,
anim=this,
orig={},
style=elem.style,
hidden=elem.nodeType&&isHiddenWithinTree(elem),
dataShow=dataPriv.get(elem,"fxshow");
if(!opts.queue){
hooks=jQuery._queueHooks(elem,"fx");
if(hooks.unqueued==null){
hooks.unqueued=0;
oldfire=hooks.empty.fire;
hooks.empty.fire=function(){
if(!hooks.unqueued){
oldfire();
}
};
}
hooks.unqueued++;
anim.always(function(){
anim.always(function(){
hooks.unqueued--;
if(!jQuery.queue(elem,"fx").length){
hooks.empty.fire();
}
});
});
}
for(prop in props){
value=props[prop];
if(rfxtypes.test(value)){
delete props[prop];
toggle=toggle||value==="toggle";
if(value===(hidden?"hide":"show")){
if(value==="show"&&dataShow&&dataShow[prop]!==undefined){
hidden=true;
}else{
continue;
}
}
orig[prop]=dataShow&&dataShow[prop]||jQuery.style(elem,prop);
}
}
propTween=!jQuery.isEmptyObject(props);
if(!propTween&&jQuery.isEmptyObject(orig)){
return;
}
if(isBox&&elem.nodeType===1){
opts.overflow=[style.overflow,style.overflowX,style.overflowY];
restoreDisplay=dataShow&&dataShow.display;
if(restoreDisplay==null){
restoreDisplay=dataPriv.get(elem,"display");
}
display=jQuery.css(elem,"display");
if(display==="none"){
if(restoreDisplay){
display=restoreDisplay;
}else{
showHide([elem],true);
restoreDisplay=elem.style.display||restoreDisplay;
display=jQuery.css(elem,"display");
showHide([elem]);
}
}
if(display==="inline"||display==="inline-block"&&restoreDisplay!=null){
if(jQuery.css(elem,"float")==="none"){
if(!propTween){
anim.done(function(){
style.display=restoreDisplay;
});
if(restoreDisplay==null){
display=style.display;
restoreDisplay=display==="none"?"":display;
}
}
style.display="inline-block";
}
}
}
if(opts.overflow){
style.overflow="hidden";
anim.always(function(){
style.overflow=opts.overflow[0];
style.overflowX=opts.overflow[1];
style.overflowY=opts.overflow[2];
});
}
propTween=false;
for(prop in orig){
if(!propTween){
if(dataShow){
if("hidden"in dataShow){
hidden=dataShow.hidden;
}
}else{
dataShow=dataPriv.access(elem,"fxshow",{display:restoreDisplay});
}
if(toggle){
dataShow.hidden=!hidden;
}
if(hidden){
showHide([elem],true);
}
anim.done(function(){
if(!hidden){
showHide([elem]);
}
dataPriv.remove(elem,"fxshow");
for(prop in orig){
jQuery.style(elem,prop,orig[prop]);
}
});
}
propTween=createTween(hidden?dataShow[prop]:0,prop,anim);
if(!(prop in dataShow)){
dataShow[prop]=propTween.start;
if(hidden){
propTween.end=propTween.start;
propTween.start=0;
}
}
}
}
function propFilter(props,specialEasing){
var index,name,easing,value,hooks;
for(index in props){
name=camelCase(index);
easing=specialEasing[name];
value=props[index];
if(Array.isArray(value)){
easing=value[1];
value=props[index]=value[0];
}
if(index!==name){
props[name]=value;
delete props[index];
}
hooks=jQuery.cssHooks[name];
if(hooks&&"expand"in hooks){
value=hooks.expand(value);
delete props[name];
for(index in value){
if(!(index in props)){
props[index]=value[index];
specialEasing[index]=easing;
}
}
}else{
specialEasing[name]=easing;
}
}
}
function Animation(elem,properties,options){
var result,
stopped,
index=0,
length=Animation.prefilters.length,
deferred=jQuery.Deferred().always(function(){
delete tick.elem;
}),
tick=function(){
if(stopped){
return false;
}
var currentTime=fxNow||createFxNow(),
remaining=Math.max(0,animation.startTime+animation.duration-currentTime),
temp=remaining/animation.duration||0,
percent=1-temp,
index=0,
length=animation.tweens.length;
for(;index<length;index++){
animation.tweens[index].run(percent);
}
deferred.notifyWith(elem,[animation,percent,remaining]);
if(percent<1&&length){
return remaining;
}
if(!length){
deferred.notifyWith(elem,[animation,1,0]);
}
deferred.resolveWith(elem,[animation]);
return false;
},
animation=deferred.promise({
elem:elem,
props:jQuery.extend({},properties),
opts:jQuery.extend(true,{
specialEasing:{},
easing:jQuery.easing._default
},options),
originalProperties:properties,
originalOptions:options,
startTime:fxNow||createFxNow(),
duration:options.duration,
tweens:[],
createTween:function(prop,end){
var tween=jQuery.Tween(elem,animation.opts,prop,end,
animation.opts.specialEasing[prop]||animation.opts.easing);
animation.tweens.push(tween);
return tween;
},
stop:function(gotoEnd){
var index=0,
length=gotoEnd?animation.tweens.length:0;
if(stopped){
return this;
}
stopped=true;
for(;index<length;index++){
animation.tweens[index].run(1);
}
if(gotoEnd){
deferred.notifyWith(elem,[animation,1,0]);
deferred.resolveWith(elem,[animation,gotoEnd]);
}else{
deferred.rejectWith(elem,[animation,gotoEnd]);
}
return this;
}
}),
props=animation.props;
propFilter(props,animation.opts.specialEasing);
for(;index<length;index++){
result=Animation.prefilters[index].call(animation,elem,props,animation.opts);
if(result){
if(isFunction(result.stop)){
jQuery._queueHooks(animation.elem,animation.opts.queue).stop=
result.stop.bind(result);
}
return result;
}
}
jQuery.map(props,createTween,animation);
if(isFunction(animation.opts.start)){
animation.opts.start.call(elem,animation);
}
animation
.progress(animation.opts.progress)
.done(animation.opts.done,animation.opts.complete)
.fail(animation.opts.fail)
.always(animation.opts.always);
jQuery.fx.timer(
jQuery.extend(tick,{
elem:elem,
anim:animation,
queue:animation.opts.queue
})
);
return animation;
}
jQuery.Animation=jQuery.extend(Animation,{
tweeners:{
"*":[function(prop,value){
var tween=this.createTween(prop,value);
adjustCSS(tween.elem,prop,rcssNum.exec(value),tween);
return tween;
}]
},
tweener:function(props,callback){
if(isFunction(props)){
callback=props;
props=["*"];
}else{
props=props.match(rnothtmlwhite);
}
var prop,
index=0,
length=props.length;
for(;index<length;index++){
prop=props[index];
Animation.tweeners[prop]=Animation.tweeners[prop]||[];
Animation.tweeners[prop].unshift(callback);
}
},
prefilters:[defaultPrefilter],
prefilter:function(callback,prepend){
if(prepend){
Animation.prefilters.unshift(callback);
}else{
Animation.prefilters.push(callback);
}
}
});
jQuery.speed=function(speed,easing,fn){
var opt=speed&&typeof speed==="object"?jQuery.extend({},speed):{
complete:fn||!fn&&easing||
isFunction(speed)&&speed,
duration:speed,
easing:fn&&easing||easing&&!isFunction(easing)&&easing
};
if(jQuery.fx.off){
opt.duration=0;
}else{
if(typeof opt.duration!=="number"){
if(opt.duration in jQuery.fx.speeds){
opt.duration=jQuery.fx.speeds[opt.duration];
}else{
opt.duration=jQuery.fx.speeds._default;
}
}
}
if(opt.queue==null||opt.queue===true){
opt.queue="fx";
}
opt.old=opt.complete;
opt.complete=function(){
if(isFunction(opt.old)){
opt.old.call(this);
}
if(opt.queue){
jQuery.dequeue(this,opt.queue);
}
};
return opt;
};
jQuery.fn.extend({
fadeTo:function(speed,to,easing,callback){
return this.filter(isHiddenWithinTree).css("opacity",0).show()
.end().animate({opacity:to},speed,easing,callback);
},
animate:function(prop,speed,easing,callback){
var empty=jQuery.isEmptyObject(prop),
optall=jQuery.speed(speed,easing,callback),
doAnimation=function(){
var anim=Animation(this,jQuery.extend({},prop),optall);
if(empty||dataPriv.get(this,"finish")){
anim.stop(true);
}
};
doAnimation.finish=doAnimation;
return empty||optall.queue===false?
this.each(doAnimation):
this.queue(optall.queue,doAnimation);
},
stop:function(type,clearQueue,gotoEnd){
var stopQueue=function(hooks){
var stop=hooks.stop;
delete hooks.stop;
stop(gotoEnd);
};
if(typeof type!=="string"){
gotoEnd=clearQueue;
clearQueue=type;
type=undefined;
}
if(clearQueue){
this.queue(type||"fx",[]);
}
return this.each(function(){
var dequeue=true,
index=type!=null&&type+"queueHooks",
timers=jQuery.timers,
data=dataPriv.get(this);
if(index){
if(data[index]&&data[index].stop){
stopQueue(data[index]);
}
}else{
for(index in data){
if(data[index]&&data[index].stop&&rrun.test(index)){
stopQueue(data[index]);
}
}
}
for(index=timers.length;index--;){
if(timers[index].elem===this&&
(type==null||timers[index].queue===type)){
timers[index].anim.stop(gotoEnd);
dequeue=false;
timers.splice(index,1);
}
}
if(dequeue||!gotoEnd){
jQuery.dequeue(this,type);
}
});
},
finish:function(type){
if(type!==false){
type=type||"fx";
}
return this.each(function(){
var index,
data=dataPriv.get(this),
queue=data[type+"queue"],
hooks=data[type+"queueHooks"],
timers=jQuery.timers,
length=queue?queue.length:0;
data.finish=true;
jQuery.queue(this,type,[]);
if(hooks&&hooks.stop){
hooks.stop.call(this,true);
}
for(index=timers.length;index--;){
if(timers[index].elem===this&&timers[index].queue===type){
timers[index].anim.stop(true);
timers.splice(index,1);
}
}
for(index=0;index<length;index++){
if(queue[index]&&queue[index].finish){
queue[index].finish.call(this);
}
}
delete data.finish;
});
}
});
jQuery.each(["toggle","show","hide"],function(_i,name){
var cssFn=jQuery.fn[name];
jQuery.fn[name]=function(speed,easing,callback){
return speed==null||typeof speed==="boolean"?
cssFn.apply(this,arguments):
this.animate(genFx(name,true),speed,easing,callback);
};
});
jQuery.each({
slideDown:genFx("show"),
slideUp:genFx("hide"),
slideToggle:genFx("toggle"),
fadeIn:{opacity:"show"},
fadeOut:{opacity:"hide"},
fadeToggle:{opacity:"toggle"}
},function(name,props){
jQuery.fn[name]=function(speed,easing,callback){
return this.animate(props,speed,easing,callback);
};
});
jQuery.timers=[];
jQuery.fx.tick=function(){
var timer,
i=0,
timers=jQuery.timers;
fxNow=Date.now();
for(;i<timers.length;i++){
timer=timers[i];
if(!timer()&&timers[i]===timer){
timers.splice(i--,1);
}
}
if(!timers.length){
jQuery.fx.stop();
}
fxNow=undefined;
};
jQuery.fx.timer=function(timer){
jQuery.timers.push(timer);
jQuery.fx.start();
};
jQuery.fx.interval=13;
jQuery.fx.start=function(){
if(inProgress){
return;
}
inProgress=true;
schedule();
};
jQuery.fx.stop=function(){
inProgress=null;
};
jQuery.fx.speeds={
slow:600,
fast:200,
_default:400
};
jQuery.fn.delay=function(time,type){
time=jQuery.fx?jQuery.fx.speeds[time]||time:time;
type=type||"fx";
return this.queue(type,function(next,hooks){
var timeout=window.setTimeout(next,time);
hooks.stop=function(){
window.clearTimeout(timeout);
};
});
};
(function(){
var input=document.createElement("input"),
select=document.createElement("select"),
opt=select.appendChild(document.createElement("option"));
input.type="checkbox";
support.checkOn=input.value!=="";
support.optSelected=opt.selected;
input=document.createElement("input");
input.value="t";
input.type="radio";
support.radioValue=input.value==="t";
})();
var boolHook,
attrHandle=jQuery.expr.attrHandle;
jQuery.fn.extend({
attr:function(name,value){
return access(this,jQuery.attr,name,value,arguments.length>1);
},
removeAttr:function(name){
return this.each(function(){
jQuery.removeAttr(this,name);
});
}
});
jQuery.extend({
attr:function(elem,name,value){
var ret,hooks,
nType=elem.nodeType;
if(nType===3||nType===8||nType===2){
return;
}
if(typeof elem.getAttribute==="undefined"){
return jQuery.prop(elem,name,value);
}
if(nType!==1||!jQuery.isXMLDoc(elem)){
hooks=jQuery.attrHooks[name.toLowerCase()]||
(jQuery.expr.match.bool.test(name)?boolHook:undefined);
}
if(value!==undefined){
if(value===null){
jQuery.removeAttr(elem,name);
return;
}
if(hooks&&"set"in hooks&&
(ret=hooks.set(elem,value,name))!==undefined){
return ret;
}
elem.setAttribute(name,value+"");
return value;
}
if(hooks&&"get"in hooks&&(ret=hooks.get(elem,name))!==null){
return ret;
}
ret=jQuery.find.attr(elem,name);
return ret==null?undefined:ret;
},
attrHooks:{
type:{
set:function(elem,value){
if(!support.radioValue&&value==="radio"&&
nodeName(elem,"input")){
var val=elem.value;
elem.setAttribute("type",value);
if(val){
elem.value=val;
}
return value;
}
}
}
},
removeAttr:function(elem,value){
var name,
i=0,
attrNames=value&&value.match(rnothtmlwhite);
if(attrNames&&elem.nodeType===1){
while((name=attrNames[i++])){
elem.removeAttribute(name);
}
}
}
});
boolHook={
set:function(elem,value,name){
if(value===false){
jQuery.removeAttr(elem,name);
}else{
elem.setAttribute(name,name);
}
return name;
}
};
jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g),function(_i,name){
var getter=attrHandle[name]||jQuery.find.attr;
attrHandle[name]=function(elem,name,isXML){
var ret,handle,
lowercaseName=name.toLowerCase();
if(!isXML){
handle=attrHandle[lowercaseName];
attrHandle[lowercaseName]=ret;
ret=getter(elem,name,isXML)!=null?
lowercaseName:
null;
attrHandle[lowercaseName]=handle;
}
return ret;
};
});
var rfocusable=/^(?:input|select|textarea|button)$/i,
rclickable=/^(?:a|area)$/i;
jQuery.fn.extend({
prop:function(name,value){
return access(this,jQuery.prop,name,value,arguments.length>1);
},
removeProp:function(name){
return this.each(function(){
delete this[jQuery.propFix[name]||name];
});
}
});
jQuery.extend({
prop:function(elem,name,value){
var ret,hooks,
nType=elem.nodeType;
if(nType===3||nType===8||nType===2){
return;
}
if(nType!==1||!jQuery.isXMLDoc(elem)){
name=jQuery.propFix[name]||name;
hooks=jQuery.propHooks[name];
}
if(value!==undefined){
if(hooks&&"set"in hooks&&
(ret=hooks.set(elem,value,name))!==undefined){
return ret;
}
return(elem[name]=value);
}
if(hooks&&"get"in hooks&&(ret=hooks.get(elem,name))!==null){
return ret;
}
return elem[name];
},
propHooks:{
tabIndex:{
get:function(elem){
var tabindex=jQuery.find.attr(elem,"tabindex");
if(tabindex){
return parseInt(tabindex,10);
}
if(
rfocusable.test(elem.nodeName)||
rclickable.test(elem.nodeName)&&
elem.href
){
return 0;
}
return-1;
}
}
},
propFix:{
"for":"htmlFor",
"class":"className"
}
});
if(!support.optSelected){
jQuery.propHooks.selected={
get:function(elem){
var parent=elem.parentNode;
if(parent&&parent.parentNode){
parent.parentNode.selectedIndex;
}
return null;
},
set:function(elem){
var parent=elem.parentNode;
if(parent){
parent.selectedIndex;
if(parent.parentNode){
parent.parentNode.selectedIndex;
}
}
}
};
}
jQuery.each([
"tabIndex",
"readOnly",
"maxLength",
"cellSpacing",
"cellPadding",
"rowSpan",
"colSpan",
"useMap",
"frameBorder",
"contentEditable"
],function(){
jQuery.propFix[this.toLowerCase()]=this;
});
function stripAndCollapse(value){
var tokens=value.match(rnothtmlwhite)||[];
return tokens.join(" ");
}
function getClass(elem){
return elem.getAttribute&&elem.getAttribute("class")||"";
}
function classesToArray(value){
if(Array.isArray(value)){
return value;
}
if(typeof value==="string"){
return value.match(rnothtmlwhite)||[];
}
return[];
}
jQuery.fn.extend({
addClass:function(value){
var classes,elem,cur,curValue,clazz,j,finalValue,
i=0;
if(isFunction(value)){
return this.each(function(j){
jQuery(this).addClass(value.call(this,j,getClass(this)));
});
}
classes=classesToArray(value);
if(classes.length){
while((elem=this[i++])){
curValue=getClass(elem);
cur=elem.nodeType===1&&(" "+stripAndCollapse(curValue)+" ");
if(cur){
j=0;
while((clazz=classes[j++])){
if(cur.indexOf(" "+clazz+" ")<0){
cur+=clazz+" ";
}
}
finalValue=stripAndCollapse(cur);
if(curValue!==finalValue){
elem.setAttribute("class",finalValue);
}
}
}
}
return this;
},
removeClass:function(value){
var classes,elem,cur,curValue,clazz,j,finalValue,
i=0;
if(isFunction(value)){
return this.each(function(j){
jQuery(this).removeClass(value.call(this,j,getClass(this)));
});
}
if(!arguments.length){
return this.attr("class","");
}
classes=classesToArray(value);
if(classes.length){
while((elem=this[i++])){
curValue=getClass(elem);
cur=elem.nodeType===1&&(" "+stripAndCollapse(curValue)+" ");
if(cur){
j=0;
while((clazz=classes[j++])){
while(cur.indexOf(" "+clazz+" ")>-1){
cur=cur.replace(" "+clazz+" "," ");
}
}
finalValue=stripAndCollapse(cur);
if(curValue!==finalValue){
elem.setAttribute("class",finalValue);
}
}
}
}
return this;
},
toggleClass:function(value,stateVal){
var type=typeof value,
isValidValue=type==="string"||Array.isArray(value);
if(typeof stateVal==="boolean"&&isValidValue){
return stateVal?this.addClass(value):this.removeClass(value);
}
if(isFunction(value)){
return this.each(function(i){
jQuery(this).toggleClass(
value.call(this,i,getClass(this),stateVal),
stateVal
);
});
}
return this.each(function(){
var className,i,self,classNames;
if(isValidValue){
i=0;
self=jQuery(this);
classNames=classesToArray(value);
while((className=classNames[i++])){
if(self.hasClass(className)){
self.removeClass(className);
}else{
self.addClass(className);
}
}
}else if(value===undefined||type==="boolean"){
className=getClass(this);
if(className){
dataPriv.set(this,"__className__",className);
}
if(this.setAttribute){
this.setAttribute("class",
className||value===false?
"":
dataPriv.get(this,"__className__")||""
);
}
}
});
},
hasClass:function(selector){
var className,elem,
i=0;
className=" "+selector+" ";
while((elem=this[i++])){
if(elem.nodeType===1&&
(" "+stripAndCollapse(getClass(elem))+" ").indexOf(className)>-1){
return true;
}
}
return false;
}
});
var rreturn=/\r/g;
jQuery.fn.extend({
val:function(value){
var hooks,ret,valueIsFunction,
elem=this[0];
if(!arguments.length){
if(elem){
hooks=jQuery.valHooks[elem.type]||
jQuery.valHooks[elem.nodeName.toLowerCase()];
if(hooks&&
"get"in hooks&&
(ret=hooks.get(elem,"value"))!==undefined
){
return ret;
}
ret=elem.value;
if(typeof ret==="string"){
return ret.replace(rreturn,"");
}
return ret==null?"":ret;
}
return;
}
valueIsFunction=isFunction(value);
return this.each(function(i){
var val;
if(this.nodeType!==1){
return;
}
if(valueIsFunction){
val=value.call(this,i,jQuery(this).val());
}else{
val=value;
}
if(val==null){
val="";
}else if(typeof val==="number"){
val+="";
}else if(Array.isArray(val)){
val=jQuery.map(val,function(value){
return value==null?"":value+"";
});
}
hooks=jQuery.valHooks[this.type]||jQuery.valHooks[this.nodeName.toLowerCase()];
if(!hooks||!("set"in hooks)||hooks.set(this,val,"value")===undefined){
this.value=val;
}
});
}
});
jQuery.extend({
valHooks:{
option:{
get:function(elem){
var val=jQuery.find.attr(elem,"value");
return val!=null?
val:
stripAndCollapse(jQuery.text(elem));
}
},
select:{
get:function(elem){
var value,option,i,
options=elem.options,
index=elem.selectedIndex,
one=elem.type==="select-one",
values=one?null:[],
max=one?index+1:options.length;
if(index<0){
i=max;
}else{
i=one?index:0;
}
for(;i<max;i++){
option=options[i];
if((option.selected||i===index)&&
!option.disabled&&
(!option.parentNode.disabled||
!nodeName(option.parentNode,"optgroup"))){
value=jQuery(option).val();
if(one){
return value;
}
values.push(value);
}
}
return values;
},
set:function(elem,value){
var optionSet,option,
options=elem.options,
values=jQuery.makeArray(value),
i=options.length;
while(i--){
option=options[i];
if(option.selected=
jQuery.inArray(jQuery.valHooks.option.get(option),values)>-1
){
optionSet=true;
}
}
if(!optionSet){
elem.selectedIndex=-1;
}
return values;
}
}
}
});
jQuery.each(["radio","checkbox"],function(){
jQuery.valHooks[this]={
set:function(elem,value){
if(Array.isArray(value)){
return(elem.checked=jQuery.inArray(jQuery(elem).val(),value)>-1);
}
}
};
if(!support.checkOn){
jQuery.valHooks[this].get=function(elem){
return elem.getAttribute("value")===null?"on":elem.value;
};
}
});
support.focusin="onfocusin"in window;
var rfocusMorph=/^(?:focusinfocus|focusoutblur)$/,
stopPropagationCallback=function(e){
e.stopPropagation();
};
jQuery.extend(jQuery.event,{
trigger:function(event,data,elem,onlyHandlers){
var i,cur,tmp,bubbleType,ontype,handle,special,lastElement,
eventPath=[elem||document],
type=hasOwn.call(event,"type")?event.type:event,
namespaces=hasOwn.call(event,"namespace")?event.namespace.split("."):[];
cur=lastElement=tmp=elem=elem||document;
if(elem.nodeType===3||elem.nodeType===8){
return;
}
if(rfocusMorph.test(type+jQuery.event.triggered)){
return;
}
if(type.indexOf(".")>-1){
namespaces=type.split(".");
type=namespaces.shift();
namespaces.sort();
}
ontype=type.indexOf(":")<0&&"on"+type;
event=event[jQuery.expando]?
event:
new jQuery.Event(type,typeof event==="object"&&event);
event.isTrigger=onlyHandlers?2:3;
event.namespace=namespaces.join(".");
event.rnamespace=event.namespace?
new RegExp("(^|\\.)"+namespaces.join("\\.(?:.*\\.|)")+"(\\.|$)"):
null;
event.result=undefined;
if(!event.target){
event.target=elem;
}
data=data==null?
[event]:
jQuery.makeArray(data,[event]);
special=jQuery.event.special[type]||{};
if(!onlyHandlers&&special.trigger&&special.trigger.apply(elem,data)===false){
return;
}
if(!onlyHandlers&&!special.noBubble&&!isWindow(elem)){
bubbleType=special.delegateType||type;
if(!rfocusMorph.test(bubbleType+type)){
cur=cur.parentNode;
}
for(;cur;cur=cur.parentNode){
eventPath.push(cur);
tmp=cur;
}
if(tmp===(elem.ownerDocument||document)){
eventPath.push(tmp.defaultView||tmp.parentWindow||window);
}
}
i=0;
while((cur=eventPath[i++])&&!event.isPropagationStopped()){
lastElement=cur;
event.type=i>1?
bubbleType:
special.bindType||type;
handle=(dataPriv.get(cur,"events")||Object.create(null))[event.type]&&
dataPriv.get(cur,"handle");
if(handle){
handle.apply(cur,data);
}
handle=ontype&&cur[ontype];
if(handle&&handle.apply&&acceptData(cur)){
event.result=handle.apply(cur,data);
if(event.result===false){
event.preventDefault();
}
}
}
event.type=type;
if(!onlyHandlers&&!event.isDefaultPrevented()){
if((!special._default||
special._default.apply(eventPath.pop(),data)===false)&&
acceptData(elem)){
if(ontype&&isFunction(elem[type])&&!isWindow(elem)){
tmp=elem[ontype];
if(tmp){
elem[ontype]=null;
}
jQuery.event.triggered=type;
if(event.isPropagationStopped()){
lastElement.addEventListener(type,stopPropagationCallback);
}
elem[type]();
if(event.isPropagationStopped()){
lastElement.removeEventListener(type,stopPropagationCallback);
}
jQuery.event.triggered=undefined;
if(tmp){
elem[ontype]=tmp;
}
}
}
}
return event.result;
},
simulate:function(type,elem,event){
var e=jQuery.extend(
new jQuery.Event(),
event,
{
type:type,
isSimulated:true
}
);
jQuery.event.trigger(e,null,elem);
}
});
jQuery.fn.extend({
trigger:function(type,data){
return this.each(function(){
jQuery.event.trigger(type,data,this);
});
},
triggerHandler:function(type,data){
var elem=this[0];
if(elem){
return jQuery.event.trigger(type,data,elem,true);
}
}
});
if(!support.focusin){
jQuery.each({focus:"focusin",blur:"focusout"},function(orig,fix){
var handler=function(event){
jQuery.event.simulate(fix,event.target,jQuery.event.fix(event));
};
jQuery.event.special[fix]={
setup:function(){
var doc=this.ownerDocument||this.document||this,
attaches=dataPriv.access(doc,fix);
if(!attaches){
doc.addEventListener(orig,handler,true);
}
dataPriv.access(doc,fix,(attaches||0)+1);
},
teardown:function(){
var doc=this.ownerDocument||this.document||this,
attaches=dataPriv.access(doc,fix)-1;
if(!attaches){
doc.removeEventListener(orig,handler,true);
dataPriv.remove(doc,fix);
}else{
dataPriv.access(doc,fix,attaches);
}
}
};
});
}
var location=window.location;
var nonce={guid:Date.now()};
var rquery=(/\?/);
jQuery.parseXML=function(data){
var xml,parserErrorElem;
if(!data||typeof data!=="string"){
return null;
}
try{
xml=(new window.DOMParser()).parseFromString(data,"text/xml");
}catch(e){}
parserErrorElem=xml&&xml.getElementsByTagName("parsererror")[0];
if(!xml||parserErrorElem){
jQuery.error("Invalid XML: "+(
parserErrorElem?
jQuery.map(parserErrorElem.childNodes,function(el){
return el.textContent;
}).join("\n"):
data
));
}
return xml;
};
var
rbracket=/\[\]$/,
rCRLF=/\r?\n/g,
rsubmitterTypes=/^(?:submit|button|image|reset|file)$/i,
rsubmittable=/^(?:input|select|textarea|keygen)/i;
function buildParams(prefix,obj,traditional,add){
var name;
if(Array.isArray(obj)){
jQuery.each(obj,function(i,v){
if(traditional||rbracket.test(prefix)){
add(prefix,v);
}else{
buildParams(
prefix+"["+(typeof v==="object"&&v!=null?i:"")+"]",
v,
traditional,
add
);
}
});
}else if(!traditional&&toType(obj)==="object"){
for(name in obj){
buildParams(prefix+"["+name+"]",obj[name],traditional,add);
}
}else{
add(prefix,obj);
}
}
jQuery.param=function(a,traditional){
var prefix,
s=[],
add=function(key,valueOrFunction){
var value=isFunction(valueOrFunction)?
valueOrFunction():
valueOrFunction;
s[s.length]=encodeURIComponent(key)+"="+
encodeURIComponent(value==null?"":value);
};
if(a==null){
return"";
}
if(Array.isArray(a)||(a.jquery&&!jQuery.isPlainObject(a))){
jQuery.each(a,function(){
add(this.name,this.value);
});
}else{
for(prefix in a){
buildParams(prefix,a[prefix],traditional,add);
}
}
return s.join("&");
};
jQuery.fn.extend({
serialize:function(){
return jQuery.param(this.serializeArray());
},
serializeArray:function(){
return this.map(function(){
var elements=jQuery.prop(this,"elements");
return elements?jQuery.makeArray(elements):this;
}).filter(function(){
var type=this.type;
return this.name&&!jQuery(this).is(":disabled")&&
rsubmittable.test(this.nodeName)&&!rsubmitterTypes.test(type)&&
(this.checked||!rcheckableType.test(type));
}).map(function(_i,elem){
var val=jQuery(this).val();
if(val==null){
return null;
}
if(Array.isArray(val)){
return jQuery.map(val,function(val){
return{name:elem.name,value:val.replace(rCRLF,"\r\n")};
});
}
return{name:elem.name,value:val.replace(rCRLF,"\r\n")};
}).get();
}
});
var
r20=/%20/g,
rhash=/#.*$/,
rantiCache=/([?&])_=[^&]*/,
rheaders=/^(.*?):[ \t]*([^\r\n]*)$/mg,
rlocalProtocol=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
rnoContent=/^(?:GET|HEAD)$/,
rprotocol=/^\/\//,
prefilters={},
transports={},
allTypes="*/".concat("*"),
originAnchor=document.createElement("a");
originAnchor.href=location.href;
function addToPrefiltersOrTransports(structure){
return function(dataTypeExpression,func){
if(typeof dataTypeExpression!=="string"){
func=dataTypeExpression;
dataTypeExpression="*";
}
var dataType,
i=0,
dataTypes=dataTypeExpression.toLowerCase().match(rnothtmlwhite)||[];
if(isFunction(func)){
while((dataType=dataTypes[i++])){
if(dataType[0]==="+"){
dataType=dataType.slice(1)||"*";
(structure[dataType]=structure[dataType]||[]).unshift(func);
}else{
(structure[dataType]=structure[dataType]||[]).push(func);
}
}
}
};
}
function inspectPrefiltersOrTransports(structure,options,originalOptions,jqXHR){
var inspected={},
seekingTransport=(structure===transports);
function inspect(dataType){
var selected;
inspected[dataType]=true;
jQuery.each(structure[dataType]||[],function(_,prefilterOrFactory){
var dataTypeOrTransport=prefilterOrFactory(options,originalOptions,jqXHR);
if(typeof dataTypeOrTransport==="string"&&
!seekingTransport&&!inspected[dataTypeOrTransport]){
options.dataTypes.unshift(dataTypeOrTransport);
inspect(dataTypeOrTransport);
return false;
}else if(seekingTransport){
return!(selected=dataTypeOrTransport);
}
});
return selected;
}
return inspect(options.dataTypes[0])||!inspected["*"]&&inspect("*");
}
function ajaxExtend(target,src){
var key,deep,
flatOptions=jQuery.ajaxSettings.flatOptions||{};
for(key in src){
if(src[key]!==undefined){
(flatOptions[key]?target:(deep||(deep={})))[key]=src[key];
}
}
if(deep){
jQuery.extend(true,target,deep);
}
return target;
}
function ajaxHandleResponses(s,jqXHR,responses){
var ct,type,finalDataType,firstDataType,
contents=s.contents,
dataTypes=s.dataTypes;
while(dataTypes[0]==="*"){
dataTypes.shift();
if(ct===undefined){
ct=s.mimeType||jqXHR.getResponseHeader("Content-Type");
}
}
if(ct){
for(type in contents){
if(contents[type]&&contents[type].test(ct)){
dataTypes.unshift(type);
break;
}
}
}
if(dataTypes[0]in responses){
finalDataType=dataTypes[0];
}else{
for(type in responses){
if(!dataTypes[0]||s.converters[type+" "+dataTypes[0]]){
finalDataType=type;
break;
}
if(!firstDataType){
firstDataType=type;
}
}
finalDataType=finalDataType||firstDataType;
}
if(finalDataType){
if(finalDataType!==dataTypes[0]){
dataTypes.unshift(finalDataType);
}
return responses[finalDataType];
}
}
function ajaxConvert(s,response,jqXHR,isSuccess){
var conv2,current,conv,tmp,prev,
converters={},
dataTypes=s.dataTypes.slice();
if(dataTypes[1]){
for(conv in s.converters){
converters[conv.toLowerCase()]=s.converters[conv];
}
}
current=dataTypes.shift();
while(current){
if(s.responseFields[current]){
jqXHR[s.responseFields[current]]=response;
}
if(!prev&&isSuccess&&s.dataFilter){
response=s.dataFilter(response,s.dataType);
}
prev=current;
current=dataTypes.shift();
if(current){
if(current==="*"){
current=prev;
}else if(prev!=="*"&&prev!==current){
conv=converters[prev+" "+current]||converters["* "+current];
if(!conv){
for(conv2 in converters){
tmp=conv2.split(" ");
if(tmp[1]===current){
conv=converters[prev+" "+tmp[0]]||
converters["* "+tmp[0]];
if(conv){
if(conv===true){
conv=converters[conv2];
}else if(converters[conv2]!==true){
current=tmp[0];
dataTypes.unshift(tmp[1]);
}
break;
}
}
}
}
if(conv!==true){
if(conv&&s.throws){
response=conv(response);
}else{
try{
response=conv(response);
}catch(e){
return{
state:"parsererror",
error:conv?e:"No conversion from "+prev+" to "+current
};
}
}
}
}
}
}
return{state:"success",data:response};
}
jQuery.extend({
active:0,
lastModified:{},
etag:{},
ajaxSettings:{
url:location.href,
type:"GET",
isLocal:rlocalProtocol.test(location.protocol),
global:true,
processData:true,
async:true,
contentType:"application/x-www-form-urlencoded; charset=UTF-8",
accepts:{
"*":allTypes,
text:"text/plain",
html:"text/html",
xml:"application/xml, text/xml",
json:"application/json, text/javascript"
},
contents:{
xml:/\bxml\b/,
html:/\bhtml/,
json:/\bjson\b/
},
responseFields:{
xml:"responseXML",
text:"responseText",
json:"responseJSON"
},
converters:{
"* text":String,
"text html":true,
"text json":JSON.parse,
"text xml":jQuery.parseXML
},
flatOptions:{
url:true,
context:true
}
},
ajaxSetup:function(target,settings){
return settings?
ajaxExtend(ajaxExtend(target,jQuery.ajaxSettings),settings):
ajaxExtend(jQuery.ajaxSettings,target);
},
ajaxPrefilter:addToPrefiltersOrTransports(prefilters),
ajaxTransport:addToPrefiltersOrTransports(transports),
ajax:function(url,options){
if(typeof url==="object"){
options=url;
url=undefined;
}
options=options||{};
var transport,
cacheURL,
responseHeadersString,
responseHeaders,
timeoutTimer,
urlAnchor,
completed,
fireGlobals,
i,
uncached,
s=jQuery.ajaxSetup({},options),
callbackContext=s.context||s,
globalEventContext=s.context&&
(callbackContext.nodeType||callbackContext.jquery)?
jQuery(callbackContext):
jQuery.event,
deferred=jQuery.Deferred(),
completeDeferred=jQuery.Callbacks("once memory"),
statusCode=s.statusCode||{},
requestHeaders={},
requestHeadersNames={},
strAbort="canceled",
jqXHR={
readyState:0,
getResponseHeader:function(key){
var match;
if(completed){
if(!responseHeaders){
responseHeaders={};
while((match=rheaders.exec(responseHeadersString))){
responseHeaders[match[1].toLowerCase()+" "]=
(responseHeaders[match[1].toLowerCase()+" "]||[])
.concat(match[2]);
}
}
match=responseHeaders[key.toLowerCase()+" "];
}
return match==null?null:match.join(", ");
},
getAllResponseHeaders:function(){
return completed?responseHeadersString:null;
},
setRequestHeader:function(name,value){
if(completed==null){
name=requestHeadersNames[name.toLowerCase()]=
requestHeadersNames[name.toLowerCase()]||name;
requestHeaders[name]=value;
}
return this;
},
overrideMimeType:function(type){
if(completed==null){
s.mimeType=type;
}
return this;
},
statusCode:function(map){
var code;
if(map){
if(completed){
jqXHR.always(map[jqXHR.status]);
}else{
for(code in map){
statusCode[code]=[statusCode[code],map[code]];
}
}
}
return this;
},
abort:function(statusText){
var finalText=statusText||strAbort;
if(transport){
transport.abort(finalText);
}
done(0,finalText);
return this;
}
};
deferred.promise(jqXHR);
s.url=((url||s.url||location.href)+"")
.replace(rprotocol,location.protocol+"//");
s.type=options.method||options.type||s.method||s.type;
s.dataTypes=(s.dataType||"*").toLowerCase().match(rnothtmlwhite)||[""];
if(s.crossDomain==null){
urlAnchor=document.createElement("a");
try{
urlAnchor.href=s.url;
urlAnchor.href=urlAnchor.href;
s.crossDomain=originAnchor.protocol+"//"+originAnchor.host!==
urlAnchor.protocol+"//"+urlAnchor.host;
}catch(e){
s.crossDomain=true;
}
}
if(s.data&&s.processData&&typeof s.data!=="string"){
s.data=jQuery.param(s.data,s.traditional);
}
inspectPrefiltersOrTransports(prefilters,s,options,jqXHR);
if(completed){
return jqXHR;
}
fireGlobals=jQuery.event&&s.global;
if(fireGlobals&&jQuery.active++===0){
jQuery.event.trigger("ajaxStart");
}
s.type=s.type.toUpperCase();
s.hasContent=!rnoContent.test(s.type);
cacheURL=s.url.replace(rhash,"");
if(!s.hasContent){
uncached=s.url.slice(cacheURL.length);
if(s.data&&(s.processData||typeof s.data==="string")){
cacheURL+=(rquery.test(cacheURL)?"&":"?")+s.data;
delete s.data;
}
if(s.cache===false){
cacheURL=cacheURL.replace(rantiCache,"$1");
uncached=(rquery.test(cacheURL)?"&":"?")+"_="+(nonce.guid++)+
uncached;
}
s.url=cacheURL+uncached;
}else if(s.data&&s.processData&&
(s.contentType||"").indexOf("application/x-www-form-urlencoded")===0){
s.data=s.data.replace(r20,"+");
}
if(s.ifModified){
if(jQuery.lastModified[cacheURL]){
jqXHR.setRequestHeader("If-Modified-Since",jQuery.lastModified[cacheURL]);
}
if(jQuery.etag[cacheURL]){
jqXHR.setRequestHeader("If-None-Match",jQuery.etag[cacheURL]);
}
}
if(s.data&&s.hasContent&&s.contentType!==false||options.contentType){
jqXHR.setRequestHeader("Content-Type",s.contentType);
}
jqXHR.setRequestHeader(
"Accept",
s.dataTypes[0]&&s.accepts[s.dataTypes[0]]?
s.accepts[s.dataTypes[0]]+
(s.dataTypes[0]!=="*"?", "+allTypes+"; q=0.01":""):
s.accepts["*"]
);
for(i in s.headers){
jqXHR.setRequestHeader(i,s.headers[i]);
}
if(s.beforeSend&&
(s.beforeSend.call(callbackContext,jqXHR,s)===false||completed)){
return jqXHR.abort();
}
strAbort="abort";
completeDeferred.add(s.complete);
jqXHR.done(s.success);
jqXHR.fail(s.error);
transport=inspectPrefiltersOrTransports(transports,s,options,jqXHR);
if(!transport){
done(-1,"No Transport");
}else{
jqXHR.readyState=1;
if(fireGlobals){
globalEventContext.trigger("ajaxSend",[jqXHR,s]);
}
if(completed){
return jqXHR;
}
if(s.async&&s.timeout>0){
timeoutTimer=window.setTimeout(function(){
jqXHR.abort("timeout");
},s.timeout);
}
try{
completed=false;
transport.send(requestHeaders,done);
}catch(e){
if(completed){
throw e;
}
done(-1,e);
}
}
function done(status,nativeStatusText,responses,headers){
var isSuccess,success,error,response,modified,
statusText=nativeStatusText;
if(completed){
return;
}
completed=true;
if(timeoutTimer){
window.clearTimeout(timeoutTimer);
}
transport=undefined;
responseHeadersString=headers||"";
jqXHR.readyState=status>0?4:0;
isSuccess=status>=200&&status<300||status===304;
if(responses){
response=ajaxHandleResponses(s,jqXHR,responses);
}
if(!isSuccess&&
jQuery.inArray("script",s.dataTypes)>-1&&
jQuery.inArray("json",s.dataTypes)<0){
s.converters["text script"]=function(){};
}
response=ajaxConvert(s,response,jqXHR,isSuccess);
if(isSuccess){
if(s.ifModified){
modified=jqXHR.getResponseHeader("Last-Modified");
if(modified){
jQuery.lastModified[cacheURL]=modified;
}
modified=jqXHR.getResponseHeader("etag");
if(modified){
jQuery.etag[cacheURL]=modified;
}
}
if(status===204||s.type==="HEAD"){
statusText="nocontent";
}else if(status===304){
statusText="notmodified";
}else{
statusText=response.state;
success=response.data;
error=response.error;
isSuccess=!error;
}
}else{
error=statusText;
if(status||!statusText){
statusText="error";
if(status<0){
status=0;
}
}
}
jqXHR.status=status;
jqXHR.statusText=(nativeStatusText||statusText)+"";
if(isSuccess){
deferred.resolveWith(callbackContext,[success,statusText,jqXHR]);
}else{
deferred.rejectWith(callbackContext,[jqXHR,statusText,error]);
}
jqXHR.statusCode(statusCode);
statusCode=undefined;
if(fireGlobals){
globalEventContext.trigger(isSuccess?"ajaxSuccess":"ajaxError",
[jqXHR,s,isSuccess?success:error]);
}
completeDeferred.fireWith(callbackContext,[jqXHR,statusText]);
if(fireGlobals){
globalEventContext.trigger("ajaxComplete",[jqXHR,s]);
if(!(--jQuery.active)){
jQuery.event.trigger("ajaxStop");
}
}
}
return jqXHR;
},
getJSON:function(url,data,callback){
return jQuery.get(url,data,callback,"json");
},
getScript:function(url,callback){
return jQuery.get(url,undefined,callback,"script");
}
});
jQuery.each(["get","post"],function(_i,method){
jQuery[method]=function(url,data,callback,type){
if(isFunction(data)){
type=type||callback;
callback=data;
data=undefined;
}
return jQuery.ajax(jQuery.extend({
url:url,
type:method,
dataType:type,
data:data,
success:callback
},jQuery.isPlainObject(url)&&url));
};
});
jQuery.ajaxPrefilter(function(s){
var i;
for(i in s.headers){
if(i.toLowerCase()==="content-type"){
s.contentType=s.headers[i]||"";
}
}
});
jQuery._evalUrl=function(url,options,doc){
return jQuery.ajax({
url:url,
type:"GET",
dataType:"script",
cache:true,
async:false,
global:false,
converters:{
"text script":function(){}
},
dataFilter:function(response){
jQuery.globalEval(response,options,doc);
}
});
};
jQuery.fn.extend({
wrapAll:function(html){
var wrap;
if(this[0]){
if(isFunction(html)){
html=html.call(this[0]);
}
wrap=jQuery(html,this[0].ownerDocument).eq(0).clone(true);
if(this[0].parentNode){
wrap.insertBefore(this[0]);
}
wrap.map(function(){
var elem=this;
while(elem.firstElementChild){
elem=elem.firstElementChild;
}
return elem;
}).append(this);
}
return this;
},
wrapInner:function(html){
if(isFunction(html)){
return this.each(function(i){
jQuery(this).wrapInner(html.call(this,i));
});
}
return this.each(function(){
var self=jQuery(this),
contents=self.contents();
if(contents.length){
contents.wrapAll(html);
}else{
self.append(html);
}
});
},
wrap:function(html){
var htmlIsFunction=isFunction(html);
return this.each(function(i){
jQuery(this).wrapAll(htmlIsFunction?html.call(this,i):html);
});
},
unwrap:function(selector){
this.parent(selector).not("body").each(function(){
jQuery(this).replaceWith(this.childNodes);
});
return this;
}
});
jQuery.expr.pseudos.hidden=function(elem){
return!jQuery.expr.pseudos.visible(elem);
};
jQuery.expr.pseudos.visible=function(elem){
return!!(elem.offsetWidth||elem.offsetHeight||elem.getClientRects().length);
};
jQuery.ajaxSettings.xhr=function(){
try{
return new window.XMLHttpRequest();
}catch(e){}
};
var xhrSuccessStatus={
0:200,
1223:204
},
xhrSupported=jQuery.ajaxSettings.xhr();
support.cors=!!xhrSupported&&("withCredentials"in xhrSupported);
support.ajax=xhrSupported=!!xhrSupported;
jQuery.ajaxTransport(function(options){
var callback,errorCallback;
if(support.cors||xhrSupported&&!options.crossDomain){
return{
send:function(headers,complete){
var i,
xhr=options.xhr();
xhr.open(
options.type,
options.url,
options.async,
options.username,
options.password
);
if(options.xhrFields){
for(i in options.xhrFields){
xhr[i]=options.xhrFields[i];
}
}
if(options.mimeType&&xhr.overrideMimeType){
xhr.overrideMimeType(options.mimeType);
}
if(!options.crossDomain&&!headers["X-Requested-With"]){
headers["X-Requested-With"]="XMLHttpRequest";
}
for(i in headers){
xhr.setRequestHeader(i,headers[i]);
}
callback=function(type){
return function(){
if(callback){
callback=errorCallback=xhr.onload=
xhr.onerror=xhr.onabort=xhr.ontimeout=
xhr.onreadystatechange=null;
if(type==="abort"){
xhr.abort();
}else if(type==="error"){
if(typeof xhr.status!=="number"){
complete(0,"error");
}else{
complete(
xhr.status,
xhr.statusText
);
}
}else{
complete(
xhrSuccessStatus[xhr.status]||xhr.status,
xhr.statusText,
(xhr.responseType||"text")!=="text"||
typeof xhr.responseText!=="string"?
{binary:xhr.response}:
{text:xhr.responseText},
xhr.getAllResponseHeaders()
);
}
}
};
};
xhr.onload=callback();
errorCallback=xhr.onerror=xhr.ontimeout=callback("error");
if(xhr.onabort!==undefined){
xhr.onabort=errorCallback;
}else{
xhr.onreadystatechange=function(){
if(xhr.readyState===4){
window.setTimeout(function(){
if(callback){
errorCallback();
}
});
}
};
}
callback=callback("abort");
try{
xhr.send(options.hasContent&&options.data||null);
}catch(e){
if(callback){
throw e;
}
}
},
abort:function(){
if(callback){
callback();
}
}
};
}
});
jQuery.ajaxPrefilter(function(s){
if(s.crossDomain){
s.contents.script=false;
}
});
jQuery.ajaxSetup({
accepts:{
script:"text/javascript, application/javascript, "+
"application/ecmascript, application/x-ecmascript"
},
contents:{
script:/\b(?:java|ecma)script\b/
},
converters:{
"text script":function(text){
jQuery.globalEval(text);
return text;
}
}
});
jQuery.ajaxPrefilter("script",function(s){
if(s.cache===undefined){
s.cache=false;
}
if(s.crossDomain){
s.type="GET";
}
});
jQuery.ajaxTransport("script",function(s){
if(s.crossDomain||s.scriptAttrs){
var script,callback;
return{
send:function(_,complete){
script=jQuery("<script>")
.attr(s.scriptAttrs||{})
.prop({charset:s.scriptCharset,src:s.url})
.on("load error",callback=function(evt){
script.remove();
callback=null;
if(evt){
complete(evt.type==="error"?404:200,evt.type);
}
});
document.head.appendChild(script[0]);
},
abort:function(){
if(callback){
callback();
}
}
};
}
});
var oldCallbacks=[],
rjsonp=/(=)\?(?=&|$)|\?\?/;
jQuery.ajaxSetup({
jsonp:"callback",
jsonpCallback:function(){
var callback=oldCallbacks.pop()||(jQuery.expando+"_"+(nonce.guid++));
this[callback]=true;
return callback;
}
});
jQuery.ajaxPrefilter("json jsonp",function(s,originalSettings,jqXHR){
var callbackName,overwritten,responseContainer,
jsonProp=s.jsonp!==false&&(rjsonp.test(s.url)?
"url":
typeof s.data==="string"&&
(s.contentType||"")
.indexOf("application/x-www-form-urlencoded")===0&&
rjsonp.test(s.data)&&"data"
);
if(jsonProp||s.dataTypes[0]==="jsonp"){
callbackName=s.jsonpCallback=isFunction(s.jsonpCallback)?
s.jsonpCallback():
s.jsonpCallback;
if(jsonProp){
s[jsonProp]=s[jsonProp].replace(rjsonp,"$1"+callbackName);
}else if(s.jsonp!==false){
s.url+=(rquery.test(s.url)?"&":"?")+s.jsonp+"="+callbackName;
}
s.converters["script json"]=function(){
if(!responseContainer){
jQuery.error(callbackName+" was not called");
}
return responseContainer[0];
};
s.dataTypes[0]="json";
overwritten=window[callbackName];
window[callbackName]=function(){
responseContainer=arguments;
};
jqXHR.always(function(){
if(overwritten===undefined){
jQuery(window).removeProp(callbackName);
}else{
window[callbackName]=overwritten;
}
if(s[callbackName]){
s.jsonpCallback=originalSettings.jsonpCallback;
oldCallbacks.push(callbackName);
}
if(responseContainer&&isFunction(overwritten)){
overwritten(responseContainer[0]);
}
responseContainer=overwritten=undefined;
});
return"script";
}
});
support.createHTMLDocument=(function(){
var body=document.implementation.createHTMLDocument("").body;
body.innerHTML="<form></form><form></form>";
return body.childNodes.length===2;
})();
jQuery.parseHTML=function(data,context,keepScripts){
if(typeof data!=="string"){
return[];
}
if(typeof context==="boolean"){
keepScripts=context;
context=false;
}
var base,parsed,scripts;
if(!context){
if(support.createHTMLDocument){
context=document.implementation.createHTMLDocument("");
base=context.createElement("base");
base.href=document.location.href;
context.head.appendChild(base);
}else{
context=document;
}
}
parsed=rsingleTag.exec(data);
scripts=!keepScripts&&[];
if(parsed){
return[context.createElement(parsed[1])];
}
parsed=buildFragment([data],context,scripts);
if(scripts&&scripts.length){
jQuery(scripts).remove();
}
return jQuery.merge([],parsed.childNodes);
};
jQuery.fn.load=function(url,params,callback){
var selector,type,response,
self=this,
off=url.indexOf(" ");
if(off>-1){
selector=stripAndCollapse(url.slice(off));
url=url.slice(0,off);
}
if(isFunction(params)){
callback=params;
params=undefined;
}else if(params&&typeof params==="object"){
type="POST";
}
if(self.length>0){
jQuery.ajax({
url:url,
type:type||"GET",
dataType:"html",
data:params
}).done(function(responseText){
response=arguments;
self.html(selector?
jQuery("<div>").append(jQuery.parseHTML(responseText)).find(selector):
responseText);
}).always(callback&&function(jqXHR,status){
self.each(function(){
callback.apply(this,response||[jqXHR.responseText,status,jqXHR]);
});
});
}
return this;
};
jQuery.expr.pseudos.animated=function(elem){
return jQuery.grep(jQuery.timers,function(fn){
return elem===fn.elem;
}).length;
};
jQuery.offset={
setOffset:function(elem,options,i){
var curPosition,curLeft,curCSSTop,curTop,curOffset,curCSSLeft,calculatePosition,
position=jQuery.css(elem,"position"),
curElem=jQuery(elem),
props={};
if(position==="static"){
elem.style.position="relative";
}
curOffset=curElem.offset();
curCSSTop=jQuery.css(elem,"top");
curCSSLeft=jQuery.css(elem,"left");
calculatePosition=(position==="absolute"||position==="fixed")&&
(curCSSTop+curCSSLeft).indexOf("auto")>-1;
if(calculatePosition){
curPosition=curElem.position();
curTop=curPosition.top;
curLeft=curPosition.left;
}else{
curTop=parseFloat(curCSSTop)||0;
curLeft=parseFloat(curCSSLeft)||0;
}
if(isFunction(options)){
options=options.call(elem,i,jQuery.extend({},curOffset));
}
if(options.top!=null){
props.top=(options.top-curOffset.top)+curTop;
}
if(options.left!=null){
props.left=(options.left-curOffset.left)+curLeft;
}
if("using"in options){
options.using.call(elem,props);
}else{
curElem.css(props);
}
}
};
jQuery.fn.extend({
offset:function(options){
if(arguments.length){
return options===undefined?
this:
this.each(function(i){
jQuery.offset.setOffset(this,options,i);
});
}
var rect,win,
elem=this[0];
if(!elem){
return;
}
if(!elem.getClientRects().length){
return{top:0,left:0};
}
rect=elem.getBoundingClientRect();
win=elem.ownerDocument.defaultView;
return{
top:rect.top+win.pageYOffset,
left:rect.left+win.pageXOffset
};
},
position:function(){
if(!this[0]){
return;
}
var offsetParent,offset,doc,
elem=this[0],
parentOffset={top:0,left:0};
if(jQuery.css(elem,"position")==="fixed"){
offset=elem.getBoundingClientRect();
}else{
offset=this.offset();
doc=elem.ownerDocument;
offsetParent=elem.offsetParent||doc.documentElement;
while(offsetParent&&
(offsetParent===doc.body||offsetParent===doc.documentElement)&&
jQuery.css(offsetParent,"position")==="static"){
offsetParent=offsetParent.parentNode;
}
if(offsetParent&&offsetParent!==elem&&offsetParent.nodeType===1){
parentOffset=jQuery(offsetParent).offset();
parentOffset.top+=jQuery.css(offsetParent,"borderTopWidth",true);
parentOffset.left+=jQuery.css(offsetParent,"borderLeftWidth",true);
}
}
return{
top:offset.top-parentOffset.top-jQuery.css(elem,"marginTop",true),
left:offset.left-parentOffset.left-jQuery.css(elem,"marginLeft",true)
};
},
offsetParent:function(){
return this.map(function(){
var offsetParent=this.offsetParent;
while(offsetParent&&jQuery.css(offsetParent,"position")==="static"){
offsetParent=offsetParent.offsetParent;
}
return offsetParent||documentElement;
});
}
});
jQuery.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(method,prop){
var top="pageYOffset"===prop;
jQuery.fn[method]=function(val){
return access(this,function(elem,method,val){
var win;
if(isWindow(elem)){
win=elem;
}else if(elem.nodeType===9){
win=elem.defaultView;
}
if(val===undefined){
return win?win[prop]:elem[method];
}
if(win){
win.scrollTo(
!top?val:win.pageXOffset,
top?val:win.pageYOffset
);
}else{
elem[method]=val;
}
},method,val,arguments.length);
};
});
jQuery.each(["top","left"],function(_i,prop){
jQuery.cssHooks[prop]=addGetHookIf(support.pixelPosition,
function(elem,computed){
if(computed){
computed=curCSS(elem,prop);
return rnumnonpx.test(computed)?
jQuery(elem).position()[prop]+"px":
computed;
}
}
);
});
jQuery.each({Height:"height",Width:"width"},function(name,type){
jQuery.each({
padding:"inner"+name,
content:type,
"":"outer"+name
},function(defaultExtra,funcName){
jQuery.fn[funcName]=function(margin,value){
var chainable=arguments.length&&(defaultExtra||typeof margin!=="boolean"),
extra=defaultExtra||(margin===true||value===true?"margin":"border");
return access(this,function(elem,type,value){
var doc;
if(isWindow(elem)){
return funcName.indexOf("outer")===0?
elem["inner"+name]:
elem.document.documentElement["client"+name];
}
if(elem.nodeType===9){
doc=elem.documentElement;
return Math.max(
elem.body["scroll"+name],doc["scroll"+name],
elem.body["offset"+name],doc["offset"+name],
doc["client"+name]
);
}
return value===undefined?
jQuery.css(elem,type,extra):
jQuery.style(elem,type,value,extra);
},type,chainable?margin:undefined,chainable);
};
});
});
jQuery.each([
"ajaxStart",
"ajaxStop",
"ajaxComplete",
"ajaxError",
"ajaxSuccess",
"ajaxSend"
],function(_i,type){
jQuery.fn[type]=function(fn){
return this.on(type,fn);
};
});
jQuery.fn.extend({
bind:function(types,data,fn){
return this.on(types,null,data,fn);
},
unbind:function(types,fn){
return this.off(types,null,fn);
},
delegate:function(selector,types,data,fn){
return this.on(types,selector,data,fn);
},
undelegate:function(selector,types,fn){
return arguments.length===1?
this.off(selector,"**"):
this.off(types,selector||"**",fn);
},
hover:function(fnOver,fnOut){
return this.mouseenter(fnOver).mouseleave(fnOut||fnOver);
}
});
jQuery.each(
("blur focus focusin focusout resize scroll click dblclick "+
"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave "+
"change select submit keydown keypress keyup contextmenu").split(" "),
function(_i,name){
jQuery.fn[name]=function(data,fn){
return arguments.length>0?
this.on(name,null,data,fn):
this.trigger(name);
};
}
);
var rtrim=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
jQuery.proxy=function(fn,context){
var tmp,args,proxy;
if(typeof context==="string"){
tmp=fn[context];
context=fn;
fn=tmp;
}
if(!isFunction(fn)){
return undefined;
}
args=slice.call(arguments,2);
proxy=function(){
return fn.apply(context||this,args.concat(slice.call(arguments)));
};
proxy.guid=fn.guid=fn.guid||jQuery.guid++;
return proxy;
};
jQuery.holdReady=function(hold){
if(hold){
jQuery.readyWait++;
}else{
jQuery.ready(true);
}
};
jQuery.isArray=Array.isArray;
jQuery.parseJSON=JSON.parse;
jQuery.nodeName=nodeName;
jQuery.isFunction=isFunction;
jQuery.isWindow=isWindow;
jQuery.camelCase=camelCase;
jQuery.type=toType;
jQuery.now=Date.now;
jQuery.isNumeric=function(obj){
var type=jQuery.type(obj);
return(type==="number"||type==="string")&&
!isNaN(obj-parseFloat(obj));
};
jQuery.trim=function(text){
return text==null?
"":
(text+"").replace(rtrim,"");
};
if(typeof define==="function"&&define.amd){
define("jquery",[],function(){
return jQuery;
});
}
var
_jQuery=window.jQuery,
_$=window.$;
jQuery.noConflict=function(deep){
if(window.$===jQuery){
window.$=_$;
}
if(deep&&window.jQuery===jQuery){
window.jQuery=_jQuery;
}
return jQuery;
};
if(typeof noGlobal==="undefined"){
window.jQuery=window.$=jQuery;
}
return jQuery;
});


/* prive/javascript/jquery.form.js?1649159400 */

(function(factory){
if(typeof define==='function'&&define.amd){
define(['jquery'],factory);
}else if(typeof module==='object'&&module.exports){
module.exports=function(root,jQuery){
if(typeof jQuery==='undefined'){
if(typeof window!=='undefined'){
jQuery=require('jquery');
}
else{
jQuery=require('jquery')(root);
}
}
factory(jQuery);
return jQuery;
};
}else{
factory(jQuery);
}
}(function($){
'use strict';
var rCRLF=/\r?\n/g;
var feature={};
feature.fileapi=$('<input type="file">').get(0).files!==undefined;
feature.formdata=(typeof window.FormData!=='undefined');
var hasProp=!!$.fn.prop;
$.fn.attr2=function(){
if(!hasProp){
return this.attr.apply(this,arguments);
}
var val=this.prop.apply(this,arguments);
if((val&&val.jquery)||typeof val==='string'){
return val;
}
return this.attr.apply(this,arguments);
};
$.fn.ajaxSubmit=function(options,data,dataType,onSuccess){
if(!this.length){
log('ajaxSubmit: skipping submit process - no element selected');
return this;
}
var method,action,url,$form=this;
if(typeof options==='function'){
options={success:options};
}else if(typeof options==='string'||(options===false&&arguments.length>0)){
options={
'url':options,
'data':data,
'dataType':dataType
};
if(typeof onSuccess==='function'){
options.success=onSuccess;
}
}else if(typeof options==='undefined'){
options={};
}
method=options.method||options.type||this.attr2('method');
action=options.url||this.attr2('action');
url=(typeof action==='string')?action.trim():'';
url=url||window.location.href||'';
if(url){
url=(url.match(/^([^#]+)/)||[])[1];
}
options=$.extend(true,{
url:url,
success:$.ajaxSettings.success,
type:method||$.ajaxSettings.type,
iframeSrc:/^https/i.test(window.location.href||'')?'javascript:false':'about:blank'
},options);
var veto={};
this.trigger('form-pre-serialize',[this,options,veto]);
if(veto.veto){
log('ajaxSubmit: submit vetoed via form-pre-serialize trigger');
return this;
}
if(options.beforeSerialize&&options.beforeSerialize(this,options)===false){
log('ajaxSubmit: submit aborted via beforeSerialize callback');
return this;
}
var traditional=options.traditional;
if(typeof traditional==='undefined'){
traditional=$.ajaxSettings.traditional;
}
var elements=[];
var qx,a=this.formToArray(options.semantic,elements,options.filtering);
if(options.data){
var optionsData=$.isFunction(options.data)?options.data(a):options.data;
options.extraData=optionsData;
qx=$.param(optionsData,traditional);
}
if(options.beforeSubmit&&options.beforeSubmit(a,this,options)===false){
log('ajaxSubmit: submit aborted via beforeSubmit callback');
return this;
}
this.trigger('form-submit-validate',[a,this,options,veto]);
if(veto.veto){
log('ajaxSubmit: submit vetoed via form-submit-validate trigger');
return this;
}
var q=$.param(a,traditional);
if(qx){
q=(q?(q+'&'+qx):qx);
}
if(options.type.toUpperCase()==='GET'){
options.url+=(options.url.indexOf('?')>=0?'&':'?')+q;
options.data=null;
}else{
options.data=q;
}
var callbacks=[];
if(options.resetForm){
callbacks.push(function(){
$form.resetForm();
});
}
if(options.clearForm){
callbacks.push(function(){
$form.clearForm(options.includeHidden);
});
}
if(!options.dataType&&options.target){
var oldSuccess=options.success||function(){};
callbacks.push(function(data,textStatus,jqXHR){
var successArguments=arguments,
fn=options.replaceTarget?'replaceWith':'html';
$(options.target)[fn](data).each(function(){
oldSuccess.apply(this,successArguments);
});
});
}else if(options.success){
if($.isArray(options.success)){
$.merge(callbacks,options.success);
}else{
callbacks.push(options.success);
}
}
options.success=function(data,status,xhr){
var context=options.context||this;
for(var i=0,max=callbacks.length;i<max;i++){
callbacks[i].apply(context,[data,status,xhr||$form,$form]);
}
};
if(options.error){
var oldError=options.error;
options.error=function(xhr,status,error){
var context=options.context||this;
oldError.apply(context,[xhr,status,error,$form]);
};
}
if(options.complete){
var oldComplete=options.complete;
options.complete=function(xhr,status){
var context=options.context||this;
oldComplete.apply(context,[xhr,status,$form]);
};
}
var fileInputs=$('input[type=file]:enabled',this).filter(function(){
return $(this).val()!=='';
});
var hasFileInputs=fileInputs.length>0;
var mp='multipart/form-data';
var multipart=($form.attr('enctype')===mp||$form.attr('encoding')===mp);
var fileAPI=feature.fileapi&&feature.formdata;
log('fileAPI :'+fileAPI);
var shouldUseFrame=(hasFileInputs||multipart)&&!fileAPI;
var jqxhr;
if(options.iframe!==false&&(options.iframe||shouldUseFrame)){
if(options.closeKeepAlive){
$.get(options.closeKeepAlive,function(){
jqxhr=fileUploadIframe(a);
});
}else{
jqxhr=fileUploadIframe(a);
}
}else if((hasFileInputs||multipart)&&fileAPI){
jqxhr=fileUploadXhr(a);
}else{
jqxhr=$.ajax(options);
}
$form.removeData('jqxhr').data('jqxhr',jqxhr);
for(var k=0;k<elements.length;k++){
elements[k]=null;
}
this.trigger('form-submit-notify',[this,options]);
return this;
function deepSerialize(extraData){
var serialized=$.param(extraData,options.traditional).split('&');
var len=serialized.length;
var result=[];
var i,part;
for(i=0;i<len;i++){
serialized[i]=serialized[i].replace(/\+/g,' ');
part=serialized[i].split('=');
result.push([decodeURIComponent(part[0]),decodeURIComponent(part[1])]);
}
return result;
}
function fileUploadXhr(a){
var formdata=new FormData();
for(var i=0;i<a.length;i++){
formdata.append(a[i].name,a[i].value);
}
if(options.extraData){
var serializedData=deepSerialize(options.extraData);
for(i=0;i<serializedData.length;i++){
if(serializedData[i]){
formdata.append(serializedData[i][0],serializedData[i][1]);
}
}
}
options.data=null;
var s=$.extend(true,{},$.ajaxSettings,options,{
contentType:false,
processData:false,
cache:false,
type:method||'POST'
});
if(options.uploadProgress){
s.xhr=function(){
var xhr=$.ajaxSettings.xhr();
if(xhr.upload){
xhr.upload.addEventListener('progress',function(event){
var percent=0;
var position=event.loaded||event.position;
var total=event.total;
if(event.lengthComputable){
percent=Math.ceil(position/total*100);
}
options.uploadProgress(event,position,total,percent);
},false);
}
return xhr;
};
}
s.data=null;
var beforeSend=s.beforeSend;
s.beforeSend=function(xhr,o){
if(options.formData){
o.data=options.formData;
}else{
o.data=formdata;
}
if(beforeSend){
beforeSend.call(this,xhr,o);
}
};
return $.ajax(s);
}
function fileUploadIframe(a){
var form=$form[0],el,i,s,g,id,$io,io,xhr,sub,n,timedOut,timeoutHandle;
var deferred=$.Deferred();
deferred.abort=function(status){
xhr.abort(status);
};
if(a){
for(i=0;i<elements.length;i++){
el=$(elements[i]);
if(hasProp){
el.prop('disabled',false);
}else{
el.removeAttr('disabled');
}
}
}
s=$.extend(true,{},$.ajaxSettings,options);
s.context=s.context||s;
id='jqFormIO'+new Date().getTime();
var ownerDocument=form.ownerDocument;
var $body=$form.closest('body');
if(s.iframeTarget){
$io=$(s.iframeTarget,ownerDocument);
n=$io.attr2('name');
if(!n){
$io.attr2('name',id);
}else{
id=n;
}
}else{
$io=$('<iframe name="'+id+'" src="'+s.iframeSrc+'" />',ownerDocument);
$io.css({position:'absolute',top:'-1000px',left:'-1000px'});
}
io=$io[0];
xhr={
aborted:0,
responseText:null,
responseXML:null,
status:0,
statusText:'n/a',
getAllResponseHeaders:function(){},
getResponseHeader:function(){},
setRequestHeader:function(){},
abort:function(status){
var e=(status==='timeout'?'timeout':'aborted');
log('aborting upload... '+e);
this.aborted=1;
try{
if(io.contentWindow.document.execCommand){
io.contentWindow.document.execCommand('Stop');
}
}catch(ignore){}
$io.attr('src',s.iframeSrc);
xhr.error=e;
if(s.error){
s.error.call(s.context,xhr,e,status);
}
if(g){
$.event.trigger('ajaxError',[xhr,s,e]);
}
if(s.complete){
s.complete.call(s.context,xhr,e);
}
}
};
g=s.global;
if(g&&$.active++===0){
$.event.trigger('ajaxStart');
}
if(g){
$.event.trigger('ajaxSend',[xhr,s]);
}
if(s.beforeSend&&s.beforeSend.call(s.context,xhr,s)===false){
if(s.global){
$.active--;
}
deferred.reject();
return deferred;
}
if(xhr.aborted){
deferred.reject();
return deferred;
}
sub=form.clk;
if(sub){
n=sub.name;
if(n&&!sub.disabled){
s.extraData=s.extraData||{};
s.extraData[n]=sub.value;
if(sub.type==='image'){
s.extraData[n+'.x']=form.clk_x;
s.extraData[n+'.y']=form.clk_y;
}
}
}
var CLIENT_TIMEOUT_ABORT=1;
var SERVER_ABORT=2;
function getDoc(frame){
var doc=null;
try{
if(frame.contentWindow){
doc=frame.contentWindow.document;
}
}catch(err){
log('cannot get iframe.contentWindow document: '+err);
}
if(doc){
return doc;
}
try{
doc=frame.contentDocument?frame.contentDocument:frame.document;
}catch(err){
log('cannot get iframe.contentDocument: '+err);
doc=frame.document;
}
return doc;
}
var csrf_token=$('meta[name=csrf-token]').attr('content');
var csrf_param=$('meta[name=csrf-param]').attr('content');
if(csrf_param&&csrf_token){
s.extraData=s.extraData||{};
s.extraData[csrf_param]=csrf_token;
}
function doSubmit(){
var t=$form.attr2('target'),
a=$form.attr2('action'),
mp='multipart/form-data',
et=$form.attr('enctype')||$form.attr('encoding')||mp;
form.setAttribute('target',id);
if(!method||/post/i.test(method)){
form.setAttribute('method','POST');
}
if(a!==s.url){
form.setAttribute('action',s.url);
}
if(!s.skipEncodingOverride&&(!method||/post/i.test(method))){
$form.attr({
encoding:'multipart/form-data',
enctype:'multipart/form-data'
});
}
if(s.timeout){
timeoutHandle=setTimeout(function(){
timedOut=true;cb(CLIENT_TIMEOUT_ABORT);
},s.timeout);
}
function checkState(){
try{
var state=getDoc(io).readyState;
log('state = '+state);
if(state&&state.toLowerCase()==='uninitialized'){
setTimeout(checkState,50);
}
}catch(e){
log('Server abort: ',e,' (',e.name,')');
cb(SERVER_ABORT);
if(timeoutHandle){
clearTimeout(timeoutHandle);
}
timeoutHandle=undefined;
}
}
var extraInputs=[];
try{
if(s.extraData){
for(var n in s.extraData){
if(s.extraData.hasOwnProperty(n)){
if($.isPlainObject(s.extraData[n])&&s.extraData[n].hasOwnProperty('name')&&s.extraData[n].hasOwnProperty('value')){
extraInputs.push(
$('<input type="hidden" name="'+s.extraData[n].name+'">',ownerDocument).val(s.extraData[n].value)
.appendTo(form)[0]);
}else{
extraInputs.push(
$('<input type="hidden" name="'+n+'">',ownerDocument).val(s.extraData[n])
.appendTo(form)[0]);
}
}
}
}
if(!s.iframeTarget){
$io.appendTo($body);
}
if(io.attachEvent){
io.attachEvent('onload',cb);
}else{
io.addEventListener('load',cb,false);
}
setTimeout(checkState,15);
try{
form.submit();
}catch(err){
var submitFn=document.createElement('form').submit;
submitFn.apply(form);
}
}finally{
form.setAttribute('action',a);
form.setAttribute('enctype',et);
if(t){
form.setAttribute('target',t);
}else{
$form.removeAttr('target');
}
$(extraInputs).remove();
}
}
if(s.forceSync){
doSubmit();
}else{
setTimeout(doSubmit,10);
}
var data,doc,domCheckCount=50,callbackProcessed;
function cb(e){
if(xhr.aborted||callbackProcessed){
return;
}
doc=getDoc(io);
if(!doc){
log('cannot access response document');
e=SERVER_ABORT;
}
if(e===CLIENT_TIMEOUT_ABORT&&xhr){
xhr.abort('timeout');
deferred.reject(xhr,'timeout');
return;
}else if(e===SERVER_ABORT&&xhr){
xhr.abort('server abort');
deferred.reject(xhr,'error','server abort');
return;
}
if(!doc||doc.location.href===s.iframeSrc){
if(!timedOut){
return;
}
}
if(io.detachEvent){
io.detachEvent('onload',cb);
}else{
io.removeEventListener('load',cb,false);
}
var status='success',errMsg;
try{
if(timedOut){
throw'timeout';
}
var isXml=s.dataType==='xml'||doc.XMLDocument||$.isXMLDoc(doc);
log('isXml='+isXml);
if(!isXml&&window.opera&&(doc.body===null||!doc.body.innerHTML)){
if(--domCheckCount){
log('requeing onLoad callback, DOM not available');
setTimeout(cb,250);
return;
}
}
var docRoot=doc.body?doc.body:doc.documentElement;
xhr.responseText=docRoot?docRoot.innerHTML:null;
xhr.responseXML=doc.XMLDocument?doc.XMLDocument:doc;
if(isXml){
s.dataType='xml';
}
xhr.getResponseHeader=function(header){
var headers={'content-type':s.dataType};
return headers[header.toLowerCase()];
};
if(docRoot){
xhr.status=Number(docRoot.getAttribute('status'))||xhr.status;
xhr.statusText=docRoot.getAttribute('statusText')||xhr.statusText;
}
var dt=(s.dataType||'').toLowerCase();
var scr=/(json|script|text)/.test(dt);
if(scr||s.textarea){
var ta=doc.getElementsByTagName('textarea')[0];
if(ta){
xhr.responseText=ta.value;
xhr.status=Number(ta.getAttribute('status'))||xhr.status;
xhr.statusText=ta.getAttribute('statusText')||xhr.statusText;
}else if(scr){
var pre=doc.getElementsByTagName('pre')[0];
var b=doc.getElementsByTagName('body')[0];
if(pre){
xhr.responseText=pre.textContent?pre.textContent:pre.innerText;
}else if(b){
xhr.responseText=b.textContent?b.textContent:b.innerText;
}
}
}else if(dt==='xml'&&!xhr.responseXML&&xhr.responseText){
xhr.responseXML=toXml(xhr.responseText);
}
try{
data=httpData(xhr,dt,s);
}catch(err){
status='parsererror';
xhr.error=errMsg=(err||status);
}
}catch(err){
log('error caught: ',err);
status='error';
xhr.error=errMsg=(err||status);
}
if(xhr.aborted){
log('upload aborted');
status=null;
}
if(xhr.status){
status=((xhr.status>=200&&xhr.status<300)||xhr.status===304)?'success':'error';
}
if(status==='success'){
if(s.success){
s.success.call(s.context,data,'success',xhr);
}
deferred.resolve(xhr.responseText,'success',xhr);
if(g){
$.event.trigger('ajaxSuccess',[xhr,s]);
}
}else if(status){
if(typeof errMsg==='undefined'){
errMsg=xhr.statusText;
}
if(s.error){
s.error.call(s.context,xhr,status,errMsg);
}
deferred.reject(xhr,'error',errMsg);
if(g){
$.event.trigger('ajaxError',[xhr,s,errMsg]);
}
}
if(g){
$.event.trigger('ajaxComplete',[xhr,s]);
}
if(g&&!--$.active){
$.event.trigger('ajaxStop');
}
if(s.complete){
s.complete.call(s.context,xhr,status);
}
callbackProcessed=true;
if(s.timeout){
clearTimeout(timeoutHandle);
}
setTimeout(function(){
if(!s.iframeTarget){
$io.remove();
}else{
$io.attr('src',s.iframeSrc);
}
xhr.responseXML=null;
},100);
}
var toXml=$.parseXML||function(s,doc){
if(window.ActiveXObject){
doc=new ActiveXObject('Microsoft.XMLDOM');
doc.async='false';
doc.loadXML(s);
}else{
doc=(new DOMParser()).parseFromString(s,'text/xml');
}
return(doc&&doc.documentElement&&doc.documentElement.nodeName!=='parsererror')?doc:null;
};
var parseJSON=$.parseJSON||function(s){
return window['eval']('('+s+')');
};
var httpData=function(xhr,type,s){
var ct=xhr.getResponseHeader('content-type')||'',
xml=((type==='xml'||!type)&&ct.indexOf('xml')>=0),
data=xml?xhr.responseXML:xhr.responseText;
if(xml&&data.documentElement.nodeName==='parsererror'){
if($.error){
$.error('parsererror');
}
}
if(s&&s.dataFilter){
data=s.dataFilter(data,type);
}
if(typeof data==='string'){
if((type==='json'||!type)&&ct.indexOf('json')>=0){
data=parseJSON(data);
}else if((type==='script'||!type)&&ct.indexOf('javascript')>=0){
$.globalEval(data);
}
}
return data;
};
return deferred;
}
};
$.fn.ajaxForm=function(options,data,dataType,onSuccess){
if(typeof options==='string'||(options===false&&arguments.length>0)){
options={
'url':options,
'data':data,
'dataType':dataType
};
if(typeof onSuccess==='function'){
options.success=onSuccess;
}
}
options=options||{};
options.delegation=options.delegation&&$.isFunction($.fn.on);
if(!options.delegation&&this.length===0){
var o={s:this.selector,c:this.context};
if(!$.isReady&&o.s){
log('DOM not ready, queuing ajaxForm');
$(function(){
$(o.s,o.c).ajaxForm(options);
});
return this;
}
log('terminating; zero elements found by selector'+($.isReady?'':' (DOM not ready)'));
return this;
}
if(options.delegation){
$(document)
.off('submit.form-plugin',this.selector,doAjaxSubmit)
.off('click.form-plugin',this.selector,captureSubmittingElement)
.on('submit.form-plugin',this.selector,options,doAjaxSubmit)
.on('click.form-plugin',this.selector,options,captureSubmittingElement);
return this;
}
return this.ajaxFormUnbind()
.on('submit.form-plugin',options,doAjaxSubmit)
.on('click.form-plugin',options,captureSubmittingElement);
};
function doAjaxSubmit(e){
var options=e.data;
if(!e.isDefaultPrevented()){
e.preventDefault();
$(e.target).closest('form').ajaxSubmit(options);
}
}
function captureSubmittingElement(e){
var target=e.target;
var $el=$(target);
if(!$el.is('[type=submit],[type=image]')){
var t=$el.closest('[type=submit]');
if(t.length===0){
return;
}
target=t[0];
}
var form=target.form;
form.clk=target;
if(target.type==='image'){
if(typeof e.offsetX!=='undefined'){
form.clk_x=e.offsetX;
form.clk_y=e.offsetY;
}else if(typeof $.fn.offset==='function'){
var offset=$el.offset();
form.clk_x=e.pageX-offset.left;
form.clk_y=e.pageY-offset.top;
}else{
form.clk_x=e.pageX-target.offsetLeft;
form.clk_y=e.pageY-target.offsetTop;
}
}
setTimeout(function(){
form.clk=form.clk_x=form.clk_y=null;
},100);
}
$.fn.ajaxFormUnbind=function(){
return this.off('submit.form-plugin click.form-plugin');
};
$.fn.formToArray=function(semantic,elements,filtering){
var a=[];
if(this.length===0){
return a;
}
var form=this[0];
var formId=this.attr('id');
var els=(semantic||typeof form.elements==='undefined')?form.getElementsByTagName('*'):form.elements;
var els2;
if(els){
els=$.makeArray(els);
}
if(formId&&(semantic||/(Edge|Trident)\//.test(navigator.userAgent))){
els2=$(':input[form="'+formId+'"]').get();
if(els2.length){
els=(els||[]).concat(els2);
}
}
if(!els||!els.length){
return a;
}
if($.isFunction(filtering)){
els=$.map(els,filtering);
}
var i,j,n,v,el,max,jmax;
for(i=0,max=els.length;i<max;i++){
el=els[i];
n=el.name;
if(!n||el.disabled){
continue;
}
if(semantic&&form.clk&&el.type==='image'){
if(form.clk===el){
a.push({name:n,value:$(el).val(),type:el.type});
a.push({name:n+'.x',value:form.clk_x},{name:n+'.y',value:form.clk_y});
}
continue;
}
v=$.fieldValue(el,true);
if(v&&v.constructor===Array){
if(elements){
elements.push(el);
}
for(j=0,jmax=v.length;j<jmax;j++){
a.push({name:n,value:v[j]});
}
}else if(feature.fileapi&&el.type==='file'){
if(elements){
elements.push(el);
}
var files=el.files;
if(files.length){
for(j=0;j<files.length;j++){
a.push({name:n,value:files[j],type:el.type});
}
}else{
a.push({name:n,value:'',type:el.type});
}
}else if(v!==null&&typeof v!=='undefined'){
if(elements){
elements.push(el);
}
a.push({name:n,value:v,type:el.type,required:el.required});
}
}
if(!semantic&&form.clk){
var $input=$(form.clk),input=$input[0];
n=input.name;
if(n&&!input.disabled&&input.type==='image'){
a.push({name:n,value:$input.val()});
a.push({name:n+'.x',value:form.clk_x},{name:n+'.y',value:form.clk_y});
}
}
return a;
};
$.fn.formSerialize=function(semantic){
return $.param(this.formToArray(semantic));
};
$.fn.fieldSerialize=function(successful){
var a=[];
this.each(function(){
var n=this.name;
if(!n){
return;
}
var v=$.fieldValue(this,successful);
if(v&&v.constructor===Array){
for(var i=0,max=v.length;i<max;i++){
a.push({name:n,value:v[i]});
}
}else if(v!==null&&typeof v!=='undefined'){
a.push({name:this.name,value:v});
}
});
return $.param(a);
};
$.fn.fieldValue=function(successful){
for(var val=[],i=0,max=this.length;i<max;i++){
var el=this[i];
var v=$.fieldValue(el,successful);
if(v===null||typeof v==='undefined'||(v.constructor===Array&&!v.length)){
continue;
}
if(v.constructor===Array){
$.merge(val,v);
}else{
val.push(v);
}
}
return val;
};
$.fieldValue=function(el,successful){
var n=el.name,t=el.type,tag=el.tagName.toLowerCase();
if(typeof successful==='undefined'){
successful=true;
}
if(successful&&(!n||el.disabled||t==='reset'||t==='button'||
(t==='checkbox'||t==='radio')&&!el.checked||
(t==='submit'||t==='image')&&el.form&&el.form.clk!==el||
tag==='select'&&el.selectedIndex===-1)){
return null;
}
if(tag==='select'){
var index=el.selectedIndex;
if(index<0){
return null;
}
var a=[],ops=el.options;
var one=(t==='select-one');
var max=(one?index+1:ops.length);
for(var i=(one?index:0);i<max;i++){
var op=ops[i];
if(op.selected&&!op.disabled){
var v=op.value;
if(!v){
v=(op.attributes&&op.attributes.value&&!(op.attributes.value.specified))?op.text:op.value;
}
if(one){
return v;
}
a.push(v);
}
}
return a;
}
return $(el).val().replace(rCRLF,'\r\n');
};
$.fn.clearForm=function(includeHidden){
return this.each(function(){
$('input,select,textarea',this).clearFields(includeHidden);
});
};
$.fn.clearFields=$.fn.clearInputs=function(includeHidden){
var re=/^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
return this.each(function(){
var t=this.type,tag=this.tagName.toLowerCase();
if(re.test(t)||tag==='textarea'){
this.value='';
}else if(t==='checkbox'||t==='radio'){
this.checked=false;
}else if(tag==='select'){
this.selectedIndex=-1;
}else if(t==='file'){
if(/MSIE/.test(navigator.userAgent)){
$(this).replaceWith($(this).clone(true));
}else{
$(this).val('');
}
}else if(includeHidden){
if((includeHidden===true&&/hidden/.test(t))||
(typeof includeHidden==='string'&&$(this).is(includeHidden))){
this.value='';
}
}
});
};
$.fn.resetForm=function(){
return this.each(function(){
var el=$(this);
var tag=this.tagName.toLowerCase();
switch(tag){
case'input':
this.checked=this.defaultChecked;
case'textarea':
this.value=this.defaultValue;
return true;
case'option':
case'optgroup':
var select=el.parents('select');
if(select.length&&select[0].multiple){
if(tag==='option'){
this.selected=this.defaultSelected;
}else{
el.find('option').resetForm();
}
}else{
select.resetForm();
}
return true;
case'select':
el.find('option').each(function(i){
this.selected=this.defaultSelected;
if(this.defaultSelected&&!el[0].multiple){
el[0].selectedIndex=i;
return false;
}
});
return true;
case'label':
var forEl=$(el.attr('for'));
var list=el.find('input,select,textarea');
if(forEl[0]){
list.unshift(forEl[0]);
}
list.resetForm();
return true;
case'form':
if(typeof this.reset==='function'||(typeof this.reset==='object'&&!this.reset.nodeType)){
this.reset();
}
return true;
default:
el.find('form,input,label,select,textarea').resetForm();
return true;
}
});
};
$.fn.enable=function(b){
if(typeof b==='undefined'){
b=true;
}
return this.each(function(){
this.disabled=!b;
});
};
$.fn.selected=function(select){
if(typeof select==='undefined'){
select=true;
}
return this.each(function(){
var t=this.type;
if(t==='checkbox'||t==='radio'){
this.checked=select;
}else if(this.tagName.toLowerCase()==='option'){
var $sel=$(this).parent('select');
if(select&&$sel[0]&&$sel[0].type==='select-one'){
$sel.find('option').selected(false);
}
this.selected=select;
}
});
};
$.fn.ajaxSubmit.debug=false;
function log(){
if(!$.fn.ajaxSubmit.debug){
return;
}
var msg='[jquery.form] '+Array.prototype.join.call(arguments,'');
if(window.console&&window.console.log){
window.console.log(msg);
}else if(window.opera&&window.opera.postError){
window.opera.postError(msg);
}
}
}));


/* prive/javascript/jquery.autosave.js?1649159400 */

(function($){
$.fn.autosave=function(opt){
opt=$.extend({
url:window.location,
confirm:false,
confirmstring:'Sauvegarder ?'
},opt);
var save_changed=function(){
$('form.autosavechanged')
.each(function(){
if(!opt.confirm||confirm(opt.confirmstring)){
var contenu=$(this).serialize();
var d=new Date();
contenu=contenu+"&__timestamp="+Math.round(d.getTime()/1000);
$.post(opt.url,{
'action':'session',
'var':'autosave_'+$('input[name=autosave]',this).val(),
'val':contenu
});
}
}).removeClass('autosavechanged');
};
$(window).on('unload',save_changed);
return this
.on('keyup',function(){
$(this).addClass('autosavechanged');
})
.on('change',function(){
$(this).addClass('autosavechanged');
save_changed();
})
.on('submit',function(){
save_changed();
});
};
})(jQuery);


/* prive/javascript/jquery.placeholder-label.js?1649159400 */

(function($){
$.placeholderLabel={
placeholder_class:null,
add_placeholder:function(){
if($(this).val()==$(this).attr('placeholder')){
$(this).val('').removeClass($.placeholderLabel.placeholder_class);
}
},
remove_placeholder:function(){
if($(this).val()==''){
$(this).val($(this).attr('placeholder')).addClass($.placeholderLabel.placeholder_class);
}
},
disable_placeholder_fields:function(){
$(this).find("input[placeholder]").each(function(){
if($(this).val()==$(this).attr('placeholder')){
$(this).val('');
}
});
return true;
}
};
$.fn.placeholderLabel=function(options){
var dummy=document.createElement('input');
if(dummy.placeholder!=undefined){
return this;
}
var config={
placeholder_class:'placeholder'
};
if(options)$.extend(config,options);
$.placeholderLabel.placeholder_class=config.placeholder_class;
this.each(function(){
var input=$(this);
input.focus($.placeholderLabel.add_placeholder);
input.blur($.placeholderLabel.remove_placeholder);
input.triggerHandler('focus');
input.triggerHandler('blur');
$(this.form).submit($.placeholderLabel.disable_placeholder_fields);
});
return this;
}
})(jQuery);


/* prive/javascript/ajaxCallback.js?1649159400 */
jQuery.spip=jQuery.spip||{};
jQuery.spip.log=function(){
if(jQuery.spip.debug&&window.console&&window.console.log)
window.console.log.apply(this,arguments);
}
jQuery.spip.test_espace_prive=function(){
if(typeof spipConfig.core.test_espace_prive!=undefined&&spipConfig.core.test_espace_prive){
return true;
}
return false;
}
if(!jQuery.spip.load_handlers){
jQuery.spip.load_handlers=new Array();
function onAjaxLoad(f){
jQuery.spip.load_handlers.push(f);
};
jQuery.spip.triggerAjaxLoad=function(root){
jQuery.spip.log('triggerAjaxLoad');
jQuery.spip.log(root);
for(var i=0;i<jQuery.spip.load_handlers.length;i++)
jQuery.spip.load_handlers[i].apply(root);
};
jQuery.spip.intercepted={};
jQuery.spip.intercepted.load=jQuery.fn.load;
jQuery.fn.load=function(url,params,callback){
if(typeof url!=="string"){
return jQuery.spip.intercepted.load.apply(this,arguments);
}
callback=callback||function(){};
if(params){
if(params.constructor==Function){
callback=params;
params=null;
}
}
params=jQuery.extend(params,{triggerAjaxLoad:false});
var callback2=function(){jQuery.spip.log('jQuery.load');jQuery.spip.triggerAjaxLoad(this);callback.apply(this,arguments);};
return jQuery.spip.intercepted.load.apply(this,[url,params,callback2]);
};
jQuery.spip.intercepted.ajaxSubmit=jQuery.fn.ajaxSubmit;
jQuery.fn.ajaxSubmit=function(options){
options=options||{};
if(typeof options.onAjaxLoad=="undefined"||options.onAjaxLoad!=false){
var me=jQuery(this).parents('div.ajax');
if(me.length)
me=me.parent();
else
me=document;
if(typeof options=='function')
options={success:options};
var callback=options.success||function(){};
options.success=function(){callback.apply(this,arguments);jQuery.spip.log('jQuery.ajaxSubmit');jQuery.spip.triggerAjaxLoad(me);}
}
return jQuery.spip.intercepted.ajaxSubmit.apply(this,[options]);
}
jQuery.spip.intercepted.ajax=jQuery.ajax;
jQuery.ajax=function(url,settings){
if(typeof settings=='undefined'){
settings={};
if(typeof url=='object'){
settings=url;
url=null;
}
}
if(typeof url=='string'){
settings['url']=url;
}
if(settings.data&&settings.data['triggerAjaxLoad']===false){
settings.data['triggerAjaxLoad']=null;
return jQuery.spip.intercepted.ajax(settings);
}
var s=jQuery.extend(true,{},jQuery.ajaxSettings,settings);
var callbackContext=s.context||s;
try{
if(jQuery.ajax.caller==jQuery.spip.intercepted.load||jQuery.ajax.caller==jQuery.spip.intercepted.ajaxSubmit)
return jQuery.spip.intercepted.ajax(settings);
}
catch(err){}
var orig_complete=s.complete||function(){};
settings.complete=function(res,status){
var dataType=settings.dataType;
var ct=(res&&(typeof res.getResponseHeader=='function'))
?res.getResponseHeader("content-type"):'';
var xml=!dataType&&ct&&ct.indexOf("xml")>=0;
orig_complete.call(callbackContext,res,status);
if((!dataType&&!xml)||dataType=="html"){
jQuery.spip.log('jQuery.ajax');
if(typeof s.onAjaxLoad=="undefined"||s.onAjaxLoad!=false)
jQuery.spip.triggerAjaxLoad(s.ajaxTarget?s.ajaxTarget:document);
}
};
return jQuery.spip.intercepted.ajax(settings);
};
}
jQuery.uaMatch=function(ua){
ua=ua.toLowerCase();
var match=/(chrome)[ \/]([\w.]+)/.exec(ua)||/(webkit)[ \/]([\w.]+)/.exec(ua)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua)||/(msie) ([\w.]+)/.exec(ua)||
ua.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua)||
[];
return{
browser:match[1]||"",
version:match[2]||"0"
};
};
if(!jQuery.browser){
matched=jQuery.uaMatch(navigator.userAgent);
browser={};
if(matched.browser){
browser[matched.browser]=true;
browser.version=matched.version;
}
if(browser.chrome){
browser.webkit=true;
}else if(browser.webkit){
browser.safari=true;
}
jQuery.browser=browser;
}
jQuery.getScript=function(url,callback){
return $.ajax({
url:url,
dataType:"script",
success:callback,
cache:true
});
}
jQuery.fn.positionner=function(force,setfocus){
var offset=jQuery(this).offset();
var hauteur=parseInt(jQuery(this).css('height'));
var marge=jQuery.spip.positionner_marge||5;
var scrolltop=self['pageYOffset']||
jQuery.boxModel&&document.documentElement['scrollTop']||
document.body['scrollTop'];
var h=jQuery(window).height();
var scroll=0;
if(force||(offset&&offset['top']-marge<=scrolltop)){
scroll=offset['top']-marge;
}
else if(offset&&offset['top']+hauteur-h+marge>scrolltop){
scroll=Math.min(offset['top']-marge-15,offset['top']+hauteur-h+40);
}
if(scroll)
jQuery('html,body')
.animate({scrollTop:scroll},300);
if(setfocus!==false)
jQuery(jQuery('*',this).filter('input[type=text],textarea')[0]).focus();
return this;
}
jQuery.spip.virtualbuffer_id='spip_virtualbufferupdate';
jQuery.spip.initReaderBuffer=function(){
if(jQuery('#'+jQuery.spip.virtualbuffer_id).length)return;
jQuery('body').append('<div style="float:left;width:0;height:0;position:absolute;left:-5000px;top:-5000px;"><input type="hidden" name="'+jQuery.spip.virtualbuffer_id+'" id="'+jQuery.spip.virtualbuffer_id+'" value="0" /></div>');
}
jQuery.spip.updateReaderBuffer=function(){
var i=jQuery('#'+jQuery.spip.virtualbuffer_id);
if(!i.length)return;
i.val(parseInt(i.val())+1);
}
jQuery.fn.formulaire_setContainer=function(){
if(!this.closest('.ajax-form-container').length){
this.find('script').remove();
var aria=this.data('aria');
var $container=jQuery('<div class="ajax-form-container"></div>');
if(aria&&typeof aria==='object'){
for(var i in aria){
$container=$container.attr(i,aria[i]);
}
}
else{
aria=false;
}
this.wrap($container);
if(aria){
jQuery('form',this).not('[aria-live]').attr('aria-live','off');
}
}
return this;
}
jQuery.fn.formulaire_dyn_ajax=function(target){
if(this.length)
jQuery.spip.initReaderBuffer();
return this.each(function(){
var scrollwhensubmit=!jQuery(this).is('.noscroll');
var cible=target||this;
jQuery(cible).formulaire_setContainer();
jQuery('form:not(.noajax):not(.bouton_action_post)',this).each(function(){
var leform=this;
var leclk,leclk_x,leclk_y;
var onError=function(xhr,status,error,$form){
jQuery(leform).ajaxFormUnbind().find('input[name="var_ajax"]').remove();
var msg="Erreur";
if(typeof(error_on_ajaxform)!=="undefined")msg=error_on_ajaxform;
jQuery(leform).prepend("<p class='error ajax-error none'>"+msg+"</p>").find('.ajax-error').show('fast');
jQuery(cible).closest('.ajax-form-container').endLoading(true);
}
jQuery(this).prepend("<input type='hidden' name='var_ajax' value='form' />")
.ajaxForm({
beforeSubmit:function(){
leclk=leform.clk;
var scrollwhensubmit_button=true;
if(leclk){
scrollwhensubmit_button=!jQuery(leclk).is('.noscroll');
var n=leclk.name;
if(n&&!leclk.disabled&&leclk.type=="image"){
leclk_x=leform.clk_x;
leclk_y=leform.clk_y;
}
}
jQuery(cible).wrap('<div />');
cible=jQuery(cible).parent();
jQuery(cible).closest('.ajax-form-container').animateLoading();
if(scrollwhensubmit&&scrollwhensubmit_button){
jQuery(cible).positionner(false,false);
}
},
error:onError,
success:function(c,status,xhr,$form){
if(c.match(/^\s*noajax\s*$/)){
jQuery("input[name=var_ajax]",leform).remove();
if(leclk){
var n=leclk.name;
if(n&&!leclk.disabled){
jQuery(leform).prepend("<input type='hidden' name='"+n+"' value='"+leclk.value+"' />");
if(leclk.type=="image"){
jQuery(leform).prepend("<input type='hidden' name='"+n+".x' value='"+leform.clk_x+"' />");
jQuery(leform).prepend("<input type='hidden' name='"+n+".y' value='"+leform.clk_y+"' />");
}
}
}
jQuery(leform).ajaxFormUnbind().submit();
}
else{
if(!c.length||c.indexOf("ajax-form-is-ok")==-1)
return onError.apply(this,[status,xhr,$form]);
var preloaded=jQuery.spip.preloaded_urls;
jQuery.spip.preloaded_urls={};
jQuery(cible).html(c);
var a=jQuery('a:first',cible).eq(0);
var d=jQuery('div.ajax',cible);
if(!d.length){
jQuery(cible).addClass('ajax');
if(!scrollwhensubmit)
jQuery(cible).addClass('noscroll');
}
else{
d.siblings('br.bugajaxie').remove();
cible=jQuery(":first",cible);
cible.unwrap();
}
if(a.length
&&a.is('a[name=ajax_ancre]')
&&jQuery(a.attr('href'),cible).length){
a=a.attr('href');
if(jQuery(a,cible).length)
setTimeout(function(){
jQuery(a,cible).positionner(false);
},10);
jQuery(cible).closest('.ajax-form-container').endLoading(true);
}
else{
if(a.length&&a.is('a[name=ajax_redirect]')){
a=a.get(0).href;
setTimeout(function(){
var cur=window.location.href.split('#');
document.location.replace(a);
if(cur[0]==a.split('#')[0]){
window.location.reload();
}
},10);
jQuery(cible).closest('.ajax-form-container').animateLoading();
}
else{
jQuery(cible).closest('.ajax-form-container').endLoading(true);
}
}
if(!jQuery('.reponse_formulaire_ok',cible).length)
jQuery.spip.preloaded_urls=preloaded;
jQuery.spip.updateReaderBuffer();
}
}
})
.addClass('noajax hasajax');
});
});
}
jQuery.fn.formulaire_verifier=function(callback,champ){
var erreurs={'message_erreur':'form non ajax'};
var me=this;
if(jQuery(me).closest('.ajax-form-container').attr('aria-busy')!='true'){
if(jQuery(me).is('form.hasajax')){
jQuery(me).ajaxSubmit({
dataType:"json",
data:{formulaire_action_verifier_json:true},
success:function(errs){
var args=[errs,champ]
if(jQuery(me).closest('.ajax-form-container').attr('aria-busy')!='true')
callback.apply(me,args);
}
});
}
else
callback.apply(me,[erreurs,champ]);
}
return this;
}
jQuery.fn.formulaire_activer_verif_auto=function(callback){
callback=callback||formulaire_actualiser_erreurs;
var me=jQuery(this).closest('.ajax-form-container');
var check=function(){
var name=jQuery(this).attr('name');
setTimeout(function(){me.find('form').formulaire_verifier(callback,name);},50);
}
var activer=function(){
if(me.find('form').attr('data-verifjson')!='on'){
me
.find('form')
.attr('data-verifjson','on')
.find('input,select,textarea')
.on('change',check);
}
}
jQuery(activer);
onAjaxLoad(function(){setTimeout(activer,150);});
}
function formulaire_actualiser_erreurs(erreurs){
var parent=jQuery(this).closest('.formulaire_spip');
if(!parent.length)return;
parent.find('.reponse_formulaire,.erreur_message').fadeOut().remove();
parent.find('.erreur').removeClass('erreur');
if(erreurs['message_ok'])
parent.find('form').before('<p class="reponse_formulaire reponse_formulaire_ok">'+erreurs['message_ok']+'</p>');
if(erreurs['message_erreur'])
parent.find('form').before('<p class="reponse_formulaire reponse_formulaire_erreur">'+erreurs['message_erreur']+'</p>');
for(var k in erreurs){
var saisie=parent.find('.editer_'+k);
if(saisie.length){
saisie.addClass('erreur');
saisie.find('label').after('<span class="erreur_message">'+erreurs[k]+'</span>');
}
}
}
var ajax_confirm=true;
var ajax_confirm_date=0;
var spip_confirm=window.confirm;
function _confirm(message){
ajax_confirm=spip_confirm(message);
if(!ajax_confirm){
var d=new Date();
ajax_confirm_date=d.getTime();
}
return ajax_confirm;
}
window.confirm=_confirm;
var ajaxbloc_selecteur;
jQuery.spip.preloaded_urls={};
jQuery.spip.on_ajax_loaded=function(blocfrag,c,href,history){
history=history||(history==null);
if(typeof href==undefined||href==null)
history=false;
if(history)
jQuery.spip.setHistoryState(blocfrag);
if(jQuery(blocfrag).attr('data-loaded-callback')){
var callback=eval(jQuery(blocfrag).attr('data-loaded-callback'));
callback.call(blocfrag,c,href,history);
}
else{
jQuery(blocfrag)
.html(c)
.endLoading();
}
if(typeof href!=undefined)
jQuery(blocfrag).attr('data-url',href);
if(history){
jQuery.spip.pushHistoryState(href);
jQuery.spip.setHistoryState(blocfrag);
}
var a=jQuery('a:first',jQuery(blocfrag)).eq(0);
if(a.length
&&a.is('a[name=ajax_ancre]')
&&jQuery(a.attr('href'),blocfrag).length){
a=a.attr('href');
jQuery(a,blocfrag).positionner(false);
}
jQuery.spip.log('on_ajax_loaded');
jQuery.spip.triggerAjaxLoad(blocfrag);
a=jQuery(blocfrag).parents('form.hasajax')
if(a.length)
a.eq(0).removeClass('noajax').parents('div.ajax').formulaire_dyn_ajax();
jQuery.spip.updateReaderBuffer();
}
jQuery.spip.on_ajax_failed=function(blocfrag,statusCode,href,history){
jQuery(blocfrag).addClass('invalid');
history=history||(history==null);
if(history){
window.location.href=href;
}
}
jQuery.spip.stateId=0;
jQuery.spip.setHistoryState=function(blocfrag){
if(!window.history.replaceState)return;
if(!blocfrag.attr('id')){
while(jQuery('#ghsid'+jQuery.spip.stateId).length)
jQuery.spip.stateId++;
blocfrag.attr('id','ghsid'+jQuery.spip.stateId);
}
var href=blocfrag.attr('data-url')||blocfrag.attr('data-origin');
href=jQuery("<"+"a href='"+href+"'></a>").get(0).href;
var state={
id:blocfrag.attr('id'),
href:href
};
var ajaxid=blocfrag.attr('class').match(/\bajax-id-[\w-]+\b/);
if(ajaxid&&ajaxid.length)
state["ajaxid"]=ajaxid[0];
window.history.replaceState(state,window.document.title,window.document.location);
}
jQuery.spip.pushHistoryState=function(href,title){
if(!window.history.pushState)
return false;
window.history.pushState({},title,href);
}
window.onpopstate=function(popState){
if(popState.state&&popState.state.href){
var blocfrag=false;
if(popState.state.id){
blocfrag=jQuery('#'+popState.state.id);
}
if((!blocfrag||!blocfrag.length)&&popState.state.ajaxid){
blocfrag=jQuery('.ajaxbloc.'+popState.state.ajaxid);
}
if(blocfrag&&blocfrag.length==1){
jQuery.spip.ajaxClick(blocfrag,popState.state.href,{history:false});
return true;
}
else{
window.location.href=popState.state.href;
}
}
}
jQuery.spip.loadAjax=function(blocfrag,url,href,options){
var force=options.force||false;
if(jQuery(blocfrag).attr('data-loading-callback')){
var callback=eval(jQuery(blocfrag).attr('data-loading-callback'));
callback.call(blocfrag,url,href,options);
}
else{
jQuery(blocfrag).animateLoading();
}
if(jQuery.spip.preloaded_urls[url]&&!force){
if(jQuery.spip.preloaded_urls[url]=="<!--loading-->"){
setTimeout(function(){jQuery.spip.loadAjax(blocfrag,url,href,options);},100);
return;
}
jQuery.spip.on_ajax_loaded(blocfrag,jQuery.spip.preloaded_urls[url],href,options.history);
}else{
var d=new Date();
jQuery.spip.preloaded_urls[url]="<!--loading-->";
jQuery.ajax({
url:parametre_url(url,'var_t',d.getTime()),
onAjaxLoad:false,
success:function(c){
jQuery.spip.on_ajax_loaded(blocfrag,c,href,options.history);
jQuery.spip.preloaded_urls[url]=c;
if(options.callback&&typeof options.callback=="function")
options.callback.apply(blocfrag);
},
error:function(e){
jQuery.spip.preloaded_urls[url]='';
jQuery.spip.on_ajax_failed(blocfrag,e.status,href,options.history);
}
});
}
}
jQuery.spip.makeAjaxUrl=function(href,ajax_env,origin){
var url=href.split('#');
url[0]=parametre_url(url[0],'var_ajax',1);
url[0]=parametre_url(url[0],'var_ajax_env',ajax_env);
if(origin){
var p=origin.indexOf('?');
if(p!==-1){
var args=origin.substring(p+1).split('&');
var val;
var arg;
for(var n=0;n<args.length;n++){
arg=args[n].split('=');
arg=arg[0];
p=arg.indexOf('[');
if(p!==-1)
arg=arg.substring(0,p);
val=parametre_url(href,arg);
if(typeof val=="undefined"||val==null)
url[0]=url[0]+'&'+arg+'=';
}
}
}
if(url[1])
url[0]=parametre_url(url[0],'var_ajax_ancre',url[1]);
return url[0];
}
jQuery.spip.ajaxReload=function(blocfrag,options){
var ajax_env=blocfrag.attr('data-ajax-env');
if(!ajax_env||ajax_env==undefined)return;
var href=options.href||blocfrag.attr('data-url')||blocfrag.attr('data-origin');
if(href&&typeof href!=undefined){
options=options||{};
var callback=options.callback||null;
var history=options.history||false;
var args=options.args||{};
for(var key in args)
href=parametre_url(href,key,args[key]==undefined?'':args[key],'&',args[key]==undefined?false:true);
var url=jQuery.spip.makeAjaxUrl(href,ajax_env,blocfrag.attr('data-origin'));
jQuery.spip.loadAjax(blocfrag,url,href,{force:true,callback:callback,history:history});
return true;
}
}
jQuery.spip.ajaxClick=function(blocfrag,href,options){
var ajax_env=blocfrag.attr('data-ajax-env');
if(!ajax_env||ajax_env==undefined)return;
if(!ajax_confirm){
ajax_confirm=true;
var d=new Date();
if((d.getTime()-ajax_confirm_date)<=2)
return false;
}
var url=jQuery.spip.makeAjaxUrl(href,ajax_env,blocfrag.attr('data-origin'));
jQuery.spip.loadAjax(blocfrag,url,href,options);
return false;
}
jQuery.fn.ajaxbloc=function(){
if(this.length)
jQuery.spip.initReaderBuffer();
if(ajaxbloc_selecteur==undefined)
ajaxbloc_selecteur='.pagination a,a.ajax';
return this.each(function(){
jQuery('div.ajaxbloc',this).ajaxbloc();
var blocfrag=jQuery(this);
var ajax_env=blocfrag.attr('data-ajax-env');
if(!ajax_env||ajax_env==undefined)return;
blocfrag.not('.bind-ajaxReload').on('ajaxReload',function(event,options){
if(jQuery.spip.ajaxReload(blocfrag,options))
event.stopPropagation();
}).addClass('bind-ajaxReload');
jQuery(ajaxbloc_selecteur,this).not('.noajax,.bind-ajax')
.click(function(){return jQuery.spip.ajaxClick(blocfrag,this.href,{force:jQuery(this).is('.nocache'),history:!(jQuery(this).is('.nohistory')||jQuery(this).closest('.box_modalbox').length)});})
.addClass('bind-ajax')
.filter('.preload').each(function(){
var href=this.href;
var url=jQuery.spip.makeAjaxUrl(href,ajax_env,blocfrag.attr('data-origin'));
if(!jQuery.spip.preloaded_urls[url]){
jQuery.spip.preloaded_urls[url]='<!--loading-->';
jQuery.ajax({url:url,onAjaxLoad:false,success:function(r){jQuery.spip.preloaded_urls[url]=r;},error:function(){jQuery.spip.preloaded_urls[url]='';}});
}
});
jQuery('form.bouton_action_post.ajax',this).not('.noajax,.bind-ajax').each(function(){
var leform=this;
var url=jQuery(this).attr('action').split('#');
var scrollwhensubmit=(!jQuery(this).is('.noscroll')&&!jQuery('.submit',this).is('.noscroll'));
jQuery(this)
.prepend("<input type='hidden' name='var_ajax' value='1' /><input type='hidden' name='var_ajax_env' value='"+(ajax_env)+"' />"+(url[1]?"<input type='hidden' name='var_ajax_ancre' value='"+url[1]+"' />":""))
.ajaxForm({
beforeSubmit:function(){
jQuery(blocfrag).animateLoading();
if(scrollwhensubmit){
jQuery(blocfrag).positionner(false);
}
},
onAjaxLoad:false,
success:function(c){
jQuery.spip.on_ajax_loaded(blocfrag,c);
jQuery.spip.preloaded_urls={};
},
error:function(e){
jQuery.spip.preloaded_urls={};
var href=parametre_url(url,'redirect');
if(!href){
href=window.location.href;
}
jQuery.spip.on_ajax_failed(blocfrag,e.status,href,e.status===400);
}
})
.addClass('bind-ajax');
});
});
};
jQuery.fn.followLink=function(){
$(this).click();
if(!$(this).is('.bind-ajax'))
window.location.href=$(this).get(0).href;
return this;
}
function ajaxReload(ajaxid,options){
jQuery('div.ajaxbloc.ajax-id-'+ajaxid).ajaxReload(options);
}
jQuery.fn.ajaxReload=function(options){
options=options||{};
jQuery(this).trigger('ajaxReload',[options]);
return this;
}
jQuery.fn.animateLoading=function(){
this.attr('aria-busy','true').addClass('loading').children().css('opacity',0.5);
if(typeof ajax_image_searching!='undefined'){
var i=(this).find('.image_loading');
if(i.length)i.eq(0).html(ajax_image_searching);
else this.prepend('<span class="image_loading">'+ajax_image_searching+'</span>');
}
return this;
}
jQuery.fn.animeajax=jQuery.fn.animateLoading;
jQuery.fn.endLoading=function(hard){
hard=hard||false;
this.attr('aria-busy','false').removeClass('loading');
if(hard){
this.children().css('opacity','');
this.find('.image_loading').html('');
}
return this;
}
jQuery.fn.animateRemove=function(callback){
if(this.length){
var me=this;
var color=$("<div class='remove'></div>").css('background-color');
var sel=$(this);
if(sel.is('tr'))
sel=sel.add('>td',sel);
sel.addClass('remove').css({backgroundColor:color}).animate({opacity:"0.0"},'fast',function(){
sel.removeClass('remove').css({backgroundColor:''});
if(callback)
callback.apply(me);
});
}
return this;
}
jQuery.fn.animateAppend=function(callback){
if(this.length){
var me=this;
var color=$("<div class='append'></div>").css('background-color');
var origin=$(this).css('background-color')||'#ffffff';
if(origin=='transparent')origin='#ffffff';
var sel=$(this);
if(sel.is('tr'))
sel=sel.add('>td',sel);
sel.css('opacity','0.0').addClass('append').css({backgroundColor:color}).animate({opacity:"1.0"},1000,function(){
sel.animate({backgroundColor:origin},3000,function(){
sel.removeClass('append').css({backgroundColor:''});
if(callback)
callback.apply(me);
});
});
}
return this;
}
function parametre_url(url,c,v,sep,force_vide){
if(typeof(url)=='undefined'){
url='';
}
var p;
var ancre='';
var a='./';
var args=[];
p=url.indexOf('#');
if(p!=-1){
ancre=url.substring(p);
url=url.substring(0,p);
}
p=url.indexOf('?');
if(p!==-1){
if(p>0)a=url.substring(0,p);
args=url.substring(p+1).split('&');
}
else
a=url;
var regexp=new RegExp('^('+c.replace('[]','\\[\\]')+'\\[?\\]?)(=.*)?$');
var ajouts=[];
var u=(typeof(v)!=='object')?encodeURIComponent(v):v;
var na=[];
var v_read=null;
for(var n=0;n<args.length;n++){
var val=args[n];
try{
val=decodeURIComponent(val);
}catch(e){}
var r=val.match(regexp);
if(r&&r.length){
if(v==null){
if(r[1].substr(-2)=='[]'){
if(!v_read)v_read=[];
v_read.push((r.length>2&&typeof r[2]!=='undefined')?r[2].substring(1):'');
}
else{
return(r.length>2&&typeof r[2]!=='undefined')?r[2].substring(1):'';
}
}
else if(!v.length){
}
else if(r[1].substr(-2)!='[]'){
na.push(r[1]+'='+u);
ajouts.push(r[1]);
}
}
else
na.push(args[n]);
}
if(v==null)return v_read;
if(v||v.length||force_vide){
ajouts="="+ajouts.join("=")+"=";
var all=c.split('|');
for(n=0;n<all.length;n++){
if(ajouts.search("="+all[n]+"=")==-1){
if(typeof(v)!=='object'){
na.push(all[n]+'='+u);
}
else{
var id=((all[n].substring(-2)=='[]')?all[n]:all[n]+"[]");
for(p=0;p<v.length;p++)
na.push(id+'='+encodeURIComponent(v[p]));
}
}
}
}
if(na.length){
if(!sep)sep='&';
a=a+"?"+na.join(sep);
}
return a+ancre;
}
function spip_logo_survol_hover(){
var me=jQuery(this);
if(me.attr('data-src-hover')){
me.attr('data-src-original',me.attr('src'));
me.attr('src',me.attr('data-src-hover'));
}
}
function spip_logo_survol_out(){
var me=jQuery(this);
if(me.attr('data-src-original')){
me.attr('src',me.attr('data-src-original'));
}
}
function disableClickAfterFormSubmit(){
jQuery(this)
.addClass('processing-submitted-form')
.find('button[type="submit"]')
.attr('disabled','disabled')
.addClass('disabled')
}
function puce_enable_survol(){
jQuery('span.puce_objet_popup a',this).not('.puce-survol-enabled').click(function(){
selec_statut(jQuery(this).attr('data-puce-id'),jQuery(this).attr('data-puce-type'),jQuery(this).attr('data-puce-decal'),jQuery('img',this).attr('src'),jQuery(this).attr('data-puce-action'));
return false;
}).addClass('puce-survol-enabled');
jQuery('span.puce_objet',this).not('.puce-survol-enabled').mouseover(function(){
if(!this.puce_loaded){
this.puce_loaded=true;prepare_selec_statut(this,jQuery(this).attr('data-puce-nom'),jQuery(this).attr('data-puce-type'),jQuery(this).attr('data-puce-id'),jQuery(this).attr('data-puce-action'));
}
}).addClass('puce-survol-enabled');
}
if(!window.var_zajax_content)
window.var_zajax_content='contenu';
jQuery(function(){
jQuery('form:not(.bouton_action_post)').parents('div.ajax')
.formulaire_dyn_ajax();
jQuery('div.ajaxbloc').ajaxbloc();
jQuery("input[placeholder]:text").placeholderLabel();
jQuery('.spip_logo_survol').hover(spip_logo_survol_hover,spip_logo_survol_out);
puce_enable_survol.apply(this);
jQuery('body').on('submit','form.bouton_action_post',disableClickAfterFormSubmit);
});
onAjaxLoad(function(){
if(jQuery){
jQuery('form:not(.bouton_action_post)',this).parents('div.ajax')
.formulaire_dyn_ajax();
if(jQuery(this).is('div.ajaxbloc'))
jQuery(this).ajaxbloc();
else if(jQuery(this).closest('div.ajaxbloc').length)
jQuery(this).closest('div.ajaxbloc').ajaxbloc();
else
jQuery('div.ajaxbloc',this).ajaxbloc();
jQuery("input[placeholder]:text",this).placeholderLabel();
jQuery('.spip_logo_survol',this).hover(spip_logo_survol_hover,spip_logo_survol_out);
puce_enable_survol.apply(this);
}
});


/* prive/javascript/js.cookie.js?1649159400 */

;(function(factory){
var registeredInModuleLoader;
if(typeof define==='function'&&define.amd){
define(factory);
registeredInModuleLoader=true;
}
if(typeof exports==='object'){
module.exports=factory();
registeredInModuleLoader=true;
}
if(!registeredInModuleLoader){
var OldCookies=window.Cookies;
var api=window.Cookies=factory();
api.noConflict=function(){
window.Cookies=OldCookies;
return api;
};
}
}(function(){
function extend(){
var i=0;
var result={};
for(;i<arguments.length;i++){
var attributes=arguments[i];
for(var key in attributes){
result[key]=attributes[key];
}
}
return result;
}
function decode(s){
return s.replace(/(%[0-9A-Z]{2})+/g,decodeURIComponent);
}
function init(converter){
function api(){}
function set(key,value,attributes){
if(typeof document==='undefined'){
return;
}
attributes=extend({
path:'/'
},api.defaults,attributes);
if(typeof attributes.expires==='number'){
attributes.expires=new Date(new Date()*1+attributes.expires*864e+5);
}
attributes.expires=attributes.expires?attributes.expires.toUTCString():'';
try{
var result=JSON.stringify(value);
if(/^[\{\[]/.test(result)){
value=result;
}
}catch(e){}
value=converter.write?
converter.write(value,key):
encodeURIComponent(String(value))
.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent);
key=encodeURIComponent(String(key))
.replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent)
.replace(/[\(\)]/g,escape);
var stringifiedAttributes='';
for(var attributeName in attributes){
if(!attributes[attributeName]){
continue;
}
stringifiedAttributes+='; '+attributeName;
if(attributes[attributeName]===true){
continue;
}
stringifiedAttributes+='='+attributes[attributeName].split(';')[0];
}
return(document.cookie=key+'='+value+stringifiedAttributes);
}
function get(key,json){
if(typeof document==='undefined'){
return;
}
var jar={};
var cookies=document.cookie?document.cookie.split('; '):[];
var i=0;
for(;i<cookies.length;i++){
var parts=cookies[i].split('=');
var cookie=parts.slice(1).join('=');
if(!json&&cookie.charAt(0)==='"'){
cookie=cookie.slice(1,-1);
}
try{
var name=decode(parts[0]);
cookie=(converter.read||converter)(cookie,name)||
decode(cookie);
if(json){
try{
cookie=JSON.parse(cookie);
}catch(e){}
}
jar[name]=cookie;
if(key===name){
break;
}
}catch(e){}
}
return key?jar[key]:jar;
}
api.set=set;
api.get=function(key){
return get(key,false);
};
api.getJSON=function(key){
return get(key,true);
};
api.remove=function(key,attributes){
set(key,'',extend(attributes,{
expires:-1
}));
};
api.defaults={};
api.withConverter=init;
return api;
}
return init(function(){});
}));


/* prive/javascript/jquery.cookie.js?1649159400 */

(function(factory){
if(typeof define==='function'&&define.amd){
define(['jquery'],factory);
}else if(typeof exports==='object'){
factory(require('jquery'));
}else{
factory(jQuery);
}
}(function($){
$.cookie=function(key,value,options){
if(value!==undefined&&!$.isFunction(value)){
console.warn("Deprecated jQuery.cookie(). Please use Cookies.set()");
console.trace();
return Cookies.set(key,value,options);
}else{
console.warn("Deprecated jQuery.cookie(). Please use Cookies.get()");
console.trace();
return Cookies.get(key,value);
}
};
$.removeCookie=function(key,options){
console.warn("Deprecated jQuery.removeCookie(). Please use Cookies.remove()");
console.trace();
Cookies.remove(key,options);
return true;
};
}));


/* plugins-dist/mediabox/lib/lity/lity.js?1649159460 */

(function(window,factory){
if(typeof define==='function'&&define.amd){
define(['jquery'],function($){
return factory(window,$);
});
}else if(typeof module==='object'&&typeof module.exports==='object'){
module.exports=factory(window,require('jquery'));
}else{
window.lity=factory(window,window.jQuery||window.Zepto);
}
}(typeof window!=="undefined"?window:this,function(window,$){
'use strict';
var document=window.document;
var _win=$(window);
var _deferred=$.Deferred;
var _html=$('html');
var _instances=[];
var _attrAriaHidden='aria-hidden';
var _dataAriaHidden='lity-'+_attrAriaHidden;
var _focusableElementsSelector='a[href],area[href],input:not([disabled]),select:not([disabled]),textarea:not([disabled]),button:not([disabled]),iframe,object,embed,[contenteditable],[tabindex]:not([tabindex^="-"])';
var _defaultOptions={
esc:true,
handler:null,
handlers:{
image:imageHandler,
inline:inlineHandler,
iframe:iframeHandler
},
forceFocusInside:false,
template:'<div class="lity" role="dialog" aria-label="Dialog Window (Press escape to close)" tabindex="-1"><div class="lity-wrap" data-lity-close role="document"><div class="lity-loader" aria-hidden="true">Loading...</div><div class="lity-container"><div class="lity-content"></div><button class="lity-close" type="button" aria-label="Close (Press escape to close)" data-lity-close>&times;</button></div></div></div>'
};
var _imageRegexp=/(^data:image\/)|(\.(png|jpe?g|gif|svg|webp|bmp|ico|tiff?)(\?\S*)?$)/i;
var _transitionEndEvent=(function(){
var el=document.createElement('div');
var transEndEventNames={
WebkitTransition:'webkitTransitionEnd',
MozTransition:'transitionend',
OTransition:'oTransitionEnd otransitionend',
transition:'transitionend'
};
for(var name in transEndEventNames){
if(el.style[name]!==undefined){
return transEndEventNames[name];
}
}
return false;
})();
function transitionEnd(element){
var deferred=_deferred();
if(!_transitionEndEvent||!element.length){
deferred.resolve();
}else{
element.one(_transitionEndEvent,deferred.resolve);
setTimeout(deferred.resolve,500);
}
return deferred.promise();
}
function settings(currSettings,key,value){
if(arguments.length===1){
return $.extend({},currSettings);
}
if(typeof key==='string'){
if(typeof value==='undefined'){
return typeof currSettings[key]==='undefined'
?null
:currSettings[key];
}
currSettings[key]=value;
}else{
$.extend(currSettings,key);
}
return this;
}
function parseQueryParams(params){
var pos=params.indexOf('?');
if(pos>-1){
params=params.substr(pos+1);
}
var pairs=decodeURI(params.split('#')[0]).split('&');
var obj={},p;
for(var i=0,n=pairs.length;i<n;i++){
if(!pairs[i]){
continue;
}
p=pairs[i].split('=');
obj[p[0]]=p[1];
}
return obj;
}
function appendQueryParams(url,params){
if(!params){
return url;
}
if('string'===$.type(params)){
params=parseQueryParams(params);
}
if(url.indexOf('?')>-1){
var split=url.split('?');
url=split.shift();
params=$.extend(
{},
parseQueryParams(split[0]),
params
)
}
return url+'?'+$.param(params);
}
function transferHash(originalUrl,newUrl){
var pos=originalUrl.indexOf('#');
if(-1===pos){
return newUrl;
}
if(pos>0){
originalUrl=originalUrl.substr(pos);
}
return newUrl+originalUrl;
}
function iframe(iframeUrl,instance,queryParams,hashUrl){
instance&&instance.element().addClass('lity-iframe');
if(queryParams){
iframeUrl=appendQueryParams(iframeUrl,queryParams);
}
if(hashUrl){
iframeUrl=transferHash(hashUrl,iframeUrl);
}
return'<div class="lity-iframe-container"><iframe frameborder="0" allowfullscreen allow="autoplay; fullscreen" src="'+iframeUrl+'"/></div>';
}
function error(msg){
return $('<span class="lity-error"></span>').append(msg);
}
function imageHandler(target,instance){
var desc=(instance.opener()&&instance.opener().data('lity-desc'))||'Image with no description';
var img=$('<img src="'+target+'" alt="'+desc+'"/>');
var deferred=_deferred();
var failed=function(){
deferred.reject(error('Failed loading image'));
};
img
.on('load',function(){
if(this.naturalWidth===0){
return failed();
}
deferred.resolve(img);
})
.on('error',failed)
;
return deferred.promise();
}
imageHandler.test=function(target){
return _imageRegexp.test(target);
};
function inlineHandler(target,instance){
var el,placeholder,hasHideClass;
try{
el=$(target);
}catch(e){
return false;
}
if(!el.length){
return false;
}
placeholder=$('<i style="display:none !important"></i>');
hasHideClass=el.hasClass('lity-hide');
instance
.element()
.one('lity:remove',function(){
placeholder
.before(el)
.remove()
;
if(hasHideClass&&!el.closest('.lity-content').length){
el.addClass('lity-hide');
}
})
;
return el
.removeClass('lity-hide')
.after(placeholder)
;
}
function iframeHandler(target,instance){
return iframe(target,instance);
}
function winHeight(){
return document.documentElement.clientHeight
?document.documentElement.clientHeight
:Math.round(_win.height());
}
function keydown(e){
var current=currentInstance();
if(!current){
return;
}
if(e.keyCode===27&&!!current.options('esc')){
current.close();
}
if(e.keyCode===9){
handleTabKey(e,current);
}
}
function handleTabKey(e,instance){
var focusableElements=instance.element().find(_focusableElementsSelector);
var focusedIndex=focusableElements.index(document.activeElement);
if(e.shiftKey&&focusedIndex<=0){
focusableElements.get(focusableElements.length-1).focus();
e.preventDefault();
}else if(!e.shiftKey&&focusedIndex===focusableElements.length-1){
focusableElements.get(0).focus();
e.preventDefault();
}
if(focusedIndex===-1&&instance.options().forceFocusInside){
focusableElements.get(0).focus();
e.preventDefault();
}
}
function resize(){
$.each(_instances,function(i,instance){
instance.resize();
});
}
function registerInstance(instanceToRegister){
if(1===_instances.unshift(instanceToRegister)){
_html.addClass('lity-active');
_win
.on({
resize:resize,
keydown:keydown
})
;
}
$('body > *').not(instanceToRegister.element())
.addClass('lity-hidden')
.each(function(){
var el=$(this);
if(undefined!==el.data(_dataAriaHidden)){
return;
}
el.data(_dataAriaHidden,el.attr(_attrAriaHidden)||null);
})
.attr(_attrAriaHidden,'true')
;
}
function removeInstance(instanceToRemove){
var show;
instanceToRemove
.element()
.attr(_attrAriaHidden,'true')
;
if(1===_instances.length){
_html.removeClass('lity-active');
_win
.off({
resize:resize,
keydown:keydown
})
;
}
_instances=$.grep(_instances,function(instance){
return instanceToRemove!==instance;
});
if(!!_instances.length){
show=_instances[0].element();
}else{
show=$('.lity-hidden');
}
show
.removeClass('lity-hidden')
.each(function(){
var el=$(this),oldAttr=el.data(_dataAriaHidden);
if(!oldAttr){
el.removeAttr(_attrAriaHidden);
}else{
el.attr(_attrAriaHidden,oldAttr);
}
el.removeData(_dataAriaHidden);
})
;
}
function currentInstance(){
if(0===_instances.length){
return null;
}
return _instances[0];
}
function factory(target,instance,handlers,preferredHandler){
var handler='inline',content;
var currentHandlers=$.extend({},handlers);
if(preferredHandler&&currentHandlers[preferredHandler]){
content=currentHandlers[preferredHandler](target,instance);
handler=preferredHandler;
}else{
$.each(['inline','iframe'],function(i,name){
delete currentHandlers[name];
currentHandlers[name]=handlers[name];
});
$.each(currentHandlers,function(name,currentHandler){
if(!currentHandler){
return true;
}
if(
currentHandler.test&&
!currentHandler.test(target,instance)
){
return true;
}
content=currentHandler(target,instance);
if(false!==content){
handler=name;
return false;
}
});
}
return{handler:handler,content:content||''};
}
function Lity(target,options,opener,activeElement){
var self=this;
var result;
var isReady=false;
var isClosed=false;
var element;
var content;
options=$.extend(
{},
_defaultOptions,
options
);
element=$(options.template);
self.element=function(){
return element;
};
self.opener=function(){
return opener;
};
self.content=function(){
return content;
};
self.options=$.proxy(settings,self,options);
self.handlers=$.proxy(settings,self,options.handlers);
self.resize=function(){
if(!isReady||isClosed){
return;
}
content
.css('max-height',winHeight()+'px')
.trigger('lity:resize',[self])
;
};
self.close=function(){
if(isClosed){
return;
}
isClosed=true;
removeInstance(self);
var deferred=_deferred();
if(
activeElement&&
(
document.activeElement===element[0]||
$.contains(element[0],document.activeElement)
)
){
try{
activeElement.focus();
}catch(e){
}
}
var trigerable=(content?content:element);
trigerable.trigger('lity:close',[self]);
element
.removeClass('lity-opened')
.addClass('lity-closed')
;
transitionEnd(trigerable.add(element))
.always(function(){
trigerable.trigger('lity:remove',[self]);
element.remove();
element=undefined;
deferred.resolve();
})
;
return deferred.promise();
};
result=factory(target,self,options.handlers,options.handler);
element
.attr(_attrAriaHidden,'false')
.addClass('lity-loading lity-opened lity-'+result.handler)
.appendTo('body')
.focus()
.on('click','[data-lity-close]',function(e){
if($(e.target).is('[data-lity-close]')){
self.close();
}
})
.trigger('lity:open',[self])
;
registerInstance(self);
$.when(result.content)
.always(ready)
;
function ready(result){
if(isClosed){
return;
}
content=$(result)
.css('max-height',winHeight()+'px')
;
element
.find('.lity-loader')
.each(function(){
var loader=$(this);
transitionEnd(loader)
.always(function(){
loader.remove();
})
;
})
;
element
.removeClass('lity-loading')
.find('.lity-content')
.empty()
.append(content)
;
isReady=true;
content
.trigger('lity:ready',[self])
;
}
}
function lity(target,options,opener){
if(!target.preventDefault){
opener=$(opener);
}else{
target.preventDefault();
opener=$(this);
target=opener.data('lity-target')||opener.attr('href')||opener.attr('src');
}
var instance=new Lity(
target,
$.extend(
{},
opener.data('lity-options')||opener.data('lity'),
options
),
opener,
document.activeElement
);
if(!target.preventDefault){
return instance;
}
}
lity.version='3.0.0-dev';
lity.options=$.proxy(settings,lity,_defaultOptions);
lity.handlers=$.proxy(settings,lity,_defaultOptions.handlers);
lity.current=currentInstance;
lity.iframe=iframe;
$(document).on('click.lity','[data-lity]',lity);
return lity;
}));


/* plugins-dist/mediabox/lity/js/lity.mediabox.js?1649159460 */
;
(function($){
var litySpip={
nameSpace:'mediabox',
config:{
forceFocusInside:true,
},
strings:{
},
callbacks:{
onOpen:[],
onShow:[],
onClose:[]
},
focusedItem:[],
isTransition:false,
adjustHeight:function(instance){
var $content=instance.content();
var $containerHeight=instance.element().find('.lity-container').height();
if($containerHeight){
var h=Math.round($containerHeight)+'px';
$content
.css('max-height',h)
.find('[data-'+litySpip.nameSpace+'-force-max-height]')
.css('max-height',h);
}
},
template:function(cfg,type,groupName,groupPosition,groupLength){
var className='';
if(!!cfg.className){
className=' '+cfg.className;
}
if(cfg.transitionOnOpen){
className+=' lity-transition-on-open-'+cfg.transitionOnOpen;
}
if(!!cfg.noTransition){
className+=' lity-no-transition';
}
if(!!cfg.slideShow){
className+=' lity-slideshow';
}
var button_next_prev='',
button_start_stop='',
group_info_text='',
group_info='';
if(groupName&&groupLength){
if(groupLength>1){
var newPosition=(groupPosition<=0?groupLength-1:groupPosition-1);
button_next_prev+='<button class="lity-previous" type="button" data-group-name="'+groupName+'" data-group-position="'+newPosition+'" aria-label="'+litySpip.strings.previous+'" data-lity-previous'
+'><b title="'+litySpip.strings.previous+'">❮</b></button>';
newPosition=(groupPosition>=groupLength-1?0:groupPosition+1);
button_next_prev+='<button class="lity-next" type="button" data-group-name="'+groupName+'" data-group-position="'+newPosition+'"  aria-label="'+litySpip.strings.next+'" data-lity-next'
+'><b title="'+litySpip.strings.next+'">❯</b></button>';
}
group_info_text=" "+litySpip.strings.current;
group_info_text=group_info_text.replace('{current}',(groupPosition+1)+'');
group_info_text=group_info_text.replace('{total}',groupLength+'');
button_start_stop+='<button class="lity-start-stop" type="button" data-lity-stop-start>'
+'<b class="lity-start" aria-label="'+litySpip.strings.slideshowStart+'" title="'+litySpip.strings.slideshowStart+'">▶</b>'
+'<b class="lity-stop" aria-label="'+litySpip.strings.slideshowStop+'" title="'+litySpip.strings.slideshowStop+'">□</b>'
+'</button>';
group_info='<div class="lity-group-caption">'
+'<span class="lity-group-start-stop">'+button_start_stop+'</span>'
+'<span class="lity-group-current">'+group_info_text+'</span>'
+'<span class="lity-group-next-prev">'+button_next_prev+'</span>'
+'<span class="lity-group-progress-bar"><span class="lity-group-progress-bar-status" style="width:0"></span></span>'
+'</div>';
}
var close_button_aria_label=litySpip.strings.close+' ('+litySpip.strings.press_escape+')';
var dialog_title=(type==='image'?litySpip.strings.dialog_title_med:litySpip.strings.dialog_title_def);
dialog_title+=group_info_text+' ('+litySpip.strings.press_escape+')';
var t=
'<dialog class="box_mediabox box_modalbox lity'+className+'" role="dialog" aria-label="'+dialog_title+'" tabindex="-1">'
+'<div class="lity-wrap" data-lity-close role="document">'
+'<div class="lity-loader" aria-hidden="true" aria-label="'+litySpip.strings.loading+'"><span class="box-loading"></span></div>'
+'<div class="lity-container">'
+'<button class="lity-close" type="button" aria-label="'+close_button_aria_label+'" data-lity-close><b data-lity-close title="'+litySpip.strings.close+'">&times;</b></button>'
+'<div class="lity-content"></div>'
+group_info
+'</div>'
+'</div>'
+'</dialog>';
return t;
},
failHandler:function(target,instance){
return'<div class="error lity-error">Failed loading content</div>';
},
ajaxHandler:function(target,instance){
var cache=instance.opener().data('lity-ajax-cache')||{};
if(cache[target]){
return $('<div class="lity-content-inner"></div>').append(cache[target]);
}
var _deferred=$.Deferred;
var deferred=_deferred();
var failed=function(){
deferred.reject($('<span class="error lity-error"></span>').append('Failed loading ajax'));
};
$.get(target)
.done(function(content){
cache[target]=content;
instance.opener().data('lity-ajax-cache',cache);
deferred.resolve($('<div class="lity-content-inner"></div>').append(content));
})
.fail(failed);
return deferred.promise();
},
imageBuildContent:function(img,desc,longdesc){
img.attr('alt',desc?desc:'');
desc=(longdesc?longdesc:desc);
if(desc){
var id='lity-image-caption-'+Date.now().toString(36)+Math.random().toString(36).substr(2);
img.attr('aria-describedby',id);
img=$('<figure class="lity-image-figure"></figure>').append(img).append('<figcaption id="'+id+'" class="lity-image-caption">'+desc+'</figcaption>');
}
else{
img=$('<figure class="lity-image-figure"></figure>').append(img);
}
return img;
},
imageHandler:function(target,instance){
var _deferred=$.Deferred;
var desc='';
var longdesc='';
var opener=instance.opener();
if(opener){
desc=opener.attr('title')||$('img[alt]',opener).eq(0).attr('alt')||'';
var by=opener.attr('aria-describedby')||$('img[aria-describedby]',opener).eq(0).attr('aria-describedby')||'';
if(by){
longdesc=$('#'+by).html();
longdesc=longdesc.trim();
}
if(!desc){
desc=desc||instance.opener().attr('aria-label')||'';
}
desc=desc.trim();
}
var img;
var cache=opener.data('lity-image-cache')||{};
if(cache[target]){
img=cache[target];
img=litySpip.imageBuildContent(img,desc,longdesc);
return img;
}
img=$('<img src="'+target+'" class="lity-image-img" data-'+litySpip.nameSpace+'-force-max-height />');
var deferred=_deferred();
var failed=function(){
deferred.reject($('<span class="error lity-error"></span>').append('Failed loading image'));
};
img
.on('load',function(){
if(this.naturalWidth===0){
img.attr("style","width:25vw;");
}
cache[target]=img;
opener.data('lity-image-cache',cache);
deferred.resolve(litySpip.imageBuildContent(img,desc,longdesc));
})
.on('error',failed)
;
return deferred.promise();
},
groupElements:function(groupName){
return $('.lity-enabled[data-'+litySpip.nameSpace+'-group'+'='+groupName+']');
},
eventSet:{},
setEvents:function(what){
if(!litySpip.eventSet[what]){
switch(what){
case'opener':
$(document).on('click','.lity-enabled',litySpip.onClickOpener);
break;
case'listener':
$(document).on('click','.lity-previous,.lity-next',litySpip.onPrevNext);
$(document).on('click','.lity-start-stop',litySpip.onSlideShowToggle);
$(window).on('keyup',litySpip.onKeyUp);
break;
}
litySpip.eventSet[what]=true;
}
},
onKeyUp:function(event){
var c={37:"previous",39:"next"}[event.keyCode];
if(c){
var current=lity.current();
if(current){
jQuery('.lity-'+c,current.element()).trigger('click');
}
}
},
openerFromPrevNext($button){
var groupName=$button.data('group-name');
var groupPosition=$button.data('group-position');
return litySpip.groupElements(groupName).eq(groupPosition);
},
onPrevNext:function(event){
var $button=$(this);
var newEl=litySpip.openerFromPrevNext($button);
if(newEl){
var element=lity.current().element();
litySpip.isTransition={oldClosed:false,newOpened:true};
element.addClass('lity-no-transition').css('visibility','hidden');
litySpip.slideshowStop(element);
var options={transitionOnOpen:$button.is('.lity-next')?'slide-from-right':'slide-from-left'};
if(element.is('.lity-slideshow')){
options.slideShow=true;
}
lity.current().close();
litySpip.elementOpener(newEl,options);
}
},
slideshowStart:function(element){
var $progress=element.find('.lity-group-progress-bar-status');
$progress.attr('style','');
$progress.css('width','100%');
var delay=litySpip.config.slideshowSpeed;
setTimeout(function(){
$progress.css('transition','width '+((delay-200)/1000)+'s linear');
$progress.css('width','0');
},200);
timer=setTimeout(function(){element.find('.lity-next').trigger('click')},delay);
element.data('lity-timer',timer);
$('.lity-start',element).attr('aria-hidden','true');
$('.lity-stop',element).attr('aria-hidden','false');
jQuery('.lity-start-stop',element).focus();
},
slideshowStop:function(element){
timer=element.data('lity-timer');
if(timer){
clearTimeout(timer);
}
},
onSlideShowToggle:function(event){
var $button=$(this);
var element=$button.closest('.lity');
var slideShowState=(element.is('.lity-slideshow')?true:false);
var timer;
if(!slideShowState){
litySpip.slideshowStart(element);
}
else{
litySpip.slideshowStop(element);
$('.lity-start',element).attr('aria-hidden','false');
$('.lity-stop',element).attr('aria-hidden','true');
}
element.toggleClass('lity-slideshow');
},
onClickOpener:function(event){
event.preventDefault();
var opener=$(this);
litySpip.elementOpener(opener);
},
elementOpener:function(opener,options){
var cfg=opener.data('mediabox-options');
if(options){
cfg=$.extend({},cfg,options);
}
var target=opener.data('href-popin')||opener.data('href')||opener.attr('href')||opener.attr('src');
litySpip.lityOpener(target,cfg,opener.get(0));
},
lityOpener:function(target,cfg,opener){
litySpip.setEvents('listener');
if(!litySpip.isTransition){
litySpip.callbacks.onOpen.push(cfg.onOpen||false);
litySpip.callbacks.onShow.push(cfg.onShow||false);
litySpip.callbacks.onClose.push(cfg.onClose||false);
litySpip.focusedItem.push($(document.activeElement));
}
var type=cfg.type||'';
if(!type&&opener){
type=$(opener).data(litySpip.nameSpace+'-type')||'';
}
var handlers=lity.handlers();
if(type==='ajax'){
handlers.ajax=litySpip.ajaxHandler;
}
handlers.image=litySpip.imageHandler;
if(type==='inline'){
var el=[];
try{
el=$(target);
}catch(e){
el=[];
}
if(!el.length){
handlers.inline=litySpip.failHandler;
}
}
cfg=$.extend({handlers:handlers},cfg);
if(type&&['image','inline','ajax','iframe'].indexOf(type)!==-1){
cfg.handler=type;
}
if(!!cfg.preloadOnly){
litySpip.lityPreLoader(target,cfg,opener);
}
else{
var groupPosition=0;
var groupLength=0;
if(opener){
var groupName=cfg.rel||(opener?$(opener).data(litySpip.nameSpace+'-group'):'');
if(groupName){
var elements=litySpip.groupElements(groupName);
groupPosition=elements.index($(opener));
groupLength=elements.length;
}
}
cfg=$.extend({template:litySpip.template(cfg,type,groupName,groupPosition,groupLength)},cfg);
lity(target,cfg,opener);
}
},
lityPreLoader:function(target,cfg,opener){
if(cfg.handler&&cfg.handlers[cfg.handler]){
if(cfg.handler==='image'||cfg.handler==='ajax'){
var instance={
opener:function(){return $(opener);}
};
var content=cfg.handlers[cfg.handler](target,instance);
}
}
}
}
jQuery.fn.extend({
mediabox:function(options){
var cfg=$.extend({},litySpip.config,options);
if(this===jQuery.fn){
var href=cfg.href||"";
litySpip.lityOpener(href,cfg,null);
return this;
}else{
if(cfg.rel){
this.attr('data-'+litySpip.nameSpace+'-group',cfg.rel);
}
else{
this.each(function(){
var rel=$(this).attr('rel');
if(rel){
$(this).attr('data-'+litySpip.nameSpace+'-group',rel);
}
});
}
litySpip.setEvents('opener');
this
.data('mediabox-options',cfg)
.addClass('lity-enabled');
(cfg.preload?this:this.filter('[data-'+litySpip.nameSpace+'-preload]')).each(function(){
litySpip.elementOpener($(this),{preloadOnly:true});
});
return this;
}
},
mediaboxClose:function(){
var $current=lity.current();
if($current){
$current.close();
return true;
}
return false;
}
});
var initConfig=function(){
var b=typeof(mediabox_settings)=='object'?mediabox_settings:{};
litySpip.nameSpace=b.ns?b.ns:'mediabox';
litySpip.strings.slideshowStart=b.str_ssStart;
litySpip.strings.slideshowStop=b.str_ssStop;
litySpip.strings.current=b.str_cur;
litySpip.strings.previous=b.str_prev;
litySpip.strings.next=b.str_next;
litySpip.strings.close=b.str_close;
litySpip.strings.loading=b.str_loading;
litySpip.strings.press_escape=b.str_petc;
litySpip.strings.dialog_title_def=b.str_dialTitDef;
litySpip.strings.dialog_title_med=b.str_dialTitMed;
litySpip.config.slideshowSpeed=(b.lity.slideshow_speed?b.lity.slideshow_speed:5000);
var styles={
'container':'',
'content':'',
};
if(b.lity.minWidth){
styles.content+="min-width:"+b.lity.minWidth.replace('%','vw')+';';
}
if(b.lity.maxWidth){
styles.container+="max-width:"+b.lity.maxWidth.replace('%','vw')+';';
}
if(b.lity.minHeight){
styles.content+="min-height:"+b.lity.minHeight.replace('%','vh')+';';
}
if(b.lity.maxHeight){
styles.container+="max-height:"+b.lity.maxHeight.replace('%','vh')+';';
}
var insert='';
for(let key in styles){
if(styles[key]){
insert+='.box_mediabox .lity-'+key+'{'+styles[key]+'}';
}
}
if(b.lity.opacite){
insert+='.box_mediabox:before{opacity:'+b.lity.opacite+'}';
}
if(insert){
$('head').append($('<style type="text/css">'+insert+'</style>'));
}
$(document).on('lity:open',function(event,instance){
if(!instance.element().is('.lity-slideshow')){
jQuery('.lity-close',instance.element()).focus();
}
if(!litySpip.isTransition){
var callback=litySpip.callbacks.onOpen.pop();
if(callback){
callback(event,instance);
}
}
});
$(document).on('lity:ready',function(event,instance){
litySpip.adjustHeight(instance);
if(jQuery.spip){
jQuery.spip.triggerAjaxLoad(instance.element().get(0))
}
if(instance.element().addClass('lity-ready').is('.lity-slideshow')){
litySpip.slideshowStart(instance.element());
}
if(!litySpip.isTransition){
var callback=litySpip.callbacks.onShow.pop();
if(callback){
callback(event,instance);
}
}
else{
litySpip.isTransition.newOpened=true;
if(litySpip.isTransition.oldClosed){
litySpip.isTransition=false;
}
instance.content().trigger('lity:resize',[instance]);
}
var $next=instance.element().find('.lity-next');
if($next.length){
$next=litySpip.openerFromPrevNext($next);
if($next){
litySpip.elementOpener($next,{preloadOnly:true});
}
}
});
$(document).on('lity:close',function(event,instance){
if(!litySpip.isTransition){
if(litySpip.callbacks.onShow.length>litySpip.callbacks.onOpen.length){
litySpip.callbacks.onShow.pop();
}
var callback=litySpip.callbacks.onClose.pop();
if(callback){
callback(event,instance);
}
}
});
$(document).on('lity:remove',function(event,instance){
if(!litySpip.isTransition){
var focused=litySpip.focusedItem.pop();
if(focused){
try{
focused.focus();
}catch(e){
}
}
}
else{
litySpip.isTransition.oldClosed=true;
if(litySpip.isTransition.newOpened){
litySpip.isTransition=false;
}
}
});
$(document).on('lity:resize',function(event,instance){
litySpip.adjustHeight(instance);
});
}
initConfig();
})(jQuery);


/* plugins-dist/mediabox/javascript/spip.mediabox.js?1649159460 */
(function($){
$.mediabox
=function(options){
if(typeof(mediabox_autodetect_href)=='function'
&&options.href
&&!options.type){
options.type=mediabox_autodetect_href(options.href);
}
$.fn.mediabox(options);
};
$.modalbox=function(href,options){$.mediabox($.extend({href:href},options));};
$.modalboxclose=$.mediaboxClose=$.fn.mediaboxClose;
$.modalboxload=function(href,options){
options=$.extend({href:href,overlayClose:true},options);
$.mediabox(options);
};
})(jQuery);
var mediaboxInit=function(){
var b=$.extend({},mediabox_settings);
if(b.ns!=='box'){
$('[data-box-type]').each(function(i,e){
var $e=$(e);
var d=$e.attr('data-box-type');
$e.removeAttr('data-box-type').attr('data-'+b.ns+'-type',d);
});
}
if(b.auto_detect){
var $popins=$('[data-href-popin],[data-'+b.ns+'-type]'+(window.var_zajax_content?',[data-var_zajax],a.popin':''))
.add(b.sel_c)
.not(ajaxbloc_selecteur,'.ajaxbloc','.hasbox');
$popins.each(function(i,e){
var $e=$(e);
var url=$e.attr('href')||"./";
var popin=$e.attr('data-href-popin');
var type=$e.attr('data-'+b.ns+'-type')||'';
if(!popin){
var env=$e.attr('data-ajax-env');
if(env){
url=parametre_url(parametre_url(url,'var_ajax',1),'var_ajax_env',env)
$e.removeAttr('data-ajax-env');
type='ajax';
}
var za=$e.attr('data-var_zajax');
if(za!==undefined||$e.hasClass('popin')){
if(za==='content'&&window.var_zajax_content){
za=window.var_zajax_content;
}
url=parametre_url(url,'var_zajax',za||window.var_zajax_content);
$e.removeAttr('data-var_zajax');
type='ajax';
}
if((url!==$e.attr('href')||(!popin&&type))&&url!=="./"){
$e.attr('data-href-popin',url);
}
}
url=popin||url;
if(!type){
var attrs=$e.data();
var types=['inline','html','iframe','image','ajax'];
types.some(function(e){
var k=b.ns+e.charAt(0).toUpperCase()+e.slice(1);
if(attrs[k]){
type=e;
return true;
}
});
}
if(!type&&typeof(mediabox_autodetect_href)=='function'){
type=mediabox_autodetect_href(url);
}
if(!type||$e.hasClass('boxIframe')){
type="iframe";
$e.removeClass('boxIframe');
}
$e.attr('data-'+b.ns+'-type',type);
});
}
if(b.sel_g){
var $items=$();
$(b.sel_g).each(function(i,e){
$items=$items.add(
$(this).is('a[type^=\'image\']')
?$(this)
:$(this).find('a[type^=\'image\']')
);
});
$items
.not('.hasbox')
.removeAttr('onclick')
.mediabox({rel:'galerieauto',slideshow:true,slideshowAuto:false,type:'image'})
.addClass('hasbox');
}
if(b.tt_img){
$('a[type^="image"],a[href$=".png"],a[href$=".jpg"],a[href$=".jpeg"],a[href$=".svg"]')
.not('.hasbox')
.removeAttr('onclick')
.mediabox({type:'image'})
.addClass('hasbox')
;
}
$(b.sel_c)
.not('.hasbox')
.mediabox()
.addClass('hasbox')
;
$('[data-href-popin]',this)
.not('.hasbox')
.click(function(){if($.modalbox)$.modalbox($(this).attr('data-href-popin'),{type:$(this).attr('data-'+b.ns+'-type')});return false;})
.addClass('hasbox');
};
(function($){
if(typeof onAjaxLoad=='function')onAjaxLoad(mediaboxInit);
$(mediaboxInit);
})(jQuery);
function mediabox_autodetect_href(href,options){
options=$.extend({
breakMode:true,
tests:{
image:/\.(gif|png|jp(e|g|eg)|bmp|ico|webp|jxr|svg)((#|\?).*)?$/i,
inline:/^[#.]\w/,
html:/^\s*<[\w!][^<]*>/,
ajax:/((\?|&(amp;)?)var_z?ajax=|cache-ajaxload\/)/i,
iframe:/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i,
},
},options);
var matched=options.breakMode?false:[];
$.each(options.tests,function(type,regex){
if(href.match(regex)){
if(options.breakMode){
matched=type;
return false;
}else{
matched.push(type);
}
}
});
return matched;
};


/* plugins/auto/tablesorter/v2.1.3/javascript/jquery.tablesorter.min.js?1649159586 */
!function(e){"function"==typeof define&&define.amd?define(["jquery"],e):"object"==typeof module&&"object"==typeof module.exports?module.exports=e(require("jquery")):e(jQuery)}(function(e){return function(A){"use strict";var L=A.tablesorter={version:"2.31.1",parsers:[],widgets:[],defaults:{theme:"default",widthFixed:!1,showProcessing:!1,headerTemplate:"{content}",onRenderTemplate:null,onRenderHeader:null,cancelSelection:!0,tabIndex:!0,dateFormat:"mmddyyyy",sortMultiSortKey:"shiftKey",sortResetKey:"ctrlKey",usNumberFormat:!0,delayInit:!1,serverSideSorting:!1,resort:!0,headers:{},ignoreCase:!0,sortForce:null,sortList:[],sortAppend:null,sortStable:!1,sortInitialOrder:"asc",sortLocaleCompare:!1,sortReset:!1,sortRestart:!1,emptyTo:"bottom",stringTo:"max",duplicateSpan:!0,textExtraction:"basic",textAttribute:"data-text",textSorter:null,numberSorter:null,initWidgets:!0,widgetClass:"widget-{name}",widgets:[],widgetOptions:{zebra:["even","odd"]},initialized:null,tableClass:"",cssAsc:"",cssDesc:"",cssNone:"",cssHeader:"",cssHeaderRow:"",cssProcessing:"",cssChildRow:"tablesorter-childRow",cssInfoBlock:"tablesorter-infoOnly",cssNoSort:"tablesorter-noSort",cssIgnoreRow:"tablesorter-ignoreRow",cssIcon:"tablesorter-icon",cssIconNone:"",cssIconAsc:"",cssIconDesc:"",cssIconDisabled:"",pointerClick:"click",pointerDown:"mousedown",pointerUp:"mouseup",selectorHeaders:"> thead th, > thead td",selectorSort:"th, td",selectorRemove:".remove-me",debug:!1,headerList:[],empties:{},strings:{},parsers:[],globalize:0,imgAttr:0},css:{table:"tablesorter",cssHasChild:"tablesorter-hasChildRow",childRow:"tablesorter-childRow",colgroup:"tablesorter-colgroup",header:"tablesorter-header",headerRow:"tablesorter-headerRow",headerIn:"tablesorter-header-inner",icon:"tablesorter-icon",processing:"tablesorter-processing",sortAsc:"tablesorter-headerAsc",sortDesc:"tablesorter-headerDesc",sortNone:"tablesorter-headerUnSorted"},language:{sortAsc:"Ascending sort applied, ",sortDesc:"Descending sort applied, ",sortNone:"No sort applied, ",sortDisabled:"sorting is disabled",nextAsc:"activate to apply an ascending sort",nextDesc:"activate to apply a descending sort",nextNone:"activate to remove the sort"},regex:{templateContent:/\{content\}/g,templateIcon:/\{icon\}/g,templateName:/\{name\}/i,spaces:/\s+/g,nonWord:/\W/g,formElements:/(input|select|button|textarea)/i,chunk:/(^([+\-]?(?:\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)?$|^0x[0-9a-f]+$|\d+)/gi,chunks:/(^\\0|\\0$)/,hex:/^0x[0-9a-f]+$/i,comma:/,/g,digitNonUS:/[\s|\.]/g,digitNegativeTest:/^\s*\([.\d]+\)/,digitNegativeReplace:/^\s*\(([.\d]+)\)/,digitTest:/^[\-+(]?\d+[)]?$/,digitReplace:/[,.'"\s]/g},string:{max:1,min:-1,emptymin:1,emptymax:-1,zero:0,none:0,"null":0,top:!0,bottom:!1},keyCodes:{enter:13},dates:{},instanceMethods:{},setup:function(t,r){if(t&&t.tHead&&0!==t.tBodies.length&&!0!==t.hasInitialized){var e,o="",s=A(t),a=A.metadata;t.hasInitialized=!1,t.isProcessing=!0,t.config=r,A.data(t,"tablesorter",r),L.debug(r,"core")&&(console[console.group?"group":"log"]("Initializing tablesorter v"+L.version),A.data(t,"startoveralltimer",new Date)),r.supportsDataObject=((e=A.fn.jquery.split("."))[0]=parseInt(e[0],10),1<e[0]||1===e[0]&&4<=parseInt(e[1],10)),r.emptyTo=r.emptyTo.toLowerCase(),r.stringTo=r.stringTo.toLowerCase(),r.last={sortList:[],clickedIndex:-1},/tablesorter\-/.test(s.attr("class"))||(o=""!==r.theme?" tablesorter-"+r.theme:""),r.namespace?r.namespace="."+r.namespace.replace(L.regex.nonWord,""):r.namespace=".tablesorter"+Math.random().toString(16).slice(2),r.table=t,r.$table=s.addClass(L.css.table+" "+r.tableClass+o+" "+r.namespace.slice(1)).attr("role","grid"),r.$headers=s.find(r.selectorHeaders),r.$table.children().children("tr").attr("role","row"),r.$tbodies=s.children("tbody:not(."+r.cssInfoBlock+")").attr({"aria-live":"polite","aria-relevant":"all"}),r.$table.children("caption").length&&((o=r.$table.children("caption")[0]).id||(o.id=r.namespace.slice(1)+"caption"),r.$table.attr("aria-labelledby",o.id)),r.widgetInit={},r.textExtraction=r.$table.attr("data-text-extraction")||r.textExtraction||"basic",L.buildHeaders(r),L.fixColumnWidth(t),L.addWidgetFromClass(t),L.applyWidgetOptions(t),L.setupParsers(r),r.totalRows=0,r.debug&&L.validateOptions(r),r.delayInit||L.buildCache(r),L.bindEvents(t,r.$headers,!0),L.bindMethods(r),r.supportsDataObject&&void 0!==s.data().sortlist?r.sortList=s.data().sortlist:a&&s.metadata()&&s.metadata().sortlist&&(r.sortList=s.metadata().sortlist),L.applyWidget(t,!0),0<r.sortList.length?(r.last.sortList=r.sortList,L.sortOn(r,r.sortList,{},!r.initWidgets)):(L.setHeadersCss(r),r.initWidgets&&L.applyWidget(t,!1)),r.showProcessing&&s.unbind("sortBegin"+r.namespace+" sortEnd"+r.namespace).bind("sortBegin"+r.namespace+" sortEnd"+r.namespace,function(e){clearTimeout(r.timerProcessing),L.isProcessing(t),"sortBegin"===e.type&&(r.timerProcessing=setTimeout(function(){L.isProcessing(t,!0)},500))}),t.hasInitialized=!0,t.isProcessing=!1,L.debug(r,"core")&&(console.log("Overall initialization time:"+L.benchmark(A.data(t,"startoveralltimer"))),L.debug(r,"core")&&console.groupEnd&&console.groupEnd()),s.triggerHandler("tablesorter-initialized",t),"function"==typeof r.initialized&&r.initialized(t)}else L.debug(r,"core")&&(t.hasInitialized?console.warn("Stopping initialization. Tablesorter has already been initialized"):console.error("Stopping initialization! No table, thead or tbody",t))},bindMethods:function(r){var e=r.$table,t=r.namespace,o="sortReset update updateRows updateAll updateHeaders addRows updateCell updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets destroy mouseup mouseleave ".split(" ").join(t+" ");e.unbind(o.replace(L.regex.spaces," ")).bind("sortReset"+t,function(e,t){e.stopPropagation(),L.sortReset(this.config,function(e){e.isApplyingWidgets?setTimeout(function(){L.applyWidget(e,"",t)},100):L.applyWidget(e,"",t)})}).bind("updateAll"+t,function(e,t,r){e.stopPropagation(),L.updateAll(this.config,t,r)}).bind("update"+t+" updateRows"+t,function(e,t,r){e.stopPropagation(),L.update(this.config,t,r)}).bind("updateHeaders"+t,function(e,t){e.stopPropagation(),L.updateHeaders(this.config,t)}).bind("updateCell"+t,function(e,t,r,o){e.stopPropagation(),L.updateCell(this.config,t,r,o)}).bind("addRows"+t,function(e,t,r,o){e.stopPropagation(),L.addRows(this.config,t,r,o)}).bind("updateComplete"+t,function(){this.isUpdating=!1}).bind("sorton"+t,function(e,t,r,o){e.stopPropagation(),L.sortOn(this.config,t,r,o)}).bind("appendCache"+t,function(e,t,r){e.stopPropagation(),L.appendCache(this.config,r),A.isFunction(t)&&t(this)}).bind("updateCache"+t,function(e,t,r){e.stopPropagation(),L.updateCache(this.config,t,r)}).bind("applyWidgetId"+t,function(e,t){e.stopPropagation(),L.applyWidgetId(this,t)}).bind("applyWidgets"+t,function(e,t){e.stopPropagation(),L.applyWidget(this,!1,t)}).bind("refreshWidgets"+t,function(e,t,r){e.stopPropagation(),L.refreshWidgets(this,t,r)}).bind("removeWidget"+t,function(e,t,r){e.stopPropagation(),L.removeWidget(this,t,r)}).bind("destroy"+t,function(e,t,r){e.stopPropagation(),L.destroy(this,t,r)}).bind("resetToLoadState"+t,function(e){e.stopPropagation(),L.removeWidget(this,!0,!1);var t=A.extend(!0,{},r.originalSettings);(r=A.extend(!0,{},L.defaults,t)).originalSettings=t,this.hasInitialized=!1,L.setup(this,r)})},bindEvents:function(e,t,r){var o,i=(e=A(e)[0]).config,s=i.namespace,d=null;!0!==r&&(t.addClass(s.slice(1)+"_extra_headers"),(o=L.getClosest(t,"table")).length&&"TABLE"===o[0].nodeName&&o[0]!==e&&A(o[0]).addClass(s.slice(1)+"_extra_table")),o=(i.pointerDown+" "+i.pointerUp+" "+i.pointerClick+" sort keyup ").replace(L.regex.spaces," ").split(" ").join(s+" "),t.find(i.selectorSort).add(t.filter(i.selectorSort)).unbind(o).bind(o,function(e,t){var r,o,s,a=A(e.target),n=" "+e.type+" ";if(!(1!==(e.which||e.button)&&!n.match(" "+i.pointerClick+" | sort | keyup ")||" keyup "===n&&e.which!==L.keyCodes.enter||n.match(" "+i.pointerClick+" ")&&void 0!==e.which||n.match(" "+i.pointerUp+" ")&&d!==e.target&&!0!==t)){if(n.match(" "+i.pointerDown+" "))return d=e.target,void("1"===(s=a.jquery.split("."))[0]&&s[1]<4&&e.preventDefault());if(d=null,r=L.getClosest(A(this),"."+L.css.header),L.regex.formElements.test(e.target.nodeName)||a.hasClass(i.cssNoSort)||0<a.parents("."+i.cssNoSort).length||r.hasClass("sorter-false")||0<a.parents("button").length)return!i.cancelSelection;i.delayInit&&L.isEmptyObject(i.cache)&&L.buildCache(i),i.last.clickedIndex=r.attr("data-column")||r.index(),(o=i.$headerIndexed[i.last.clickedIndex][0])&&!o.sortDisabled&&L.initSort(i,o,e)}}),i.cancelSelection&&t.attr("unselectable","on").bind("selectstart",!1).css({"user-select":"none",MozUserSelect:"none"})},buildHeaders:function(d){var e,l,t,r;for(d.headerList=[],d.headerContent=[],d.sortVars=[],L.debug(d,"core")&&(t=new Date),d.columns=L.computeColumnIndex(d.$table.children("thead, tfoot").children("tr")),l=d.cssIcon?'<i class="'+(d.cssIcon===L.css.icon?L.css.icon:d.cssIcon+""+L.css.icon)+'"></i>':"",d.$headers=A(A.map(d.$table.find(d.selectorHeaders),function(e,t){var r,o,s,a,n,i=A(e);if(!L.getClosest(i,"tr").hasClass(d.cssIgnoreRow))return/(th|td)/i.test(e.nodeName)||(n=L.getClosest(i,"th, td"),i.attr("data-column",n.attr("data-column"))),r=L.getColumnData(d.table,d.headers,t,!0),d.headerContent[t]=i.html(),""===d.headerTemplate||i.find("."+L.css.headerIn).length||(a=d.headerTemplate.replace(L.regex.templateContent,i.html()).replace(L.regex.templateIcon,i.find("."+L.css.icon).length?"":l),d.onRenderTemplate&&(o=d.onRenderTemplate.apply(i,[t,a]))&&"string"==typeof o&&(a=o),i.html('<div class="'+L.css.headerIn+'">'+a+"</div>")),d.onRenderHeader&&d.onRenderHeader.apply(i,[t,d,d.$table]),s=parseInt(i.attr("data-column"),10),e.column=s,n=L.getOrder(L.getData(i,r,"sortInitialOrder")||d.sortInitialOrder),d.sortVars[s]={count:-1,order:n?d.sortReset?[1,0,2]:[1,0]:d.sortReset?[0,1,2]:[0,1],lockedOrder:!1,sortedBy:""},void 0!==(n=L.getData(i,r,"lockedOrder")||!1)&&!1!==n&&(d.sortVars[s].lockedOrder=!0,d.sortVars[s].order=L.getOrder(n)?[1,1]:[0,0]),d.headerList[t]=e,i.addClass(L.css.header+" "+d.cssHeader),L.getClosest(i,"tr").addClass(L.css.headerRow+" "+d.cssHeaderRow).attr("role","row"),d.tabIndex&&i.attr("tabindex",0),e})),d.$headerIndexed=[],r=0;r<d.columns;r++)L.isEmptyObject(d.sortVars[r])&&(d.sortVars[r]={}),e=d.$headers.filter('[data-column="'+r+'"]'),d.$headerIndexed[r]=e.length?e.not(".sorter-false").length?e.not(".sorter-false").filter(":last"):e.filter(":last"):A();d.$table.find(d.selectorHeaders).attr({scope:"col",role:"columnheader"}),L.updateHeader(d),L.debug(d,"core")&&(console.log("Built headers:"+L.benchmark(t)),console.log(d.$headers))},addInstanceMethods:function(e){A.extend(L.instanceMethods,e)},setupParsers:function(e,t){var r,o,s,a,n,i,d,l,c,g,p,u,f,h,m=e.table,b=0,y=L.debug(e,"core"),w={};if(e.$tbodies=e.$table.children("tbody:not(."+e.cssInfoBlock+")"),0===(h=(f=void 0===t?e.$tbodies:t).length))return y?console.warn("Warning: *Empty table!* Not building a parser cache"):"";for(y&&(u=new Date,console[console.group?"group":"log"]("Detecting parsers for each column")),o={extractors:[],parsers:[]};b<h;){if((r=f[b].rows).length)for(n=0,a=e.columns,i=0;i<a;i++){if((d=e.$headerIndexed[n])&&d.length&&(l=L.getColumnData(m,e.headers,n),p=L.getParserById(L.getData(d,l,"extractor")),g=L.getParserById(L.getData(d,l,"sorter")),c="false"===L.getData(d,l,"parser"),e.empties[n]=(L.getData(d,l,"empty")||e.emptyTo||(e.emptyToBottom?"bottom":"top")).toLowerCase(),e.strings[n]=(L.getData(d,l,"string")||e.stringTo||"max").toLowerCase(),c&&(g=L.getParserById("no-parser")),p||(p=!1),g||(g=L.detectParserForColumn(e,r,-1,n)),y&&(w["("+n+") "+d.text()]={parser:g.id,extractor:p?p.id:"none",string:e.strings[n],empty:e.empties[n]}),o.parsers[n]=g,o.extractors[n]=p,0<(s=d[0].colSpan-1)))for(n+=s,a+=s;0<s+1;)o.parsers[n-s]=g,o.extractors[n-s]=p,s--;n++}b+=o.parsers.length?h:1}y&&(L.isEmptyObject(w)?console.warn("  No parsers detected!"):console[console.table?"table":"log"](w),console.log("Completed detecting parsers"+L.benchmark(u)),console.groupEnd&&console.groupEnd()),e.parsers=o.parsers,e.extractors=o.extractors},addParser:function(e){var t,r=L.parsers.length,o=!0;for(t=0;t<r;t++)L.parsers[t].id.toLowerCase()===e.id.toLowerCase()&&(o=!1);o&&(L.parsers[L.parsers.length]=e)},getParserById:function(e){if("false"==e)return!1;var t,r=L.parsers.length;for(t=0;t<r;t++)if(L.parsers[t].id.toLowerCase()===e.toString().toLowerCase())return L.parsers[t];return!1},detectParserForColumn:function(e,t,r,o){for(var s,a,n,i=L.parsers.length,d=!1,l="",c=L.debug(e,"core"),g=!0;""===l&&g;)(n=t[++r])&&r<50?n.className.indexOf(L.cssIgnoreRow)<0&&(d=t[r].cells[o],l=L.getElementText(e,d,o),a=A(d),c&&console.log("Checking if value was empty on row "+r+", column: "+o+': "'+l+'"')):g=!1;for(;0<=--i;)if((s=L.parsers[i])&&"text"!==s.id&&s.is&&s.is(l,e.table,d,a))return s;return L.getParserById("text")},getElementText:function(e,t,r){if(!t)return"";var o,s=e.textExtraction||"",a=t.jquery?t:A(t);return"string"==typeof s?"basic"===s&&void 0!==(o=a.attr(e.textAttribute))?A.trim(o):A.trim(t.textContent||a.text()):"function"==typeof s?A.trim(s(a[0],e.table,r)):"function"==typeof(o=L.getColumnData(e.table,s,r))?A.trim(o(a[0],e.table,r)):A.trim(a[0].textContent||a.text())},getParsedText:function(e,t,r,o){void 0===o&&(o=L.getElementText(e,t,r));var s=""+o,a=e.parsers[r],n=e.extractors[r];return a&&(n&&"function"==typeof n.format&&(o=n.format(o,e.table,t,r)),s="no-parser"===a.id?"":a.format(""+o,e.table,t,r),e.ignoreCase&&"string"==typeof s&&(s=s.toLowerCase())),s},buildCache:function(e,t,r){var o,s,a,n,i,d,l,c,g,p,u,f,h,m,b,y,w,x,v,C,$,I,D=e.table,R=e.parsers,T=L.debug(e,"core");if(e.$tbodies=e.$table.children("tbody:not(."+e.cssInfoBlock+")"),l=void 0===r?e.$tbodies:r,e.cache={},e.totalRows=0,!R)return T?console.warn("Warning: *Empty table!* Not building a cache"):"";for(T&&(f=new Date),e.showProcessing&&L.isProcessing(D,!0),d=0;d<l.length;d++){for(y=[],o=e.cache[d]={normalized:[]},h=l[d]&&l[d].rows.length||0,n=0;n<h;++n)if(m={child:[],raw:[]},g=[],!(c=A(l[d].rows[n])).hasClass(e.selectorRemove.slice(1)))if(c.hasClass(e.cssChildRow)&&0!==n)for($=o.normalized.length-1,(b=o.normalized[$][e.columns]).$row=b.$row.add(c),c.prev().hasClass(e.cssChildRow)||c.prev().addClass(L.css.cssHasChild),p=c.children("th, td"),$=b.child.length,b.child[$]=[],x=0,C=e.columns,i=0;i<C;i++)(u=p[i])&&(b.child[$][i]=L.getParsedText(e,u,i),0<(w=p[i].colSpan-1)&&(x+=w,C+=w)),x++;else{for(m.$row=c,m.order=n,x=0,C=e.columns,i=0;i<C;++i){if((u=c[0].cells[i])&&x<e.columns&&(!(v=void 0!==R[x])&&T&&console.warn("No parser found for row: "+n+", column: "+i+'; cell containing: "'+A(u).text()+'"; does it have a header?'),s=L.getElementText(e,u,x),m.raw[x]=s,a=L.getParsedText(e,u,x,s),g[x]=a,v&&"numeric"===(R[x].type||"").toLowerCase()&&(y[x]=Math.max(Math.abs(a)||0,y[x]||0)),0<(w=u.colSpan-1))){for(I=0;I<=w;)a=e.duplicateSpan||0===I?s:"string"!=typeof e.textExtraction&&L.getElementText(e,u,x+I)||"",m.raw[x+I]=a,g[x+I]=a,I++;x+=w,C+=w}x++}g[e.columns]=m,o.normalized[o.normalized.length]=g}o.colMax=y,e.totalRows+=o.normalized.length}if(e.showProcessing&&L.isProcessing(D),T){for($=Math.min(5,e.cache[0].normalized.length),console[console.group?"group":"log"]("Building cache for "+e.totalRows+" rows (showing "+$+" rows in log) and "+e.columns+" columns"+L.benchmark(f)),s={},i=0;i<e.columns;i++)for(x=0;x<$;x++)s["row: "+x]||(s["row: "+x]={}),s["row: "+x][e.$headerIndexed[i].text()]=e.cache[0].normalized[x][i];console[console.table?"table":"log"](s),console.groupEnd&&console.groupEnd()}A.isFunction(t)&&t(D)},getColumnText:function(e,t,r,o){var s,a,n,i,d,l,c,g,p,u,f="function"==typeof r,h="all"===t,m={raw:[],parsed:[],$cell:[]},b=(e=A(e)[0]).config;if(!L.isEmptyObject(b)){for(d=b.$tbodies.length,s=0;s<d;s++)for(l=(n=b.cache[s].normalized).length,a=0;a<l;a++)i=n[a],o&&!i[b.columns].$row.is(o)||(u=!0,g=h?i.slice(0,b.columns):i[t],i=i[b.columns],c=h?i.raw:i.raw[t],p=h?i.$row.children():i.$row.children().eq(t),f&&(u=r({tbodyIndex:s,rowIndex:a,parsed:g,raw:c,$row:i.$row,$cell:p})),!1!==u&&(m.parsed[m.parsed.length]=g,m.raw[m.raw.length]=c,m.$cell[m.$cell.length]=p));return m}L.debug(b,"core")&&console.warn("No cache found - aborting getColumnText function!")},setHeadersCss:function(a){var e,t,r=a.sortList,o=r.length,s=L.css.sortNone+" "+a.cssNone,n=[L.css.sortAsc+" "+a.cssAsc,L.css.sortDesc+" "+a.cssDesc],i=[a.cssIconAsc,a.cssIconDesc,a.cssIconNone],d=["ascending","descending"],l=function(e,t){e.removeClass(s).addClass(n[t]).attr("aria-sort",d[t]).find("."+L.css.icon).removeClass(i[2]).addClass(i[t])},c=a.$table.find("tfoot tr").children("td, th").add(A(a.namespace+"_extra_headers")).removeClass(n.join(" ")),g=a.$headers.add(A("thead "+a.namespace+"_extra_headers")).removeClass(n.join(" ")).addClass(s).attr("aria-sort","none").find("."+L.css.icon).removeClass(i.join(" ")).end();for(g.not(".sorter-false").find("."+L.css.icon).addClass(i[2]),a.cssIconDisabled&&g.filter(".sorter-false").find("."+L.css.icon).addClass(a.cssIconDisabled),e=0;e<o;e++)if(2!==r[e][1]){if((g=(g=a.$headers.filter(function(e){for(var t=!0,r=a.$headers.eq(e),o=parseInt(r.attr("data-column"),10),s=o+L.getClosest(r,"th, td")[0].colSpan;o<s;o++)t=!!t&&(t||-1<L.isValueInArray(o,a.sortList));return t})).not(".sorter-false").filter('[data-column="'+r[e][0]+'"]'+(1===o?":last":""))).length)for(t=0;t<g.length;t++)g[t].sortDisabled||l(g.eq(t),r[e][1]);c.length&&l(c.filter('[data-column="'+r[e][0]+'"]'),r[e][1])}for(o=a.$headers.length,e=0;e<o;e++)L.setColumnAriaLabel(a,a.$headers.eq(e))},getClosest:function(e,t){return A.fn.closest?e.closest(t):e.is(t)?e:e.parents(t).filter(":first")},setColumnAriaLabel:function(e,t,r){if(t.length){var o=parseInt(t.attr("data-column"),10),s=e.sortVars[o],a=t.hasClass(L.css.sortAsc)?"sortAsc":t.hasClass(L.css.sortDesc)?"sortDesc":"sortNone",n=A.trim(t.text())+": "+L.language[a];t.hasClass("sorter-false")||!1===r?n+=L.language.sortDisabled:(a=(s.count+1)%s.order.length,r=s.order[a],n+=L.language[0===r?"nextAsc":1===r?"nextDesc":"nextNone"]),t.attr("aria-label",n),s.sortedBy?t.attr("data-sortedBy",s.sortedBy):t.removeAttr("data-sortedBy")}},updateHeader:function(e){var t,r,o,s,a=e.table,n=e.$headers.length;for(t=0;t<n;t++)o=e.$headers.eq(t),s=L.getColumnData(a,e.headers,t,!0),r="false"===L.getData(o,s,"sorter")||"false"===L.getData(o,s,"parser"),L.setColumnSort(e,o,r)},setColumnSort:function(e,t,r){var o=e.table.id;t[0].sortDisabled=r,t[r?"addClass":"removeClass"]("sorter-false").attr("aria-disabled",""+r),e.tabIndex&&(r?t.removeAttr("tabindex"):t.attr("tabindex","0")),o&&(r?t.removeAttr("aria-controls"):t.attr("aria-controls",o))},updateHeaderSortCount:function(e,t){var r,o,s,a,n,i,d,l,c=t||e.sortList,g=c.length;for(e.sortList=[],a=0;a<g;a++)if(d=c[a],(r=parseInt(d[0],10))<e.columns){switch(e.sortVars[r].order||(l=L.getOrder(e.sortInitialOrder)?e.sortReset?[1,0,2]:[1,0]:e.sortReset?[0,1,2]:[0,1],e.sortVars[r].order=l,e.sortVars[r].count=0),l=e.sortVars[r].order,o=(o=(""+d[1]).match(/^(1|d|s|o|n)/))?o[0]:""){case"1":case"d":o=1;break;case"s":o=n||0;break;case"o":o=0===(i=l[(n||0)%l.length])?1:1===i?0:2;break;case"n":o=l[++e.sortVars[r].count%l.length];break;default:o=0}n=0===a?o:n,s=[r,parseInt(o,10)||0],e.sortList[e.sortList.length]=s,o=A.inArray(s[1],l),e.sortVars[r].count=0<=o?o:s[1]%l.length}},updateAll:function(e,t,r){var o=e.table;o.isUpdating=!0,L.refreshWidgets(o,!0,!0),L.buildHeaders(e),L.bindEvents(o,e.$headers,!0),L.bindMethods(e),L.commonUpdate(e,t,r)},update:function(e,t,r){e.table.isUpdating=!0,L.updateHeader(e),L.commonUpdate(e,t,r)},updateHeaders:function(e,t){e.table.isUpdating=!0,L.buildHeaders(e),L.bindEvents(e.table,e.$headers,!0),L.resortComplete(e,t)},updateCell:function(e,t,r,o){if(A(t).closest("tr").hasClass(e.cssChildRow))console.warn('Tablesorter Warning! "updateCell" for child row content has been disabled, use "update" instead');else{if(L.isEmptyObject(e.cache))return L.updateHeader(e),void L.commonUpdate(e,r,o);e.table.isUpdating=!0,e.$table.find(e.selectorRemove).remove();var s,a,n,i,d,l,c=e.$tbodies,g=A(t),p=c.index(L.getClosest(g,"tbody")),u=e.cache[p],f=L.getClosest(g,"tr");if(t=g[0],c.length&&0<=p){if(n=c.eq(p).find("tr").not("."+e.cssChildRow).index(f),d=u.normalized[n],(l=f[0].cells.length)!==e.columns)for(s=!1,a=i=0;a<l;a++)s||f[0].cells[a]===t?s=!0:i+=f[0].cells[a].colSpan;else i=g.index();s=L.getElementText(e,t,i),d[e.columns].raw[i]=s,s=L.getParsedText(e,t,i,s),d[i]=s,"numeric"===(e.parsers[i].type||"").toLowerCase()&&(u.colMax[i]=Math.max(Math.abs(s)||0,u.colMax[i]||0)),!1!==(s="undefined"!==r?r:e.resort)?L.checkResort(e,s,o):L.resortComplete(e,o)}else L.debug(e,"core")&&console.error("updateCell aborted, tbody missing or not within the indicated table"),e.table.isUpdating=!1}},addRows:function(e,t,r,o){var s,a,n,i,d,l,c,g,p,u,f,h,m,b="string"==typeof t&&1===e.$tbodies.length&&/<tr/.test(t||""),y=e.table;if(b)t=A(t),e.$tbodies.append(t);else if(!(t&&t instanceof A&&L.getClosest(t,"table")[0]===e.table))return L.debug(e,"core")&&console.error("addRows method requires (1) a jQuery selector reference to rows that have already been added to the table, or (2) row HTML string to be added to a table with only one tbody"),!1;if(y.isUpdating=!0,L.isEmptyObject(e.cache))L.updateHeader(e),L.commonUpdate(e,r,o);else{for(d=t.filter("tr").attr("role","row").length,n=e.$tbodies.index(t.parents("tbody").filter(":first")),e.parsers&&e.parsers.length||L.setupParsers(e),i=0;i<d;i++){for(p=0,c=t[i].cells.length,g=e.cache[n].normalized.length,f=[],u={child:[],raw:[],$row:t.eq(i),order:g},l=0;l<c;l++)h=t[i].cells[l],s=L.getElementText(e,h,p),u.raw[p]=s,a=L.getParsedText(e,h,p,s),f[p]=a,"numeric"===(e.parsers[p].type||"").toLowerCase()&&(e.cache[n].colMax[p]=Math.max(Math.abs(a)||0,e.cache[n].colMax[p]||0)),0<(m=h.colSpan-1)&&(p+=m),p++;f[e.columns]=u,e.cache[n].normalized[g]=f}L.checkResort(e,r,o)}},updateCache:function(e,t,r){e.parsers&&e.parsers.length||L.setupParsers(e,r),L.buildCache(e,t,r)},appendCache:function(e,t){var r,o,s,a,n,i,d,l=e.table,c=e.$tbodies,g=[],p=e.cache;if(L.isEmptyObject(p))return e.appender?e.appender(l,g):l.isUpdating?e.$table.triggerHandler("updateComplete",l):"";for(L.debug(e,"core")&&(d=new Date),i=0;i<c.length;i++)if((s=c.eq(i)).length){for(a=L.processTbody(l,s,!0),o=(r=p[i].normalized).length,n=0;n<o;n++)g[g.length]=r[n][e.columns].$row,e.appender&&(!e.pager||e.pager.removeRows||e.pager.ajax)||a.append(r[n][e.columns].$row);L.processTbody(l,a,!1)}e.appender&&e.appender(l,g),L.debug(e,"core")&&console.log("Rebuilt table"+L.benchmark(d)),t||e.appender||L.applyWidget(l),l.isUpdating&&e.$table.triggerHandler("updateComplete",l)},commonUpdate:function(e,t,r){e.$table.find(e.selectorRemove).remove(),L.setupParsers(e),L.buildCache(e),L.checkResort(e,t,r)},initSort:function(t,e,r){if(t.table.isUpdating)return setTimeout(function(){L.initSort(t,e,r)},50);var o,s,a,n,i,d,l,c=!r[t.sortMultiSortKey],g=t.table,p=t.$headers.length,u=L.getClosest(A(e),"th, td"),f=parseInt(u.attr("data-column"),10),h="mouseup"===r.type?"user":r.type,m=t.sortVars[f].order;if(u=u[0],t.$table.triggerHandler("sortStart",g),d=(t.sortVars[f].count+1)%m.length,t.sortVars[f].count=r[t.sortResetKey]?2:d,t.sortRestart)for(a=0;a<p;a++)l=t.$headers.eq(a),f!==(d=parseInt(l.attr("data-column"),10))&&(c||l.hasClass(L.css.sortNone))&&(t.sortVars[d].count=-1);if(c){if(A.each(t.sortVars,function(e){t.sortVars[e].sortedBy=""}),t.sortList=[],t.last.sortList=[],null!==t.sortForce)for(o=t.sortForce,s=0;s<o.length;s++)o[s][0]!==f&&(t.sortList[t.sortList.length]=o[s],t.sortVars[o[s][0]].sortedBy="sortForce");if((n=m[t.sortVars[f].count])<2&&(t.sortList[t.sortList.length]=[f,n],t.sortVars[f].sortedBy=h,1<u.colSpan))for(s=1;s<u.colSpan;s++)t.sortList[t.sortList.length]=[f+s,n],t.sortVars[f+s].count=A.inArray(n,m),t.sortVars[f+s].sortedBy=h}else if(t.sortList=A.extend([],t.last.sortList),0<=L.isValueInArray(f,t.sortList))for(t.sortVars[f].sortedBy=h,s=0;s<t.sortList.length;s++)(d=t.sortList[s])[0]===f&&(d[1]=m[t.sortVars[f].count],2===d[1]&&(t.sortList.splice(s,1),t.sortVars[f].count=-1));else if(n=m[t.sortVars[f].count],t.sortVars[f].sortedBy=h,n<2&&(t.sortList[t.sortList.length]=[f,n],1<u.colSpan))for(s=1;s<u.colSpan;s++)t.sortList[t.sortList.length]=[f+s,n],t.sortVars[f+s].count=A.inArray(n,m),t.sortVars[f+s].sortedBy=h;if(t.last.sortList=A.extend([],t.sortList),t.sortList.length&&t.sortAppend&&(o=A.isArray(t.sortAppend)?t.sortAppend:t.sortAppend[t.sortList[0][0]],!L.isEmptyObject(o)))for(s=0;s<o.length;s++)if(o[s][0]!==f&&L.isValueInArray(o[s][0],t.sortList)<0){if(i=(""+(n=o[s][1])).match(/^(a|d|s|o|n)/))switch(d=t.sortList[0][1],i[0]){case"d":n=1;break;case"s":n=d;break;case"o":n=0===d?1:0;break;case"n":n=(d+1)%m.length;break;default:n=0}t.sortList[t.sortList.length]=[o[s][0],n],t.sortVars[o[s][0]].sortedBy="sortAppend"}t.$table.triggerHandler("sortBegin",g),setTimeout(function(){L.setHeadersCss(t),L.multisort(t),L.appendCache(t),t.$table.triggerHandler("sortBeforeEnd",g),t.$table.triggerHandler("sortEnd",g)},1)},multisort:function(l){var e,t,c,r,g=l.table,p=[],u=0,f=l.textSorter||"",h=l.sortList,m=h.length,o=l.$tbodies.length;if(!l.serverSideSorting&&!L.isEmptyObject(l.cache)){if(L.debug(l,"core")&&(t=new Date),"object"==typeof f)for(c=l.columns;c--;)"function"==typeof(r=L.getColumnData(g,f,c))&&(p[c]=r);for(e=0;e<o;e++)c=l.cache[e].colMax,l.cache[e].normalized.sort(function(e,t){var r,o,s,a,n,i,d;for(r=0;r<m;r++){if(s=h[r][0],a=h[r][1],u=0===a,l.sortStable&&e[s]===t[s]&&1===m)return e[l.columns].order-t[l.columns].order;if(n=(o=/n/i.test(L.getSortType(l.parsers,s)))&&l.strings[s]?(o="boolean"==typeof L.string[l.strings[s]]?(u?1:-1)*(L.string[l.strings[s]]?-1:1):l.strings[s]&&L.string[l.strings[s]]||0,l.numberSorter?l.numberSorter(e[s],t[s],u,c[s],g):L["sortNumeric"+(u?"Asc":"Desc")](e[s],t[s],o,c[s],s,l)):(i=u?e:t,d=u?t:e,"function"==typeof f?f(i[s],d[s],u,s,g):"function"==typeof p[s]?p[s](i[s],d[s],u,s,g):L["sortNatural"+(u?"Asc":"Desc")](e[s]||"",t[s]||"",s,l)))return n}return e[l.columns].order-t[l.columns].order});L.debug(l,"core")&&console.log("Applying sort "+h.toString()+L.benchmark(t))}},resortComplete:function(e,t){e.table.isUpdating&&e.$table.triggerHandler("updateComplete",e.table),A.isFunction(t)&&t(e.table)},checkResort:function(e,t,r){var o=A.isArray(t)?t:e.sortList;!1===(void 0===t?e.resort:t)||e.serverSideSorting||e.table.isProcessing?(L.resortComplete(e,r),L.applyWidget(e.table,!1)):o.length?L.sortOn(e,o,function(){L.resortComplete(e,r)},!0):L.sortReset(e,function(){L.resortComplete(e,r),L.applyWidget(e.table,!1)})},sortOn:function(e,t,r,o){var s,a=e.table;for(e.$table.triggerHandler("sortStart",a),s=0;s<e.columns;s++)e.sortVars[s].sortedBy=-1<L.isValueInArray(s,t)?"sorton":"";L.updateHeaderSortCount(e,t),L.setHeadersCss(e),e.delayInit&&L.isEmptyObject(e.cache)&&L.buildCache(e),e.$table.triggerHandler("sortBegin",a),L.multisort(e),L.appendCache(e,o),e.$table.triggerHandler("sortBeforeEnd",a),e.$table.triggerHandler("sortEnd",a),L.applyWidget(a),A.isFunction(r)&&r(a)},sortReset:function(e,t){var r;for(e.sortList=[],r=0;r<e.columns;r++)e.sortVars[r].count=-1,e.sortVars[r].sortedBy="";L.setHeadersCss(e),L.multisort(e),L.appendCache(e),A.isFunction(t)&&t(e.table)},getSortType:function(e,t){return e&&e[t]&&e[t].type||""},getOrder:function(e){return/^d/i.test(e)||1===e},sortNatural:function(e,t){if(e===t)return 0;e=(e||"").toString(),t=(t||"").toString();var r,o,s,a,n,i,d=L.regex;if(d.hex.test(t)){if((r=parseInt(e.match(d.hex),16))<(o=parseInt(t.match(d.hex),16)))return-1;if(o<r)return 1}for(r=e.replace(d.chunk,"\\0$1\\0").replace(d.chunks,"").split("\\0"),o=t.replace(d.chunk,"\\0$1\\0").replace(d.chunks,"").split("\\0"),i=Math.max(r.length,o.length),n=0;n<i;n++){if(s=isNaN(r[n])?r[n]||0:parseFloat(r[n])||0,a=isNaN(o[n])?o[n]||0:parseFloat(o[n])||0,isNaN(s)!==isNaN(a))return isNaN(s)?1:-1;if(typeof s!=typeof a&&(s+="",a+=""),s<a)return-1;if(a<s)return 1}return 0},sortNaturalAsc:function(e,t,r,o){if(e===t)return 0;var s=L.string[o.empties[r]||o.emptyTo];return""===e&&0!==s?"boolean"==typeof s?s?-1:1:-s||-1:""===t&&0!==s?"boolean"==typeof s?s?1:-1:s||1:L.sortNatural(e,t)},sortNaturalDesc:function(e,t,r,o){if(e===t)return 0;var s=L.string[o.empties[r]||o.emptyTo];return""===e&&0!==s?"boolean"==typeof s?s?-1:1:s||1:""===t&&0!==s?"boolean"==typeof s?s?1:-1:-s||-1:L.sortNatural(t,e)},sortText:function(e,t){return t<e?1:e<t?-1:0},getTextValue:function(e,t,r){if(r){var o,s=e?e.length:0,a=r+t;for(o=0;o<s;o++)a+=e.charCodeAt(o);return t*a}return 0},sortNumericAsc:function(e,t,r,o,s,a){if(e===t)return 0;var n=L.string[a.empties[s]||a.emptyTo];return""===e&&0!==n?"boolean"==typeof n?n?-1:1:-n||-1:""===t&&0!==n?"boolean"==typeof n?n?1:-1:n||1:(isNaN(e)&&(e=L.getTextValue(e,r,o)),isNaN(t)&&(t=L.getTextValue(t,r,o)),e-t)},sortNumericDesc:function(e,t,r,o,s,a){if(e===t)return 0;var n=L.string[a.empties[s]||a.emptyTo];return""===e&&0!==n?"boolean"==typeof n?n?-1:1:n||1:""===t&&0!==n?"boolean"==typeof n?n?1:-1:-n||-1:(isNaN(e)&&(e=L.getTextValue(e,r,o)),isNaN(t)&&(t=L.getTextValue(t,r,o)),t-e)},sortNumeric:function(e,t){return e-t},addWidget:function(e){e.id&&!L.isEmptyObject(L.getWidgetById(e.id))&&console.warn('"'+e.id+'" widget was loaded more than once!'),L.widgets[L.widgets.length]=e},hasWidget:function(e,t){return(e=A(e)).length&&e[0].config&&e[0].config.widgetInit[t]||!1},getWidgetById:function(e){var t,r,o=L.widgets.length;for(t=0;t<o;t++)if((r=L.widgets[t])&&r.id&&r.id.toLowerCase()===e.toLowerCase())return r},applyWidgetOptions:function(e){var t,r,o,s=e.config,a=s.widgets.length;if(a)for(t=0;t<a;t++)(r=L.getWidgetById(s.widgets[t]))&&r.options&&(o=A.extend(!0,{},r.options),s.widgetOptions=A.extend(!0,o,s.widgetOptions),A.extend(!0,L.defaults.widgetOptions,r.options))},addWidgetFromClass:function(e){var t,r,o=e.config,s="^"+o.widgetClass.replace(L.regex.templateName,"(\\S+)+")+"$",a=new RegExp(s,"g"),n=(e.className||"").split(L.regex.spaces);if(n.length)for(t=n.length,r=0;r<t;r++)n[r].match(a)&&(o.widgets[o.widgets.length]=n[r].replace(a,"$1"))},applyWidgetId:function(e,t,r){var o,s,a,n=(e=A(e)[0]).config,i=n.widgetOptions,d=L.debug(n,"core"),l=L.getWidgetById(t);l&&(a=l.id,o=!1,A.inArray(a,n.widgets)<0&&(n.widgets[n.widgets.length]=a),d&&(s=new Date),!r&&n.widgetInit[a]||(n.widgetInit[a]=!0,e.hasInitialized&&L.applyWidgetOptions(e),"function"==typeof l.init&&(o=!0,d&&console[console.group?"group":"log"]("Initializing "+a+" widget"),l.init(e,l,n,i))),r||"function"!=typeof l.format||(o=!0,d&&console[console.group?"group":"log"]("Updating "+a+" widget"),l.format(e,n,i,!1)),d&&o&&(console.log("Completed "+(r?"initializing ":"applying ")+a+" widget"+L.benchmark(s)),console.groupEnd&&console.groupEnd()))},applyWidget:function(e,t,r){var o,s,a,n,i,d=(e=A(e)[0]).config,l=L.debug(d,"core"),c=[];if(!1===t||!e.hasInitialized||!e.isApplyingWidgets&&!e.isUpdating){if(l&&(i=new Date),L.addWidgetFromClass(e),clearTimeout(d.timerReady),d.widgets.length){for(e.isApplyingWidgets=!0,d.widgets=A.grep(d.widgets,function(e,t){return A.inArray(e,d.widgets)===t}),s=(a=d.widgets||[]).length,o=0;o<s;o++)(n=L.getWidgetById(a[o]))&&n.id?(n.priority||(n.priority=10),c[o]=n):l&&console.warn('"'+a[o]+'" was enabled, but the widget code has not been loaded!');for(c.sort(function(e,t){return e.priority<t.priority?-1:e.priority===t.priority?0:1}),s=c.length,l&&console[console.group?"group":"log"]("Start "+(t?"initializing":"applying")+" widgets"),o=0;o<s;o++)(n=c[o])&&n.id&&L.applyWidgetId(e,n.id,t);l&&console.groupEnd&&console.groupEnd()}d.timerReady=setTimeout(function(){e.isApplyingWidgets=!1,A.data(e,"lastWidgetApplication",new Date),d.$table.triggerHandler("tablesorter-ready"),t||"function"!=typeof r||r(e),l&&(n=d.widgets.length,console.log("Completed "+(!0===t?"initializing ":"applying ")+n+" widget"+(1!==n?"s":"")+L.benchmark(i)))},10)}},removeWidget:function(e,t,r){var o,s,a,n,i=(e=A(e)[0]).config;if(!0===t)for(t=[],n=L.widgets.length,a=0;a<n;a++)(s=L.widgets[a])&&s.id&&(t[t.length]=s.id);else t=(A.isArray(t)?t.join(","):t||"").toLowerCase().split(/[\s,]+/);for(n=t.length,o=0;o<n;o++)s=L.getWidgetById(t[o]),0<=(a=A.inArray(t[o],i.widgets))&&!0!==r&&i.widgets.splice(a,1),s&&s.remove&&(L.debug(i,"core")&&console.log((r?"Refreshing":"Removing")+' "'+t[o]+'" widget'),s.remove(e,i,i.widgetOptions,r),i.widgetInit[t[o]]=!1);i.$table.triggerHandler("widgetRemoveEnd",e)},refreshWidgets:function(e,t,r){var o,s,a=(e=A(e)[0]).config.widgets,n=L.widgets,i=n.length,d=[],l=function(e){A(e).triggerHandler("refreshComplete")};for(o=0;o<i;o++)(s=n[o])&&s.id&&(t||A.inArray(s.id,a)<0)&&(d[d.length]=s.id);L.removeWidget(e,d.join(","),!0),!0!==r?(L.applyWidget(e,t||!1,l),t&&L.applyWidget(e,!1,l)):l(e)},benchmark:function(e){return" ("+((new Date).getTime()-e.getTime())+" ms)"},log:function(){console.log(arguments)},debug:function(e,t){return e&&(!0===e.debug||"string"==typeof e.debug&&-1<e.debug.indexOf(t))},isEmptyObject:function(e){for(var t in e)return!1;return!0},isValueInArray:function(e,t){var r,o=t&&t.length||0;for(r=0;r<o;r++)if(t[r][0]===e)return r;return-1},formatFloat:function(e,t){return"string"!=typeof e||""===e?e:(e=(t&&t.config?!1!==t.config.usNumberFormat:void 0===t||t)?e.replace(L.regex.comma,""):e.replace(L.regex.digitNonUS,"").replace(L.regex.comma,"."),L.regex.digitNegativeTest.test(e)&&(e=e.replace(L.regex.digitNegativeReplace,"-$1")),r=parseFloat(e),isNaN(r)?A.trim(e):r);var r},isDigit:function(e){return isNaN(e)?L.regex.digitTest.test(e.toString().replace(L.regex.digitReplace,"")):""!==e},computeColumnIndex:function(e,t){var r,o,s,a,n,i,d,l,c,g,p=t&&t.columns||0,u=[],f=new Array(p);for(r=0;r<e.length;r++)for(i=e[r].cells,o=0;o<i.length;o++){for(d=r,l=(n=i[o]).rowSpan||1,c=n.colSpan||1,void 0===u[d]&&(u[d]=[]),s=0;s<u[d].length+1;s++)if(void 0===u[d][s]){g=s;break}for(p&&n.cellIndex===g||(n.setAttribute?n.setAttribute("data-column",g):A(n).attr("data-column",g)),s=d;s<d+l;s++)for(void 0===u[s]&&(u[s]=[]),f=u[s],a=g;a<g+c;a++)f[a]="x"}return L.checkColumnCount(e,u,f.length),f.length},checkColumnCount:function(e,t,r){var o,s,a=!0,n=[];for(o=0;o<t.length;o++)if(t[o]&&(s=t[o].length,t[o].length!==r)){a=!1;break}a||(e.each(function(e,t){var r=t.parentElement.nodeName;n.indexOf(r)<0&&n.push(r)}),console.error("Invalid or incorrect number of columns in the "+n.join(" or ")+"; expected "+r+", but found "+s+" columns"))},fixColumnWidth:function(e){var t,r,o,s,a,n=(e=A(e)[0]).config,i=n.$table.children("colgroup");if(i.length&&i.hasClass(L.css.colgroup)&&i.remove(),n.widthFixed&&0===n.$table.children("colgroup").length){for(i=A('<colgroup class="'+L.css.colgroup+'">'),t=n.$table.width(),s=(o=n.$tbodies.find("tr:first").children(":visible")).length,a=0;a<s;a++)r=parseInt(o.eq(a).width()/t*1e3,10)/10+"%",i.append(A("<col>").css("width",r));n.$table.prepend(i)}},getData:function(e,t,r){var o,s,a="",n=A(e);return n.length?(o=!!A.metadata&&n.metadata(),s=" "+(n.attr("class")||""),void 0!==n.data(r)||void 0!==n.data(r.toLowerCase())?a+=n.data(r)||n.data(r.toLowerCase()):o&&void 0!==o[r]?a+=o[r]:t&&void 0!==t[r]?a+=t[r]:" "!==s&&s.match(" "+r+"-")&&(a=s.match(new RegExp("\\s"+r+"-([\\w-]+)"))[1]||""),A.trim(a)):""},getColumnData:function(e,t,r,o,s){if("object"!=typeof t||null===t)return t;var a,n=(e=A(e)[0]).config,i=s||n.$headers,d=n.$headerIndexed&&n.$headerIndexed[r]||i.find('[data-column="'+r+'"]:last');if(void 0!==t[r])return o?t[r]:t[i.index(d)];for(a in t)if("string"==typeof a&&d.filter(a).add(d.find(a)).length)return t[a]},isProcessing:function(e,t,r){var o=(e=A(e))[0].config,s=r||e.find("."+L.css.header);t?(void 0!==r&&0<o.sortList.length&&(s=s.filter(function(){return!this.sortDisabled&&0<=L.isValueInArray(parseFloat(A(this).attr("data-column")),o.sortList)})),e.add(s).addClass(L.css.processing+" "+o.cssProcessing)):e.add(s).removeClass(L.css.processing+" "+o.cssProcessing)},processTbody:function(e,t,r){if(e=A(e)[0],r)return e.isProcessing=!0,t.before('<colgroup class="tablesorter-savemyplace"/>'),A.fn.detach?t.detach():t.remove();var o=A(e).find("colgroup.tablesorter-savemyplace");t.insertAfter(o),o.remove(),e.isProcessing=!1},clearTableBody:function(e){A(e)[0].config.$tbodies.children().detach()},characterEquivalents:{a:"áàâãäąå",A:"ÁÀÂÃÄĄÅ",c:"çćč",C:"ÇĆČ",e:"éèêëěę",E:"ÉÈÊËĚĘ",i:"íìİîïı",I:"ÍÌİÎÏ",o:"óòôõöō",O:"ÓÒÔÕÖŌ",ss:"ß",SS:"ẞ",u:"úùûüů",U:"ÚÙÛÜŮ"},replaceAccents:function(e){var t,r="[",o=L.characterEquivalents;if(!L.characterRegex){for(t in L.characterRegexArray={},o)"string"==typeof t&&(r+=o[t],L.characterRegexArray[t]=new RegExp("["+o[t]+"]","g"));L.characterRegex=new RegExp(r+"]")}if(L.characterRegex.test(e))for(t in o)"string"==typeof t&&(e=e.replace(L.characterRegexArray[t],t));return e},validateOptions:function(e){var t,r,o,s,a="headers sortForce sortList sortAppend widgets".split(" "),n=e.originalSettings;if(n){for(t in L.debug(e,"core")&&(s=new Date),n)if("undefined"===(o=typeof L.defaults[t]))console.warn('Tablesorter Warning! "table.config.'+t+'" option not recognized');else if("object"===o)for(r in n[t])o=L.defaults[t]&&typeof L.defaults[t][r],A.inArray(t,a)<0&&"undefined"===o&&console.warn('Tablesorter Warning! "table.config.'+t+"."+r+'" option not recognized');L.debug(e,"core")&&console.log("validate options time:"+L.benchmark(s))}},restoreHeaders:function(e){var t,r,o=A(e)[0].config,s=o.$table.find(o.selectorHeaders),a=s.length;for(t=0;t<a;t++)(r=s.eq(t)).find("."+L.css.headerIn).length&&r.html(o.headerContent[t])},destroy:function(e,t,r){if((e=A(e)[0]).hasInitialized){L.removeWidget(e,!0,!1);var o,s=A(e),a=e.config,n=s.find("thead:first"),i=n.find("tr."+L.css.headerRow).removeClass(L.css.headerRow+" "+a.cssHeaderRow),d=s.find("tfoot:first > tr").children("th, td");!1===t&&0<=A.inArray("uitheme",a.widgets)&&(s.triggerHandler("applyWidgetId",["uitheme"]),s.triggerHandler("applyWidgetId",["zebra"])),n.find("tr").not(i).remove(),o="sortReset update updateRows updateAll updateHeaders updateCell addRows updateComplete sorton appendCache updateCache applyWidgetId applyWidgets refreshWidgets removeWidget destroy mouseup mouseleave "+"keypress sortBegin sortEnd resetToLoadState ".split(" ").join(a.namespace+" "),s.removeData("tablesorter").unbind(o.replace(L.regex.spaces," ")),a.$headers.add(d).removeClass([L.css.header,a.cssHeader,a.cssAsc,a.cssDesc,L.css.sortAsc,L.css.sortDesc,L.css.sortNone].join(" ")).removeAttr("data-column").removeAttr("aria-label").attr("aria-disabled","true"),i.find(a.selectorSort).unbind("mousedown mouseup keypress ".split(" ").join(a.namespace+" ").replace(L.regex.spaces," ")),L.restoreHeaders(e),s.toggleClass(L.css.table+" "+a.tableClass+" tablesorter-"+a.theme,!1===t),s.removeClass(a.namespace.slice(1)),e.hasInitialized=!1,delete e.config.cache,"function"==typeof r&&r(e),L.debug(a,"core")&&console.log("tablesorter has been removed")}}};A.fn.tablesorter=function(t){return this.each(function(){var e=A.extend(!0,{},L.defaults,t,L.instanceMethods);e.originalSettings=t,!this.hasInitialized&&L.buildTable&&"TABLE"!==this.nodeName?L.buildTable(this,e):L.setup(this,e)})},window.console&&window.console.log||(L.logs=[],console={},console.log=console.warn=console.error=console.table=function(){var e=1<arguments.length?arguments:arguments[0];L.logs[L.logs.length]={date:Date.now(),log:e}}),L.addParser({id:"no-parser",is:function(){return!1},format:function(){return""},type:"text"}),L.addParser({id:"text",is:function(){return!0},format:function(e,t){var r=t.config;return e&&(e=A.trim(r.ignoreCase?e.toLocaleLowerCase():e),e=r.sortLocaleCompare?L.replaceAccents(e):e),e},type:"text"}),L.regex.nondigit=/[^\w,. \-()]/g,L.addParser({id:"digit",is:function(e){return L.isDigit(e)},format:function(e,t){var r=L.formatFloat((e||"").replace(L.regex.nondigit,""),t);return e&&"number"==typeof r?r:e?A.trim(e&&t.config.ignoreCase?e.toLocaleLowerCase():e):e},type:"numeric"}),L.regex.currencyReplace=/[+\-,. ]/g,L.regex.currencyTest=/^\(?\d+[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]|[\u00a3$\u20ac\u00a4\u00a5\u00a2?.]\d+\)?$/,L.addParser({id:"currency",is:function(e){return e=(e||"").replace(L.regex.currencyReplace,""),L.regex.currencyTest.test(e)},format:function(e,t){var r=L.formatFloat((e||"").replace(L.regex.nondigit,""),t);return e&&"number"==typeof r?r:e?A.trim(e&&t.config.ignoreCase?e.toLocaleLowerCase():e):e},type:"numeric"}),L.regex.urlProtocolTest=/^(https?|ftp|file):\/\//,L.regex.urlProtocolReplace=/(https?|ftp|file):\/\/(www\.)?/,L.addParser({id:"url",is:function(e){return L.regex.urlProtocolTest.test(e)},format:function(e){return e?A.trim(e.replace(L.regex.urlProtocolReplace,"")):e},type:"text"}),L.regex.dash=/-/g,L.regex.isoDate=/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}/,L.addParser({id:"isoDate",is:function(e){return L.regex.isoDate.test(e)},format:function(e){var t=e?new Date(e.replace(L.regex.dash,"/")):e;return t instanceof Date&&isFinite(t)?t.getTime():e},type:"numeric"}),L.regex.percent=/%/g,L.regex.percentTest=/(\d\s*?%|%\s*?\d)/,L.addParser({id:"percent",is:function(e){return L.regex.percentTest.test(e)&&e.length<15},format:function(e,t){return e?L.formatFloat(e.replace(L.regex.percent,""),t):e},type:"numeric"}),L.addParser({id:"image",is:function(e,t,r,o){return 0<o.find("img").length},format:function(e,t,r){return A(r).find("img").attr(t.config.imgAttr||"alt")||e},parsed:!0,type:"text"}),L.regex.dateReplace=/(\S)([AP]M)$/i,L.regex.usLongDateTest1=/^[A-Z]{3,10}\.?\s+\d{1,2},?\s+(\d{4})(\s+\d{1,2}:\d{2}(:\d{2})?(\s+[AP]M)?)?$/i,L.regex.usLongDateTest2=/^\d{1,2}\s+[A-Z]{3,10}\s+\d{4}/i,L.addParser({id:"usLongDate",is:function(e){return L.regex.usLongDateTest1.test(e)||L.regex.usLongDateTest2.test(e)},format:function(e){var t=e?new Date(e.replace(L.regex.dateReplace,"$1 $2")):e;return t instanceof Date&&isFinite(t)?t.getTime():e},type:"numeric"}),L.regex.shortDateTest=/(^\d{1,2}[\/\s]\d{1,2}[\/\s]\d{4})|(^\d{4}[\/\s]\d{1,2}[\/\s]\d{1,2})/,L.regex.shortDateReplace=/[\-.,]/g,L.regex.shortDateXXY=/(\d{1,2})[\/\s](\d{1,2})[\/\s](\d{4})/,L.regex.shortDateYMD=/(\d{4})[\/\s](\d{1,2})[\/\s](\d{1,2})/,L.convertFormat=function(e,t){e=(e||"").replace(L.regex.spaces," ").replace(L.regex.shortDateReplace,"/"),"mmddyyyy"===t?e=e.replace(L.regex.shortDateXXY,"$3/$1/$2"):"ddmmyyyy"===t?e=e.replace(L.regex.shortDateXXY,"$3/$2/$1"):"yyyymmdd"===t&&(e=e.replace(L.regex.shortDateYMD,"$1/$2/$3"));var r=new Date(e);return r instanceof Date&&isFinite(r)?r.getTime():""},L.addParser({id:"shortDate",is:function(e){return e=(e||"").replace(L.regex.spaces," ").replace(L.regex.shortDateReplace,"/"),L.regex.shortDateTest.test(e)},format:function(e,t,r,o){if(e){var s=t.config,a=s.$headerIndexed[o],n=a.length&&a.data("dateFormat")||L.getData(a,L.getColumnData(t,s.headers,o),"dateFormat")||s.dateFormat;return a.length&&a.data("dateFormat",n),L.convertFormat(e,n)||e}return e},type:"numeric"}),L.regex.timeTest=/^(0?[1-9]|1[0-2]):([0-5]\d)(\s[AP]M)$|^((?:[01]\d|[2][0-4]):[0-5]\d)$/i,L.regex.timeMatch=/(0?[1-9]|1[0-2]):([0-5]\d)(\s[AP]M)|((?:[01]\d|[2][0-4]):[0-5]\d)/i,L.addParser({id:"time",is:function(e){return L.regex.timeTest.test(e)},format:function(e){var t=(e||"").match(L.regex.timeMatch),r=new Date(e),o=e&&(null!==t?t[0]:"00:00 AM"),s=o?new Date("2000/01/01 "+o.replace(L.regex.dateReplace,"$1 $2")):o;return s instanceof Date&&isFinite(s)?(r instanceof Date&&isFinite(r)?r.getTime():0)?parseFloat(s.getTime()+"."+r.getTime()):s.getTime():e},type:"numeric"}),L.addParser({id:"metadata",is:function(){return!1},format:function(e,t,r){var o=t.config,s=o.parserMetadataName?o.parserMetadataName:"sortValue";return A(r).metadata()[s]},type:"numeric"}),L.addWidget({id:"zebra",priority:90,format:function(e,t,r){var o,s,a,n,i,d,l,c=new RegExp(t.cssChildRow,"i"),g=t.$tbodies.add(A(t.namespace+"_extra_table").children("tbody:not(."+t.cssInfoBlock+")"));for(i=0;i<g.length;i++)for(a=0,l=(o=g.eq(i).children("tr:visible").not(t.selectorRemove)).length,d=0;d<l;d++)s=o.eq(d),c.test(s[0].className)||a++,n=a%2==0,s.removeClass(r.zebra[n?1:0]).addClass(r.zebra[n?0:1])},remove:function(e,t,r,o){if(!o){var s,a,n=t.$tbodies,i=(r.zebra||["even","odd"]).join(" ");for(s=0;s<n.length;s++)(a=L.processTbody(e,n.eq(s),!0)).children().removeClass(i),L.processTbody(e,a,!1)}}})}(e),e.tablesorter});


/* plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.js?1649159526 */

;(function($){
"use strict";
var version='2.1.6';
$.fn.cycle=function(options){
var o;
if(this.length===0&&!$.isReady){
o={s:this.selector,c:this.context};
$.fn.cycle.log('requeuing slideshow (dom not ready)');
$(function(){
$(o.s,o.c).cycle(options);
});
return this;
}
return this.each(function(){
var data,opts,shortName,val;
var container=$(this);
var log=$.fn.cycle.log;
if(container.data('cycle.opts'))
return;
if(container.data('cycle-log')===false||
(options&&options.log===false)||
(opts&&opts.log===false)){
log=$.noop;
}
log('--c2 init--');
data=container.data();
for(var p in data){
if(data.hasOwnProperty(p)&&/^cycle[A-Z]+/.test(p)){
val=data[p];
shortName=p.match(/^cycle(.*)/)[1].replace(/^[A-Z]/,lowerCase);
log(shortName+':',val,'('+typeof val+')');
data[shortName]=val;
}
}
opts=$.extend({},$.fn.cycle.defaults,data,options||{});
opts.timeoutId=0;
opts.paused=opts.paused||false;
opts.container=container;
opts._maxZ=opts.maxZ;
opts.API=$.extend({_container:container},$.fn.cycle.API);
opts.API.log=log;
opts.API.trigger=function(eventName,args){
opts.container.trigger(eventName,args);
return opts.API;
};
container.data('cycle.opts',opts);
container.data('cycle.API',opts.API);
opts.API.trigger('cycle-bootstrap',[opts,opts.API]);
opts.API.addInitialSlides();
opts.API.preInitSlideshow();
if(opts.slides.length)
opts.API.initSlideshow();
});
};
$.fn.cycle.API={
opts:function(){
return this._container.data('cycle.opts');
},
addInitialSlides:function(){
var opts=this.opts();
var slides=opts.slides;
opts.slideCount=0;
opts.slides=$();
slides=slides.jquery?slides:opts.container.find(slides);
if(opts.random){
slides.sort(function(){return Math.random()-0.5;});
}
opts.API.add(slides);
},
preInitSlideshow:function(){
var opts=this.opts();
opts.API.trigger('cycle-pre-initialize',[opts]);
var tx=$.fn.cycle.transitions[opts.fx];
if(tx&&$.isFunction(tx.preInit))
tx.preInit(opts);
opts._preInitialized=true;
},
postInitSlideshow:function(){
var opts=this.opts();
opts.API.trigger('cycle-post-initialize',[opts]);
var tx=$.fn.cycle.transitions[opts.fx];
if(tx&&$.isFunction(tx.postInit))
tx.postInit(opts);
},
initSlideshow:function(){
var opts=this.opts();
var pauseObj=opts.container;
var slideOpts;
opts.API.calcFirstSlide();
if(opts.container.css('position')=='static')
opts.container.css('position','relative');
$(opts.slides[opts.currSlide]).css({
opacity:1,
display:'block',
visibility:'visible'
});
opts.API.stackSlides(opts.slides[opts.currSlide],opts.slides[opts.nextSlide],!opts.reverse);
if(opts.pauseOnHover){
if(opts.pauseOnHover!==true)
pauseObj=$(opts.pauseOnHover);
pauseObj.hover(
function(){opts.API.pause(true);},
function(){opts.API.resume(true);}
);
}
if(opts.timeout){
slideOpts=opts.API.getSlideOpts(opts.currSlide);
opts.API.queueTransition(slideOpts,slideOpts.timeout+opts.delay);
}
opts._initialized=true;
opts.API.updateView(true);
opts.API.trigger('cycle-initialized',[opts]);
opts.API.postInitSlideshow();
},
pause:function(hover){
var opts=this.opts(),
slideOpts=opts.API.getSlideOpts(),
alreadyPaused=opts.hoverPaused||opts.paused;
if(hover)
opts.hoverPaused=true;
else
opts.paused=true;
if(!alreadyPaused){
opts.container.addClass('cycle-paused');
opts.API.trigger('cycle-paused',[opts]).log('cycle-paused');
if(slideOpts.timeout){
clearTimeout(opts.timeoutId);
opts.timeoutId=0;
opts._remainingTimeout-=($.now()-opts._lastQueue);
if(opts._remainingTimeout<0||isNaN(opts._remainingTimeout))
opts._remainingTimeout=undefined;
}
}
},
resume:function(hover){
var opts=this.opts(),
alreadyResumed=!opts.hoverPaused&&!opts.paused,
remaining;
if(hover)
opts.hoverPaused=false;
else
opts.paused=false;
if(!alreadyResumed){
opts.container.removeClass('cycle-paused');
if(opts.slides.filter(':animated').length===0)
opts.API.queueTransition(opts.API.getSlideOpts(),opts._remainingTimeout);
opts.API.trigger('cycle-resumed',[opts,opts._remainingTimeout]).log('cycle-resumed');
}
},
add:function(slides,prepend){
var opts=this.opts();
var oldSlideCount=opts.slideCount;
var startSlideshow=false;
var len;
if($.type(slides)=='string')
slides=$.trim(slides);
$(slides).each(function(i){
var slideOpts;
var slide=$(this);
if(prepend)
opts.container.prepend(slide);
else
opts.container.append(slide);
opts.slideCount++;
slideOpts=opts.API.buildSlideOpts(slide);
if(prepend)
opts.slides=$(slide).add(opts.slides);
else
opts.slides=opts.slides.add(slide);
opts.API.initSlide(slideOpts,slide,--opts._maxZ);
slide.data('cycle.opts',slideOpts);
opts.API.trigger('cycle-slide-added',[opts,slideOpts,slide]);
});
opts.API.updateView(true);
startSlideshow=opts._preInitialized&&(oldSlideCount<2&&opts.slideCount>=1);
if(startSlideshow){
if(!opts._initialized)
opts.API.initSlideshow();
else if(opts.timeout){
len=opts.slides.length;
opts.nextSlide=opts.reverse?len-1:1;
if(!opts.timeoutId){
opts.API.queueTransition(opts);
}
}
}
},
calcFirstSlide:function(){
var opts=this.opts();
var firstSlideIndex;
firstSlideIndex=parseInt(opts.startingSlide||0,10);
if(firstSlideIndex>=opts.slides.length||firstSlideIndex<0)
firstSlideIndex=0;
opts.currSlide=firstSlideIndex;
if(opts.reverse){
opts.nextSlide=firstSlideIndex-1;
if(opts.nextSlide<0)
opts.nextSlide=opts.slides.length-1;
}
else{
opts.nextSlide=firstSlideIndex+1;
if(opts.nextSlide==opts.slides.length)
opts.nextSlide=0;
}
},
calcNextSlide:function(){
var opts=this.opts();
var roll;
if(opts.reverse){
roll=(opts.nextSlide-1)<0;
opts.nextSlide=roll?opts.slideCount-1:opts.nextSlide-1;
opts.currSlide=roll?0:opts.nextSlide+1;
}
else{
roll=(opts.nextSlide+1)==opts.slides.length;
opts.nextSlide=roll?0:opts.nextSlide+1;
opts.currSlide=roll?opts.slides.length-1:opts.nextSlide-1;
}
},
calcTx:function(slideOpts,manual){
var opts=slideOpts;
var tx;
if(opts._tempFx)
tx=$.fn.cycle.transitions[opts._tempFx];
else if(manual&&opts.manualFx)
tx=$.fn.cycle.transitions[opts.manualFx];
if(!tx)
tx=$.fn.cycle.transitions[opts.fx];
opts._tempFx=null;
this.opts()._tempFx=null;
if(!tx){
tx=$.fn.cycle.transitions.fade;
opts.API.log('Transition "'+opts.fx+'" not found.  Using fade.');
}
return tx;
},
prepareTx:function(manual,fwd){
var opts=this.opts();
var after,curr,next,slideOpts,tx;
if(opts.slideCount<2){
opts.timeoutId=0;
return;
}
if(manual&&(!opts.busy||opts.manualTrump)){
opts.API.stopTransition();
opts.busy=false;
clearTimeout(opts.timeoutId);
opts.timeoutId=0;
}
if(opts.busy)
return;
if(opts.timeoutId===0&&!manual)
return;
curr=opts.slides[opts.currSlide];
next=opts.slides[opts.nextSlide];
slideOpts=opts.API.getSlideOpts(opts.nextSlide);
tx=opts.API.calcTx(slideOpts,manual);
opts._tx=tx;
if(manual&&slideOpts.manualSpeed!==undefined)
slideOpts.speed=slideOpts.manualSpeed;
if(opts.nextSlide!=opts.currSlide&&
(manual||(!opts.paused&&!opts.hoverPaused&&opts.timeout))){
opts.API.trigger('cycle-before',[slideOpts,curr,next,fwd]);
if(tx.before)
tx.before(slideOpts,curr,next,fwd);
after=function(){
opts.busy=false;
if(!opts.container.data('cycle.opts'))
return;
if(tx.after)
tx.after(slideOpts,curr,next,fwd);
opts.API.trigger('cycle-after',[slideOpts,curr,next,fwd]);
opts.API.queueTransition(slideOpts);
opts.API.updateView(true);
};
opts.busy=true;
if(tx.transition)
tx.transition(slideOpts,curr,next,fwd,after);
else
opts.API.doTransition(slideOpts,curr,next,fwd,after);
opts.API.calcNextSlide();
opts.API.updateView();
}else{
opts.API.queueTransition(slideOpts);
}
},
doTransition:function(slideOpts,currEl,nextEl,fwd,callback){
var opts=slideOpts;
var curr=$(currEl),next=$(nextEl);
var fn=function(){
next.animate(opts.animIn||{opacity:1},opts.speed,opts.easeIn||opts.easing,callback);
};
next.css(opts.cssBefore||{});
curr.animate(opts.animOut||{},opts.speed,opts.easeOut||opts.easing,function(){
curr.css(opts.cssAfter||{});
if(!opts.sync){
fn();
}
});
if(opts.sync){
fn();
}
},
queueTransition:function(slideOpts,specificTimeout){
var opts=this.opts();
var timeout=specificTimeout!==undefined?specificTimeout:slideOpts.timeout;
if(opts.nextSlide===0&&--opts.loop===0){
opts.API.log('terminating; loop=0');
opts.timeout=0;
if(timeout){
setTimeout(function(){
opts.API.trigger('cycle-finished',[opts]);
},timeout);
}
else{
opts.API.trigger('cycle-finished',[opts]);
}
opts.nextSlide=opts.currSlide;
return;
}
if(opts.continueAuto!==undefined){
if(opts.continueAuto===false||
($.isFunction(opts.continueAuto)&&opts.continueAuto()===false)){
opts.API.log('terminating automatic transitions');
opts.timeout=0;
if(opts.timeoutId)
clearTimeout(opts.timeoutId);
return;
}
}
if(timeout){
opts._lastQueue=$.now();
if(specificTimeout===undefined)
opts._remainingTimeout=slideOpts.timeout;
if(!opts.paused&&!opts.hoverPaused){
opts.timeoutId=setTimeout(function(){
opts.API.prepareTx(false,!opts.reverse);
},timeout);
}
}
},
stopTransition:function(){
var opts=this.opts();
if(opts.slides.filter(':animated').length){
opts.slides.stop(false,true);
opts.API.trigger('cycle-transition-stopped',[opts]);
}
if(opts._tx&&opts._tx.stopTransition)
opts._tx.stopTransition(opts);
},
advanceSlide:function(val){
var opts=this.opts();
clearTimeout(opts.timeoutId);
opts.timeoutId=0;
opts.nextSlide=opts.currSlide+val;
if(opts.nextSlide<0)
opts.nextSlide=opts.slides.length-1;
else if(opts.nextSlide>=opts.slides.length)
opts.nextSlide=0;
opts.API.prepareTx(true,val>=0);
return false;
},
buildSlideOpts:function(slide){
var opts=this.opts();
var val,shortName;
var slideOpts=slide.data()||{};
for(var p in slideOpts){
if(slideOpts.hasOwnProperty(p)&&/^cycle[A-Z]+/.test(p)){
val=slideOpts[p];
shortName=p.match(/^cycle(.*)/)[1].replace(/^[A-Z]/,lowerCase);
opts.API.log('['+(opts.slideCount-1)+']',shortName+':',val,'('+typeof val+')');
slideOpts[shortName]=val;
}
}
slideOpts=$.extend({},$.fn.cycle.defaults,opts,slideOpts);
slideOpts.slideNum=opts.slideCount;
try{
delete slideOpts.API;
delete slideOpts.slideCount;
delete slideOpts.currSlide;
delete slideOpts.nextSlide;
delete slideOpts.slides;
}catch(e){
}
return slideOpts;
},
getSlideOpts:function(index){
var opts=this.opts();
if(index===undefined)
index=opts.currSlide;
var slide=opts.slides[index];
var slideOpts=$(slide).data('cycle.opts');
return $.extend({},opts,slideOpts);
},
initSlide:function(slideOpts,slide,suggestedZindex){
var opts=this.opts();
slide.css(slideOpts.slideCss||{});
if(suggestedZindex>0)
slide.css('zIndex',suggestedZindex);
if(isNaN(slideOpts.speed))
slideOpts.speed=$.fx.speeds[slideOpts.speed]||$.fx.speeds._default;
if(!slideOpts.sync)
slideOpts.speed=slideOpts.speed/2;
slide.addClass(opts.slideClass);
},
updateView:function(isAfter,isDuring,forceEvent){
var opts=this.opts();
if(!opts._initialized)
return;
var slideOpts=opts.API.getSlideOpts();
var currSlide=opts.slides[opts.currSlide];
if(!isAfter&&isDuring!==true){
opts.API.trigger('cycle-update-view-before',[opts,slideOpts,currSlide]);
if(opts.updateView<0)
return;
}
if(opts.slideActiveClass){
opts.slides.removeClass(opts.slideActiveClass)
.eq(opts.currSlide).addClass(opts.slideActiveClass);
}
if(isAfter&&opts.hideNonActive)
opts.slides.filter(':not(.'+opts.slideActiveClass+')').css('visibility','hidden');
if(opts.updateView===0){
setTimeout(function(){
opts.API.trigger('cycle-update-view',[opts,slideOpts,currSlide,isAfter]);
},slideOpts.speed/(opts.sync?2:1));
}
if(opts.updateView!==0)
opts.API.trigger('cycle-update-view',[opts,slideOpts,currSlide,isAfter]);
if(isAfter)
opts.API.trigger('cycle-update-view-after',[opts,slideOpts,currSlide]);
},
getComponent:function(name){
var opts=this.opts();
var selector=opts[name];
if(typeof selector==='string'){
return(/^\s*[\>|\+|~]/).test(selector)?opts.container.find(selector):$(selector);
}
if(selector.jquery)
return selector;
return $(selector);
},
stackSlides:function(curr,next,fwd){
var opts=this.opts();
if(!curr){
curr=opts.slides[opts.currSlide];
next=opts.slides[opts.nextSlide];
fwd=!opts.reverse;
}
$(curr).css('zIndex',opts.maxZ);
var i;
var z=opts.maxZ-2;
var len=opts.slideCount;
if(fwd){
for(i=opts.currSlide+1;i<len;i++)
$(opts.slides[i]).css('zIndex',z--);
for(i=0;i<opts.currSlide;i++)
$(opts.slides[i]).css('zIndex',z--);
}
else{
for(i=opts.currSlide-1;i>=0;i--)
$(opts.slides[i]).css('zIndex',z--);
for(i=len-1;i>opts.currSlide;i--)
$(opts.slides[i]).css('zIndex',z--);
}
$(next).css('zIndex',opts.maxZ-1);
},
getSlideIndex:function(el){
return this.opts().slides.index(el);
}
};
$.fn.cycle.log=function log(){
if(window.console&&console.log)
console.log('[cycle2] '+Array.prototype.join.call(arguments,' '));
};
$.fn.cycle.version=function(){return'Cycle2: '+version;};
function lowerCase(s){
return(s||'').toLowerCase();
}
$.fn.cycle.transitions={
custom:{
},
none:{
before:function(opts,curr,next,fwd){
opts.API.stackSlides(next,curr,fwd);
opts.cssBefore={opacity:1,visibility:'visible',display:'block'};
}
},
fade:{
before:function(opts,curr,next,fwd){
var css=opts.API.getSlideOpts(opts.nextSlide).slideCss||{};
opts.API.stackSlides(curr,next,fwd);
opts.cssBefore=$.extend(css,{opacity:0,visibility:'visible',display:'block'});
opts.animIn={opacity:1};
opts.animOut={opacity:0};
}
},
fadeout:{
before:function(opts,curr,next,fwd){
var css=opts.API.getSlideOpts(opts.nextSlide).slideCss||{};
opts.API.stackSlides(curr,next,fwd);
opts.cssBefore=$.extend(css,{opacity:1,visibility:'visible',display:'block'});
opts.animOut={opacity:0};
}
},
scrollHorz:{
before:function(opts,curr,next,fwd){
opts.API.stackSlides(curr,next,fwd);
var w=opts.container.css('overflow','hidden').width();
opts.cssBefore={left:fwd?w:-w,top:0,opacity:1,visibility:'visible',display:'block'};
opts.cssAfter={zIndex:opts._maxZ-2,left:0};
opts.animIn={left:0};
opts.animOut={left:fwd?-w:w};
}
}
};
$.fn.cycle.defaults={
allowWrap:true,
autoSelector:'.cycle-slideshow[data-cycle-auto-init!=false]',
delay:0,
easing:null,
fx:'fade',
hideNonActive:true,
loop:0,
manualFx:undefined,
manualSpeed:undefined,
manualTrump:true,
maxZ:100,
pauseOnHover:false,
reverse:false,
slideActiveClass:'cycle-slide-active',
slideClass:'cycle-slide',
slideCss:{position:'absolute',top:0,left:0},
slides:'> img',
speed:500,
startingSlide:0,
sync:true,
timeout:4000,
updateView:0
};
$(document).ready(function(){
$($.fn.cycle.defaults.autoSelector).cycle();
});
})(jQuery);
(function($){
"use strict";
$.extend($.fn.cycle.defaults,{
autoHeight:0,
autoHeightSpeed:250,
autoHeightEasing:null
});
$(document).on('cycle-initialized',function(e,opts){
var autoHeight=opts.autoHeight;
var t=$.type(autoHeight);
var resizeThrottle=null;
var ratio;
if(t!=='string'&&t!=='number')
return;
opts.container.on('cycle-slide-added cycle-slide-removed',initAutoHeight);
opts.container.on('cycle-destroyed',onDestroy);
if(autoHeight=='container'){
opts.container.on('cycle-before',onBefore);
}
else if(t==='string'&&/\d+\:\d+/.test(autoHeight)){
ratio=autoHeight.match(/(\d+)\:(\d+)/);
ratio=ratio[1]/ratio[2];
opts._autoHeightRatio=ratio;
}
if(t!=='number'){
opts._autoHeightOnResize=function(){
clearTimeout(resizeThrottle);
resizeThrottle=setTimeout(onResize,50);
};
$(window).on('resize orientationchange',opts._autoHeightOnResize);
}
setTimeout(onResize,30);
function onResize(){
initAutoHeight(e,opts);
}
});
function initAutoHeight(e,opts){
var clone,height,sentinelIndex;
var autoHeight=opts.autoHeight;
if(autoHeight=='container'){
height=$(opts.slides[opts.currSlide]).outerHeight();
opts.container.height(height);
}
else if(opts._autoHeightRatio){
opts.container.height(opts.container.width()/opts._autoHeightRatio);
}
else if(autoHeight==='calc'||($.type(autoHeight)=='number'&&autoHeight>=0)){
if(autoHeight==='calc')
sentinelIndex=calcSentinelIndex(e,opts);
else if(autoHeight>=opts.slides.length)
sentinelIndex=0;
else
sentinelIndex=autoHeight;
if(sentinelIndex==opts._sentinelIndex)
return;
opts._sentinelIndex=sentinelIndex;
if(opts._sentinel)
opts._sentinel.remove();
clone=$(opts.slides[sentinelIndex].cloneNode(true));
clone.removeAttr('id name rel').find('[id],[name],[rel]').removeAttr('id name rel');
clone.css({
position:'static',
visibility:'hidden',
display:'block'
}).prependTo(opts.container).addClass('cycle-sentinel cycle-slide').removeClass('cycle-slide-active');
clone.find('*').css('visibility','hidden');
opts._sentinel=clone;
}
}
function calcSentinelIndex(e,opts){
var index=0,max=-1;
opts.slides.each(function(i){
var h=$(this).height();
if(h>max){
max=h;
index=i;
}
});
return index;
}
function onBefore(e,opts,outgoing,incoming,forward){
var h=$(incoming).outerHeight();
opts.container.animate({height:h},opts.autoHeightSpeed,opts.autoHeightEasing);
}
function onDestroy(e,opts){
if(opts._autoHeightOnResize){
$(window).off('resize orientationchange',opts._autoHeightOnResize);
opts._autoHeightOnResize=null;
}
opts.container.off('cycle-slide-added cycle-slide-removed',initAutoHeight);
opts.container.off('cycle-destroyed',onDestroy);
opts.container.off('cycle-before',onBefore);
if(opts._sentinel){
opts._sentinel.remove();
opts._sentinel=null;
}
}
})(jQuery);
(function($){
"use strict";
$.extend($.fn.cycle.defaults,{
caption:'> .cycle-caption',
captionTemplate:'{{slideNum}} / {{slideCount}}',
overlay:'> .cycle-overlay',
overlayTemplate:'<div>{{title}}</div><div>{{desc}}</div>',
captionModule:'caption'
});
$(document).on('cycle-update-view',function(e,opts,slideOpts,currSlide){
if(opts.captionModule!=='caption')
return;
var el;
$.each(['caption','overlay'],function(){
var name=this;
var template=slideOpts[name+'Template'];
var el=opts.API.getComponent(name);
if(el.length&&template){
el.html(opts.API.tmpl(template,slideOpts,opts,currSlide));
el.show();
}
else{
el.hide();
}
});
});
$(document).on('cycle-destroyed',function(e,opts){
var el;
$.each(['caption','overlay'],function(){
var name=this,template=opts[name+'Template'];
if(opts[name]&&template){
el=opts.API.getComponent('caption');
el.empty();
}
});
});
})(jQuery);
(function($){
"use strict";
var c2=$.fn.cycle;
$.fn.cycle=function(options){
var cmd,cmdFn,opts;
var args=$.makeArray(arguments);
if($.type(options)=='number'){
return this.cycle('goto',options);
}
if($.type(options)=='string'){
return this.each(function(){
var cmdArgs;
cmd=options;
opts=$(this).data('cycle.opts');
if(opts===undefined){
c2.log('slideshow must be initialized before sending commands; "'+cmd+'" ignored');
return;
}
else{
cmd=cmd=='goto'?'jump':cmd;
cmdFn=opts.API[cmd];
if($.isFunction(cmdFn)){
cmdArgs=$.makeArray(args);
cmdArgs.shift();
return cmdFn.apply(opts.API,cmdArgs);
}
else{
c2.log('unknown command: ',cmd);
}
}
});
}
else{
return c2.apply(this,arguments);
}
};
$.extend($.fn.cycle,c2);
$.extend(c2.API,{
next:function(){
var opts=this.opts();
if(opts.busy&&!opts.manualTrump)
return;
var count=opts.reverse?-1:1;
if(opts.allowWrap===false&&(opts.currSlide+count)>=opts.slideCount)
return;
opts.API.advanceSlide(count);
opts.API.trigger('cycle-next',[opts]).log('cycle-next');
},
prev:function(){
var opts=this.opts();
if(opts.busy&&!opts.manualTrump)
return;
var count=opts.reverse?1:-1;
if(opts.allowWrap===false&&(opts.currSlide+count)<0)
return;
opts.API.advanceSlide(count);
opts.API.trigger('cycle-prev',[opts]).log('cycle-prev');
},
destroy:function(){
this.stop();
var opts=this.opts();
var clean=$.isFunction($._data)?$._data:$.noop;
clearTimeout(opts.timeoutId);
opts.timeoutId=0;
opts.API.stop();
opts.API.trigger('cycle-destroyed',[opts]).log('cycle-destroyed');
opts.container.removeData();
clean(opts.container[0],'parsedAttrs',false);
if(!opts.retainStylesOnDestroy){
opts.container.removeAttr('style');
opts.slides.removeAttr('style');
opts.slides.removeClass(opts.slideActiveClass);
}
opts.slides.each(function(){
var slide=$(this);
slide.removeData();
slide.removeClass(opts.slideClass);
clean(this,'parsedAttrs',false);
});
},
jump:function(index,fx){
var fwd;
var opts=this.opts();
if(opts.busy&&!opts.manualTrump)
return;
var num=parseInt(index,10);
if(isNaN(num)||num<0||num>=opts.slides.length){
opts.API.log('goto: invalid slide index: '+num);
return;
}
if(num==opts.currSlide){
opts.API.log('goto: skipping, already on slide',num);
return;
}
opts.nextSlide=num;
clearTimeout(opts.timeoutId);
opts.timeoutId=0;
opts.API.log('goto: ',num,' (zero-index)');
fwd=opts.currSlide<opts.nextSlide;
opts._tempFx=fx;
opts.API.prepareTx(true,fwd);
},
stop:function(){
var opts=this.opts();
var pauseObj=opts.container;
clearTimeout(opts.timeoutId);
opts.timeoutId=0;
opts.API.stopTransition();
if(opts.pauseOnHover){
if(opts.pauseOnHover!==true)
pauseObj=$(opts.pauseOnHover);
pauseObj.off('mouseenter mouseleave');
}
opts.API.trigger('cycle-stopped',[opts]).log('cycle-stopped');
},
reinit:function(){
var opts=this.opts();
opts.API.destroy();
opts.container.cycle();
},
remove:function(index){
var opts=this.opts();
var slide,slideToRemove,slides=[],slideNum=1;
for(var i=0;i<opts.slides.length;i++){
slide=opts.slides[i];
if(i==index){
slideToRemove=slide;
}
else{
slides.push(slide);
$(slide).data('cycle.opts').slideNum=slideNum;
slideNum++;
}
}
if(slideToRemove){
opts.slides=$(slides);
opts.slideCount--;
$(slideToRemove).remove();
if(index==opts.currSlide)
opts.API.advanceSlide(1);
else if(index<opts.currSlide)
opts.currSlide--;
else
opts.currSlide++;
opts.API.trigger('cycle-slide-removed',[opts,index,slideToRemove]).log('cycle-slide-removed');
opts.API.updateView();
}
}
});
$(document).on('click.cycle','[data-cycle-cmd]',function(e){
e.preventDefault();
var el=$(this);
var command=el.data('cycle-cmd');
var context=el.data('cycle-context')||'.cycle-slideshow';
$(context).cycle(command,el.data('cycle-arg'));
});
})(jQuery);
(function($){
"use strict";
$(document).on('cycle-pre-initialize',function(e,opts){
onHashChange(opts,true);
opts._onHashChange=function(){
onHashChange(opts,false);
};
$(window).on('hashchange',opts._onHashChange);
});
$(document).on('cycle-update-view',function(e,opts,slideOpts){
if(slideOpts.hash&&('#'+slideOpts.hash)!=window.location.hash){
opts._hashFence=true;
window.location.hash=slideOpts.hash;
}
});
$(document).on('cycle-destroyed',function(e,opts){
if(opts._onHashChange){
$(window).off('hashchange',opts._onHashChange);
}
});
function onHashChange(opts,setStartingSlide){
var hash;
if(opts._hashFence){
opts._hashFence=false;
return;
}
hash=window.location.hash.substring(1);
opts.slides.each(function(i){
if($(this).data('cycle-hash')==hash){
if(setStartingSlide===true){
opts.startingSlide=i;
}
else{
var fwd=opts.currSlide<i;
opts.nextSlide=i;
opts.API.prepareTx(true,fwd);
}
return false;
}
});
}
})(jQuery);
(function($){
"use strict";
$.extend($.fn.cycle.defaults,{
loader:false
});
$(document).on('cycle-bootstrap',function(e,opts){
var addFn;
if(!opts.loader)
return;
addFn=opts.API.add;
opts.API.add=add;
function add(slides,prepend){
var slideArr=[];
if($.type(slides)=='string')
slides=$.trim(slides);
else if($.type(slides)==='array'){
for(var i=0;i<slides.length;i++)
slides[i]=$(slides[i])[0];
}
slides=$(slides);
var slideCount=slides.length;
if(!slideCount)
return;
slides.css('visibility','hidden').appendTo('body').each(function(i){
var count=0;
var slide=$(this);
var images=slide.is('img')?slide:slide.find('img');
slide.data('index',i);
images=images.filter(':not(.cycle-loader-ignore)').filter(':not([src=""])');
if(!images.length){
--slideCount;
slideArr.push(slide);
return;
}
count=images.length;
images.each(function(){
if(this.complete){
imageLoaded();
}
else{
$(this).load(function(){
imageLoaded();
}).on("error",function(){
if(--count===0){
opts.API.log('slide skipped; img not loaded:',this.src);
if(--slideCount===0&&opts.loader=='wait'){
addFn.apply(opts.API,[slideArr,prepend]);
}
}
});
}
});
function imageLoaded(){
if(--count===0){
--slideCount;
addSlide(slide);
}
}
});
if(slideCount)
opts.container.addClass('cycle-loading');
function addSlide(slide){
var curr;
if(opts.loader=='wait'){
slideArr.push(slide);
if(slideCount===0){
slideArr.sort(sorter);
addFn.apply(opts.API,[slideArr,prepend]);
opts.container.removeClass('cycle-loading');
}
}
else{
curr=$(opts.slides[opts.currSlide]);
addFn.apply(opts.API,[slide,prepend]);
curr.show();
opts.container.removeClass('cycle-loading');
}
}
function sorter(a,b){
return a.data('index')-b.data('index');
}
}
});
})(jQuery);
(function($){
"use strict";
$.extend($.fn.cycle.defaults,{
pager:'> .cycle-pager',
pagerActiveClass:'cycle-pager-active',
pagerEvent:'click.cycle',
pagerEventBubble:undefined,
pagerTemplate:'<span>&bull;</span>'
});
$(document).on('cycle-bootstrap',function(e,opts,API){
API.buildPagerLink=buildPagerLink;
});
$(document).on('cycle-slide-added',function(e,opts,slideOpts,slideAdded){
if(opts.pager){
opts.API.buildPagerLink(opts,slideOpts,slideAdded);
opts.API.page=page;
}
});
$(document).on('cycle-slide-removed',function(e,opts,index,slideRemoved){
if(opts.pager){
var pagers=opts.API.getComponent('pager');
pagers.each(function(){
var pager=$(this);
$(pager.children()[index]).remove();
});
}
});
$(document).on('cycle-update-view',function(e,opts,slideOpts){
var pagers;
if(opts.pager){
pagers=opts.API.getComponent('pager');
pagers.each(function(){
$(this).children().removeClass(opts.pagerActiveClass)
.eq(opts.currSlide).addClass(opts.pagerActiveClass);
});
}
});
$(document).on('cycle-destroyed',function(e,opts){
var pager=opts.API.getComponent('pager');
if(pager){
pager.children().off(opts.pagerEvent);
if(opts.pagerTemplate)
pager.empty();
}
});
function buildPagerLink(opts,slideOpts,slide){
var pagerLink;
var pagers=opts.API.getComponent('pager');
pagers.each(function(){
var pager=$(this);
if(slideOpts.pagerTemplate){
var markup=opts.API.tmpl(slideOpts.pagerTemplate,slideOpts,opts,slide[0]);
pagerLink=$(markup).appendTo(pager);
}
else{
pagerLink=pager.children().eq(opts.slideCount-1);
}
pagerLink.on(opts.pagerEvent,function(e){
if(!opts.pagerEventBubble)
e.preventDefault();
opts.API.page(pager,e.currentTarget);
});
});
}
function page(pager,target){
var opts=this.opts();
if(opts.busy&&!opts.manualTrump)
return;
var index=pager.children().index(target);
var nextSlide=index;
var fwd=opts.currSlide<nextSlide;
if(opts.currSlide==nextSlide){
return;
}
opts.nextSlide=nextSlide;
opts._tempFx=opts.pagerFx;
opts.API.prepareTx(true,fwd);
opts.API.trigger('cycle-pager-activated',[opts,pager,target]);
}
})(jQuery);
(function($){
"use strict";
$.extend($.fn.cycle.defaults,{
next:'> .cycle-next',
nextEvent:'click.cycle',
disabledClass:'disabled',
prev:'> .cycle-prev',
prevEvent:'click.cycle',
swipe:false
});
$(document).on('cycle-initialized',function(e,opts){
opts.API.getComponent('next').on(opts.nextEvent,function(e){
e.preventDefault();
opts.API.next();
});
opts.API.getComponent('prev').on(opts.prevEvent,function(e){
e.preventDefault();
opts.API.prev();
});
if(opts.swipe){
var nextEvent=opts.swipeVert?'swipeUp.cycle':'swipeLeft.cycle swipeleft.cycle';
var prevEvent=opts.swipeVert?'swipeDown.cycle':'swipeRight.cycle swiperight.cycle';
opts.container.on(nextEvent,function(e){
opts._tempFx=opts.swipeFx;
opts.API.next();
});
opts.container.on(prevEvent,function(){
opts._tempFx=opts.swipeFx;
opts.API.prev();
});
}
});
$(document).on('cycle-update-view',function(e,opts,slideOpts,currSlide){
if(opts.allowWrap)
return;
var cls=opts.disabledClass;
var next=opts.API.getComponent('next');
var prev=opts.API.getComponent('prev');
var prevBoundry=opts._prevBoundry||0;
var nextBoundry=(opts._nextBoundry!==undefined)?opts._nextBoundry:opts.slideCount-1;
if(opts.currSlide==nextBoundry)
next.addClass(cls).prop('disabled',true);
else
next.removeClass(cls).prop('disabled',false);
if(opts.currSlide===prevBoundry)
prev.addClass(cls).prop('disabled',true);
else
prev.removeClass(cls).prop('disabled',false);
});
$(document).on('cycle-destroyed',function(e,opts){
opts.API.getComponent('prev').off(opts.nextEvent);
opts.API.getComponent('next').off(opts.prevEvent);
opts.container.off('swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle');
});
})(jQuery);
(function($){
"use strict";
$.extend($.fn.cycle.defaults,{
progressive:false
});
$(document).on('cycle-pre-initialize',function(e,opts){
if(!opts.progressive)
return;
var API=opts.API;
var nextFn=API.next;
var prevFn=API.prev;
var prepareTxFn=API.prepareTx;
var type=$.type(opts.progressive);
var slides,scriptEl;
if(type=='array'){
slides=opts.progressive;
}
else if($.isFunction(opts.progressive)){
slides=opts.progressive(opts);
}
else if(type=='string'){
scriptEl=$(opts.progressive);
slides=$.trim(scriptEl.html());
if(!slides)
return;
if(/^(\[)/.test(slides)){
try{
slides=$.parseJSON(slides);
}
catch(err){
API.log('error parsing progressive slides',err);
return;
}
}
else{
slides=slides.split(new RegExp(scriptEl.data('cycle-split')||'\n'));
if(!slides[slides.length-1])
slides.pop();
}
}
if(prepareTxFn){
API.prepareTx=function(manual,fwd){
var index,slide;
if(manual||slides.length===0){
prepareTxFn.apply(opts.API,[manual,fwd]);
return;
}
if(fwd&&opts.currSlide==(opts.slideCount-1)){
slide=slides[0];
slides=slides.slice(1);
opts.container.one('cycle-slide-added',function(e,opts){
setTimeout(function(){
opts.API.advanceSlide(1);
},50);
});
opts.API.add(slide);
}
else if(!fwd&&opts.currSlide===0){
index=slides.length-1;
slide=slides[index];
slides=slides.slice(0,index);
opts.container.one('cycle-slide-added',function(e,opts){
setTimeout(function(){
opts.currSlide=1;
opts.API.advanceSlide(-1);
},50);
});
opts.API.add(slide,true);
}
else{
prepareTxFn.apply(opts.API,[manual,fwd]);
}
};
}
if(nextFn){
API.next=function(){
var opts=this.opts();
if(slides.length&&opts.currSlide==(opts.slideCount-1)){
var slide=slides[0];
slides=slides.slice(1);
opts.container.one('cycle-slide-added',function(e,opts){
nextFn.apply(opts.API);
opts.container.removeClass('cycle-loading');
});
opts.container.addClass('cycle-loading');
opts.API.add(slide);
}
else{
nextFn.apply(opts.API);
}
};
}
if(prevFn){
API.prev=function(){
var opts=this.opts();
if(slides.length&&opts.currSlide===0){
var index=slides.length-1;
var slide=slides[index];
slides=slides.slice(0,index);
opts.container.one('cycle-slide-added',function(e,opts){
opts.currSlide=1;
opts.API.advanceSlide(-1);
opts.container.removeClass('cycle-loading');
});
opts.container.addClass('cycle-loading');
opts.API.add(slide,true);
}
else{
prevFn.apply(opts.API);
}
};
}
});
})(jQuery);
(function($){
"use strict";
$.extend($.fn.cycle.defaults,{
tmplRegex:'{{((.)?.*?)}}'
});
$.extend($.fn.cycle.API,{
tmpl:function(str,opts){
var regex=new RegExp(opts.tmplRegex||$.fn.cycle.defaults.tmplRegex,'g');
var args=$.makeArray(arguments);
args.shift();
return str.replace(regex,function(_,str){
var i,j,obj,prop,names=str.split('.');
for(i=0;i<args.length;i++){
obj=args[i];
if(!obj)
continue;
if(names.length>1){
prop=obj;
for(j=0;j<names.length;j++){
obj=prop;
prop=prop[names[j]]||str;
}
}else{
prop=obj[str];
}
if($.isFunction(prop))
return prop.apply(obj,args);
if(prop!==undefined&&prop!==null&&prop!=str)
return prop;
}
return str;
});
}
});
})(jQuery);


/* plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.flip.js?1649159526 */

(function($){
"use strict";
var backface,
style=document.createElement('div').style,
tx=$.fn.cycle.transitions,
supported=style.transform!==undefined||
style.MozTransform!==undefined||
style.webkitTransform!==undefined||
style.oTransform!==undefined||
style.msTransform!==undefined;
if(supported&&style.msTransform!==undefined){
style.msTransform='rotateY(0deg)';
if(!style.msTransform)
supported=false;
}
if(supported){
tx.flipHorz=getTransition(getRotate('Y'));
tx.flipVert=getTransition(getRotate('X'));
backface={
'-webkit-backface-visibility':'hidden',
'-moz-backface-visibility':'hidden',
'-o-backface-visibility':'hidden',
'backface-visibility':'hidden'
};
}
else{
tx.flipHorz=tx.scrollHorz;
tx.flipVert=tx.scrollVert||tx.scrollHorz;
}
function getTransition(rotateFn){
return{
preInit:function(opts){
opts.slides.css(backface);
},
transition:function(slideOpts,currEl,nextEl,fwd,callback){
var opts=slideOpts,
curr=$(currEl),
next=$(nextEl),
speed=opts.speed/2;
rotateFn.call(next,-90);
next.css({
'display':'block',
'visibility':'visible',
'background-position':'-90px',
'opacity':1
});
curr.css('background-position','0px');
curr.animate({backgroundPosition:90},{
step:rotateFn,
duration:speed,
easing:opts.easeOut||opts.easing,
complete:function(){
slideOpts.API.updateView(false,true);
next.animate({backgroundPosition:0},{
step:rotateFn,
duration:speed,
easing:opts.easeIn||opts.easing,
complete:callback
});
}
});
}
};
}
function getRotate(dir){
return function(degrees){
var el=$(this);
el.css({
'-webkit-transform':'rotate'+dir+'('+degrees+'deg)',
'-moz-transform':'rotate'+dir+'('+degrees+'deg)',
'-ms-transform':'rotate'+dir+'('+degrees+'deg)',
'-o-transform':'rotate'+dir+'('+degrees+'deg)',
'transform':'rotate'+dir+'('+degrees+'deg)'
});
};
}
})(jQuery);


/* plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.carousel.js?1649159525 */

(function($){
"use strict";
$(document).on('cycle-bootstrap',function(e,opts,API){
if(opts.fx!=='carousel')
return;
API.getSlideIndex=function(el){
var slides=this.opts()._carouselWrap.children();
var i=slides.index(el);
return i%slides.length;
};
API.next=function(){
var count=opts.reverse?-1:1;
if(opts.allowWrap===false&&(opts.currSlide+count)>opts.slideCount-opts.carouselVisible)
return;
opts.API.advanceSlide(count);
opts.API.trigger('cycle-next',[opts]).log('cycle-next');
};
});
$.fn.cycle.transitions.carousel={
preInit:function(opts){
opts.hideNonActive=false;
opts.container.on('cycle-destroyed',$.proxy(this.onDestroy,opts.API));
opts.API.stopTransition=this.stopTransition;
for(var i=0;i<opts.startingSlide;i++){
opts.container.append(opts.slides[0]);
}
},
postInit:function(opts){
var i,j,slide,pagerCutoffIndex,wrap;
var vert=opts.carouselVertical;
if(opts.carouselVisible&&opts.carouselVisible>opts.slideCount)
opts.carouselVisible=opts.slideCount-1;
var visCount=opts.carouselVisible||opts.slides.length;
var slideCSS={display:vert?'block':'inline-block',position:'static'};
opts.container.css({position:'relative',overflow:'hidden'});
opts.slides.css(slideCSS);
opts._currSlide=opts.currSlide;
wrap=$('<div class="cycle-carousel-wrap"></div>')
.prependTo(opts.container)
.css({margin:0,padding:0,top:0,left:0,position:'absolute'})
.append(opts.slides);
opts._carouselWrap=wrap;
if(!vert)
wrap.css('white-space','nowrap');
if(opts.allowWrap!==false){
for(j=0;j<(opts.carouselVisible===undefined?2:1);j++){
for(i=0;i<opts.slideCount;i++){
wrap.append(opts.slides[i].cloneNode(true));
}
i=opts.slideCount;
while(i--){
wrap.prepend(opts.slides[i].cloneNode(true));
}
}
wrap.find('.cycle-slide-active').removeClass('cycle-slide-active');
opts.slides.eq(opts.startingSlide).addClass('cycle-slide-active');
}
if(opts.pager&&opts.allowWrap===false){
pagerCutoffIndex=opts.slideCount-visCount;
$(opts.pager).children().filter(':gt('+pagerCutoffIndex+')').hide();
}
opts._nextBoundry=opts.slideCount-opts.carouselVisible;
this.prepareDimensions(opts);
},
prepareDimensions:function(opts){
var dim,offset,pagerCutoffIndex,tmp;
var vert=opts.carouselVertical;
var visCount=opts.carouselVisible||opts.slides.length;
if(opts.carouselFluid&&opts.carouselVisible){
if(!opts._carouselResizeThrottle){
this.fluidSlides(opts);
}
}
else if(opts.carouselVisible&&opts.carouselSlideDimension){
dim=visCount*opts.carouselSlideDimension;
opts.container[vert?'height':'width'](dim);
}
else if(opts.carouselVisible){
dim=visCount*$(opts.slides[0])[vert?'outerHeight':'outerWidth'](true);
opts.container[vert?'height':'width'](dim);
}
offset=(opts.carouselOffset||0);
if(opts.allowWrap!==false){
if(opts.carouselSlideDimension){
offset-=((opts.slideCount+opts.currSlide)*opts.carouselSlideDimension);
}
else{
tmp=opts._carouselWrap.children();
for(var j=0;j<(opts.slideCount+opts.currSlide);j++){
offset-=$(tmp[j])[vert?'outerHeight':'outerWidth'](true);
}
}
}
opts._carouselWrap.css(vert?'top':'left',offset);
},
fluidSlides:function(opts){
var timeout;
var slide=opts.slides.eq(0);
var adjustment=slide.outerWidth()-slide.width();
var prepareDimensions=this.prepareDimensions;
$(window).on('resize',resizeThrottle);
opts._carouselResizeThrottle=resizeThrottle;
onResize();
function resizeThrottle(){
clearTimeout(timeout);
timeout=setTimeout(onResize,20);
}
function onResize(){
opts._carouselWrap.stop(false,true);
var slideWidth=opts.container.width()/opts.carouselVisible;
slideWidth=Math.ceil(slideWidth-adjustment);
opts._carouselWrap.children().width(slideWidth);
if(opts._sentinel)
opts._sentinel.width(slideWidth);
prepareDimensions(opts);
}
},
transition:function(opts,curr,next,fwd,callback){
var moveBy,props={};
var hops=opts.nextSlide-opts.currSlide;
var vert=opts.carouselVertical;
var speed=opts.speed;
if(opts.allowWrap===false){
fwd=hops>0;
var currSlide=opts._currSlide;
var maxCurr=opts.slideCount-opts.carouselVisible;
if(hops>0&&opts.nextSlide>maxCurr&&currSlide==maxCurr){
hops=0;
}
else if(hops>0&&opts.nextSlide>maxCurr){
hops=opts.nextSlide-currSlide-(opts.nextSlide-maxCurr);
}
else if(hops<0&&opts.currSlide>maxCurr&&opts.nextSlide>maxCurr){
hops=0;
}
else if(hops<0&&opts.currSlide>maxCurr){
hops+=opts.currSlide-maxCurr;
}
else
currSlide=opts.currSlide;
moveBy=this.getScroll(opts,vert,currSlide,hops);
opts.API.opts()._currSlide=opts.nextSlide>maxCurr?maxCurr:opts.nextSlide;
}
else{
if(fwd&&opts.nextSlide===0){
moveBy=this.getDim(opts,opts.currSlide,vert);
callback=this.genCallback(opts,fwd,vert,callback);
}
else if(!fwd&&opts.nextSlide==opts.slideCount-1){
moveBy=this.getDim(opts,opts.currSlide,vert);
callback=this.genCallback(opts,fwd,vert,callback);
}
else{
moveBy=this.getScroll(opts,vert,opts.currSlide,hops);
}
}
props[vert?'top':'left']=fwd?("-="+moveBy):("+="+moveBy);
if(opts.throttleSpeed)
speed=(moveBy/$(opts.slides[0])[vert?'height':'width']())*opts.speed;
opts._carouselWrap.animate(props,speed,opts.easing,callback);
},
getDim:function(opts,index,vert){
var slide=$(opts.slides[index]);
return slide[vert?'outerHeight':'outerWidth'](true);
},
getScroll:function(opts,vert,currSlide,hops){
var i,moveBy=0;
if(hops>0){
for(i=currSlide;i<currSlide+hops;i++)
moveBy+=this.getDim(opts,i,vert);
}
else{
for(i=currSlide;i>currSlide+hops;i--)
moveBy+=this.getDim(opts,i,vert);
}
return moveBy;
},
genCallback:function(opts,fwd,vert,callback){
return function(){
var pos=$(opts.slides[opts.nextSlide]).position();
var offset=0-pos[vert?'top':'left']+(opts.carouselOffset||0);
opts._carouselWrap.css(opts.carouselVertical?'top':'left',offset);
callback();
};
},
stopTransition:function(){
var opts=this.opts();
opts.slides.stop(false,true);
opts._carouselWrap.stop(false,true);
},
onDestroy:function(e){
var opts=this.opts();
if(opts._carouselResizeThrottle)
$(window).off('resize',opts._carouselResizeThrottle);
opts.slides.prependTo(opts.container);
opts._carouselWrap.remove();
}
};
})(jQuery);


/* plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.scrollVert.js?1649159526 */

(function($){
"use strict";
$.fn.cycle.transitions.scrollVert={
before:function(opts,curr,next,fwd){
opts.API.stackSlides(opts,curr,next,fwd);
var height=opts.container.css('overflow','hidden').height();
opts.cssBefore={top:fwd?-height:height,left:0,opacity:1,display:'block',visibility:'visible'};
opts.animIn={top:0};
opts.animOut={top:fwd?height:-height};
}
};
})(jQuery);


/* plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.shuffle.js?1649159526 */

(function($){
"use strict";
$.fn.cycle.transitions.shuffle={
transition:function(opts,currEl,nextEl,fwd,callback){
$(nextEl).css({
display:'block',
visibility:'visible'
});
var width=opts.container.css('overflow','visible').width();
var speed=opts.speed/2;
var element=fwd?currEl:nextEl;
opts=opts.API.getSlideOpts(fwd?opts.currSlide:opts.nextSlide);
var props1={left:-width,top:15};
var props2=opts.slideCss||{left:0,top:0};
if(opts.shuffleLeft!==undefined){
props1.left=props1.left+parseInt(opts.shuffleLeft,10)||0;
}
else if(opts.shuffleRight!==undefined){
props1.left=width+parseInt(opts.shuffleRight,10)||0;
}
if(opts.shuffleTop){
props1.top=opts.shuffleTop;
}
$(element)
.animate(props1,speed,opts.easeIn||opts.easing)
.queue('fx',$.proxy(reIndex,this))
.animate(props2,speed,opts.easeOut||opts.easing,callback);
function reIndex(nextFn){
this.stack(opts,currEl,nextEl,fwd);
nextFn();
}
},
stack:function(opts,currEl,nextEl,fwd){
var i,z;
if(fwd){
opts.API.stackSlides(nextEl,currEl,fwd);
$(currEl).css('zIndex',1);
}
else{
z=1;
for(i=opts.nextSlide-1;i>=0;i--){
$(opts.slides[i]).css('zIndex',z++);
}
for(i=opts.slideCount-1;i>opts.nextSlide;i--){
$(opts.slides[i]).css('zIndex',z++);
}
$(nextEl).css('zIndex',opts.maxZ);
$(currEl).css('zIndex',opts.maxZ-1);
}
}
};
})(jQuery);


/* plugins/auto/sjcycle/v3.5.2/lib/cycle2/jquery.cycle2.tile.js?1649159526 */

(function($){
"use strict";
$.fn.cycle.transitions.tileSlide=
$.fn.cycle.transitions.tileBlind={
before:function(opts,curr,next,fwd){
opts.API.stackSlides(curr,next,fwd);
$(curr).css({
display:'block',
visibility:'visible'
});
opts.container.css('overflow','hidden');
opts.tileDelay=opts.tileDelay||opts.fx=='tileSlide'?100:125;
opts.tileCount=opts.tileCount||7;
opts.tileVertical=opts.tileVertical!==false;
if(!opts.container.data('cycleTileInitialized')){
opts.container.on('cycle-destroyed',$.proxy(this.onDestroy,opts.API));
opts.container.data('cycleTileInitialized',true);
}
},
transition:function(opts,curr,next,fwd,callback){
opts.slides.not(curr).not(next).css('visibility','hidden');
var tiles=$();
var $curr=$(curr),$next=$(next);
var tile,tileWidth,tileHeight,lastTileWidth,lastTileHeight,
num=opts.tileCount,
vert=opts.tileVertical,
height=opts.container.height(),
width=opts.container.width();
if(vert){
tileWidth=Math.floor(width/num);
lastTileWidth=width-(tileWidth*(num-1));
tileHeight=lastTileHeight=height;
}
else{
tileWidth=lastTileWidth=width;
tileHeight=Math.floor(height/num);
lastTileHeight=height-(tileHeight*(num-1));
}
opts.container.find('.cycle-tiles-container').remove();
var animCSS;
var tileCSS={left:0,top:0,overflow:'hidden',position:'absolute',margin:0,padding:0};
if(vert){
animCSS=opts.fx=='tileSlide'?{top:height}:{width:0};
}
else{
animCSS=opts.fx=='tileSlide'?{left:width}:{height:0};
}
var tilesContainer=$('<div class="cycle-tiles-container"></div>');
tilesContainer.css({
zIndex:$curr.css('z-index'),
overflow:'visible',
position:'absolute',
top:0,
left:0,
direction:'ltr'
});
tilesContainer.insertBefore(next);
for(var i=0;i<num;i++){
tile=$('<div></div>')
.css(tileCSS)
.css({
width:((num-1===i)?lastTileWidth:tileWidth),
height:((num-1===i)?lastTileHeight:tileHeight),
marginLeft:vert?((i*tileWidth)):0,
marginTop:vert?0:(i*tileHeight)
})
.append($curr.clone().css({
position:'relative',
maxWidth:'none',
width:$curr.width(),
margin:0,padding:0,
marginLeft:vert?-(i*tileWidth):0,
marginTop:vert?0:-(i*tileHeight)
}));
tiles=tiles.add(tile);
}
tilesContainer.append(tiles);
$curr.css('visibility','hidden');
$next.css({
opacity:1,
display:'block',
visibility:'visible'
});
animateTile(fwd?0:num-1);
opts._tileAniCallback=function(){
$next.css({
display:'block',
visibility:'visible'
});
$curr.css('visibility','hidden');
tilesContainer.remove();
callback();
};
function animateTile(i){
tiles.eq(i).animate(animCSS,{
duration:opts.speed,
easing:opts.easing,
complete:function(){
if(fwd?(num-1===i):(0===i)){
opts._tileAniCallback();
}
}
});
setTimeout(function(){
if(fwd?(num-1!==i):(0!==i)){
animateTile(fwd?(i+1):(i-1));
}
},opts.tileDelay);
}
},
stopTransition:function(opts){
opts.container.find('*').stop(true,true);
if(opts._tileAniCallback)
opts._tileAniCallback();
},
onDestroy:function(e){
var opts=this.opts();
opts.container.find('.cycle-tiles-container').remove();
}
};
})(jQuery);


/* plugins/auto/chartjs/v1.0.5/js/Chart.js */

(function(){
"use strict";
var root=this,
previous=root.Chart;
var Chart=function(context){
var chart=this;
this.canvas=context.canvas;
this.ctx=context;
var width=this.width=context.canvas.width;
var height=this.height=context.canvas.height;
this.aspectRatio=this.width/this.height;
helpers.retinaScale(this);
return this;
};
Chart.defaults={
global:{
animation:true,
animationSteps:60,
animationEasing:"easeOutQuart",
showScale:true,
scaleOverride:false,
scaleSteps:null,
scaleStepWidth:null,
scaleStartValue:null,
scaleLineColor:"rgba(0,0,0,.1)",
scaleLineWidth:1,
scaleShowLabels:true,
scaleLabel:"<%=value%>",
scaleIntegersOnly:true,
scaleBeginAtZero:false,
scaleFontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
scaleFontSize:12,
scaleFontStyle:"normal",
scaleFontColor:"#666",
responsive:false,
maintainAspectRatio:true,
showTooltips:true,
tooltipEvents:["mousemove","touchstart","touchmove","mouseout"],
tooltipFillColor:"rgba(0,0,0,0.8)",
tooltipFontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
tooltipFontSize:14,
tooltipFontStyle:"normal",
tooltipFontColor:"#fff",
tooltipTitleFontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
tooltipTitleFontSize:14,
tooltipTitleFontStyle:"bold",
tooltipTitleFontColor:"#fff",
tooltipYPadding:6,
tooltipXPadding:6,
tooltipCaretSize:8,
tooltipCornerRadius:6,
tooltipXOffset:10,
tooltipTemplate:"<%if (label){%><%=label%>: <%}%><%= value %>",
multiTooltipTemplate:"<%= value %>",
multiTooltipKeyBackground:'#fff',
onAnimationProgress:function(){},
onAnimationComplete:function(){}
}
};
Chart.types={};
var helpers=Chart.helpers={};
var each=helpers.each=function(loopable,callback,self){
var additionalArgs=Array.prototype.slice.call(arguments,3);
if(loopable){
if(loopable.length===+loopable.length){
var i;
for(i=0;i<loopable.length;i++){
callback.apply(self,[loopable[i],i].concat(additionalArgs));
}
}
else{
for(var item in loopable){
callback.apply(self,[loopable[item],item].concat(additionalArgs));
}
}
}
},
clone=helpers.clone=function(obj){
var objClone={};
each(obj,function(value,key){
if(obj.hasOwnProperty(key))objClone[key]=value;
});
return objClone;
},
extend=helpers.extend=function(base){
each(Array.prototype.slice.call(arguments,1),function(extensionObject){
each(extensionObject,function(value,key){
if(extensionObject.hasOwnProperty(key))base[key]=value;
});
});
return base;
},
merge=helpers.merge=function(base,master){
var args=Array.prototype.slice.call(arguments,0);
args.unshift({});
return extend.apply(null,args);
},
indexOf=helpers.indexOf=function(arrayToSearch,item){
if(Array.prototype.indexOf){
return arrayToSearch.indexOf(item);
}
else{
for(var i=0;i<arrayToSearch.length;i++){
if(arrayToSearch[i]===item)return i;
}
return-1;
}
},
where=helpers.where=function(collection,filterCallback){
var filtered=[];
helpers.each(collection,function(item){
if(filterCallback(item)){
filtered.push(item);
}
});
return filtered;
},
findNextWhere=helpers.findNextWhere=function(arrayToSearch,filterCallback,startIndex){
if(!startIndex){
startIndex=-1;
}
for(var i=startIndex+1;i<arrayToSearch.length;i++){
var currentItem=arrayToSearch[i];
if(filterCallback(currentItem)){
return currentItem;
}
};
},
findPreviousWhere=helpers.findPreviousWhere=function(arrayToSearch,filterCallback,startIndex){
if(!startIndex){
startIndex=arrayToSearch.length;
}
for(var i=startIndex-1;i>=0;i--){
var currentItem=arrayToSearch[i];
if(filterCallback(currentItem)){
return currentItem;
}
};
},
inherits=helpers.inherits=function(extensions){
var parent=this;
var ChartElement=(extensions&&extensions.hasOwnProperty("constructor"))?extensions.constructor:function(){return parent.apply(this,arguments);};
var Surrogate=function(){this.constructor=ChartElement;};
Surrogate.prototype=parent.prototype;
ChartElement.prototype=new Surrogate();
ChartElement.extend=inherits;
if(extensions)extend(ChartElement.prototype,extensions);
ChartElement.__super__=parent.prototype;
return ChartElement;
},
noop=helpers.noop=function(){},
uid=helpers.uid=(function(){
var id=0;
return function(){
return"chart-"+id++;
};
})(),
warn=helpers.warn=function(str){
if(window.console&&typeof window.console.warn=="function")console.warn(str);
},
amd=helpers.amd=(typeof define=='function'&&define.amd),
isNumber=helpers.isNumber=function(n){
return!isNaN(parseFloat(n))&&isFinite(n);
},
max=helpers.max=function(array){
return Math.max.apply(Math,array);
},
min=helpers.min=function(array){
return Math.min.apply(Math,array);
},
cap=helpers.cap=function(valueToCap,maxValue,minValue){
if(isNumber(maxValue)){
if(valueToCap>maxValue){
return maxValue;
}
}
else if(isNumber(minValue)){
if(valueToCap<minValue){
return minValue;
}
}
return valueToCap;
},
getDecimalPlaces=helpers.getDecimalPlaces=function(num){
if(num%1!==0&&isNumber(num)){
return num.toString().split(".")[1].length;
}
else{
return 0;
}
},
toRadians=helpers.radians=function(degrees){
return degrees*(Math.PI/180);
},
getAngleFromPoint=helpers.getAngleFromPoint=function(centrePoint,anglePoint){
var distanceFromXCenter=anglePoint.x-centrePoint.x,
distanceFromYCenter=anglePoint.y-centrePoint.y,
radialDistanceFromCenter=Math.sqrt(distanceFromXCenter*distanceFromXCenter+distanceFromYCenter*distanceFromYCenter);
var angle=Math.PI*2+Math.atan2(distanceFromYCenter,distanceFromXCenter);
if(distanceFromXCenter<0&&distanceFromYCenter<0){
angle+=Math.PI*2;
}
return{
angle:angle,
distance:radialDistanceFromCenter
};
},
aliasPixel=helpers.aliasPixel=function(pixelWidth){
return(pixelWidth%2===0)?0:0.5;
},
splineCurve=helpers.splineCurve=function(FirstPoint,MiddlePoint,AfterPoint,t){
var d01=Math.sqrt(Math.pow(MiddlePoint.x-FirstPoint.x,2)+Math.pow(MiddlePoint.y-FirstPoint.y,2)),
d12=Math.sqrt(Math.pow(AfterPoint.x-MiddlePoint.x,2)+Math.pow(AfterPoint.y-MiddlePoint.y,2)),
fa=t*d01/(d01+d12),
fb=t*d12/(d01+d12);
return{
inner:{
x:MiddlePoint.x-fa*(AfterPoint.x-FirstPoint.x),
y:MiddlePoint.y-fa*(AfterPoint.y-FirstPoint.y)
},
outer:{
x:MiddlePoint.x+fb*(AfterPoint.x-FirstPoint.x),
y:MiddlePoint.y+fb*(AfterPoint.y-FirstPoint.y)
}
};
},
calculateOrderOfMagnitude=helpers.calculateOrderOfMagnitude=function(val){
return Math.floor(Math.log(val)/Math.LN10);
},
calculateScaleRange=helpers.calculateScaleRange=function(valuesArray,drawingSize,textSize,startFromZero,integersOnly){
var minSteps=2,
maxSteps=Math.floor(drawingSize/(textSize*1.5)),
skipFitting=(minSteps>=maxSteps);
var maxValue=max(valuesArray),
minValue=min(valuesArray);
if(maxValue===minValue){
maxValue+=0.5;
if(minValue>=0.5&&!startFromZero){
minValue-=0.5;
}
else{
maxValue+=0.5;
}
}
var valueRange=Math.abs(maxValue-minValue),
rangeOrderOfMagnitude=calculateOrderOfMagnitude(valueRange),
graphMax=Math.ceil(maxValue/(1*Math.pow(10,rangeOrderOfMagnitude)))*Math.pow(10,rangeOrderOfMagnitude),
graphMin=(startFromZero)?0:Math.floor(minValue/(1*Math.pow(10,rangeOrderOfMagnitude)))*Math.pow(10,rangeOrderOfMagnitude),
graphRange=graphMax-graphMin,
stepValue=Math.pow(10,rangeOrderOfMagnitude),
numberOfSteps=Math.round(graphRange/stepValue);
while((numberOfSteps>maxSteps||(numberOfSteps*2)<maxSteps)&&!skipFitting){
if(numberOfSteps>maxSteps){
stepValue*=2;
numberOfSteps=Math.round(graphRange/stepValue);
if(numberOfSteps%1!==0){
skipFitting=true;
}
}
else{
if(integersOnly&&rangeOrderOfMagnitude>=0){
if(stepValue/2%1===0){
stepValue/=2;
numberOfSteps=Math.round(graphRange/stepValue);
}
else{
break;
}
}
else{
stepValue/=2;
numberOfSteps=Math.round(graphRange/stepValue);
}
}
}
if(skipFitting){
numberOfSteps=minSteps;
stepValue=graphRange/numberOfSteps;
}
return{
steps:numberOfSteps,
stepValue:stepValue,
min:graphMin,
max:graphMin+(numberOfSteps*stepValue)
};
},
template=helpers.template=function(templateString,valuesObject){
if(templateString instanceof Function){
return templateString(valuesObject);
}
var cache={};
function tmpl(str,data){
var fn=!/\W/.test(str)?
cache[str]=cache[str]:
new Function("obj",
"var p=[],print=function(){p.push.apply(p,arguments);};"+
"with(obj){p.push('"+
str
.replace(/[\r\t\n]/g," ")
.split("<%").join("\t")
.replace(/((^|%>)[^\t]*)'/g,"$1\r")
.replace(/\t=(.*?)%>/g,"',$1,'")
.split("\t").join("');")
.split("%>").join("p.push('")
.split("\r").join("\\'")+
"');}return p.join('');"
);
return data?fn(data):fn;
}
return tmpl(templateString,valuesObject);
},
generateLabels=helpers.generateLabels=function(templateString,numberOfSteps,graphMin,stepValue){
var labelsArray=new Array(numberOfSteps);
if(labelTemplateString){
each(labelsArray,function(val,index){
labelsArray[index]=template(templateString,{value:(graphMin+(stepValue*(index+1)))});
});
}
return labelsArray;
},
easingEffects=helpers.easingEffects={
linear:function(t){
return t;
},
easeInQuad:function(t){
return t*t;
},
easeOutQuad:function(t){
return-1*t*(t-2);
},
easeInOutQuad:function(t){
if((t/=1/ 2) < 1) return 1 /2*t*t;
return-1/2*((--t)*(t-2)-1);
},
easeInCubic:function(t){
return t*t*t;
},
easeOutCubic:function(t){
return 1*((t=t/1-1)*t*t+1);
},
easeInOutCubic:function(t){
if((t/=1/ 2) < 1) return 1 /2*t*t*t;
return 1/2*((t-=2)*t*t+2);
},
easeInQuart:function(t){
return t*t*t*t;
},
easeOutQuart:function(t){
return-1*((t=t/1-1)*t*t*t-1);
},
easeInOutQuart:function(t){
if((t/=1/ 2) < 1) return 1 /2*t*t*t*t;
return-1/2*((t-=2)*t*t*t-2);
},
easeInQuint:function(t){
return 1*(t/=1)*t*t*t*t;
},
easeOutQuint:function(t){
return 1*((t=t/1-1)*t*t*t*t+1);
},
easeInOutQuint:function(t){
if((t/=1/ 2) < 1) return 1 /2*t*t*t*t*t;
return 1/2*((t-=2)*t*t*t*t+2);
},
easeInSine:function(t){
return-1*Math.cos(t/1*(Math.PI/2))+1;
},
easeOutSine:function(t){
return 1*Math.sin(t/1*(Math.PI/2));
},
easeInOutSine:function(t){
return-1/2*(Math.cos(Math.PI*t/1)-1);
},
easeInExpo:function(t){
return(t===0)?1:1*Math.pow(2,10*(t/1-1));
},
easeOutExpo:function(t){
return(t===1)?1:1*(-Math.pow(2,-10*t/1)+1);
},
easeInOutExpo:function(t){
if(t===0)return 0;
if(t===1)return 1;
if((t/=1/ 2) < 1) return 1 /2*Math.pow(2,10*(t-1));
return 1/2*(-Math.pow(2,-10*--t)+2);
},
easeInCirc:function(t){
if(t>=1)return t;
return-1*(Math.sqrt(1-(t/=1)*t)-1);
},
easeOutCirc:function(t){
return 1*Math.sqrt(1-(t=t/1-1)*t);
},
easeInOutCirc:function(t){
if((t/=1/ 2) < 1) return -1 /2*(Math.sqrt(1-t*t)-1);
return 1/2*(Math.sqrt(1-(t-=2)*t)+1);
},
easeInElastic:function(t){
var s=1.70158;
var p=0;
var a=1;
if(t===0)return 0;
if((t/=1)==1)return 1;
if(!p)p=1*0.3;
if(a<Math.abs(1)){
a=1;
s=p/4;
}else s=p/(2*Math.PI)*Math.asin(1/a);
return-(a*Math.pow(2,10*(t-=1))*Math.sin((t*1-s)*(2*Math.PI)/p));
},
easeOutElastic:function(t){
var s=1.70158;
var p=0;
var a=1;
if(t===0)return 0;
if((t/=1)==1)return 1;
if(!p)p=1*0.3;
if(a<Math.abs(1)){
a=1;
s=p/4;
}else s=p/(2*Math.PI)*Math.asin(1/a);
return a*Math.pow(2,-10*t)*Math.sin((t*1-s)*(2*Math.PI)/p)+1;
},
easeInOutElastic:function(t){
var s=1.70158;
var p=0;
var a=1;
if(t===0)return 0;
if((t/=1/2)==2)return 1;
if(!p)p=1*(0.3*1.5);
if(a<Math.abs(1)){
a=1;
s=p/4;
}else s=p/(2*Math.PI)*Math.asin(1/a);
if(t<1)return-0.5*(a*Math.pow(2,10*(t-=1))*Math.sin((t*1-s)*(2*Math.PI)/p));
return a*Math.pow(2,-10*(t-=1))*Math.sin((t*1-s)*(2*Math.PI)/p)*0.5+1;
},
easeInBack:function(t){
var s=1.70158;
return 1*(t/=1)*t*((s+1)*t-s);
},
easeOutBack:function(t){
var s=1.70158;
return 1*((t=t/1-1)*t*((s+1)*t+s)+1);
},
easeInOutBack:function(t){
var s=1.70158;
if((t/=1/ 2) < 1) return 1 /2*(t*t*(((s*=(1.525))+1)*t-s));
return 1/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2);
},
easeInBounce:function(t){
return 1-easingEffects.easeOutBounce(1-t);
},
easeOutBounce:function(t){
if((t/=1)<(1/2.75)){
return 1*(7.5625*t*t);
}else if(t<(2/2.75)){
return 1*(7.5625*(t-=(1.5/2.75))*t+0.75);
}else if(t<(2.5/2.75)){
return 1*(7.5625*(t-=(2.25/2.75))*t+0.9375);
}else{
return 1*(7.5625*(t-=(2.625/2.75))*t+0.984375);
}
},
easeInOutBounce:function(t){
if(t<1/2)return easingEffects.easeInBounce(t*2)*0.5;
return easingEffects.easeOutBounce(t*2-1)*0.5+1*0.5;
}
},
requestAnimFrame=helpers.requestAnimFrame=(function(){
return window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame||
function(callback){
return window.setTimeout(callback,1000/60);
};
})(),
cancelAnimFrame=helpers.cancelAnimFrame=(function(){
return window.cancelAnimationFrame||
window.webkitCancelAnimationFrame||
window.mozCancelAnimationFrame||
window.oCancelAnimationFrame||
window.msCancelAnimationFrame||
function(callback){
return window.clearTimeout(callback,1000/60);
};
})(),
animationLoop=helpers.animationLoop=function(callback,totalSteps,easingString,onProgress,onComplete,chartInstance){
var currentStep=0,
easingFunction=easingEffects[easingString]||easingEffects.linear;
var animationFrame=function(){
currentStep++;
var stepDecimal=currentStep/totalSteps;
var easeDecimal=easingFunction(stepDecimal);
callback.call(chartInstance,easeDecimal,stepDecimal,currentStep);
onProgress.call(chartInstance,easeDecimal,stepDecimal);
if(currentStep<totalSteps){
chartInstance.animationFrame=requestAnimFrame(animationFrame);
}else{
onComplete.apply(chartInstance);
}
};
requestAnimFrame(animationFrame);
},
getRelativePosition=helpers.getRelativePosition=function(evt){
var mouseX,mouseY;
var e=evt.originalEvent||evt,
canvas=evt.currentTarget||evt.srcElement,
boundingRect=canvas.getBoundingClientRect();
if(e.touches){
mouseX=e.touches[0].clientX-boundingRect.left;
mouseY=e.touches[0].clientY-boundingRect.top;
}
else{
mouseX=e.clientX-boundingRect.left;
mouseY=e.clientY-boundingRect.top;
}
return{
x:mouseX,
y:mouseY
};
},
addEvent=helpers.addEvent=function(node,eventType,method){
if(node.addEventListener){
node.addEventListener(eventType,method);
}else if(node.attachEvent){
node.attachEvent("on"+eventType,method);
}else{
node["on"+eventType]=method;
}
},
removeEvent=helpers.removeEvent=function(node,eventType,handler){
if(node.removeEventListener){
node.removeEventListener(eventType,handler,false);
}else if(node.detachEvent){
node.detachEvent("on"+eventType,handler);
}else{
node["on"+eventType]=noop;
}
},
bindEvents=helpers.bindEvents=function(chartInstance,arrayOfEvents,handler){
if(!chartInstance.events)chartInstance.events={};
each(arrayOfEvents,function(eventName){
chartInstance.events[eventName]=function(){
handler.apply(chartInstance,arguments);
};
addEvent(chartInstance.chart.canvas,eventName,chartInstance.events[eventName]);
});
},
unbindEvents=helpers.unbindEvents=function(chartInstance,arrayOfEvents){
each(arrayOfEvents,function(handler,eventName){
removeEvent(chartInstance.chart.canvas,eventName,handler);
});
},
getMaximumWidth=helpers.getMaximumWidth=function(domNode){
var container=domNode.parentNode;
return container.clientWidth;
},
getMaximumHeight=helpers.getMaximumHeight=function(domNode){
var container=domNode.parentNode;
return container.clientHeight;
},
getMaximumSize=helpers.getMaximumSize=helpers.getMaximumWidth,
retinaScale=helpers.retinaScale=function(chart){
var ctx=chart.ctx,
width=chart.canvas.width,
height=chart.canvas.height;
if(window.devicePixelRatio){
ctx.canvas.style.width=width+"px";
ctx.canvas.style.height=height+"px";
ctx.canvas.height=height*window.devicePixelRatio;
ctx.canvas.width=width*window.devicePixelRatio;
ctx.scale(window.devicePixelRatio,window.devicePixelRatio);
}
},
clear=helpers.clear=function(chart){
chart.ctx.clearRect(0,0,chart.width,chart.height);
},
fontString=helpers.fontString=function(pixelSize,fontStyle,fontFamily){
return fontStyle+" "+pixelSize+"px "+fontFamily;
},
longestText=helpers.longestText=function(ctx,font,arrayOfStrings){
ctx.font=font;
var longest=0;
each(arrayOfStrings,function(string){
var textWidth=ctx.measureText(string).width;
longest=(textWidth>longest)?textWidth:longest;
});
return longest;
},
drawRoundedRectangle=helpers.drawRoundedRectangle=function(ctx,x,y,width,height,radius){
ctx.beginPath();
ctx.moveTo(x+radius,y);
ctx.lineTo(x+width-radius,y);
ctx.quadraticCurveTo(x+width,y,x+width,y+radius);
ctx.lineTo(x+width,y+height-radius);
ctx.quadraticCurveTo(x+width,y+height,x+width-radius,y+height);
ctx.lineTo(x+radius,y+height);
ctx.quadraticCurveTo(x,y+height,x,y+height-radius);
ctx.lineTo(x,y+radius);
ctx.quadraticCurveTo(x,y,x+radius,y);
ctx.closePath();
};
Chart.instances={};
Chart.Type=function(data,options,chart){
this.options=options;
this.chart=chart;
this.id=uid();
Chart.instances[this.id]=this;
if(options.responsive){
this.resize();
}
this.initialize.call(this,data);
};
extend(Chart.Type.prototype,{
initialize:function(){return this;},
clear:function(){
clear(this.chart);
return this;
},
stop:function(){
helpers.cancelAnimFrame.call(root,this.animationFrame);
return this;
},
resize:function(callback){
this.stop();
var canvas=this.chart.canvas,
newWidth=getMaximumWidth(this.chart.canvas),
newHeight=this.options.maintainAspectRatio?newWidth/this.chart.aspectRatio:getMaximumHeight(this.chart.canvas);
canvas.width=this.chart.width=newWidth;
canvas.height=this.chart.height=newHeight;
retinaScale(this.chart);
if(typeof callback==="function"){
callback.apply(this,Array.prototype.slice.call(arguments,1));
}
return this;
},
reflow:noop,
render:function(reflow){
if(reflow){
this.reflow();
}
if(this.options.animation&&!reflow){
helpers.animationLoop(
this.draw,
this.options.animationSteps,
this.options.animationEasing,
this.options.onAnimationProgress,
this.options.onAnimationComplete,
this
);
}
else{
this.draw();
this.options.onAnimationComplete.call(this);
}
return this;
},
generateLegend:function(){
return template(this.options.legendTemplate,this);
},
destroy:function(){
this.clear();
unbindEvents(this,this.events);
delete Chart.instances[this.id];
},
showTooltip:function(ChartElements,forceRedraw){
if(typeof this.activeElements==='undefined')this.activeElements=[];
var isChanged=(function(Elements){
var changed=false;
if(Elements.length!==this.activeElements.length){
changed=true;
return changed;
}
each(Elements,function(element,index){
if(element!==this.activeElements[index]){
changed=true;
}
},this);
return changed;
}).call(this,ChartElements);
if(!isChanged&&!forceRedraw){
return;
}
else{
this.activeElements=ChartElements;
}
this.draw();
if(ChartElements.length>0){
if(this.datasets&&this.datasets.length>1){
var dataArray,
dataIndex;
for(var i=this.datasets.length-1;i>=0;i--){
dataArray=this.datasets[i].points||this.datasets[i].bars||this.datasets[i].segments;
dataIndex=indexOf(dataArray,ChartElements[0]);
if(dataIndex!==-1){
break;
}
}
var tooltipLabels=[],
tooltipColors=[],
medianPosition=(function(index){
var Elements=[],
dataCollection,
xPositions=[],
yPositions=[],
xMax,
yMax,
xMin,
yMin;
helpers.each(this.datasets,function(dataset){
dataCollection=dataset.points||dataset.bars||dataset.segments;
if(dataCollection[dataIndex]&&dataCollection[dataIndex].hasValue()){
Elements.push(dataCollection[dataIndex]);
}
});
helpers.each(Elements,function(element){
xPositions.push(element.x);
yPositions.push(element.y);
tooltipLabels.push(helpers.template(this.options.multiTooltipTemplate,element));
tooltipColors.push({
fill:element._saved.fillColor||element.fillColor,
stroke:element._saved.strokeColor||element.strokeColor
});
},this);
yMin=min(yPositions);
yMax=max(yPositions);
xMin=min(xPositions);
xMax=max(xPositions);
return{
x:(xMin>this.chart.width/2)?xMin:xMax,
y:(yMin+yMax)/2
};
}).call(this,dataIndex);
new Chart.MultiTooltip({
x:medianPosition.x,
y:medianPosition.y,
xPadding:this.options.tooltipXPadding,
yPadding:this.options.tooltipYPadding,
xOffset:this.options.tooltipXOffset,
fillColor:this.options.tooltipFillColor,
textColor:this.options.tooltipFontColor,
fontFamily:this.options.tooltipFontFamily,
fontStyle:this.options.tooltipFontStyle,
fontSize:this.options.tooltipFontSize,
titleTextColor:this.options.tooltipTitleFontColor,
titleFontFamily:this.options.tooltipTitleFontFamily,
titleFontStyle:this.options.tooltipTitleFontStyle,
titleFontSize:this.options.tooltipTitleFontSize,
cornerRadius:this.options.tooltipCornerRadius,
labels:tooltipLabels,
legendColors:tooltipColors,
legendColorBackground:this.options.multiTooltipKeyBackground,
title:ChartElements[0].label,
chart:this.chart,
ctx:this.chart.ctx
}).draw();
}else{
each(ChartElements,function(Element){
var tooltipPosition=Element.tooltipPosition();
new Chart.Tooltip({
x:Math.round(tooltipPosition.x),
y:Math.round(tooltipPosition.y),
xPadding:this.options.tooltipXPadding,
yPadding:this.options.tooltipYPadding,
fillColor:this.options.tooltipFillColor,
textColor:this.options.tooltipFontColor,
fontFamily:this.options.tooltipFontFamily,
fontStyle:this.options.tooltipFontStyle,
fontSize:this.options.tooltipFontSize,
caretHeight:this.options.tooltipCaretSize,
cornerRadius:this.options.tooltipCornerRadius,
text:template(this.options.tooltipTemplate,Element),
chart:this.chart
}).draw();
},this);
}
}
return this;
},
toBase64Image:function(){
return this.chart.canvas.toDataURL.apply(this.chart.canvas,arguments);
}
});
Chart.Type.extend=function(extensions){
var parent=this;
var ChartType=function(){
return parent.apply(this,arguments);
};
ChartType.prototype=clone(parent.prototype);
extend(ChartType.prototype,extensions);
ChartType.extend=Chart.Type.extend;
if(extensions.name||parent.prototype.name){
var chartName=extensions.name||parent.prototype.name;
var baseDefaults=(Chart.defaults[parent.prototype.name])?clone(Chart.defaults[parent.prototype.name]):{};
Chart.defaults[chartName]=extend(baseDefaults,extensions.defaults);
Chart.types[chartName]=ChartType;
Chart.prototype[chartName]=function(data,options){
var config=merge(Chart.defaults.global,Chart.defaults[chartName],options||{});
return new ChartType(data,config,this);
};
}else{
warn("Name not provided for this chart, so it hasn't been registered");
}
return parent;
};
Chart.Element=function(configuration){
extend(this,configuration);
this.initialize.apply(this,arguments);
this.save();
};
extend(Chart.Element.prototype,{
initialize:function(){},
restore:function(props){
if(!props){
extend(this,this._saved);
}else{
each(props,function(key){
this[key]=this._saved[key];
},this);
}
return this;
},
save:function(){
this._saved=clone(this);
delete this._saved._saved;
return this;
},
update:function(newProps){
each(newProps,function(value,key){
this._saved[key]=this[key];
this[key]=value;
},this);
return this;
},
transition:function(props,ease){
each(props,function(value,key){
this[key]=((value-this._saved[key])*ease)+this._saved[key];
},this);
return this;
},
tooltipPosition:function(){
return{
x:this.x,
y:this.y
};
},
hasValue:function(){
return isNumber(this.value);
}
});
Chart.Element.extend=inherits;
Chart.Point=Chart.Element.extend({
display:true,
inRange:function(chartX,chartY){
var hitDetectionRange=this.hitDetectionRadius+this.radius;
return((Math.pow(chartX-this.x,2)+Math.pow(chartY-this.y,2))<Math.pow(hitDetectionRange,2));
},
draw:function(){
if(this.display){
var ctx=this.ctx;
ctx.beginPath();
ctx.arc(this.x,this.y,this.radius,0,Math.PI*2);
ctx.closePath();
ctx.strokeStyle=this.strokeColor;
ctx.lineWidth=this.strokeWidth;
ctx.fillStyle=this.fillColor;
ctx.fill();
ctx.stroke();
}
}
});
Chart.Arc=Chart.Element.extend({
inRange:function(chartX,chartY){
var pointRelativePosition=helpers.getAngleFromPoint(this,{
x:chartX,
y:chartY
});
var betweenAngles=(pointRelativePosition.angle>=this.startAngle&&pointRelativePosition.angle<=this.endAngle),
withinRadius=(pointRelativePosition.distance>=this.innerRadius&&pointRelativePosition.distance<=this.outerRadius);
return(betweenAngles&&withinRadius);
},
tooltipPosition:function(){
var centreAngle=this.startAngle+((this.endAngle-this.startAngle)/2),
rangeFromCentre=(this.outerRadius-this.innerRadius)/2+this.innerRadius;
return{
x:this.x+(Math.cos(centreAngle)*rangeFromCentre),
y:this.y+(Math.sin(centreAngle)*rangeFromCentre)
};
},
draw:function(animationPercent){
var easingDecimal=animationPercent||1;
var ctx=this.ctx;
ctx.beginPath();
ctx.arc(this.x,this.y,this.outerRadius,this.startAngle,this.endAngle);
ctx.arc(this.x,this.y,this.innerRadius,this.endAngle,this.startAngle,true);
ctx.closePath();
ctx.strokeStyle=this.strokeColor;
ctx.lineWidth=this.strokeWidth;
ctx.fillStyle=this.fillColor;
ctx.fill();
ctx.lineJoin='bevel';
if(this.showStroke){
ctx.stroke();
}
}
});
Chart.Rectangle=Chart.Element.extend({
draw:function(){
var ctx=this.ctx,
halfWidth=this.width/2,
leftX=this.x-halfWidth,
rightX=this.x+halfWidth,
top=this.base-(this.base-this.y),
halfStroke=this.strokeWidth/2;
if(this.showStroke){
leftX+=halfStroke;
rightX-=halfStroke;
top+=halfStroke;
}
ctx.beginPath();
ctx.fillStyle=this.fillColor;
ctx.strokeStyle=this.strokeColor;
ctx.lineWidth=this.strokeWidth;
ctx.moveTo(leftX,this.base);
ctx.lineTo(leftX,top);
ctx.lineTo(rightX,top);
ctx.lineTo(rightX,this.base);
ctx.fill();
if(this.showStroke){
ctx.stroke();
}
},
height:function(){
return this.base-this.y;
},
inRange:function(chartX,chartY){
return(chartX>=this.x-this.width/2&&chartX<=this.x+this.width/2)&&(chartY>=this.y&&chartY<=this.base);
}
});
Chart.Tooltip=Chart.Element.extend({
draw:function(){
var ctx=this.chart.ctx;
ctx.font=fontString(this.fontSize,this.fontStyle,this.fontFamily);
this.xAlign="center";
this.yAlign="above";
var caretPadding=2;
var tooltipWidth=ctx.measureText(this.text).width+2*this.xPadding,
tooltipRectHeight=this.fontSize+2*this.yPadding,
tooltipHeight=tooltipRectHeight+this.caretHeight+caretPadding;
if(this.x+tooltipWidth/2>this.chart.width){
this.xAlign="left";
}else if(this.x-tooltipWidth/2<0){
this.xAlign="right";
}
if(this.y-tooltipHeight<0){
this.yAlign="below";
}
var tooltipX=this.x-tooltipWidth/2,
tooltipY=this.y-tooltipHeight;
ctx.fillStyle=this.fillColor;
switch(this.yAlign)
{
case"above":
ctx.beginPath();
ctx.moveTo(this.x,this.y-caretPadding);
ctx.lineTo(this.x+this.caretHeight,this.y-(caretPadding+this.caretHeight));
ctx.lineTo(this.x-this.caretHeight,this.y-(caretPadding+this.caretHeight));
ctx.closePath();
ctx.fill();
break;
case"below":
tooltipY=this.y+caretPadding+this.caretHeight;
ctx.beginPath();
ctx.moveTo(this.x,this.y+caretPadding);
ctx.lineTo(this.x+this.caretHeight,this.y+caretPadding+this.caretHeight);
ctx.lineTo(this.x-this.caretHeight,this.y+caretPadding+this.caretHeight);
ctx.closePath();
ctx.fill();
break;
}
switch(this.xAlign)
{
case"left":
tooltipX=this.x-tooltipWidth+(this.cornerRadius+this.caretHeight);
break;
case"right":
tooltipX=this.x-(this.cornerRadius+this.caretHeight);
break;
}
drawRoundedRectangle(ctx,tooltipX,tooltipY,tooltipWidth,tooltipRectHeight,this.cornerRadius);
ctx.fill();
ctx.fillStyle=this.textColor;
ctx.textAlign="center";
ctx.textBaseline="middle";
ctx.fillText(this.text,tooltipX+tooltipWidth/2,tooltipY+tooltipRectHeight/2);
}
});
Chart.MultiTooltip=Chart.Element.extend({
initialize:function(){
this.font=fontString(this.fontSize,this.fontStyle,this.fontFamily);
this.titleFont=fontString(this.titleFontSize,this.titleFontStyle,this.titleFontFamily);
this.height=(this.labels.length*this.fontSize)+((this.labels.length-1)*(this.fontSize/2))+(this.yPadding*2)+this.titleFontSize*1.5;
this.ctx.font=this.titleFont;
var titleWidth=this.ctx.measureText(this.title).width,
labelWidth=longestText(this.ctx,this.font,this.labels)+this.fontSize+3,
longestTextWidth=max([labelWidth,titleWidth]);
this.width=longestTextWidth+(this.xPadding*2);
var halfHeight=this.height/2;
if(this.y-halfHeight<0){
this.y=halfHeight;
}else if(this.y+halfHeight>this.chart.height){
this.y=this.chart.height-halfHeight;
}
if(this.x>this.chart.width/2){
this.x-=this.xOffset+this.width;
}else{
this.x+=this.xOffset;
}
},
getLineHeight:function(index){
var baseLineHeight=this.y-(this.height/2)+this.yPadding,
afterTitleIndex=index-1;
if(index===0){
return baseLineHeight+this.titleFontSize/2;
}else{
return baseLineHeight+((this.fontSize*1.5*afterTitleIndex)+this.fontSize/2)+this.titleFontSize*1.5;
}
},
draw:function(){
drawRoundedRectangle(this.ctx,this.x,this.y-this.height/2,this.width,this.height,this.cornerRadius);
var ctx=this.ctx;
ctx.fillStyle=this.fillColor;
ctx.fill();
ctx.closePath();
ctx.textAlign="left";
ctx.textBaseline="middle";
ctx.fillStyle=this.titleTextColor;
ctx.font=this.titleFont;
ctx.fillText(this.title,this.x+this.xPadding,this.getLineHeight(0));
ctx.font=this.font;
helpers.each(this.labels,function(label,index){
ctx.fillStyle=this.textColor;
ctx.fillText(label,this.x+this.xPadding+this.fontSize+3,this.getLineHeight(index+1));
ctx.fillStyle=this.legendColorBackground;
ctx.fillRect(this.x+this.xPadding,this.getLineHeight(index+1)-this.fontSize/2,this.fontSize,this.fontSize);
ctx.fillStyle=this.legendColors[index].fill;
ctx.fillRect(this.x+this.xPadding,this.getLineHeight(index+1)-this.fontSize/2,this.fontSize,this.fontSize);
},this);
}
});
Chart.Scale=Chart.Element.extend({
initialize:function(){
this.fit();
},
buildYLabels:function(){
this.yLabels=[];
var stepDecimalPlaces=getDecimalPlaces(this.stepValue);
for(var i=0;i<=this.steps;i++){
this.yLabels.push(template(this.templateString,{value:(this.min+(i*this.stepValue)).toFixed(stepDecimalPlaces)}));
}
this.yLabelWidth=(this.display&&this.showLabels)?longestText(this.ctx,this.font,this.yLabels):0;
},
addXLabel:function(label){
this.xLabels.push(label);
this.valuesCount++;
this.fit();
},
removeXLabel:function(){
this.xLabels.shift();
this.valuesCount--;
this.fit();
},
fit:function(){
this.startPoint=(this.display)?this.fontSize:0;
this.endPoint=(this.display)?this.height-(this.fontSize*1.5)-5:this.height;
this.startPoint+=this.padding;
this.endPoint-=this.padding;
var cachedHeight=this.endPoint-this.startPoint,
cachedYLabelWidth;
this.calculateYRange(cachedHeight);
this.buildYLabels();
this.calculateXLabelRotation();
while((cachedHeight>this.endPoint-this.startPoint)){
cachedHeight=this.endPoint-this.startPoint;
cachedYLabelWidth=this.yLabelWidth;
this.calculateYRange(cachedHeight);
this.buildYLabels();
if(cachedYLabelWidth<this.yLabelWidth){
this.calculateXLabelRotation();
}
}
},
calculateXLabelRotation:function(){
this.ctx.font=this.font;
var firstWidth=this.ctx.measureText(this.xLabels[0]).width,
lastWidth=this.ctx.measureText(this.xLabels[this.xLabels.length-1]).width,
firstRotated,
lastRotated;
this.xScalePaddingRight=lastWidth/2+3;
this.xScalePaddingLeft=(firstWidth/2>this.yLabelWidth+10)?firstWidth/2:this.yLabelWidth+10;
this.xLabelRotation=0;
if(this.display){
var originalLabelWidth=longestText(this.ctx,this.font,this.xLabels),
cosRotation,
firstRotatedWidth;
this.xLabelWidth=originalLabelWidth;
var xGridWidth=Math.floor(this.calculateX(1)-this.calculateX(0))-6;
while((this.xLabelWidth>xGridWidth&&this.xLabelRotation===0)||(this.xLabelWidth>xGridWidth&&this.xLabelRotation<=90&&this.xLabelRotation>0)){
cosRotation=Math.cos(toRadians(this.xLabelRotation));
firstRotated=cosRotation*firstWidth;
lastRotated=cosRotation*lastWidth;
if(firstRotated+this.fontSize/2>this.yLabelWidth+8){
this.xScalePaddingLeft=firstRotated+this.fontSize/2;
}
this.xScalePaddingRight=this.fontSize/2;
this.xLabelRotation++;
this.xLabelWidth=cosRotation*originalLabelWidth;
}
if(this.xLabelRotation>0){
this.endPoint-=Math.sin(toRadians(this.xLabelRotation))*originalLabelWidth+3;
}
}
else{
this.xLabelWidth=0;
this.xScalePaddingRight=this.padding;
this.xScalePaddingLeft=this.padding;
}
},
calculateYRange:noop,
drawingArea:function(){
return this.startPoint-this.endPoint;
},
calculateY:function(value){
var scalingFactor=this.drawingArea()/(this.min-this.max);
return this.endPoint-(scalingFactor*(value-this.min));
},
calculateX:function(index){
var isRotated=(this.xLabelRotation>0),
innerWidth=this.width-(this.xScalePaddingLeft+this.xScalePaddingRight),
valueWidth=innerWidth/(this.valuesCount-((this.offsetGridLines)?0:1)),
valueOffset=(valueWidth*index)+this.xScalePaddingLeft;
if(this.offsetGridLines){
valueOffset+=(valueWidth/2);
}
return Math.round(valueOffset);
},
update:function(newProps){
helpers.extend(this,newProps);
this.fit();
},
draw:function(){
var ctx=this.ctx,
yLabelGap=(this.endPoint-this.startPoint)/this.steps,
xStart=Math.round(this.xScalePaddingLeft);
if(this.display){
ctx.fillStyle=this.textColor;
ctx.font=this.font;
each(this.yLabels,function(labelString,index){
var yLabelCenter=this.endPoint-(yLabelGap*index),
linePositionY=Math.round(yLabelCenter);
ctx.textAlign="right";
ctx.textBaseline="middle";
if(this.showLabels){
ctx.fillText(labelString,xStart-10,yLabelCenter);
}
ctx.beginPath();
if(index>0){
ctx.lineWidth=this.gridLineWidth;
ctx.strokeStyle=this.gridLineColor;
}else{
ctx.lineWidth=this.lineWidth;
ctx.strokeStyle=this.lineColor;
}
linePositionY+=helpers.aliasPixel(ctx.lineWidth);
ctx.moveTo(xStart,linePositionY);
ctx.lineTo(this.width,linePositionY);
ctx.stroke();
ctx.closePath();
ctx.lineWidth=this.lineWidth;
ctx.strokeStyle=this.lineColor;
ctx.beginPath();
ctx.moveTo(xStart-5,linePositionY);
ctx.lineTo(xStart,linePositionY);
ctx.stroke();
ctx.closePath();
},this);
each(this.xLabels,function(label,index){
var xPos=this.calculateX(index)+aliasPixel(this.lineWidth),
linePos=this.calculateX(index-(this.offsetGridLines?0.5:0))+aliasPixel(this.lineWidth),
isRotated=(this.xLabelRotation>0);
ctx.beginPath();
if(index>0){
ctx.lineWidth=this.gridLineWidth;
ctx.strokeStyle=this.gridLineColor;
}else{
ctx.lineWidth=this.lineWidth;
ctx.strokeStyle=this.lineColor;
}
ctx.moveTo(linePos,this.endPoint);
ctx.lineTo(linePos,this.startPoint-3);
ctx.stroke();
ctx.closePath();
ctx.lineWidth=this.lineWidth;
ctx.strokeStyle=this.lineColor;
ctx.beginPath();
ctx.moveTo(linePos,this.endPoint);
ctx.lineTo(linePos,this.endPoint+5);
ctx.stroke();
ctx.closePath();
ctx.save();
ctx.translate(xPos,(isRotated)?this.endPoint+12:this.endPoint+8);
ctx.rotate(toRadians(this.xLabelRotation)*-1);
ctx.font=this.font;
ctx.textAlign=(isRotated)?"right":"center";
ctx.textBaseline=(isRotated)?"middle":"top";
ctx.fillText(label,0,0);
ctx.restore();
},this);
}
}
});
Chart.RadialScale=Chart.Element.extend({
initialize:function(){
this.size=min([this.height,this.width]);
this.drawingArea=(this.display)?(this.size/2)-(this.fontSize/2+this.backdropPaddingY):(this.size/2);
},
calculateCenterOffset:function(value){
var scalingFactor=this.drawingArea/(this.max-this.min);
return(value-this.min)*scalingFactor;
},
update:function(){
if(!this.lineArc){
this.setScaleSize();
}else{
this.drawingArea=(this.display)?(this.size/2)-(this.fontSize/2+this.backdropPaddingY):(this.size/2);
}
this.buildYLabels();
},
buildYLabels:function(){
this.yLabels=[];
var stepDecimalPlaces=getDecimalPlaces(this.stepValue);
for(var i=0;i<=this.steps;i++){
this.yLabels.push(template(this.templateString,{value:(this.min+(i*this.stepValue)).toFixed(stepDecimalPlaces)}));
}
},
getCircumference:function(){
return((Math.PI*2)/this.valuesCount);
},
setScaleSize:function(){
var largestPossibleRadius=min([(this.height/2-this.pointLabelFontSize-5),this.width/2]),
pointPosition,
i,
textWidth,
halfTextWidth,
furthestRight=this.width,
furthestRightIndex,
furthestRightAngle,
furthestLeft=0,
furthestLeftIndex,
furthestLeftAngle,
xProtrusionLeft,
xProtrusionRight,
radiusReductionRight,
radiusReductionLeft,
maxWidthRadius;
this.ctx.font=fontString(this.pointLabelFontSize,this.pointLabelFontStyle,this.pointLabelFontFamily);
for(i=0;i<this.valuesCount;i++){
pointPosition=this.getPointPosition(i,largestPossibleRadius);
textWidth=this.ctx.measureText(template(this.templateString,{value:this.labels[i]})).width+5;
if(i===0||i===this.valuesCount/2){
halfTextWidth=textWidth/2;
if(pointPosition.x+halfTextWidth>furthestRight){
furthestRight=pointPosition.x+halfTextWidth;
furthestRightIndex=i;
}
if(pointPosition.x-halfTextWidth<furthestLeft){
furthestLeft=pointPosition.x-halfTextWidth;
furthestLeftIndex=i;
}
}
else if(i<this.valuesCount/2){
if(pointPosition.x+textWidth>furthestRight){
furthestRight=pointPosition.x+textWidth;
furthestRightIndex=i;
}
}
else if(i>this.valuesCount/2){
if(pointPosition.x-textWidth<furthestLeft){
furthestLeft=pointPosition.x-textWidth;
furthestLeftIndex=i;
}
}
}
xProtrusionLeft=furthestLeft;
xProtrusionRight=Math.ceil(furthestRight-this.width);
furthestRightAngle=this.getIndexAngle(furthestRightIndex);
furthestLeftAngle=this.getIndexAngle(furthestLeftIndex);
radiusReductionRight=xProtrusionRight/Math.sin(furthestRightAngle+Math.PI/2);
radiusReductionLeft=xProtrusionLeft/Math.sin(furthestLeftAngle+Math.PI/2);
radiusReductionRight=(isNumber(radiusReductionRight))?radiusReductionRight:0;
radiusReductionLeft=(isNumber(radiusReductionLeft))?radiusReductionLeft:0;
this.drawingArea=largestPossibleRadius-(radiusReductionLeft+radiusReductionRight)/2;
this.setCenterPoint(radiusReductionLeft,radiusReductionRight);
},
setCenterPoint:function(leftMovement,rightMovement){
var maxRight=this.width-rightMovement-this.drawingArea,
maxLeft=leftMovement+this.drawingArea;
this.xCenter=(maxLeft+maxRight)/2;
this.yCenter=(this.height/2);
},
getIndexAngle:function(index){
var angleMultiplier=(Math.PI*2)/this.valuesCount;
return index*angleMultiplier-(Math.PI/2);
},
getPointPosition:function(index,distanceFromCenter){
var thisAngle=this.getIndexAngle(index);
return{
x:(Math.cos(thisAngle)*distanceFromCenter)+this.xCenter,
y:(Math.sin(thisAngle)*distanceFromCenter)+this.yCenter
};
},
draw:function(){
if(this.display){
var ctx=this.ctx;
each(this.yLabels,function(label,index){
if(index>0){
var yCenterOffset=index*(this.drawingArea/this.steps),
yHeight=this.yCenter-yCenterOffset,
pointPosition;
if(this.lineWidth>0){
ctx.strokeStyle=this.lineColor;
ctx.lineWidth=this.lineWidth;
if(this.lineArc){
ctx.beginPath();
ctx.arc(this.xCenter,this.yCenter,yCenterOffset,0,Math.PI*2);
ctx.closePath();
ctx.stroke();
}else{
ctx.beginPath();
for(var i=0;i<this.valuesCount;i++)
{
pointPosition=this.getPointPosition(i,this.calculateCenterOffset(this.min+(index*this.stepValue)));
if(i===0){
ctx.moveTo(pointPosition.x,pointPosition.y);
}else{
ctx.lineTo(pointPosition.x,pointPosition.y);
}
}
ctx.closePath();
ctx.stroke();
}
}
if(this.showLabels){
ctx.font=fontString(this.fontSize,this.fontStyle,this.fontFamily);
if(this.showLabelBackdrop){
var labelWidth=ctx.measureText(label).width;
ctx.fillStyle=this.backdropColor;
ctx.fillRect(
this.xCenter-labelWidth/2-this.backdropPaddingX,
yHeight-this.fontSize/2-this.backdropPaddingY,
labelWidth+this.backdropPaddingX*2,
this.fontSize+this.backdropPaddingY*2
);
}
ctx.textAlign='center';
ctx.textBaseline="middle";
ctx.fillStyle=this.fontColor;
ctx.fillText(label,this.xCenter,yHeight);
}
}
},this);
if(!this.lineArc){
ctx.lineWidth=this.angleLineWidth;
ctx.strokeStyle=this.angleLineColor;
for(var i=this.valuesCount-1;i>=0;i--){
if(this.angleLineWidth>0){
var outerPosition=this.getPointPosition(i,this.calculateCenterOffset(this.max));
ctx.beginPath();
ctx.moveTo(this.xCenter,this.yCenter);
ctx.lineTo(outerPosition.x,outerPosition.y);
ctx.stroke();
ctx.closePath();
}
var pointLabelPosition=this.getPointPosition(i,this.calculateCenterOffset(this.max)+5);
ctx.font=fontString(this.pointLabelFontSize,this.pointLabelFontStyle,this.pointLabelFontFamily);
ctx.fillStyle=this.pointLabelFontColor;
var labelsCount=this.labels.length,
halfLabelsCount=this.labels.length/2,
quarterLabelsCount=halfLabelsCount/2,
upperHalf=(i<quarterLabelsCount||i>labelsCount-quarterLabelsCount),
exactQuarter=(i===quarterLabelsCount||i===labelsCount-quarterLabelsCount);
if(i===0){
ctx.textAlign='center';
}else if(i===halfLabelsCount){
ctx.textAlign='center';
}else if(i<halfLabelsCount){
ctx.textAlign='left';
}else{
ctx.textAlign='right';
}
if(exactQuarter){
ctx.textBaseline='middle';
}else if(upperHalf){
ctx.textBaseline='bottom';
}else{
ctx.textBaseline='top';
}
ctx.fillText(this.labels[i],pointLabelPosition.x,pointLabelPosition.y);
}
}
}
}
});
helpers.addEvent(window,"resize",(function(){
var timeout;
return function(){
clearTimeout(timeout);
timeout=setTimeout(function(){
each(Chart.instances,function(instance){
if(instance.options.responsive){
instance.resize(instance.render,true);
}
});
},50);
};
})());
if(amd){
define(function(){
return Chart;
});
}else if(typeof module==='object'&&module.exports){
module.exports=Chart;
}
root.Chart=Chart;
Chart.noConflict=function(){
root.Chart=previous;
return Chart;
};
}).call(this);
(function(){
"use strict";
var root=this,
Chart=root.Chart,
helpers=Chart.helpers;
var defaultConfig={
scaleBeginAtZero:true,
scaleShowGridLines:true,
scaleGridLineColor:"rgba(0,0,0,.05)",
scaleGridLineWidth:1,
barShowStroke:true,
barStrokeWidth:2,
barValueSpacing:5,
barDatasetSpacing:1,
legendTemplate:"<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
};
Chart.Type.extend({
name:"Bar",
defaults:defaultConfig,
initialize:function(data){
var options=this.options;
this.ScaleClass=Chart.Scale.extend({
offsetGridLines:true,
calculateBarX:function(datasetCount,datasetIndex,barIndex){
var xWidth=this.calculateBaseWidth(),
xAbsolute=this.calculateX(barIndex)-(xWidth/2),
barWidth=this.calculateBarWidth(datasetCount);
return xAbsolute+(barWidth*datasetIndex)+(datasetIndex*options.barDatasetSpacing)+barWidth/2;
},
calculateBaseWidth:function(){
return(this.calculateX(1)-this.calculateX(0))-(2*options.barValueSpacing);
},
calculateBarWidth:function(datasetCount){
var baseWidth=this.calculateBaseWidth()-((datasetCount-1)*options.barDatasetSpacing);
return(baseWidth/datasetCount);
}
});
this.datasets=[];
if(this.options.showTooltips){
helpers.bindEvents(this,this.options.tooltipEvents,function(evt){
var activeBars=(evt.type!=='mouseout')?this.getBarsAtEvent(evt):[];
this.eachBars(function(bar){
bar.restore(['fillColor','strokeColor']);
});
helpers.each(activeBars,function(activeBar){
activeBar.fillColor=activeBar.highlightFill;
activeBar.strokeColor=activeBar.highlightStroke;
});
this.showTooltip(activeBars);
});
}
this.BarClass=Chart.Rectangle.extend({
strokeWidth:this.options.barStrokeWidth,
showStroke:this.options.barShowStroke,
ctx:this.chart.ctx
});
helpers.each(data.datasets,function(dataset,datasetIndex){
var datasetObject={
label:dataset.label||null,
fillColor:dataset.fillColor,
strokeColor:dataset.strokeColor,
bars:[]
};
this.datasets.push(datasetObject);
helpers.each(dataset.data,function(dataPoint,index){
datasetObject.bars.push(new this.BarClass({
value:dataPoint,
label:data.labels[index],
datasetLabel:dataset.label,
strokeColor:dataset.strokeColor,
fillColor:dataset.fillColor,
highlightFill:dataset.highlightFill||dataset.fillColor,
highlightStroke:dataset.highlightStroke||dataset.strokeColor
}));
},this);
},this);
this.buildScale(data.labels);
this.BarClass.prototype.base=this.scale.endPoint;
this.eachBars(function(bar,index,datasetIndex){
helpers.extend(bar,{
width:this.scale.calculateBarWidth(this.datasets.length),
x:this.scale.calculateBarX(this.datasets.length,datasetIndex,index),
y:this.scale.endPoint
});
bar.save();
},this);
this.render();
},
update:function(){
this.scale.update();
helpers.each(this.activeElements,function(activeElement){
activeElement.restore(['fillColor','strokeColor']);
});
this.eachBars(function(bar){
bar.save();
});
this.render();
},
eachBars:function(callback){
helpers.each(this.datasets,function(dataset,datasetIndex){
helpers.each(dataset.bars,callback,this,datasetIndex);
},this);
},
getBarsAtEvent:function(e){
var barsArray=[],
eventPosition=helpers.getRelativePosition(e),
datasetIterator=function(dataset){
barsArray.push(dataset.bars[barIndex]);
},
barIndex;
for(var datasetIndex=0;datasetIndex<this.datasets.length;datasetIndex++){
for(barIndex=0;barIndex<this.datasets[datasetIndex].bars.length;barIndex++){
if(this.datasets[datasetIndex].bars[barIndex].inRange(eventPosition.x,eventPosition.y)){
helpers.each(this.datasets,datasetIterator);
return barsArray;
}
}
}
return barsArray;
},
buildScale:function(labels){
var self=this;
var dataTotal=function(){
var values=[];
self.eachBars(function(bar){
values.push(bar.value);
});
return values;
};
var scaleOptions={
templateString:this.options.scaleLabel,
height:this.chart.height,
width:this.chart.width,
ctx:this.chart.ctx,
textColor:this.options.scaleFontColor,
fontSize:this.options.scaleFontSize,
fontStyle:this.options.scaleFontStyle,
fontFamily:this.options.scaleFontFamily,
valuesCount:labels.length,
beginAtZero:this.options.scaleBeginAtZero,
integersOnly:this.options.scaleIntegersOnly,
calculateYRange:function(currentHeight){
var updatedRanges=helpers.calculateScaleRange(
dataTotal(),
currentHeight,
this.fontSize,
this.beginAtZero,
this.integersOnly
);
helpers.extend(this,updatedRanges);
},
xLabels:labels,
font:helpers.fontString(this.options.scaleFontSize,this.options.scaleFontStyle,this.options.scaleFontFamily),
lineWidth:this.options.scaleLineWidth,
lineColor:this.options.scaleLineColor,
gridLineWidth:(this.options.scaleShowGridLines)?this.options.scaleGridLineWidth:0,
gridLineColor:(this.options.scaleShowGridLines)?this.options.scaleGridLineColor:"rgba(0,0,0,0)",
padding:(this.options.showScale)?0:(this.options.barShowStroke)?this.options.barStrokeWidth:0,
showLabels:this.options.scaleShowLabels,
display:this.options.showScale
};
if(this.options.scaleOverride){
helpers.extend(scaleOptions,{
calculateYRange:helpers.noop,
steps:this.options.scaleSteps,
stepValue:this.options.scaleStepWidth,
min:this.options.scaleStartValue,
max:this.options.scaleStartValue+(this.options.scaleSteps*this.options.scaleStepWidth)
});
}
this.scale=new this.ScaleClass(scaleOptions);
},
addData:function(valuesArray,label){
helpers.each(valuesArray,function(value,datasetIndex){
this.datasets[datasetIndex].bars.push(new this.BarClass({
value:value,
label:label,
x:this.scale.calculateBarX(this.datasets.length,datasetIndex,this.scale.valuesCount+1),
y:this.scale.endPoint,
width:this.scale.calculateBarWidth(this.datasets.length),
base:this.scale.endPoint,
strokeColor:this.datasets[datasetIndex].strokeColor,
fillColor:this.datasets[datasetIndex].fillColor
}));
},this);
this.scale.addXLabel(label);
this.update();
},
removeData:function(){
this.scale.removeXLabel();
helpers.each(this.datasets,function(dataset){
dataset.bars.shift();
},this);
this.update();
},
reflow:function(){
helpers.extend(this.BarClass.prototype,{
y:this.scale.endPoint,
base:this.scale.endPoint
});
var newScaleProps=helpers.extend({
height:this.chart.height,
width:this.chart.width
});
this.scale.update(newScaleProps);
},
draw:function(ease){
var easingDecimal=ease||1;
this.clear();
var ctx=this.chart.ctx;
this.scale.draw(easingDecimal);
helpers.each(this.datasets,function(dataset,datasetIndex){
helpers.each(dataset.bars,function(bar,index){
if(bar.hasValue()){
bar.base=this.scale.endPoint;
bar.transition({
x:this.scale.calculateBarX(this.datasets.length,datasetIndex,index),
y:this.scale.calculateY(bar.value),
width:this.scale.calculateBarWidth(this.datasets.length)
},easingDecimal).draw();
}
},this);
},this);
}
});
}).call(this);
(function(){
"use strict";
var root=this,
Chart=root.Chart,
helpers=Chart.helpers;
var defaultConfig={
segmentShowStroke:true,
segmentStrokeColor:"#fff",
segmentStrokeWidth:2,
percentageInnerCutout:50,
animationSteps:100,
animationEasing:"easeOutBounce",
animateRotate:true,
animateScale:false,
legendTemplate:"<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
};
Chart.Type.extend({
name:"Doughnut",
defaults:defaultConfig,
initialize:function(data){
this.segments=[];
this.outerRadius=(helpers.min([this.chart.width,this.chart.height])-this.options.segmentStrokeWidth/2)/2;
this.SegmentArc=Chart.Arc.extend({
ctx:this.chart.ctx,
x:this.chart.width/2,
y:this.chart.height/2
});
if(this.options.showTooltips){
helpers.bindEvents(this,this.options.tooltipEvents,function(evt){
var activeSegments=(evt.type!=='mouseout')?this.getSegmentsAtEvent(evt):[];
helpers.each(this.segments,function(segment){
segment.restore(["fillColor"]);
});
helpers.each(activeSegments,function(activeSegment){
activeSegment.fillColor=activeSegment.highlightColor;
});
this.showTooltip(activeSegments);
});
}
this.calculateTotal(data);
helpers.each(data,function(datapoint,index){
this.addData(datapoint,index,true);
},this);
this.render();
},
getSegmentsAtEvent:function(e){
var segmentsArray=[];
var location=helpers.getRelativePosition(e);
helpers.each(this.segments,function(segment){
if(segment.inRange(location.x,location.y))segmentsArray.push(segment);
},this);
return segmentsArray;
},
addData:function(segment,atIndex,silent){
var index=atIndex||this.segments.length;
this.segments.splice(index,0,new this.SegmentArc({
value:segment.value,
outerRadius:(this.options.animateScale)?0:this.outerRadius,
innerRadius:(this.options.animateScale)?0:(this.outerRadius/100)*this.options.percentageInnerCutout,
fillColor:segment.color,
highlightColor:segment.highlight||segment.color,
showStroke:this.options.segmentShowStroke,
strokeWidth:this.options.segmentStrokeWidth,
strokeColor:this.options.segmentStrokeColor,
startAngle:Math.PI*1.5,
circumference:(this.options.animateRotate)?0:this.calculateCircumference(segment.value),
label:segment.label
}));
if(!silent){
this.reflow();
this.update();
}
},
calculateCircumference:function(value){
return(Math.PI*2)*(value/this.total);
},
calculateTotal:function(data){
this.total=0;
helpers.each(data,function(segment){
this.total+=segment.value;
},this);
},
update:function(){
this.calculateTotal(this.segments);
helpers.each(this.activeElements,function(activeElement){
activeElement.restore(['fillColor']);
});
helpers.each(this.segments,function(segment){
segment.save();
});
this.render();
},
removeData:function(atIndex){
var indexToDelete=(helpers.isNumber(atIndex))?atIndex:this.segments.length-1;
this.segments.splice(indexToDelete,1);
this.reflow();
this.update();
},
reflow:function(){
helpers.extend(this.SegmentArc.prototype,{
x:this.chart.width/2,
y:this.chart.height/2
});
this.outerRadius=(helpers.min([this.chart.width,this.chart.height])-this.options.segmentStrokeWidth/2)/2;
helpers.each(this.segments,function(segment){
segment.update({
outerRadius:this.outerRadius,
innerRadius:(this.outerRadius/100)*this.options.percentageInnerCutout
});
},this);
},
draw:function(easeDecimal){
var animDecimal=(easeDecimal)?easeDecimal:1;
this.clear();
helpers.each(this.segments,function(segment,index){
segment.transition({
circumference:this.calculateCircumference(segment.value),
outerRadius:this.outerRadius,
innerRadius:(this.outerRadius/100)*this.options.percentageInnerCutout
},animDecimal);
segment.endAngle=segment.startAngle+segment.circumference;
segment.draw();
if(index===0){
segment.startAngle=Math.PI*1.5;
}
if(index<this.segments.length-1){
this.segments[index+1].startAngle=segment.endAngle;
}
},this);
}
});
Chart.types.Doughnut.extend({
name:"Pie",
defaults:helpers.merge(defaultConfig,{percentageInnerCutout:0})
});
}).call(this);
(function(){
"use strict";
var root=this,
Chart=root.Chart,
helpers=Chart.helpers;
var defaultConfig={
scaleShowGridLines:true,
scaleGridLineColor:"rgba(0,0,0,.05)",
scaleGridLineWidth:1,
bezierCurve:true,
bezierCurveTension:0.4,
pointDot:true,
pointDotRadius:4,
pointDotStrokeWidth:1,
pointHitDetectionRadius:20,
datasetStroke:true,
datasetStrokeWidth:2,
datasetFill:true,
legendTemplate:"<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
};
Chart.Type.extend({
name:"Line",
defaults:defaultConfig,
initialize:function(data){
this.PointClass=Chart.Point.extend({
strokeWidth:this.options.pointDotStrokeWidth,
radius:this.options.pointDotRadius,
display:this.options.pointDot,
hitDetectionRadius:this.options.pointHitDetectionRadius,
ctx:this.chart.ctx,
inRange:function(mouseX){
return(Math.pow(mouseX-this.x,2)<Math.pow(this.radius+this.hitDetectionRadius,2));
}
});
this.datasets=[];
if(this.options.showTooltips){
helpers.bindEvents(this,this.options.tooltipEvents,function(evt){
var activePoints=(evt.type!=='mouseout')?this.getPointsAtEvent(evt):[];
this.eachPoints(function(point){
point.restore(['fillColor','strokeColor']);
});
helpers.each(activePoints,function(activePoint){
activePoint.fillColor=activePoint.highlightFill;
activePoint.strokeColor=activePoint.highlightStroke;
});
this.showTooltip(activePoints);
});
}
helpers.each(data.datasets,function(dataset){
var datasetObject={
label:dataset.label||null,
fillColor:dataset.fillColor,
strokeColor:dataset.strokeColor,
pointColor:dataset.pointColor,
pointStrokeColor:dataset.pointStrokeColor,
points:[]
};
this.datasets.push(datasetObject);
helpers.each(dataset.data,function(dataPoint,index){
datasetObject.points.push(new this.PointClass({
value:dataPoint,
label:data.labels[index],
datasetLabel:dataset.label,
strokeColor:dataset.pointStrokeColor,
fillColor:dataset.pointColor,
highlightFill:dataset.pointHighlightFill||dataset.pointColor,
highlightStroke:dataset.pointHighlightStroke||dataset.pointStrokeColor
}));
},this);
this.buildScale(data.labels);
this.eachPoints(function(point,index){
helpers.extend(point,{
x:this.scale.calculateX(index),
y:this.scale.endPoint
});
point.save();
},this);
},this);
this.render();
},
update:function(){
this.scale.update();
helpers.each(this.activeElements,function(activeElement){
activeElement.restore(['fillColor','strokeColor']);
});
this.eachPoints(function(point){
point.save();
});
this.render();
},
eachPoints:function(callback){
helpers.each(this.datasets,function(dataset){
helpers.each(dataset.points,callback,this);
},this);
},
getPointsAtEvent:function(e){
var pointsArray=[],
eventPosition=helpers.getRelativePosition(e);
helpers.each(this.datasets,function(dataset){
helpers.each(dataset.points,function(point){
if(point.inRange(eventPosition.x,eventPosition.y))pointsArray.push(point);
});
},this);
return pointsArray;
},
buildScale:function(labels){
var self=this;
var dataTotal=function(){
var values=[];
self.eachPoints(function(point){
values.push(point.value);
});
return values;
};
var scaleOptions={
templateString:this.options.scaleLabel,
height:this.chart.height,
width:this.chart.width,
ctx:this.chart.ctx,
textColor:this.options.scaleFontColor,
fontSize:this.options.scaleFontSize,
fontStyle:this.options.scaleFontStyle,
fontFamily:this.options.scaleFontFamily,
valuesCount:labels.length,
beginAtZero:this.options.scaleBeginAtZero,
integersOnly:this.options.scaleIntegersOnly,
calculateYRange:function(currentHeight){
var updatedRanges=helpers.calculateScaleRange(
dataTotal(),
currentHeight,
this.fontSize,
this.beginAtZero,
this.integersOnly
);
helpers.extend(this,updatedRanges);
},
xLabels:labels,
font:helpers.fontString(this.options.scaleFontSize,this.options.scaleFontStyle,this.options.scaleFontFamily),
lineWidth:this.options.scaleLineWidth,
lineColor:this.options.scaleLineColor,
gridLineWidth:(this.options.scaleShowGridLines)?this.options.scaleGridLineWidth:0,
gridLineColor:(this.options.scaleShowGridLines)?this.options.scaleGridLineColor:"rgba(0,0,0,0)",
padding:(this.options.showScale)?0:this.options.pointDotRadius+this.options.pointDotStrokeWidth,
showLabels:this.options.scaleShowLabels,
display:this.options.showScale
};
if(this.options.scaleOverride){
helpers.extend(scaleOptions,{
calculateYRange:helpers.noop,
steps:this.options.scaleSteps,
stepValue:this.options.scaleStepWidth,
min:this.options.scaleStartValue,
max:this.options.scaleStartValue+(this.options.scaleSteps*this.options.scaleStepWidth)
});
}
this.scale=new Chart.Scale(scaleOptions);
},
addData:function(valuesArray,label){
helpers.each(valuesArray,function(value,datasetIndex){
this.datasets[datasetIndex].points.push(new this.PointClass({
value:value,
label:label,
x:this.scale.calculateX(this.scale.valuesCount+1),
y:this.scale.endPoint,
strokeColor:this.datasets[datasetIndex].pointStrokeColor,
fillColor:this.datasets[datasetIndex].pointColor
}));
},this);
this.scale.addXLabel(label);
this.update();
},
removeData:function(){
this.scale.removeXLabel();
helpers.each(this.datasets,function(dataset){
dataset.points.shift();
},this);
this.update();
},
reflow:function(){
var newScaleProps=helpers.extend({
height:this.chart.height,
width:this.chart.width
});
this.scale.update(newScaleProps);
},
draw:function(ease){
var easingDecimal=ease||1;
this.clear();
var ctx=this.chart.ctx;
var hasValue=function(item){
return item.value!==null;
},
nextPoint=function(point,collection,index){
return helpers.findNextWhere(collection,hasValue,index)||point;
},
previousPoint=function(point,collection,index){
return helpers.findPreviousWhere(collection,hasValue,index)||point;
};
this.scale.draw(easingDecimal);
helpers.each(this.datasets,function(dataset){
var pointsWithValues=helpers.where(dataset.points,hasValue);
helpers.each(dataset.points,function(point,index){
if(point.hasValue()){
point.transition({
y:this.scale.calculateY(point.value),
x:this.scale.calculateX(index)
},easingDecimal);
}
},this);
if(this.options.bezierCurve){
helpers.each(pointsWithValues,function(point,index){
var tension=(index>0&&index<pointsWithValues.length-1)?this.options.bezierCurveTension:0;
point.controlPoints=helpers.splineCurve(
previousPoint(point,pointsWithValues,index),
point,
nextPoint(point,pointsWithValues,index),
tension
);
if(point.controlPoints.outer.y>this.scale.endPoint){
point.controlPoints.outer.y=this.scale.endPoint;
}
else if(point.controlPoints.outer.y<this.scale.startPoint){
point.controlPoints.outer.y=this.scale.startPoint;
}
if(point.controlPoints.inner.y>this.scale.endPoint){
point.controlPoints.inner.y=this.scale.endPoint;
}
else if(point.controlPoints.inner.y<this.scale.startPoint){
point.controlPoints.inner.y=this.scale.startPoint;
}
},this);
}
ctx.lineWidth=this.options.datasetStrokeWidth;
ctx.strokeStyle=dataset.strokeColor;
ctx.beginPath();
helpers.each(pointsWithValues,function(point,index){
if(index===0){
ctx.moveTo(point.x,point.y);
}
else{
if(this.options.bezierCurve){
var previous=previousPoint(point,pointsWithValues,index);
ctx.bezierCurveTo(
previous.controlPoints.outer.x,
previous.controlPoints.outer.y,
point.controlPoints.inner.x,
point.controlPoints.inner.y,
point.x,
point.y
);
}
else{
ctx.lineTo(point.x,point.y);
}
}
},this);
ctx.stroke();
if(this.options.datasetFill&&pointsWithValues.length>0){
ctx.lineTo(pointsWithValues[pointsWithValues.length-1].x,this.scale.endPoint);
ctx.lineTo(pointsWithValues[0].x,this.scale.endPoint);
ctx.fillStyle=dataset.fillColor;
ctx.closePath();
ctx.fill();
}
helpers.each(pointsWithValues,function(point){
point.draw();
});
},this);
}
});
}).call(this);
(function(){
"use strict";
var root=this,
Chart=root.Chart,
helpers=Chart.helpers;
var defaultConfig={
scaleShowLabelBackdrop:true,
scaleBackdropColor:"rgba(255,255,255,0.75)",
scaleBeginAtZero:true,
scaleBackdropPaddingY:2,
scaleBackdropPaddingX:2,
scaleShowLine:true,
segmentShowStroke:true,
segmentStrokeColor:"#fff",
segmentStrokeWidth:2,
animationSteps:100,
animationEasing:"easeOutBounce",
animateRotate:true,
animateScale:false,
legendTemplate:"<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
};
Chart.Type.extend({
name:"PolarArea",
defaults:defaultConfig,
initialize:function(data){
this.segments=[];
this.SegmentArc=Chart.Arc.extend({
showStroke:this.options.segmentShowStroke,
strokeWidth:this.options.segmentStrokeWidth,
strokeColor:this.options.segmentStrokeColor,
ctx:this.chart.ctx,
innerRadius:0,
x:this.chart.width/2,
y:this.chart.height/2
});
this.scale=new Chart.RadialScale({
display:this.options.showScale,
fontStyle:this.options.scaleFontStyle,
fontSize:this.options.scaleFontSize,
fontFamily:this.options.scaleFontFamily,
fontColor:this.options.scaleFontColor,
showLabels:this.options.scaleShowLabels,
showLabelBackdrop:this.options.scaleShowLabelBackdrop,
backdropColor:this.options.scaleBackdropColor,
backdropPaddingY:this.options.scaleBackdropPaddingY,
backdropPaddingX:this.options.scaleBackdropPaddingX,
lineWidth:(this.options.scaleShowLine)?this.options.scaleLineWidth:0,
lineColor:this.options.scaleLineColor,
lineArc:true,
width:this.chart.width,
height:this.chart.height,
xCenter:this.chart.width/2,
yCenter:this.chart.height/2,
ctx:this.chart.ctx,
templateString:this.options.scaleLabel,
valuesCount:data.length
});
this.updateScaleRange(data);
this.scale.update();
helpers.each(data,function(segment,index){
this.addData(segment,index,true);
},this);
if(this.options.showTooltips){
helpers.bindEvents(this,this.options.tooltipEvents,function(evt){
var activeSegments=(evt.type!=='mouseout')?this.getSegmentsAtEvent(evt):[];
helpers.each(this.segments,function(segment){
segment.restore(["fillColor"]);
});
helpers.each(activeSegments,function(activeSegment){
activeSegment.fillColor=activeSegment.highlightColor;
});
this.showTooltip(activeSegments);
});
}
this.render();
},
getSegmentsAtEvent:function(e){
var segmentsArray=[];
var location=helpers.getRelativePosition(e);
helpers.each(this.segments,function(segment){
if(segment.inRange(location.x,location.y))segmentsArray.push(segment);
},this);
return segmentsArray;
},
addData:function(segment,atIndex,silent){
var index=atIndex||this.segments.length;
this.segments.splice(index,0,new this.SegmentArc({
fillColor:segment.color,
highlightColor:segment.highlight||segment.color,
label:segment.label,
value:segment.value,
outerRadius:(this.options.animateScale)?0:this.scale.calculateCenterOffset(segment.value),
circumference:(this.options.animateRotate)?0:this.scale.getCircumference(),
startAngle:Math.PI*1.5
}));
if(!silent){
this.reflow();
this.update();
}
},
removeData:function(atIndex){
var indexToDelete=(helpers.isNumber(atIndex))?atIndex:this.segments.length-1;
this.segments.splice(indexToDelete,1);
this.reflow();
this.update();
},
calculateTotal:function(data){
this.total=0;
helpers.each(data,function(segment){
this.total+=segment.value;
},this);
this.scale.valuesCount=this.segments.length;
},
updateScaleRange:function(datapoints){
var valuesArray=[];
helpers.each(datapoints,function(segment){
valuesArray.push(segment.value);
});
var scaleSizes=(this.options.scaleOverride)?
{
steps:this.options.scaleSteps,
stepValue:this.options.scaleStepWidth,
min:this.options.scaleStartValue,
max:this.options.scaleStartValue+(this.options.scaleSteps*this.options.scaleStepWidth)
}:
helpers.calculateScaleRange(
valuesArray,
helpers.min([this.chart.width,this.chart.height])/2,
this.options.scaleFontSize,
this.options.scaleBeginAtZero,
this.options.scaleIntegersOnly
);
helpers.extend(
this.scale,
scaleSizes,
{
size:helpers.min([this.chart.width,this.chart.height]),
xCenter:this.chart.width/2,
yCenter:this.chart.height/2
}
);
},
update:function(){
this.calculateTotal(this.segments);
helpers.each(this.segments,function(segment){
segment.save();
});
this.render();
},
reflow:function(){
helpers.extend(this.SegmentArc.prototype,{
x:this.chart.width/2,
y:this.chart.height/2
});
this.updateScaleRange(this.segments);
this.scale.update();
helpers.extend(this.scale,{
xCenter:this.chart.width/2,
yCenter:this.chart.height/2
});
helpers.each(this.segments,function(segment){
segment.update({
outerRadius:this.scale.calculateCenterOffset(segment.value)
});
},this);
},
draw:function(ease){
var easingDecimal=ease||1;
this.clear();
helpers.each(this.segments,function(segment,index){
segment.transition({
circumference:this.scale.getCircumference(),
outerRadius:this.scale.calculateCenterOffset(segment.value)
},easingDecimal);
segment.endAngle=segment.startAngle+segment.circumference;
if(index===0){
segment.startAngle=Math.PI*1.5;
}
if(index<this.segments.length-1){
this.segments[index+1].startAngle=segment.endAngle;
}
segment.draw();
},this);
this.scale.draw();
}
});
}).call(this);
(function(){
"use strict";
var root=this,
Chart=root.Chart,
helpers=Chart.helpers;
Chart.Type.extend({
name:"Radar",
defaults:{
scaleShowLine:true,
angleShowLineOut:true,
scaleShowLabels:false,
scaleBeginAtZero:true,
angleLineColor:"rgba(0,0,0,.1)",
angleLineWidth:1,
pointLabelFontFamily:"'Arial'",
pointLabelFontStyle:"normal",
pointLabelFontSize:10,
pointLabelFontColor:"#666",
pointDot:true,
pointDotRadius:3,
pointDotStrokeWidth:1,
pointHitDetectionRadius:20,
datasetStroke:true,
datasetStrokeWidth:2,
datasetFill:true,
legendTemplate:"<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
},
initialize:function(data){
this.PointClass=Chart.Point.extend({
strokeWidth:this.options.pointDotStrokeWidth,
radius:this.options.pointDotRadius,
display:this.options.pointDot,
hitDetectionRadius:this.options.pointHitDetectionRadius,
ctx:this.chart.ctx
});
this.datasets=[];
this.buildScale(data);
if(this.options.showTooltips){
helpers.bindEvents(this,this.options.tooltipEvents,function(evt){
var activePointsCollection=(evt.type!=='mouseout')?this.getPointsAtEvent(evt):[];
this.eachPoints(function(point){
point.restore(['fillColor','strokeColor']);
});
helpers.each(activePointsCollection,function(activePoint){
activePoint.fillColor=activePoint.highlightFill;
activePoint.strokeColor=activePoint.highlightStroke;
});
this.showTooltip(activePointsCollection);
});
}
helpers.each(data.datasets,function(dataset){
var datasetObject={
label:dataset.label||null,
fillColor:dataset.fillColor,
strokeColor:dataset.strokeColor,
pointColor:dataset.pointColor,
pointStrokeColor:dataset.pointStrokeColor,
points:[]
};
this.datasets.push(datasetObject);
helpers.each(dataset.data,function(dataPoint,index){
var pointPosition;
if(!this.scale.animation){
pointPosition=this.scale.getPointPosition(index,this.scale.calculateCenterOffset(dataPoint));
}
datasetObject.points.push(new this.PointClass({
value:dataPoint,
label:data.labels[index],
datasetLabel:dataset.label,
x:(this.options.animation)?this.scale.xCenter:pointPosition.x,
y:(this.options.animation)?this.scale.yCenter:pointPosition.y,
strokeColor:dataset.pointStrokeColor,
fillColor:dataset.pointColor,
highlightFill:dataset.pointHighlightFill||dataset.pointColor,
highlightStroke:dataset.pointHighlightStroke||dataset.pointStrokeColor
}));
},this);
},this);
this.render();
},
eachPoints:function(callback){
helpers.each(this.datasets,function(dataset){
helpers.each(dataset.points,callback,this);
},this);
},
getPointsAtEvent:function(evt){
var mousePosition=helpers.getRelativePosition(evt),
fromCenter=helpers.getAngleFromPoint({
x:this.scale.xCenter,
y:this.scale.yCenter
},mousePosition);
var anglePerIndex=(Math.PI*2)/this.scale.valuesCount,
pointIndex=Math.round((fromCenter.angle-Math.PI*1.5)/anglePerIndex),
activePointsCollection=[];
if(pointIndex>=this.scale.valuesCount||pointIndex<0){
pointIndex=0;
}
if(fromCenter.distance<=this.scale.drawingArea){
helpers.each(this.datasets,function(dataset){
activePointsCollection.push(dataset.points[pointIndex]);
});
}
return activePointsCollection;
},
buildScale:function(data){
this.scale=new Chart.RadialScale({
display:this.options.showScale,
fontStyle:this.options.scaleFontStyle,
fontSize:this.options.scaleFontSize,
fontFamily:this.options.scaleFontFamily,
fontColor:this.options.scaleFontColor,
showLabels:this.options.scaleShowLabels,
showLabelBackdrop:this.options.scaleShowLabelBackdrop,
backdropColor:this.options.scaleBackdropColor,
backdropPaddingY:this.options.scaleBackdropPaddingY,
backdropPaddingX:this.options.scaleBackdropPaddingX,
lineWidth:(this.options.scaleShowLine)?this.options.scaleLineWidth:0,
lineColor:this.options.scaleLineColor,
angleLineColor:this.options.angleLineColor,
angleLineWidth:(this.options.angleShowLineOut)?this.options.angleLineWidth:0,
pointLabelFontColor:this.options.pointLabelFontColor,
pointLabelFontSize:this.options.pointLabelFontSize,
pointLabelFontFamily:this.options.pointLabelFontFamily,
pointLabelFontStyle:this.options.pointLabelFontStyle,
height:this.chart.height,
width:this.chart.width,
xCenter:this.chart.width/2,
yCenter:this.chart.height/2,
ctx:this.chart.ctx,
templateString:this.options.scaleLabel,
labels:data.labels,
valuesCount:data.datasets[0].data.length
});
this.scale.setScaleSize();
this.updateScaleRange(data.datasets);
this.scale.buildYLabels();
},
updateScaleRange:function(datasets){
var valuesArray=(function(){
var totalDataArray=[];
helpers.each(datasets,function(dataset){
if(dataset.data){
totalDataArray=totalDataArray.concat(dataset.data);
}
else{
helpers.each(dataset.points,function(point){
totalDataArray.push(point.value);
});
}
});
return totalDataArray;
})();
var scaleSizes=(this.options.scaleOverride)?
{
steps:this.options.scaleSteps,
stepValue:this.options.scaleStepWidth,
min:this.options.scaleStartValue,
max:this.options.scaleStartValue+(this.options.scaleSteps*this.options.scaleStepWidth)
}:
helpers.calculateScaleRange(
valuesArray,
helpers.min([this.chart.width,this.chart.height])/2,
this.options.scaleFontSize,
this.options.scaleBeginAtZero,
this.options.scaleIntegersOnly
);
helpers.extend(
this.scale,
scaleSizes
);
},
addData:function(valuesArray,label){
this.scale.valuesCount++;
helpers.each(valuesArray,function(value,datasetIndex){
var pointPosition=this.scale.getPointPosition(this.scale.valuesCount,this.scale.calculateCenterOffset(value));
this.datasets[datasetIndex].points.push(new this.PointClass({
value:value,
label:label,
x:pointPosition.x,
y:pointPosition.y,
strokeColor:this.datasets[datasetIndex].pointStrokeColor,
fillColor:this.datasets[datasetIndex].pointColor
}));
},this);
this.scale.labels.push(label);
this.reflow();
this.update();
},
removeData:function(){
this.scale.valuesCount--;
this.scale.labels.shift();
helpers.each(this.datasets,function(dataset){
dataset.points.shift();
},this);
this.reflow();
this.update();
},
update:function(){
this.eachPoints(function(point){
point.save();
});
this.reflow();
this.render();
},
reflow:function(){
helpers.extend(this.scale,{
width:this.chart.width,
height:this.chart.height,
size:helpers.min([this.chart.width,this.chart.height]),
xCenter:this.chart.width/2,
yCenter:this.chart.height/2
});
this.updateScaleRange(this.datasets);
this.scale.setScaleSize();
this.scale.buildYLabels();
},
draw:function(ease){
var easeDecimal=ease||1,
ctx=this.chart.ctx;
this.clear();
this.scale.draw();
helpers.each(this.datasets,function(dataset){
helpers.each(dataset.points,function(point,index){
if(point.hasValue()){
point.transition(this.scale.getPointPosition(index,this.scale.calculateCenterOffset(point.value)),easeDecimal);
}
},this);
ctx.lineWidth=this.options.datasetStrokeWidth;
ctx.strokeStyle=dataset.strokeColor;
ctx.beginPath();
helpers.each(dataset.points,function(point,index){
if(index===0){
ctx.moveTo(point.x,point.y);
}
else{
ctx.lineTo(point.x,point.y);
}
},this);
ctx.closePath();
ctx.stroke();
ctx.fillStyle=dataset.fillColor;
ctx.fill();
helpers.each(dataset.points,function(point){
if(point.hasValue()){
point.draw();
}
});
},this);
}
});
}).call(this);


/* plugins/auto/chartjs/v1.0.5/js/chart_extra.js */

Chart.defaults.global.responsive=true;


/* local/cache-js/jsdyn-_js_cibloc_js-102cf2dd.js?1651594293 */

var cibloc_accordeon_id=0;
var cibloc_question_id=0;
var cibloc_ancre=window.location.hash;
var cibloc_ancre_existe=false;
function cibloc_accordeon_replier_tout(id_question_click){
var bloc_id='';
var tableau='';
var tableau_click=id_question_click.split('-');
var id_bloc_click='';
if(typeof tableau_click[0]!=='undefined'&&typeof tableau_click[1]!=='undefined'){
id_bloc_click=tableau_click[0]+'-'+tableau_click[1];
}
var lesLiens=$('.cibloc_accordeon a.accordeon_lien');
for(var i=0;i<lesLiens.length;i++){
bloc_id='';
question_id=$(lesLiens[i]).attr('aria-controls');
tableau=question_id.split('-');
if(typeof tableau[0]!=='undefined'&&typeof tableau[1]!=='undefined'){
bloc_id=tableau[0]+'-'+tableau[1];
}
if(bloc_id==id_bloc_click){
$(lesLiens[i]).attr('aria-expanded','false');
$(lesLiens[i]).removeClass('expanded');
$('#'+question_id).attr('aria-hidden','true');
$('#'+question_id).css('display','none');
}
}
};
function cibloc_init_js(){
var intertitre="h2";
$('.cibloc_accordeon').each(function(){
cibloc_accordeon_id++;
cibloc_question_id=0;
$(this).find(intertitre).each(function(){
var $this=$(this);
cibloc_question_id++;
var prefixe='accordeon-'+cibloc_accordeon_id+'-';
var id=prefixe+cibloc_question_id;
var style=' style="display:none;"';
var ariahidden=' aria-hidden="true"';
var ariaexpanded=' aria-expanded="false"';
var lienclass=' class="accordeon_lien"';
if(cibloc_ancre){
if(cibloc_ancre=='#'+id){
style='';
ariahidden=' aria-hidden="false"';
ariaexpanded=' aria-expanded="true"';
lienclass=' class="accordeon_lien expanded"';
cibloc_ancre_existe=true;
}else if(cibloc_ancre.substring(1,prefixe.length+1)==prefixe){
cibloc_ancre_existe=true;
}
}
if(!cibloc_ancre_existe&&cibloc_question_id==1){
style='';
ariahidden=' aria-hidden="false"';
ariaexpanded=' aria-expanded="true"'
lienclass=' class="accordeon_lien expanded"';
}
$this.children("a.sommaire-back").css("display","none");
$this.nextUntil(intertitre).wrapAll('<div id="'+id+'"'+ariahidden+style+' class="cibloc_reponse">');
var reponse=$this.next();
if($this.parents().hasClass("cibloc_sans_vignette")){
$this.wrapInner('<span>');
$this.wrapInner('<span role="button" '+lienclass+ariaexpanded+' aria-controls="'+id+'">');
}else{
$this.wrapInner('<span>');
$this.wrapInner('<a href="#'+id+'" role="button" '+lienclass+ariaexpanded+' aria-controls="'+id+'">');
var lien=$this.children('a');
lien.on('click',function(event){
if($(this).attr('aria-expanded')==='false'){
cibloc_accordeon_replier_tout($(this).attr('aria-controls'));
$(this).attr('aria-expanded','true');
$(this).addClass('expanded');
reponse.attr('aria-hidden','false');
reponse.css('display','block');
event.preventDefault();
event.stopPropagation();
}else{
$(this).attr('aria-expanded','false');
$(this).removeClass('expanded');
reponse.attr('aria-hidden','true');
reponse.css('display','none');
event.preventDefault();
event.stopPropagation();
}
});
}
});
});
$('.cibloc_accordeon2').each(function(){
cibloc_accordeon_id++;
cibloc_question_id=0;
$(this).find(intertitre).each(function(){
var $this=$(this);
cibloc_question_id++;
var id='accordeon-'+cibloc_accordeon_id+'-'+cibloc_question_id;
var style=' style="display:none;"';
var ariahidden=' aria-hidden="true"';
var ariaexpanded=' aria-expanded="false"';
var lienclass=' class="accordeon_lien"';
if(cibloc_ancre&&cibloc_ancre=='#'+id){
style='';
ariahidden=' aria-hidden="false"';
ariaexpanded=' aria-expanded="true"';
lienclass=' class="accordeon_lien expanded"';
cibloc_ancre_existe=true;
}
$this.children("a.sommaire-back").css("display","none");
$this.nextUntil(intertitre).wrapAll('<div id="'+id+'"'+ariahidden+style+' class="cibloc_reponse">');
var reponse=$this.next();
if($this.parents().hasClass("cibloc_sans_vignette")){
$this.wrapInner('<span>');
$this.wrapInner('<span role="button" '+lienclass+ariaexpanded+' aria-controls="'+id+'">');
}else{
$this.wrapInner('<span>');
$this.wrapInner('<a href="#'+id+'" role="button" '+lienclass+ariaexpanded+' aria-controls="'+id+'">');
var lien=$this.children('a');
lien.on('click',function(event){
if($(this).attr('aria-expanded')==='false'){
$(this).attr('aria-expanded','true');
$(this).addClass('expanded');
reponse.attr('aria-hidden','false');
reponse.css('display','block');
event.preventDefault();
event.stopPropagation();
}else{
$(this).attr('aria-expanded','false');
$(this).removeClass('expanded');
reponse.attr('aria-hidden','true');
reponse.css('display','none');
event.preventDefault();
event.stopPropagation();
}
});
}
});
});
$('.cibloc_accordeon3').each(function(){
cibloc_accordeon_id++;
cibloc_question_id=0;
var lienclass=' class="accordeon_lien expanded"';
$(this).find(intertitre).each(function(){
var $this=$(this);
cibloc_question_id++;
var id='accordeon-'+cibloc_accordeon_id+'-'+cibloc_question_id;
$this.children("a.sommaire-back").css("display","none");
$this.nextUntil(intertitre).wrapAll('<div id="'+id+'" class="cibloc_reponse">');
if($this.parents().hasClass("cibloc_sans_vignette")){
$this.wrapInner('<span>');
$this.wrapInner('<span '+lienclass+'>');
}else{
$this.wrapInner('<span>');
$this.wrapInner('<a href="#'+id+'" '+lienclass+'>');
}
});
});
};
jQuery(document).ready(function(){
cibloc_init_js();
if(cibloc_ancre_existe){
$('html, body')
.stop()
.animate({scrollTop:$(cibloc_ancre).offset().top},500);
}
if("onhashchange"in window){
window.onhashchange=function(){
var nouvelle_ancre=window.location.hash;
var cible_id;
var reponse2;
if(nouvelle_ancre.substring(0,11)=='#accordeon-'){
cible_id=nouvelle_ancre.substring(1);
}else{
cible_id=$(nouvelle_ancre).children("a.accordeon_lien").attr('aria-controls');
}
if(cible_id){
cibloc_accordeon_replier_tout(cible_id);
var lesLiens2=$('.cibloc_accordeon a.accordeon_lien, .cibloc_accordeon2 a.accordeon_lien');
for(var i=0;i<lesLiens2.length;i++){
if($(lesLiens2[i]).attr('aria-controls')===cible_id){
reponse2=$('#'+cible_id);
$(lesLiens2[i]).attr('aria-expanded','true');
$(lesLiens2[i]).addClass('expanded');
reponse2.attr('aria-hidden','false');
reponse2.css('display','block');
$('html, body')
.stop()
.animate({scrollTop:$(reponse2).offset().top},500);
}
}
}
cibloc_ancre=nouvelle_ancre;
};
}
});


/* plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-collapse.js */

!function($){
"use strict";
var Collapse=function(element,options){
this.$element=$(element)
this.options=$.extend({},$.fn.collapse.defaults,options)
if(this.options.parent){
this.$parent=$(this.options.parent)
}
this.options.toggle&&this.toggle()
}
Collapse.prototype={
constructor:Collapse
,dimension:function(){
var hasWidth=this.$element.hasClass('width')
return hasWidth?'width':'height'
}
,show:function(){
var dimension
,scroll
,actives
,hasData
if(this.transitioning||this.$element.hasClass('in'))return
dimension=this.dimension()
scroll=$.camelCase(['scroll',dimension].join('-'))
actives=this.$parent&&this.$parent.find('> .accordion-group > .in')
if(actives&&actives.length){
hasData=actives.data('collapse')
if(hasData&&hasData.transitioning)return
actives.collapse('hide')
hasData||actives.data('collapse',null)
}
this.$element[dimension](0)
this.transition('addClass',$.Event('show'),'shown')
$.support.transition&&this.$element[dimension](this.$element[0][scroll])
}
,hide:function(){
var dimension
if(this.transitioning||!this.$element.hasClass('in'))return
dimension=this.dimension()
this.reset(this.$element[dimension]())
this.transition('removeClass',$.Event('hide'),'hidden')
this.$element[dimension](0)
}
,reset:function(size){
var dimension=this.dimension()
this.$element
.removeClass('collapse')
[dimension](size||'auto')
[0].offsetWidth
this.$element[size!==null?'addClass':'removeClass']('collapse')
return this
}
,transition:function(method,startEvent,completeEvent){
var that=this
,complete=function(){
if(startEvent.type=='show')that.reset()
that.transitioning=0
that.$element.trigger(completeEvent)
}
this.$element.trigger(startEvent)
if(startEvent.isDefaultPrevented())return
this.transitioning=1
this.$element[method]('in')
$.support.transition&&this.$element.hasClass('collapse')?
this.$element.one($.support.transition.end,complete):
complete()
}
,toggle:function(){
this[this.$element.hasClass('in')?'hide':'show']()
}
}
var old=$.fn.collapse
$.fn.collapse=function(option){
return this.each(function(){
var $this=$(this)
,data=$this.data('collapse')
,options=$.extend({},$.fn.collapse.defaults,$this.data(),typeof option=='object'&&option)
if(!data)$this.data('collapse',(data=new Collapse(this,options)))
if(typeof option=='string')data[option]()
})
}
$.fn.collapse.defaults={
toggle:true
}
$.fn.collapse.Constructor=Collapse
$.fn.collapse.noConflict=function(){
$.fn.collapse=old
return this
}
$(document).on('click.collapse.data-api','[data-toggle=collapse]',function(e){
var $this=$(this),href
,target=$this.attr('data-target')
||e.preventDefault()
||(href=$this.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,'')
,option=$(target).data('collapse')?'toggle':$this.data()
$this[$(target).hasClass('in')?'addClass':'removeClass']('collapsed')
$(target).collapse(option)
})
}(window.jQuery);


/* plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-dropdown.js */

!function($){
"use strict";
var toggle='[data-toggle=dropdown]'
,Dropdown=function(element){
var $el=$(element).on('click.dropdown.data-api',this.toggle)
$('html').on('click.dropdown.data-api',function(){
$el.parent().removeClass('open')
})
}
Dropdown.prototype={
constructor:Dropdown
,toggle:function(e){
var $this=$(this)
,$parent
,isActive
if($this.is('.disabled, :disabled'))return
$parent=getParent($this)
isActive=$parent.hasClass('open')
clearMenus()
if(!isActive){
$parent.toggleClass('open')
}
$this.focus()
return false
}
,keydown:function(e){
var $this
,$items
,$active
,$parent
,isActive
,index
if(!/(38|40|27)/.test(e.keyCode))return
$this=$(this)
e.preventDefault()
e.stopPropagation()
if($this.is('.disabled, :disabled'))return
$parent=getParent($this)
isActive=$parent.hasClass('open')
if(!isActive||(isActive&&e.keyCode==27)){
if(e.which==27)$parent.find(toggle).focus()
return $this.click()
}
$items=$('[role=menu] li:not(.divider):visible a',$parent)
if(!$items.length)return
index=$items.index($items.filter(':focus'))
if(e.keyCode==38&&index>0)index--
if(e.keyCode==40&&index<$items.length-1)index++
if(!~index)index=0
$items
.eq(index)
.focus()
}
}
function clearMenus(){
$('.dropdown-backdrop').remove()
$(toggle).each(function(){
getParent($(this)).removeClass('open')
})
}
function getParent($this){
var selector=$this.attr('data-target')
,$parent
if(!selector){
selector=$this.attr('href')
selector=selector&&/#/.test(selector)&&selector.replace(/.*(?=#[^\s]*$)/,'')
}
$parent=selector&&$(selector)
if(!$parent||!$parent.length)$parent=$this.parent()
return $parent
}
var old=$.fn.dropdown
$.fn.dropdown=function(option){
return this.each(function(){
var $this=$(this)
,data=$this.data('dropdown')
if(!data)$this.data('dropdown',(data=new Dropdown(this)))
if(typeof option=='string')data[option].call($this)
})
}
$.fn.dropdown.Constructor=Dropdown
$.fn.dropdown.noConflict=function(){
$.fn.dropdown=old
return this
}
$(document)
.on('click.dropdown.data-api',clearMenus)
.on('click.dropdown.data-api','.dropdown form',function(e){e.stopPropagation()})
.on('click.dropdown.data-api','.dropdown-menu a',function(e){e.stopPropagation()})
.on('click.dropdown.data-api',toggle,Dropdown.prototype.toggle)
.on('keydown.dropdown.data-api',toggle+', [role=menu]',Dropdown.prototype.keydown)
}(window.jQuery);


/* plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-carousel.js */

!function($){
"use strict";
var Carousel=function(element,options){
this.$element=$(element)
this.$indicators=this.$element.find('.carousel-indicators')
this.options=options
this.options.pause=='hover'&&this.$element
.on('mouseenter',$.proxy(this.pause,this))
.on('mouseleave',$.proxy(this.cycle,this))
}
Carousel.prototype={
cycle:function(e){
if(!e)this.paused=false
if(this.interval)clearInterval(this.interval);
this.options.interval
&&!this.paused
&&(this.interval=setInterval($.proxy(this.next,this),this.options.interval))
return this
}
,getActiveIndex:function(){
this.$active=this.$element.find('.item.active')
this.$items=this.$active.parent().children()
return this.$items.index(this.$active)
}
,to:function(pos){
var activeIndex=this.getActiveIndex()
,that=this
if(pos>(this.$items.length-1)||pos<0)return
if(this.sliding){
return this.$element.one('slid',function(){
that.to(pos)
})
}
if(activeIndex==pos){
return this.pause().cycle()
}
return this.slide(pos>activeIndex?'next':'prev',$(this.$items[pos]))
}
,pause:function(e){
if(!e)this.paused=true
if(this.$element.find('.next, .prev').length&&$.support.transition.end){
this.$element.trigger($.support.transition.end)
this.cycle(true)
}
clearInterval(this.interval)
this.interval=null
return this
}
,next:function(){
if(this.sliding)return
return this.slide('next')
}
,prev:function(){
if(this.sliding)return
return this.slide('prev')
}
,slide:function(type,next){
var $active=this.$element.find('.item.active')
,$next=next||$active[type]()
,isCycling=this.interval
,direction=type=='next'?'left':'right'
,fallback=type=='next'?'first':'last'
,that=this
,e
this.sliding=true
isCycling&&this.pause()
$next=$next.length?$next:this.$element.find('.item')[fallback]()
e=$.Event('slide',{
relatedTarget:$next[0]
,direction:direction
})
if($next.hasClass('active'))return
if(this.$indicators.length){
this.$indicators.find('.active').removeClass('active')
this.$element.one('slid',function(){
var $nextIndicator=$(that.$indicators.children()[that.getActiveIndex()])
$nextIndicator&&$nextIndicator.addClass('active')
})
}
if($.support.transition&&this.$element.hasClass('slide')){
this.$element.trigger(e)
if(e.isDefaultPrevented())return
$next.addClass(type)
$next[0].offsetWidth
$active.addClass(direction)
$next.addClass(direction)
this.$element.one($.support.transition.end,function(){
$next.removeClass([type,direction].join(' ')).addClass('active')
$active.removeClass(['active',direction].join(' '))
that.sliding=false
setTimeout(function(){that.$element.trigger('slid')},0)
})
}else{
this.$element.trigger(e)
if(e.isDefaultPrevented())return
$active.removeClass('active')
$next.addClass('active')
this.sliding=false
this.$element.trigger('slid')
}
isCycling&&this.cycle()
return this
}
}
var old=$.fn.carousel
$.fn.carousel=function(option){
return this.each(function(){
var $this=$(this)
,data=$this.data('carousel')
,options=$.extend({},$.fn.carousel.defaults,typeof option=='object'&&option)
,action=typeof option=='string'?option:options.slide
if(!data)$this.data('carousel',(data=new Carousel(this,options)))
if(typeof option=='number')data.to(option)
else if(action)data[action]()
else if(options.interval)data.pause().cycle()
})
}
$.fn.carousel.defaults={
interval:5000
,pause:'hover'
}
$.fn.carousel.Constructor=Carousel
$.fn.carousel.noConflict=function(){
$.fn.carousel=old
return this
}
$(document).on('click.carousel.data-api','[data-slide], [data-slide-to]',function(e){
var $this=$(this),href
,$target=$($this.attr('data-target')||(href=$this.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,''))
,options=$.extend({},$target.data(),$this.data())
,slideIndex
$target.carousel(options)
if(slideIndex=$this.attr('data-slide-to')){
$target.data('carousel').pause().to(slideIndex).cycle()
}
e.preventDefault()
})
}(window.jQuery);


/* plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-transition.js */

!function($){
"use strict";
$(function(){
$.support.transition=(function(){
var transitionEnd=(function(){
var el=document.createElement('bootstrap')
,transEndEventNames={
'WebkitTransition':'webkitTransitionEnd'
,'MozTransition':'transitionend'
,'OTransition':'oTransitionEnd otransitionend'
,'transition':'transitionend'
}
,name
for(name in transEndEventNames){
if(el.style[name]!==undefined){
return transEndEventNames[name]
}
}
}())
return transitionEnd&&{
end:transitionEnd
}
})()
})
}(window.jQuery);


/* plugins/auto/bootstrap/v2.1.11/bootstrap/js/bootstrap-tab.js */

!function($){
"use strict";
var Tab=function(element){
this.element=$(element)
}
Tab.prototype={
constructor:Tab
,show:function(){
var $this=this.element
,$ul=$this.closest('ul:not(.dropdown-menu)')
,selector=$this.attr('data-target')
,previous
,$target
,e
if(!selector){
selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')
}
if($this.parent('li').hasClass('active'))return
previous=$ul.find('.active:last a')[0]
e=$.Event('show',{
relatedTarget:previous
})
$this.trigger(e)
if(e.isDefaultPrevented())return
$target=$(selector)
this.activate($this.parent('li'),$ul)
this.activate($target,$target.parent(),function(){
$this.trigger({
type:'shown'
,relatedTarget:previous
})
})
}
,activate:function(element,container,callback){
var $active=container.find('> .active')
,transition=callback
&&$.support.transition
&&$active.hasClass('fade')
function next(){
$active
.removeClass('active')
.find('> .dropdown-menu > .active')
.removeClass('active')
element.addClass('active')
if(transition){
element[0].offsetWidth
element.addClass('in')
}else{
element.removeClass('fade')
}
if(element.parent('.dropdown-menu')){
element.closest('li.dropdown').addClass('active')
}
callback&&callback()
}
transition?
$active.one($.support.transition.end,next):
next()
$active.removeClass('in')
}
}
var old=$.fn.tab
$.fn.tab=function(option){
return this.each(function(){
var $this=$(this)
,data=$this.data('tab')
if(!data)$this.data('tab',(data=new Tab(this)))
if(typeof option=='string')data[option]()
})
}
$.fn.tab.Constructor=Tab
$.fn.tab.noConflict=function(){
$.fn.tab=old
return this
}
$(document).on('click.tab.data-api','[data-toggle="tab"], [data-toggle="pill"]',function(e){
e.preventDefault()
$(this).tab('show')
})
}(window.jQuery);


/* local/cache-js/jsdyn-js_scolaspip_js-9b116fe0.js?1651594293 */

function activeToggleBreves(){
if($(".liste.breves li.item div.contenu_breve").css("display")!="none"){
$(".liste.breves li.item div.contenu_breve").css({display:'none'});
$(".liste.breves li.item h3.h3 a").removeClass("ouvert");
$(".liste.breves li.item h3.h3 a").addClass("ouvrable");
$(".liste.breves li.item h3.h3").click(function(){
$(this).next().slideToggle();
$(this).children().toggleClass("ouvrable");
$(this).children().toggleClass("ouvert");
return false;
});
}
}
$.removeCookie=function(key,options){
if($.cookie(key)===undefined){
return false;
}
$.cookie(key,'',$.extend({},options,{expires:-1}));
return!$.cookie(key);
};
$(function(){
$("body").append('<div id="backtotop" title="Haut de page"><span>▲</span></div>');
$(window).scroll(function(){
if($('html').scrollTop()>300||$('body').scrollTop()>300){$('#backtotop').fadeIn();}
else{$('#backtotop').fadeOut();}
});
$('#backtotop').on('click',function(){
$('html,body').animate({scrollTop:0},900,function(){
$("html, body").off("scroll mousedown DOMMouseScroll mousewheel keyup");
});
return false;
});
$('a.spip_out,.articles-virtuels a').attr("target","_blank");
activeToggleBreves();
if(typeof onAjaxLoad=='function')onAjaxLoad(activeToggleBreves);
if(!$.cookie('stop-carousel')){
$('#carousel').carousel({interval:8000});
$('#carousel-pause-cancel').hide();
}
else{
$('#carousel-pause').hide();
}
$('#carousel-pause').on('click',function(){
$('#carousel').carousel('pause');
$.cookie('stop-carousel',true,{expires:7});
$(this).hide();
$('#carousel-pause-cancel').show();
return false;
});
$('#carousel-pause-cancel').on('click',function(){
$.removeCookie('stop-carousel');
$('#carousel').carousel('next');
$('#carousel').carousel({interval:8000});
$(this).hide();
$('#carousel-pause').show();
return false;
});
});


