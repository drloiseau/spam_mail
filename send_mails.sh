#!/bin/bash
for file in $(ls to_send)
do
    recipient=$(head -1 to_send/$file)
    subject=$(head -3 to_send/$file | tail -1)
    body=$(tail -n +5 to_send/$file)
    echo "sending to " $recipient
    thunderbird -compose "subject='$subject',to='$recipient',body='$body'" &
    sleep 10                  # Wait for the window to open
    xdotool mousemove 40 85 # Find the exact position of the send button manually
    sleep 0.25               # Might not be needed
    xdotool click 1          # Click it
    mv to_send/$file sent/$file # move file
    sleep 10 # wait for the mail to be sent
done
echo "Done!"
