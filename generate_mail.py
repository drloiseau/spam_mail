import re
import itertools

with open("Annuaire.html") as annuaire:
    ann = annuaire.read()

ecoles = re.findall("Ecole.*\(034\d.*\)", ann)
colleges = re.findall("Collège.*\(034\d.*\)", ann)
lycees = re.findall("Lycée.*\(034\d.*\)", ann)

ecoles = [re.sub("\(([^(]*)\)", "\\1", x) for x in ecoles]
ecoles = [re.split('\</b\> ', x) for x in ecoles]

colleges = [re.sub("\(([^(]*)\)", "\\1", x) for x in colleges]
colleges = [re.split('\</b\> ', x) for x in colleges]

lycees = [re.sub("\(([^(]*)\)", "\\1", x) for x in lycees]
lycees = [re.split('\</b\> ', x) for x in lycees]

contacts = ecoles + colleges + lycees

first = ["je me permets de vous contacter pour",
         "je vous contacte afin de",
         "je vous contacte de la part de l'association Myxonautes pour"]
scol = ["scolaire", ""]
assoc = ["Nous sommes une association nouvellement crée et nous proposons",
         "Notre association propose",
         "L'association Myxonautes propose",
         "Nous animons",
         "Nous organisons"]
details = ["Pour plus de détails, vous pouvez télécharger notre plaquette de présentation ici",
           "Pour plus de détails, notre plaquette de présentation est disponible ici",
           "Pour plus de détails, notre plaquette de présentation est accessible via ce lien",
           "Pour plus de détails, vous pouvez télécharger ce fichier de présentation",
           "Des informations plus détaillées sont disponibles via ce lien",
           "Voici un lien vers une présentation plus détaillée de nos activités"]
enseign = ["enseignants",
           "personnels"]
send = ["faire suivre",
        "transmettre",
        "transférer"]

# str_list = [[a,b,c,d,e,f] for a in first for b in scol for c in assoc for]
list_str = list(itertools.product(*[first, scol, assoc, details, enseign, send]))

for etablissement in contacts:
    type_etab = re.sub(" .*", "", etablissement[0])
    if type_etab == "Ecole":
        attention = f"À l'attention de l'{etablissement[0]}"
    else:
        attention = f"À l'attention du {etablissement[0]}"
    index = contacts.index(etablissement)
    perso = list_str[index]
    print(
        f"""
ce.{etablissement[1]}@ac-montpellier.fr

Proposition d'intervention animation scientifique - association Myxonautes

{attention}

Bonjour,

{perso[0]} vous proposer d'animer des ateliers de médiation scientifique dans votre établissement pour l'année {perso[1]} 2022-2023.

{perso[2]} des conférences et ateliers pour adultes et enfants, principalement en biologie (microbiologie, botanique, champignons) mais également en électronique ou en informatique. Nous pouvons intervenir ponctuellement dans l'année mais nous serions aussi intéressés par un projet filé sur un trimestre voire sur l'année entière.

{perso[3]} : https://cloud.lebib.org/s/psWTcTkJoQAsweN

Si des {perso[4]} de votre établissement seraient susceptibles d'être intéressés par ce projet, serait-il possible de leur {perso[5]} ce mail ?

Merci d'avance,
Cordialement,
Lucas von Gastrow, pour Myxonautes
"""
    )
